#!/usr/bin/env python3

from pycparser import c_ast, parse_file
import ctypes

from env import starpart_includes, libgraph_includes

typedefs = {}
functions = {}
enums = {}
enumvalues = {}

includes = ['../include/starpart.h', '../src/all/all.h', '../src/core/aux.h']


def name_to_ctypes(name):
    if name == ['int']:
        ctype = ctypes.c_int
    elif name == ['char']:
        ctype = ctypes.c_char
    elif name == ['float']:
        ctype = ctypes.c_float
    elif name == ['double']:
        ctype = ctypes.c_double
    elif name == ['_Bool']:
        ctype = ctypes.c_bool
    elif name == ['void']:
        ctype = None
    elif name == ['unsigned', 'int']:
        ctype = ctypes.c_uint
    else:
        print("### Not implemented for %s ###" % name)
        raise ValueError

    return ctype


def analyseType(decl):
    if isinstance(decl, c_ast.Decl) or isinstance(decl, c_ast.Typename):
        ctype = analyseType(decl.type)

    elif isinstance(decl, c_ast.TypeDecl):
        ctype = analyseType(decl.type)

    elif isinstance(decl, c_ast.IdentifierType):
        # We have only one element
        ctype = get_typedef(decl.names)

    elif isinstance(decl, c_ast.ArrayDecl) or isinstance(decl, c_ast.PtrDecl):
        target = analyseType(decl.type)
        if target == ctypes.c_char:
            ctype = ctypes.c_char_p
        elif target == ctypes.Structure:
            ctype = ctypes.c_void_p
        else:
            ctype = ctypes.POINTER(target)

    elif isinstance(decl, c_ast.Struct):
        # We do not expose structures directly, so no need to parse them
        ctype = ctypes.Structure

    elif isinstance(decl, c_ast.Enum):
        ctype = ctypes.c_int
        if decl.name not in enums:
            enum = analyseEnum(decl)
            enums[decl.name] = enum
            if enum is not None:
                enumvalues.update(enum)

    elif isinstance(decl, c_ast.FuncDecl):
        # Function ptr not tested!
        ctype = ctypes.c_void_p

    elif isinstance(decl, c_ast.EllipsisParam):
        # Variadic not tested!
        ctype = None

    else:
        print("### Not implemented: %s ###" % decl)
        raise ValueError

    return ctype


def get_typedef(name):
    if type(name) is list:
        if name[0] in typedefs:
            return get_typedef(typedefs[name[0]])
        else:
            return name_to_ctypes(name)
    else:
        return name


def analyseFunction(decl):
    paramDecls = decl.args.params if decl.args else []
    returnDecl = decl.type

    params = []
    for paramDecl in paramDecls:
        params.append(analyseType(paramDecl))
    ret = analyseType(returnDecl)
    return params, ret


def analyseEnum(decl):
    enums = {}
    cur_value = -1
    for e in decl.values.enumerators:
        v = e.value
        if v is not None:
            try:
                v = int(v.value)
                cur_value = v
            except AttributeError:
                print(f"Enum {decl.name} not handled")
                return None
        else:
            cur_value += 1
        enums[e.name] = cur_value
    return enums


def analyseTypedef(decl):
    declTypedef = decl.type
    return analyseType(declTypedef)


def into_cpp_args(include_list):
    return list(map(lambda i: '-I'+i, include_list))


def parse():
    for include in includes:
        node = parse_file(
            include, use_cpp=True,
            cpp_args=['-Ifake_includes'] +
            into_cpp_args(starpart_includes) +
            into_cpp_args(libgraph_includes)
        )

        for decl in node.ext:
            if isinstance(decl, c_ast.Decl):
                if isinstance(decl.type, c_ast.FuncDecl):
                    # Keep only functions starting with  StarPart
                    if decl.name.startswith('StarPart'):
                        functions[decl.name] = analyseFunction(decl.type)

                elif isinstance(decl.type, c_ast.Enum):
                    enum = analyseEnum(decl.type)
                    enums[decl.type.name] = enum
                    if enum is not None:
                        enumvalues.update(enum)

                else:
                    print(f'{type(decl.type)} ({decl.name}) not handled ###')

            elif isinstance(decl, c_ast.Typedef):
                typedefs[decl.name] = analyseTypedef(decl)
            else:
                print(f'{decl} not handled ###')
