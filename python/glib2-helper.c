#define _GNU_SOURCE
#include <glib.h>

/* GQueue */
int gqueue_len(GQueue *queue)
{
    return g_queue_get_length(queue);
}

void **gqueue_to_array(GQueue *queue)
{
    int len = gqueue_len(queue);
    GList *cur = g_queue_peek_head_link(queue);

    void **array = (void **)malloc(sizeof(void *[len]));

    for (int i = 0; i < len; i++) {
        array[i] = cur->data;
        cur = cur->next;
    }

    return array;
}


/* GHashTable */
int ghashtable_len(GHashTable *hash_table)
{
    return g_hash_table_size (hash_table);
}

void **ghashtable_to_array(GHashTable *hash_table)
{
    GHashTableIter iter;
    void **array;
    int length = ghashtable_len(hash_table);
    array = (void **)malloc(sizeof(void *[2*length]));

    g_hash_table_iter_init (&iter, hash_table);
    int i = 0;
    while (g_hash_table_iter_next (&iter, array+i, array+length+i))
    {
        i++;
    }

    return array;
}
