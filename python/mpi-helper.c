#include <mpi.h>

/* This wrapper is needed because MPI_Comm_f2c and MPI_Comm_c2f can be
 * macros */

MPI_Comm _MPI_Comm_f2c(MPI_Fint comm)
{
    return MPI_Comm_f2c(comm);
}

MPI_Fint _MPI_Comm_c2f(MPI_Comm comm)
{
    return MPI_Comm_c2f(comm);
}
