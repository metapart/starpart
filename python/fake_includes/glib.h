/*
 * glib.h
 * Copyright (C) 2018 rools <rools@hup>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef GLIB_H
#define GLIB_H

typedef struct _GHashTable GHashTable;
typedef struct _GNode GNode;
typedef struct _GQueue GQueue;

#endif /* !GLIB_H */
