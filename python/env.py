import os
import sys

try:
    starpart_lib = os.environ['STARPART_LIB_PATH']
    starpart_includes = os.environ['STARPART_INCLUDE_PATH'].split(':')
    libgraph_includes = os.environ['LIBGRAPH_INCLUDE_PATH'].split(':')
    glib_helper_lib = os.environ[
        'STARPART_PYHELPER_PATH']+'/libglib2-helper.so'
    mpi_helper_lib = os.environ['STARPART_PYHELPER_PATH']+'/libmpi-helper.so'
    if os.environ['STARPART_MPI'] == 'ON':
        use_mpi = True
    else:
        use_mpi = False

except KeyError as k:
    print('You need to set %s, please source env.sh' % k, file=sys.stderr)
    sys.exit(1)
