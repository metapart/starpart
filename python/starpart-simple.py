#!/usr/bin/env python3
from starpart import Context, Task

debug = True
autoconnect = False

ctx = Context()
ctx.set_debug(debug)
ctx.register_all_methods()

grid = Task(ctx, "BASIC/GRID")
random = Task(ctx, "BASIC/RANDOM")
diag = Task(ctx, "BASIC/DIAG")
vtk = Task(ctx, "BASIC/VTK")

random.insert_task_args("nparts=4, seed=0")
vtk.insert_task_args("filename=simple.vtk")

ctx.submit_all_tasks()

if not autoconnect:
    grid.connect("#out", "@0")  # OUT
    random.connect("#in", "@0")  # INOUT
    diag.connect("#in", "@0")  # IN
    vtk.connect("#in", "@0")  # IN
else:
    ctx.connect_all_tasks()

# exec all tasks in queue
ctx.run()
