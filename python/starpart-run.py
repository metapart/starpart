#!/usr/bin/env python3

import argparse
import sys

from starpart import Context

print(sys.argv)

debug = False
parallel = False
master = False
lst = False
printdesc = None
strat = None

parser = argparse.ArgumentParser()
parser.add_argument('-l', action='store_true',
                    help='List all available methods')
parser.add_argument('-p', nargs=1, type=str, metavar='package/method',
                    help='Print method description')
parser.add_argument('-r', nargs=1, type=str, metavar='strat',
                    help='Run a string strategy')
parser.add_argument('-m', action='store_true', help='MPI master node')
parser.add_argument('-s', action='store_true', help='MPI slave node')
parser.add_argument('-g', action='store_true', help='Set debug mode')

args = parser.parse_args()

if args.l:
    lst = True
if args.p:
    printdesc = args.p[0]
if args.r:
    strat = args.r[0]
if args.m:
    parallel = True
    master = True
if args.s:
    parallel = True
    master = False
if args.g:
    debug = True

# TODO check starpart compiled with USE_MPI

if parallel:
    try:
        from mpi4py import MPI
    except ModuleNotFoundError:
        sys.exit('You need to install mpi4py to run in parallel')

    if not master:
        intercomm = MPI.Comm.Get_parent()

    ctx = Context(mpi_comm=MPI.COMM_WORLD)

else:
    ctx = Context()

ctx.set_debug(debug)
ctx.register_all_methods()

if lst:
    ctx.list_all_methods()

elif printdesc:
    method = ctx.get_method(printdesc)
    if method:
        method.print_desc()

elif strat:
    ctx.parse_strat(strat)
    ctx.submit_all_tasks()
    ctx.connect_all_tasks()
    ctx.run()

else:
    parser.print_help()

if parallel and not master:
    MPI.Comm.Barrier(intercomm)
