#!/usr/bin/env python3
from starpart import Context, Task

debug = True
autoconnect = False

ctx = Context()
ctx.set_debug(debug)
ctx.register_all_methods()

grid = Task(ctx, "BASIC/GRID")
random1 = Task(ctx, "BASIC/RANDOM", "nparts=4, seed=-2")
diag1 = Task(ctx, "BASIC/DIAG")
dup = Task(ctx, "BASIC/DUP")
random2 = Task(ctx, "BASIC/RANDOM", "nparts=4, seed=-2")
diag2 = Task(ctx, "BASIC/DIAG")
comp = Task(ctx, "BASIC/CMP")
diag3 = Task(ctx, "BASIC/DIAG")

ctx.submit_all_tasks()

if not autoconnect:
    grid.connect("#out", "@1")  # OUT
    dup.connect("#in", "@1")  # IN
    dup.connect("#out", "@2")  # OUT
    random1.connect("#in", "@1")  # INOUT
    random2.connect("#in", "@2")  # INOUT
    diag1.connect("#in", "@1")  # IN
    diag2.connect("#in", "@2")  # IN
    comp.connect("#in0", "@1")  # IN0
    comp.connect("#in1", "@2")  # IN1
    comp.connect("#out", "@3")  # OUT
    diag3.connect("#in", "@3")  # IN

else:
    grid.connect("#out", "@1")  # OUT
    dup.connect("#out", "@2")  # OUT
    comp.connect("#in0", "@1")  # IN0
    comp.connect("#in1", "@2")  # IN1

    ctx.connect_all_tasks()

# exec all tasks in queue
ctx.run()
