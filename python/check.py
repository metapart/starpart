#!/usr/bin/env python3
import re
import sys

import ctypes
import cparser
import starpart

pyfilename = 'starpart.py'

skipped_functions = [
        'StarPart_getTaskContext',  # implemented directly in Python
        # Function pointer is not handled
        'StarPart_registerNewMethod',
        'StarPart_newMethod',
        'StarPart_registerNewHook',
        'StarPart_getCallFunc',
        'StarPart_getInitFunc',
        # Variadic functions
        'StarPart_newIntArrayV',
        'StarPart_newStrArrayV',
        'StarPart_print',
        'StarPart_printRed',
        'StarPart_printBlue',
        'StarPart_warning',
        'StarPart_error',
        # Prototype depending on USE_MPI (FIXME)
        'StarPart_newParallelContext',
        'StarPart_getParallelComm',
        # To implement (TODO)
        'StarPart_getArray',
        'StarPart_setArray',
        'StarPart_allocArray',
        'StarPart_initArray',
        # In aux.h
        'StarPart_newHook',
        'StarPart_freeHook',
        ]


def find_next_string(lines, start, string):
    for i, l in enumerate(lines[start:]):
        l = remove_comm(l)
        if string in l:
            return i+start
    return -1


def find_closing_bracket(string, opened):
    i = 0
    for i, c in enumerate(string):
        if c == '[':
            opened += 1
        elif c == ']':
            opened -= 1
            if opened == 0:
                break
        elif c == '#':
            break
    return opened, string[:i+1]


def remove_comm(line):
    comm_idx = line.find('#')
    if comm_idx != -1:
        line = line[:comm_idx]
    return line


# Get value after '=' sign
# Delim is for line like 'c_fun.argtypes = [...]'
def get_r_value(lines, line_number, delim=False):
    if not delim:
        line = remove_comm(lines[line_number])
        match = re.search(' *= *(.*)', line)
        if match is None:
            raise Exception
        else:
            return match.group(1)

    else:
        line = remove_comm(lines[line_number])
        complete_string = ''
        start = line.find('=')+1
        line_idx = line_number
        if len(line) == start:
            line_idx += 1
        opened = 0
        while True:
            if line_idx == line_number:
                line = line[start:]
            opened, string = find_closing_bracket(line, opened)
            complete_string += string

            if not opened:
                break

            line_idx += 1
            if line_idx == len(lines):
                raise Exception
            line = remove_comm(lines[line_idx])
        return complete_string


def extract_func_name(line):
    line = remove_comm(line)
    match = re.search('_lib.(StarPart_[a-zA-Z]*)', line)
    if match is None:
        return match
    else:
        return match.group(1)


if __name__ == '__main__':
    # Read C file and it to build typedef and function dictionnaries
    success = True
    cparser.parse()

    # Check ignored functions exist
    bad_skipped_functions = [f for f in skipped_functions
                             if f not in cparser.functions]
    if bad_skipped_functions:
        print('Following skipped functions do not exist: ' +
              ', '.join(bad_skipped_functions))
        success = False

    # Read Python file and build function information
    with open(pyfilename) as f:
        lines = f.read().splitlines()

    # Captured functions
    call_lines = []
    for i, l in enumerate(lines):
        l = remove_comm(l)
        if "_lib.StarPart_" in l:
            if len(call_lines):
                if call_lines[-1][1] > i or call_lines[-1][2] > i:
                    print('Parsing failed for %s (line %d)' %
                          (lines[call_lines[-1][0]], call_lines[-1][0]))
                    del call_lines[-1]
            argline = find_next_string(lines, i, ".argtypes")
            resline = find_next_string(lines, i, ".restype")
            if -1 in (argline, resline):
                print('Parsing failed for %s (line %d)' % (lines[i], i))
            call_lines.append((i, argline, resline))

    # Get arguments
    called_functions = {}
    for f, a, r in call_lines:
        func_name = extract_func_name(lines[f])
        args = get_r_value(lines, a, True)
        ret = get_r_value(lines, r)
        if func_name in called_functions:
            ident = called_functions[func_name]
            if args != ident[0] or ret != ident[1]:
                print('Problem with '+func_name)
                success = False
        else:
            called_functions[func_name] = (args, ret)

    # Compare C and Python

    # Check if prototypes are correct
    for name, c_proto in list(cparser.functions.items()):
        if name not in called_functions:
            if name not in skipped_functions:
                print('[%s] No Python binding' % name)
                success = False
            continue

        if name in skipped_functions:
            continue

        py_proto = called_functions[name]

        for c, p in zip(c_proto, py_proto):
            p = eval(p)
            if c != p:
                if c == [None] and p == []:
                    continue
                print('[%s] Bad prototype (%s != %s)' % (name, c, p))
                success = False

    # Check functions in Python are defined in C file
    for f in called_functions:
        if f not in cparser.functions:
            print('%s is not defined in Starpart include file' % f)
            success = False

    # Check enums
    # StaPart_Type
    enums_to_test = [
        'STARPART_VOID',
        'STARPART_STRAT',
        'STARPART_INT',
        'STARPART_DOUBLE',
        'STARPART_STR',
        'STARPART_INTARRAY',
        'STARPART_DOUBLEARRAY',
        'STARPART_STRARRAY',
        'STARPART_GRAPH',
        'STARPART_DISTGRAPH',
        'STARPART_HYPERGRAPH',
        'STARPART_MESH',
        'STARPART_HYBRIDMESH',
    ]
    for e in enums_to_test:
        if (e not in cparser.enumvalues):
            print(f'enum {e} not found in C header files')
            success = False
        else:
            cvalue = cparser.enumvalues[e]
            pyvalue = getattr(starpart, e)
            if cvalue != pyvalue:
                print(f'Bad value for enum {e} ({cvalue} != {pyvalue})')
                success = False

    if not success:
        sys.exit(-1)
