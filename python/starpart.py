#!/usr/bin/env python3
import os
import ctypes

from env import starpart_lib, glib_helper_lib, mpi_helper_lib, use_mpi

if use_mpi:
    from mpi4py import MPI

_lib = ctypes.cdll.LoadLibrary(os.path.abspath(starpart_lib +
                                               "/libstarpart-all.so"))

_object_dir = {}

# StaPart_Type
STARPART_VOID = 0
STARPART_STRAT = 1
STARPART_INT = 2
STARPART_FLOAT = 3
STARPART_DOUBLE = 4
STARPART_STR = 5
STARPART_INTARRAY = 6
STARPART_FLOATARRAY = 7
STARPART_DOUBLEARRAY = 8
STARPART_STRARRAY = 9
STARPART_GRAPH = 10
STARPART_DISTGRAPH = 11
STARPART_HYPERGRAPH = 12
STARPART_MESH = 13
STARPART_HYBRIDMESH = 14

if use_mpi:
    _mpihelper = ctypes.cdll.LoadLibrary(os.path.abspath(mpi_helper_lib))
    mpicomm_f2c = _mpihelper._MPI_Comm_f2c
    mpicomm_f2c.argtypes = [ctypes.c_int]
    mpicomm_f2c.restype = ctypes.c_void_p
    mpicomm_c2f = _mpihelper._MPI_Comm_c2f
    mpicomm_c2f.argtypes = [ctypes.c_void_p]
    mpicomm_c2f.restype = ctypes.c_int

_glibhelper = ctypes.cdll.LoadLibrary(os.path.abspath(glib_helper_lib))


def _mpicomm_py2c(comm):
    fcomm = comm.py2f()
    return mpicomm_f2c(fcomm)


def _mpicomm_c2py(c_comm):
    fcomm = mpicomm_c2f(c_comm)
    return MPI.Comm.f2py(fcomm)


def _runFunc(fun, args=[]):
    if len(args) != len(fun.argtypes):
        raise Exception
    return fun(*args)


def _encodeStr(string):
    if string is None:
        return None
    else:
        return string.encode()


def _c_ptr_to_starpart_obj(cls, ptr):
    if ptr in _object_dir:
        return _object_dir[ptr]
    else:
        py_object = cls.from_pointer(ptr)
        _object_dir[ptr] = py_object
        return py_object


_gqueue_len = _glibhelper.gqueue_len
_gqueue_len.argtypes = [ctypes.c_void_p]
_gqueue_len.restype = ctypes.c_int
_gqueue_to_array = _glibhelper.gqueue_to_array
_gqueue_to_array.argtypes = [ctypes.c_void_p]
_gqueue_to_array.restype = ctypes.POINTER(ctypes.c_void_p)


def _gqueue_to_list(ptr, cls):
    length = _gqueue_len(ptr)
    array = _gqueue_to_array(ptr)[:length]
    return list(map(lambda p: _c_ptr_to_starpart_obj(cls, p), array))


_ghastable_len = _glibhelper.ghashtable_len
_ghastable_len.argtypes = [ctypes.c_void_p]
_ghastable_len.restype = ctypes.c_int
_ghastable_to_array = _glibhelper.ghashtable_to_array
_ghastable_to_array.argtypes = [ctypes.c_void_p]
_ghastable_to_array.restype = ctypes.POINTER(ctypes.c_void_p)


def _ghashtable_to_dict(ptr, cls):
    length = _ghastable_len(ptr)
    array = _ghastable_to_array(ptr)[:2*length]
    keys = list(map(lambda p: ctypes.c_char_p(p).value.decode(),
                    array[:length]))
    values = list(map(lambda p: _c_ptr_to_starpart_obj(cls, p),
                      array[length:2*length]))

    return dict(zip(keys, values))


def get_context_stack():
    get_context_stack_fun = _lib.StarPart_getContextStack
    get_context_stack_fun.argtypes = []
    get_context_stack_fun.restype = ctypes.c_void_p
    stack = get_context_stack_fun()
    return _gqueue_to_list(stack, Context)


class Context:
    """ Implement StarPart_Context """
    def __init__(self, parent_context=None, mpi_comm=None, ptr=None):
        self.parent_ctx = None
        self.to_free = True

        if None not in (mpi_comm, parent_context):
            raise ValueError('Need parent_context XOR mpi_comm')

        if parent_context is not None:
            self.parent_ctx = parent_context
            c_fun = _lib.StarPart_newNestedContext
            c_fun.argtypes = [ctypes.c_void_p]
            c_fun.restype = ctypes.c_void_p
            self.ptr = _runFunc(c_fun, [parent_context])

        elif mpi_comm is not None:
            c_fun = _lib.StarPart_newParallelContext
            c_fun.argtypes = [ctypes.c_void_p]
            c_fun.restype = ctypes.c_void_p
            mpi_c_comm = _mpicomm_py2c(mpi_comm)
            self.ptr = _runFunc(c_fun, [mpi_c_comm])

        elif ptr is not None:
            self.ptr = ptr
            self.to_free = False

        else:
            c_fun = _lib.StarPart_newContext
            c_fun.argtypes = []
            c_fun.restype = ctypes.c_void_p
            self.ptr = _runFunc(c_fun)

        _object_dir[self.ptr] = self

    def __del__(self):
        self.release()

    def release(self):
        if self.to_free:
            self.free_all_tasks()

            if not self.parent_ctx:
                c_fun = _lib.StarPart_freeContext
                c_fun.argtypes = [ctypes.c_void_p]
                c_fun.restype = None
            else:
                c_fun = _lib.StarPart_freeNestedContext
                c_fun.argtypes = [ctypes.c_void_p]
                c_fun.restype = None
            _runFunc(c_fun, [self.ptr])
            self.to_free = False

    @classmethod
    def from_pointer(cls, ptr):
        return cls(ptr=ptr)

    def submit_all_tasks(self):
        c_fun = _lib.StarPart_submitAllTasks
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr])

    def connect_all_tasks(self, ptr1=None, ptr2=None):
        c_fun = _lib.StarPart_connectAllTasks
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_char_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(ptr1), _encodeStr(ptr2)])

    def step(self):
        c_fun = _lib.StarPart_step
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        _runFunc(c_fun, [self.ptr])
        c_task = _runFunc(c_fun, [self.ptr])
        return _c_ptr_to_starpart_obj(Task, c_task)

    def run(self):
        c_fun = _lib.StarPart_run
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr])

    def free_all_tasks(self):
        if self.to_free:
            self.to_free = False
            c_fun = _lib.StarPart_freeAllTasks
            c_fun.argtypes = [ctypes.c_void_p]
            c_fun.restype = None
            _runFunc(c_fun, [self.ptr])

    def register_all_methods(self):
        c_fun = _lib.StarPart_registerAllMethods
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr])

    def get_method_dict(self):
        c_fun = _lib.StarPart_getMethodTable
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        c_table = _runFunc(c_fun, [self.ptr])
        return _ghashtable_to_dict(c_table, Method)

    def get_data_dict(self):
        c_fun = _lib.StarPart_getDataTable
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        c_table = _runFunc(c_fun, [self.ptr])
        return _ghashtable_to_dict(c_table, DataTable)

    def get_tag_dict(self):
        c_fun = _lib.StarPart_getHookTable
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        c_table = _runFunc(c_fun, [self.ptr])
        return _ghashtable_to_dict(c_table, Hook)

    def register_data(self, data):
        c_fun = _lib.StarPart_registerData
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, data.ptr])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def unregister_data(self, ident):
        c_fun = _lib.StarPart_unregisterData
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, _encodeStr(ident)])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def get_data(self, ident):
        c_fun = _lib.StarPart_getData
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, _encodeStr(ident)])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def cancel_all_tasks(self):
        c_fun = _lib.StarPart_cancelAllTasks
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr])

    def get_head_task(self):
        c_fun = _lib.StarPart_getHeadTask
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        c_task = _runFunc(c_fun, [self.ptr])
        return _c_ptr_to_starpart_obj(Task, c_task)

    def get_tail_task(self):
        c_fun = _lib.StarPart_getTailTask
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        c_task = _runFunc(c_fun, [self.ptr])
        return _c_ptr_to_starpart_obj(Task, c_task)

    def parse_strat(self, strat):
        c_fun = _lib.StarPart_parseStrat
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(strat)])

    def get_parallel_comm(self):
        c_fun = _lib.StarPart_getParallelComm
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        c_comm = _runFunc(c_fun, [self.ptr])
        return _mpicomm_c2py(c_comm)

    def get_parallel_comm_rank(self):
        c_fun = _lib.StarPart_getParallelCommRank
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_int
        return _runFunc(c_fun, [self.ptr])

    def get_parallel_comm_size(self):
        c_fun = _lib.StarPart_getParallelCommSize
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_int
        return _runFunc(c_fun, [self.ptr])

    def is_parallel(self):
        c_fun = _lib.StarPart_isParallelContext
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_bool
        return _runFunc(c_fun, [self.ptr])

    def set_debug(self, mode):
        c_fun = _lib.StarPart_setDebug
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_bool]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, mode])

    def is_debug(self):
        c_fun = _lib.StarPart_isDebug
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_bool
        return _runFunc(c_fun, [self.ptr])

    def list_all_methods(self):
        c_fun = _lib.StarPart_listAllMethods
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr])

    def list_all_packages(self):
        c_fun = _lib.StarPart_listAllPackages
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr])

    def list_all_data(self):
        c_fun = _lib.StarPart_listAllData
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr])

    def print_all_data(self):
        c_fun = _lib.StarPart_printAllData
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr])

    def unregister_method(self, ident):
        c_fun = _lib.StarPart_unregisterMethod
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_method = _runFunc(c_fun, [self.ptr, _encodeStr(ident)])
        return _c_ptr_to_starpart_obj(Method, c_method)

    def get_method(self, ident):
        c_fun = _lib.StarPart_getMethod
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_method = _runFunc(c_fun, [self.ptr, _encodeStr(ident)])
        return _c_ptr_to_starpart_obj(Method, c_method)

    def get_method_from_package(self, package, method_name):
        c_fun = _lib.StarPart_getMethodFromPackage
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_method = _runFunc(c_fun, [self.ptr, _encodeStr(package),
                                    _encodeStr(method_name)])
        return _c_ptr_to_starpart_obj(Method, c_method)

    def unregister_tag(self, ident):
        c_fun = _lib.StarPart_unregisterHook
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_tag = _runFunc(c_fun, [self.ptr, _encodeStr(ident)])
        return _c_ptr_to_starpart_obj(Hook, c_tag)

    def get_tag(self, ident):
        c_fun = _lib.StarPart_getHook
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_tag = _runFunc(c_fun, [self.ptr, _encodeStr(ident)])
        return _c_ptr_to_starpart_obj(Hook, c_tag)

    @classmethod
    def get_current(cls):
        c_fun = _lib.StarPart_getCurrentContext
        c_fun.argtypes = []
        c_fun.restype = ctypes.c_void_p
        c_context = c_fun()
        return _c_ptr_to_starpart_obj(cls, c_context)


# METHOD
class Method:
    def __init__(self, ptr):
        # TODO builder with StarPart_newMethod and StarPart_registerNewMethod
        self.ptr = ptr
        self.to_free = False

    def __del__(self):
        if self.to_free:
            c_fun = _lib.StarPart_freeMethod
            c_fun.argtypes = [ctypes.c_void_p]
            c_fun.restype = None
            _runFunc(c_fun, [self.ptr])
            self.to_free = False

    @classmethod
    def from_pointer(cls, ptr):
        return cls(ptr=ptr)

    def get_id(self):
        c_fun = _lib.StarPart_getMethodID
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_char_p
        return _runFunc(c_fun, [self.ptr]).decode()

    def get_desc(self):
        c_fun = _lib.StarPart_getMethodDesc
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_char_p
        return _runFunc(c_fun, [self.ptr]).decode()

    def get_context(self):
        c_fun = _lib.StarPart_getMethodContext
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        c_context = _runFunc(c_fun, [self.ptr])
        return _c_ptr_to_starpart_obj(Context, c_context)

    def get_package(self):
        c_fun = _lib.StarPart_getMethodPackage
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_char_p
        return _runFunc(c_fun, [self.ptr]).decode()

    def get_name(self):
        c_fun = _lib.StarPart_getMethodName
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_char_p
        return _runFunc(c_fun, [self.ptr]).decode()

    def get_nb_inputs(self):
        c_fun = _lib.StarPart_getMethodNbInputs
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_int
        return _runFunc(c_fun, [self.ptr])

    def get_nb_oututs(self):
        c_fun = _lib.StarPart_getMethodNbOutputs
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_int
        return _runFunc(c_fun, [self.ptr])

    def get_input(self, rank):
        c_fun = _lib.StarPart_getMethodInput
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_int]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, rank])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def get_output(self, rank):
        c_fun = _lib.StarPart_getMethodOutput
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_int]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, rank])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def get_local(self):
        c_fun = _lib.StarPart_getMethodLocal
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def print_desc(self):
        c_fun = _lib.StarPart_printMethodDesc
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr])

    def add_arg(self, arg_id, flags):
        c_fun = _lib.StarPart_addMethodArg
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_int]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, _encodeStr(arg_id), flags])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def get_arg_table(self):
        c_fun = _lib.StarPart_getMethodArgTable
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        c_data_table = _runFunc(c_fun, [self.ptr])
        return _c_ptr_to_starpart_obj(DataTable, c_data_table)

    def get_arg(self, arg_id):
        c_fun = _lib.StarPart_getMethodArg
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, _encodeStr(arg_id)])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def get_alias(self, alias):
        c_fun = _lib.StarPart_getAlias
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_char_p
        return _runFunc(c_fun, [self.ptr, _encodeStr(alias)]).decode()


# DATA
class DataTable():
    def __init__(self, ptr=None):
        if ptr is not None:
            self.ptr = ptr
            self.to_free = False

        else:
            c_fun = _lib.StarPart_newDataTable
            c_fun.argtypes = []
            c_fun.restype = ctypes.c_void_p
            self.ptr = _runFunc(c_fun, [])
            self.to_free = True

        _object_dir[self.ptr] = self

    def __del__(self):
        if self.to_free:
            c_fun = _lib.StarPart_freeDataTable
            c_fun.argtypes = [ctypes.c_void_p]
            c_fun.restype = None
            _runFunc(c_fun, [self.ptr])
            self.to_free = False

    @classmethod
    def from_pointer(cls, ptr):
        return cls(ptr=ptr)

    def is_data(self, ident):
        c_fun = _lib.StarPart_isDataFromTable
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_bool
        return _runFunc(c_fun, [self.ptr, _encodeStr(ident)])

    def get_data(self, ident):
        c_fun = _lib.StarPart_getDataFromTable
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, _encodeStr(ident)])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def add_data(self, data):
        c_fun = _lib.StarPart_addDataInTable
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, data])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def remove_data(self, ident):
        c_fun = _lib.StarPart_removeDataFromTable
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, _encodeStr(ident)])
        return _c_ptr_to_starpart_obj(Data, c_data)


class Data():
    def __init__(self, ident, flags, data_table=None, ctx=None, ptr=None):
        if ptr is not None:
            self.ptr = ptr
            self.to_free = False

        else:
            if data_table is not None and ctx is not None:
                raise ValueError('Need data_table XOR ctx')

            if data_table is None and ctx is None:
                c_fun = _lib.StarPart_newData
                c_fun.argtypes = [ctypes.c_char_p, ctypes.c_int]
                c_fun.restype = ctypes.c_void_p
                self.ptr = _runFunc(c_fun, [_encodeStr(ident), flags])
            elif data_table:
                c_fun = _lib.StarPart_addNewDataInTable
                c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p,
                                  ctypes.c_int]
                c_fun.restype = ctypes.c_void_p
                self.ptr = _runFunc(c_fun,
                                    [data_table.ptr, _encodeStr(ident), flags])
            elif ctx:
                c_fun = _lib.StarPart_registerNewData
                c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p,
                                  ctypes.c_int]
                c_fun.restype = ctypes.c_void_p
                self.ptr = _runFunc(c_fun, [ctx.ptr, _encodeStr(ident), flags])

            self.to_free = True

        _object_dir[self.ptr] = self

    def __del__(self):
        if self.to_free:
            c_fun = _lib.StarPart_freeData
            c_fun.argtypes = [ctypes.c_void_p]
            c_fun.restype = None
            _runFunc(c_fun, [self.ptr])
            self.to_free = False

    @classmethod
    def from_pointer(cls, ptr):
        return cls(None, None, ptr=ptr)

    def dup(self, deep):
        c_fun = _lib.StarPart_dupData
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_bool]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, deep])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def dup_items(self, dst, deep):
        c_fun = _lib.StarPart_dupItems
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_void_p, ctypes.c_bool]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, dst, deep])

    def get_id(self):
        c_fun = _lib.StarPart_getDataID
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_char_p
        return _runFunc(c_fun, [self.ptr]).decode()

    def get_flags(self):
        c_fun = _lib.StarPart_getDataFlags
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_int
        return _runFunc(c_fun, [self.ptr])

    def get_items(self):
        c_fun = _lib.StarPart_getDataItems
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        c_table = _runFunc(c_fun, [self.ptr])
        return _ghashtable_to_dict(c_table, Item)

    def print(self):
        c_fun = _lib.StarPart_printData
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr])

    def print_items(self,  prefix):
        c_fun = _lib.StarPart_printItems
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(prefix)])

    def get_str(self, item_id):
        c_fun = _lib.StarPart_getStr
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_char_p
        return _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])

    def get_strat(self, item_id):
        c_fun = _lib.StarPart_getStrat
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_char_p
        return _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])

    def get_int(self, item_id):
        c_fun = _lib.StarPart_getInt
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_int
        return _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])

    def get_float(self, item_id):
        c_fun = _lib.StarPart_getFloat
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_float
        return _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])

    def get_double(self, item_id):
        c_fun = _lib.StarPart_getDouble
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_double
        return _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])

    def get_array_length(self, item_id):
        c_fun = _lib.StarPart_getArrayLength
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_int
        return _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])

    def get_int_array(self, item_id):
        c_fun = _lib.StarPart_getIntArray
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.POINTER(ctypes.c_int)

        length = self.get_array_length(item_id)
        return _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])[:length]

    def get_float_array(self, item_id):
        c_fun = _lib.StarPart_getFloatArray
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.POINTER(ctypes.c_float)

        length = self.get_array_length(item_id)
        return _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])[:length]

    def get_double_array(self, item_id):
        c_fun = _lib.StarPart_getDoubleArray
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.POINTER(ctypes.c_double)

        length = self.get_array_length(item_id)
        return _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])[:length]

    def get_str_array(self, item_id):
        c_fun = _lib.StarPart_getStrArray
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.POINTER(ctypes.c_char_p)
        c_str_array = _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])

        length = self.get_array_length(item_id)
        return list(map(bytes.decode, c_str_array[:length]))

    def get_graph(self, item_id):
        c_fun = _lib.StarPart_getGraph
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_graph = _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])
        return _c_ptr_to_starpart_obj(LibGraph_Graph, c_graph)

    def get_hypergraph(self, item_id):
        c_fun = _lib.StarPart_getHypergraph
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_hypergraph = _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])
        return _c_ptr_to_starpart_obj(LibGraph_Graph, c_hypergraph)

    def get_mesh(self, item_id):
        c_fun = _lib.StarPart_getMesh
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_mesh = _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])
        return _c_ptr_to_starpart_obj(LibGraph_Mesh, c_mesh)

    def get_hybrid_mesh(self, item_id):
        c_fun = _lib.StarPart_getHybridMesh
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_hybrid_mesh = _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])
        return _c_ptr_to_starpart_obj(LibGraph_HybridMesh, c_hybrid_mesh)

    def get_dist_graph(self, item_id):
        c_fun = _lib.StarPart_getDistGraph
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_dist_graph = _runFunc(c_fun, [self.ptr, _encodeStr(item_id)])
        return _c_ptr_to_starpart_obj(LibGraph_DistGraph, c_dist_graph)

    def set_graph(self, item_id, g, owner):
        c_fun = _lib.StarPart_setGraph
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_void_p,
                          ctypes.c_bool]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), g.ptr, owner])

    def set_hypergraph(self, item_id, hg, owner):
        c_fun = _lib.StarPart_setHypergraph
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_void_p,
                          ctypes.c_bool]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), hg.ptr, owner])

    def set_mesh(self, item_id, m, owner):
        c_fun = _lib.StarPart_setMesh
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_void_p,
                          ctypes.c_bool]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), m.ptr, owner])

    def set_hybrid_mesh(self, item_id, hm, owner):
        c_fun = _lib.StarPart_setHybridMesh
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_void_p,
                          ctypes.c_bool]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), hm.ptr, owner])

    def set_dist_graph(self, item_id, dg, owner):
        c_fun = _lib.StarPart_setDistGraph
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_void_p,
                          ctypes.c_bool]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), dg.ptr, owner])

    def set_str(self, item_id, string):
        c_fun = _lib.StarPart_setStr
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_char_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), _encodeStr(string)])

    def set_int(self, item_id, val):
        c_fun = _lib.StarPart_setInt
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_int]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), val])

    def set_float(self, item_id, val):
        c_fun = _lib.StarPart_setFloat
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_float]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), val])

    def set_double(self, item_id, val):
        c_fun = _lib.StarPart_setDouble
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_double]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), val])

    def set_int_array(self, item_id, array):
        c_fun = _lib.StarPart_setIntArray
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_int,
                          ctypes.POINTER(ctypes.c_int), ctypes.c_bool]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), len(array), array,
                         False])

    def set_float_array(self, item_id, array):
        c_fun = _lib.StarPart_setFloatArray
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_int,
                          ctypes.POINTER(ctypes.c_float), ctypes.c_bool]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), len(array), array,
                         False])

    def set_double_array(self, item_id, array):
        c_fun = _lib.StarPart_setDoubleArray
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_int,
                          ctypes.POINTER(ctypes.c_double), ctypes.c_bool]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), len(array), array,
                         False])

    def set_str_array(self, item_id, array):
        c_fun = _lib.StarPart_setStrArray
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_int,
                          ctypes.POINTER(ctypes.c_char_p), ctypes.c_bool]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), len(array), array,
                         False])

    def check_str(self, item_id, checklist):
        c_fun = _lib.StarPart_checkStr
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_char_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), _encodeStr(checklist)])

    def check_str_array(self, item_id, checklist):
        c_fun = _lib.StarPart_checkStrArray
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_char_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), _encodeStr(checklist)])

    def check_array(self, item_id, length):
        c_fun = _lib.StarPart_checkArray
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_int]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(item_id), length])

    def add_item(self, item):
        c_fun = _lib.StarPart_addItem
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_void_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, item])

    def remove_item(self, ident):
        c_fun = _lib.StarPart_removeItem
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(ident)])

    def is_item(self, ident):
        c_fun = _lib.StarPart_isItem
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_bool
        return _runFunc(c_fun, [self.ptr, _encodeStr(ident)])

    def has_item_value(self, ident):
        c_fun = _lib.StarPart_hasItemValue
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_bool
        return _runFunc(c_fun, [self.ptr, _encodeStr(ident)])

    def get_item(self, ident):
        c_fun = _lib.StarPart_getItem
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_item = _runFunc(c_fun, [self.ptr, _encodeStr(ident)])
        return _c_ptr_to_starpart_obj(Item, c_item)


class Item():
    def __init__(self, ident, kind, val, owner, flags, data=None, ptr=None):
        if ptr is not None:
            self.ptr = ptr
            self.to_free = False

        else:
            # val can be void* or char*
            if isinstance(val, str):
                c_fun = _lib.StarPart_newItemFromStr
                c_fun.argtypes = [ctypes.c_char_p, ctypes.c_int,
                                  ctypes.c_char_p, ctypes.c_int]
                c_fun.restype = ctypes.c_void_p
                self.ptr = _runFunc(c_fun, [_encodeStr(ident), kind,
                                            _encodeStr(val), owner, flags])
            elif data is None:
                c_fun = _lib.StarPart_newItem
                # XXX what is val (void *)?
                c_fun.argtypes = [ctypes.c_char_p, ctypes.c_int,
                                  ctypes.c_void_p, ctypes.c_bool, ctypes.c_int]
                c_fun.restype = ctypes.c_void_p
                self.ptr = _runFunc(c_fun, [_encodeStr(ident), kind, val,
                                            owner, flags])
            else:
                c_fun = _lib.StarPart_addNewItem
                c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p,
                                  ctypes.c_int, ctypes.c_void_p, ctypes.c_bool,
                                  ctypes.c_int]
                c_fun.restype = ctypes.c_void_p
                self.ptr = _runFunc(c_fun, [data.ptr, _encodeStr(ident), kind,
                                            val, owner, flags])

            self.to_free = True

        _object_dir[self.ptr] = self

    def __del__(self):
        if self.to_free:
            c_fun = _lib.StarPart_freeItem
            c_fun.argtypes = [ctypes.c_void_p]
            c_fun.restype = None
            _runFunc(c_fun, [self.ptr])
            self.to_free = False

    @classmethod
    def from_pointer(cls, ptr):
        return cls(*[None]*5, ptr=ptr)

    def get_id(self):
        c_fun = _lib.StarPart_getItemID
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_char_p
        return _runFunc(c_fun, [self.ptr]).decode()

    def get_type(self):
        c_fun = _lib.StarPart_getItemType
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_int
        return _runFunc(c_fun, [self.ptr])

    def get_addr(self):
        c_fun = _lib.StarPart_getItemAddr
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        return _runFunc(c_fun, [self.ptr])

    def get_flags(self):
        c_fun = _lib.StarPart_getItemFlags
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_int
        return _runFunc(c_fun, [self.ptr])

    def set_flags(self, flags):
        c_fun = _lib.StarPart_setItemFlags
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_int]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, flags])

    def set_type(self, item_type):
        c_fun = _lib.StarPart_setItemType
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_int]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, item_type])

    def set_id(self, ident):
        c_fun = _lib.StarPart_setItemID
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(ident)])

    def get_version(self, version):
        c_fun = _lib.StarPart_getItemVersion
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_int]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, _encodeStr(version)])
        return _c_ptr_to_starpart_obj(Item, c_data)

    def store_version(self):
        c_fun = _lib.StarPart_storeItemVersion
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_uint
        return _runFunc(c_fun, [self.ptr])

    def get_nb_versions(self):
        c_fun = _lib.StarPart_getItemNbVersions
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_uint
        return _runFunc(c_fun, [self.ptr])

    def dup(self, deep):
        c_fun = _lib.StarPart_dupItem
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_bool]
        c_fun.restype = ctypes.c_void_p
        c_item = _runFunc(c_fun, [self.ptr, deep])
        return _c_ptr_to_starpart_obj(Item, c_item)

    def print(self, dump, prefix):
        c_fun = _lib.StarPart_printItem
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_bool, ctypes.c_char_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, dump, _encodeStr(prefix)])


# PRIMITIVE

class Int():
    def __init__(self, x):
        c_fun = _lib.StarPart_newInt
        c_fun.argtypes = [ctypes.c_int]
        c_fun.restype = ctypes.POINTER(ctypes.c_int)
        self.ptr = _runFunc(c_fun, [x])


class Double():
    def __init__(self, x):
        c_fun = _lib.StarPart_newDouble
        c_fun.argtypes = [ctypes.c_double]
        c_fun.restype = ctypes.POINTER(ctypes.c_double)
        self.ptr = _runFunc(c_fun, [x])


class Str():
    def __init__(self, x):
        c_fun = _lib.StarPart_newStr
        c_fun.argtypes = [ctypes.c_char_p]
        c_fun.restype = ctypes.c_char_p
        self.ptr = _runFunc(c_fun, [_encodeStr(x)]).decode()


class Strat():
    def __init__(self, x):
        c_fun = _lib.StarPart_newStrat
        c_fun.argtypes = [ctypes.c_char_p]
        c_fun.restype = ctypes.c_char_p
        self.ptr = _runFunc(c_fun, [_encodeStr(x)]).decode()


class IntArray():
    def __init__(self, array):
        length = len(array)
        c_array = (ctypes.c_int*length)(*array)

        c_fun = _lib.StarPart_newIntArray
        c_fun.argtypes = [ctypes.c_int, ctypes.POINTER(ctypes.c_int)]
        c_fun.restype = ctypes.c_void_p
        self.ptr = _runFunc(c_fun, [length, c_array])


class StrArray():
    def __init__(self, array):
        length = len(array)
        c_array = (ctypes.c_char_p*length)(*map(_encodeStr, array))

        c_fun = _lib.StarPart_newStrArray
        c_fun.argtypes = [ctypes.c_int, ctypes.POINTER(ctypes.c_char_p)]
        c_fun.restype = ctypes.c_void_p
        self.ptr = _runFunc(c_fun, [length, c_array])


class LibGraph_Graph():
    def __init__(self, ptr=None):
        if ptr is not None:
            self.ptr = ptr

        else:
            c_fun = _lib.StarPart_newGraph
            c_fun.argtypes = []
            c_fun.restype = ctypes.c_void_p
            self.ptr = _runFunc(c_fun, [])

        _object_dir[self.ptr] = self

    @classmethod
    def from_pointer(cls, ptr):
        return cls(ptr=ptr)


class LibGraph_DistGraph():
    def __init__(self, ptr=None):
        if ptr is not None:
            self.ptr = ptr

        else:
            c_fun = _lib.StarPart_newDistGraph
            c_fun.argtypes = []
            c_fun.restype = ctypes.c_void_p
            self.ptr = _runFunc(c_fun, [])

        _object_dir[self.ptr] = self

    @classmethod
    def from_pointer(cls, ptr):
        return cls(ptr=ptr)


class LibGraph_Mesh():
    def __init__(self, ptr=None):
        if ptr is not None:
            self.ptr = ptr

        else:
            c_fun = _lib.StarPart_newMesh
            c_fun.argtypes = []
            c_fun.restype = ctypes.c_void_p
            self.ptr = _runFunc(c_fun, [])

        _object_dir[self.ptr] = self

    @classmethod
    def from_pointer(cls, ptr):
        return cls(ptr=ptr)


class LibGraph_Hypergraph():
    def __init__(self, ptr=None):
        if ptr is not None:
            self.ptr = ptr

        else:
            c_fun = _lib.StarPart_newHypergraph
            c_fun.argtypes = []
            c_fun.restype = ctypes.c_void_p
            self.ptr = _runFunc(c_fun, [])

        _object_dir[self.ptr] = self

    @classmethod
    def from_pointer(cls, ptr):
        return cls(ptr=ptr)


class LibGraph_HybridMesh():
    def __init__(self, nb_components=None, ptr=None):
        if ptr is not None:
            self.ptr = ptr

        elif nb_components is None:
            raise ValueError('You need to specify nb_components')

        else:
            c_fun = _lib.StarPart_newHybridMesh
            c_fun.argtypes = [ctypes.c_int]
            c_fun.restype = ctypes.c_void_p
            self.ptr = _runFunc(c_fun, [nb_components])

        _object_dir[self.ptr] = self

    @classmethod
    def from_pointer(cls, ptr):
        return cls(ptr=ptr)


class Task:
    def __init__(self, ctx=None, name=None, method=None, args=None, ptr=None):
        self.to_free = False
        if ptr is not None:
            self.ptr = ptr
            self.ctx = None

        else:
            if ctx is None:
                raise ValueError('You need to specify a context')

            self.ctx = ctx

            if name:
                c_fun = _lib.StarPart_newTaskByName
                c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p,
                                  ctypes.c_char_p]
                c_fun.restype = ctypes.c_void_p
                self.ptr = _runFunc(c_fun, [ctx.ptr, _encodeStr(name),
                                            _encodeStr(args)])
            else:
                c_fun = _lib.StarPart_newTask
                c_fun.argtypes = [ctypes.c_void_p, ctypes.c_void_p,
                                  ctypes.c_char_p]
                c_fun.restype = ctypes.c_void_p
                self.ptr = _runFunc(c_fun, [ctx.ptr, method.ptr,
                                            _encodeStr(args)])

            #self.to_free = True # FIXME

        _object_dir[self.ptr] = self

    def __del__(self):
        if self.to_free:
            c_fun = _lib.StarPart_freeTask
            c_fun.argtypes = [ctypes.c_void_p]
            c_fun.restype = None
            _runFunc(c_fun, [self.ptr])
            self.to_free = False

    @classmethod
    def from_pointer(cls, ptr):
        return cls(ptr=ptr)

    def insert_task_args(self, string):
        c_fun = _lib.StarPart_insertTaskArgs
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_int
        _runFunc(c_fun, [self.ptr, _encodeStr(string)])

    def get_id(self):
        c_fun = _lib.StarPart_getTaskID
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_char_p
        return _runFunc(c_fun, [self.ptr]).decode()

    def print(self, flags):
        c_fun = _lib.StarPart_printTask
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_int]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, flags])

    def connect(self, arg_id, data_id):
        c_fun = _lib.StarPart_connectTask
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p, ctypes.c_char_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, _encodeStr(arg_id), _encodeStr(data_id)])

    def disconnect(self):
        c_fun = _lib.StarPart_disconnectTask
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr])

    def get_data(self, arg_id):
        c_fun = _lib.StarPart_getTaskData
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, _encodeStr(arg_id)])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def get_nb_inputs(self):
        c_fun = _lib.StarPart_getTaskNbInputs
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_int
        return _runFunc(c_fun, [self.ptr])

    def get_nb_outputs(self):
        c_fun = _lib.StarPart_getTaskNbOutputs
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_int
        return _runFunc(c_fun, [self.ptr])

    def submit(self):
        c_fun = _lib.StarPart_submitTask
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr])

    def cancel(self):
        c_fun = _lib.StarPart_cancelTask
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr])

    def get_local(self):
        c_fun = _lib.StarPart_getTaskLocal
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def get_output(self, rank):
        c_fun = _lib.StarPart_getTaskOutput
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_int]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, rank])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def get_input(self, rank):
        c_fun = _lib.StarPart_getTaskInput
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_int]
        c_fun.restype = ctypes.c_void_p
        c_data = _runFunc(c_fun, [self.ptr, rank])
        return _c_ptr_to_starpart_obj(Data, c_data)

    def get_conn(self, arg_id):
        c_fun = _lib.StarPart_getTaskConn
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
        c_fun.restype = ctypes.c_char_p
        return _runFunc(c_fun, [self.ptr, arg_id]).decode()

    def is_parallel(self):
        c_fun = _lib.StarPart_isParallelTask
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_bool
        return _runFunc(c_fun, [self.ptr])

    def set_parallel(self, parallel):
        c_fun = _lib.StarPart_setParallelTask
        c_fun.argtypes = [ctypes.c_void_p, ctypes.c_bool]
        c_fun.restype = None
        _runFunc(c_fun, [self.ptr, parallel])

    def get_context(self):
        if self.ctx is None:
            c_fun = _lib.StarPart_getTaskContext
            c_fun.argtypes = [ctypes.c_void_p]
            c_fun.restype = ctypes.c_void_p
            c_context = _runFunc(c_fun, [self.ptr])
            self.ctx = _c_ptr_to_starpart_obj(Context, c_context)

        return self.ctx

    def get_method(self):
        c_fun = _lib.StarPart_getTaskMethod
        c_fun.argtypes = [ctypes.c_void_p]
        c_fun.restype = ctypes.c_void_p
        c_method = _runFunc(c_fun, [self.ptr])
        return _c_ptr_to_starpart_obj(Method, c_method)


class Hook():
    def __init__(self, ptr):
        self.ptr = ptr

    @classmethod
    def from_pointer(cls, ptr):
        return cls(ptr=ptr)
