# Continuous Integration

## Just Before

Once you have an account <https://ci.inria.fr>, you can start to manage your project and to create a slave VM.
Upload your public SSH key on your account and edit your `ssh/.config` with the following lines:

```text
Host *.ci
User ci
ProxyCommand ssh aesn002@ci-ssh.inria.fr "/usr/bin/nc `basename %h .ci` %p"
```

Then you can create a VM (or slave) on <https://ci.inria.fr/project/starpart/slaves> based on Ubuntu (64 bits), that is named `starpart-ubuntu` for instance.

## Installation of a VM

First of all, upload your ssh ID: `scp -r ~/.ssh/ ci@starpart-ubuntu.ci:`, using the Unix password `ci` for this account. Now, you should connect your VM with ssh proxy as follows: `ssh ci@starpart-ubuntu.ci`.

Then, install all the wanted packages

```bash
sudo apt-get update     # sudoer password = ci
sudo apt-get upgrade    # grub should be installed on /dev/sda
sudo apt-get install cmake git gcc libglib2.0-dev libpng-dev gcovr valgrind doxygen graphviz openmpi-* python3
```

And finally, you setup a specific *Runner* manually for your gitlab project at <https://gitlab.inria.fr>. See in the *CI/CD settings* section of your gitlab project. In this section, you will also find a registration token *xxxxxxxxxxxxxxxxxxxx*.

Install *gitlab-runner* as it is explained on <https://docs.gitlab.com/runner/install/>.

```bash
sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
￼sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
￼sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```

Then, register your *gitlab-runner* as explained on <https://docs.gitlab.com/runner/register/index.html>.

```bash
sudo gitlab-runner register
￼# URL <- https://gitlab.inria.fr/
# token <- xxxxxxxxxxxxxxxxxxxx
# description  <- starpart-ubuntu
# tags <-
# ...
# executor: shell
Runner registered successfully.
```
