#define _GNU_SOURCE
#include <assert.h>
#include <glib.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "starpart.h"
#include "struct.h"

/* *********************************************************** */

#define MAXLEN 6  // 5 chars + the terminating null byte
static void generateNewDataID(char* data_id)
{
  assert(data_id);
  static unsigned int k = 1000;
  sprintf(data_id, "@%d", k);
  assert(k <= 9999);
  assert(strlen(data_id) < MAXLEN);
  k++;
  assert(k >= 0 && k <= UINT_MAX);
}

/* *********************************************************** */

static void execHooks(StarPart_Task* task, bool pre)
{
  assert(task && task->ctx);
  StarPart_Data* data = StarPart_getTaskLocal(task);
  assert(data);

  /* for all hooks in table */
  GHashTableIter iter;
  gpointer key, val;
  g_hash_table_iter_init(&iter, task->ctx->hooktable);
  while (g_hash_table_iter_next(&iter, &key, &val)) {
    char* hook_id = (char*)key;
    StarPart_Hook* hook = (StarPart_Hook*)val;
    /* lookup for hook id in task local data */
    if (StarPart_isItem(data, hook_id)) {
      if (pre && hook->pre) hook->pre(task);
      if (!pre && hook->post) hook->post(task);
    }
  }
}

/* *********************************************************** */

static void initializeTaskData(StarPart_Task* task)
{
  assert(task && task->ctx);
  StarPart_Method* m = StarPart_getTaskMethod(task);

  /* for all method args, init task data */
  GHashTableIter iter;
  gpointer key, val;
  g_hash_table_iter_init(&iter, m->args);
  while (g_hash_table_iter_next(&iter, &key, &val)) {
    char* arg_id = (char*)key;
    StarPart_Data* arg_data = (StarPart_Data*)val;  // default data
    assert(arg_data);

    // int flags = StarPart_getMethodArgFlags(task->method, arg_id);
    if (task->parallel && (arg_data->flags & STARPART_MASTER) && (task->ctx->prank != 0)) continue;  // skip this arg

    char* flow_data_id = StarPart_getTaskConn(task, arg_id);
    // if(!flow_data_id) StarPart_error("no data connected to arg \"%s\" of method \"%s\"!\n",
    // arg_id, task->id);

    // connect task (if not yet connected)
    if (!flow_data_id) {
      if (arg_data->flags & STARPART_IN)
        StarPart_error("no data connected to input arg \"%s\" of method \"%s\"!\n", arg_id, task->id);
      // connect LOCAL or OUTPUT automatically...
      char new_id[MAXLEN];
      generateNewDataID(new_id);
      StarPart_connectTask(task, arg_id, new_id);
      flow_data_id = StarPart_getTaskConn(task, arg_id);
    }

    // create & register new data if required
    assert(flow_data_id);
    // bool new_data = false;
    StarPart_Data* flow_data = StarPart_getData(task->ctx, flow_data_id);
    if (!flow_data) {
      flow_data = StarPart_registerNewData(task->ctx, flow_data_id, 0);  // flags?
      // new_data = true;
    }

    assert(flow_data);
    // if(!flow_data) StarPart_error("no data connected to arg \"%s\" of method \"%s\"!\n", arg_id,
    // task->id);

    // StarPart_print("task \"%s\" / arg \"%s\" -> data \"%s\" %s\n", task->id, arg_id,
    // flow_data->id, new_data?"(new)":"");

    /* set user items */
    StarPart_Data* user_data = StarPart_getDataFromTable(task->userargs, arg_id);  // user data
    GHashTableIter item_iter;
    gpointer item_key, item_val;
    if (user_data) {
      g_hash_table_iter_init(&item_iter, user_data->table);
      while (g_hash_table_iter_next(&item_iter, &item_key, &item_val)) {
        char* item_id = (char*)item_key;
        StarPart_Item* user_item = (StarPart_Item*)item_val;
        assert(user_item);
        StarPart_Item* flow_item = StarPart_getItem(flow_data, item_id);
        if (flow_item) StarPart_warning("user item \"%s/%s\" override a flow item\n", arg_id, item_id);
        StarPart_Item* new_item = StarPart_dupItem(user_item, true); /* deep copy of item */
        StarPart_addItem(flow_data, new_item);
      }
    }

    /* set default arg items */
    g_hash_table_iter_init(&item_iter, arg_data->table);
    while (g_hash_table_iter_next(&item_iter, &item_key, &item_val)) {
      char* item_id = (char*)item_key;
      StarPart_Item* default_item = (StarPart_Item*)item_val;
      assert(default_item);
      StarPart_Item* flow_item = StarPart_getItem(flow_data, item_id);
      if (!flow_item) {
        StarPart_Item* new_item = StarPart_dupItem(default_item, true); /* deep copy of item */
        StarPart_addItem(flow_data, new_item);
      }
      else { /* update only item flags for read/write mode... */
        flow_item->flags = default_item->flags;
      }
    }
  }
}

/* *********************************************************** */

static void finalizeTaskData(StarPart_Task* task)
{
  assert(task && task->ctx);
  StarPart_Method* m = StarPart_getTaskMethod(task);

  /* for all method args */
  GHashTableIter iter;
  gpointer key, val;
  g_hash_table_iter_init(&iter, m->args);
  while (g_hash_table_iter_next(&iter, &key, &val)) {
    char* arg_id = (char*)key;
    StarPart_Data* arg_data = (StarPart_Data*)val;  // default data
    assert(arg_data);
    if (task->parallel && (arg_data->flags & STARPART_MASTER) && (task->ctx->prank != 0)) continue;  // skip this arg

    StarPart_Data* flow_data = StarPart_getTaskData(task, arg_id);  // flow data
    if (!flow_data) StarPart_error("no flow data for \"%s\"!\n", arg_id);

    /* remove local data  */
    if (flow_data && (arg_data->flags & STARPART_LOCAL)) {
      StarPart_unregisterData(task->ctx, flow_data->id);
      StarPart_freeData(flow_data);
      continue;
    }

    /* reset flags */
    flow_data->flags = 0;
    GHashTableIter item_iter;
    gpointer item_key, item_val;
    g_hash_table_iter_init(&item_iter, flow_data->table);
    while (g_hash_table_iter_next(&item_iter, &item_key, &item_val)) {
      // char* item_id = (char*)item_key;
      StarPart_Item* flow_item = (StarPart_Item*)item_val;
      assert(flow_item);
      flow_item->flags = 0;  // no flags => all RW access granted!
    }
  }
}

/* *********************************************************** */

static void checkTaskReady(StarPart_Task* task, int flags)
{
  assert(task && task->ctx);

  // check locals
  if (flags & STARPART_LOCAL || flags == 0) {
    // char* arg_id = StarPart_getMethodLocal(task->method);
    StarPart_Data* arg = StarPart_getMethodLocal(task->method);
    StarPart_Data* local = StarPart_getTaskLocal(task);
    if (!local) StarPart_error("task \"%s\": local arg \"%s\" is not connected!\n", task->id, arg->id);
  }

  // check inputs
  if (flags & STARPART_IN || flags == 0) {
    int nbinputs = StarPart_getTaskNbInputs(task);
    for (int i = 0; i < nbinputs; i++) {
      // char* arg_id = StarPart_getMethodInput(task->method, i);
      // int _flags = StarPart_getMethodArgFlags(task->method, arg_id);
      StarPart_Data* arg = StarPart_getMethodInput(task->method, i);
      if (task->parallel && (arg->flags & STARPART_MASTER) && (task->ctx->prank != 0)) continue;  // skip this arg
      StarPart_Data* input = StarPart_getTaskInput(task, i);
      if (!input) StarPart_error("task \"%s\": input arg \"%s\" is not connected!\n", task->id, arg->id);
    }
  }

  // check outputs
  if (flags & STARPART_OUT || flags == 0) {
    int nboutputs = StarPart_getTaskNbOutputs(task);
    for (int i = 0; i < nboutputs; i++) {
      // char* arg_id = StarPart_getMethodOutput(task->method, i);
      // int _flags = StarPart_getMethodArgFlags(task->method, arg_id);
      StarPart_Data* arg = StarPart_getMethodOutput(task->method, i);
      if (task->parallel && (arg->flags & STARPART_MASTER) && (task->ctx->prank != 0)) continue;  // skip this arg
      StarPart_Data* output = StarPart_getTaskOutput(task, i);
      if (!output) StarPart_error("task \"%s\": output arg \"%s\" is not connected!\n", task->id, arg->id);
    }
  }
}

/* *********************************************************** */

static void _execTask(StarPart_Task* task)
{
  assert(task && task->ctx);

  if (StarPart_isDebug(task->ctx)) { StarPart_printBlue("=> exec task \"%s\"\n", task->id); }

  /* get method */
  StarPart_Method* m = StarPart_getTaskMethod(task);
  if (!m) StarPart_error("method \"%s\" unknown!\n", task->id);
  StarPart_MethodCallFunc execMethod = StarPart_getCallFunc(m);

  initializeTaskData(task);

  /* check task is connected */
  checkTaskReady(task, STARPART_LOCAL | STARPART_IN | STARPART_OUT);

  /* method pre processing */
  execHooks(task, true);

  if (StarPart_isDebug(task->ctx)) { StarPart_printTask(task, STARPART_LOCAL | STARPART_IN); }

  /* exec method */
  execMethod(task);

  /* method post processing */
  execHooks(task, false);

  if (StarPart_isDebug(task->ctx)) { StarPart_printTask(task, STARPART_OUT); }

  finalizeTaskData(task);
}

/* *********************************************************** */

static void execTask(StarPart_Task* task)
{
  assert(task && task->ctx);
  /* parallel context */
  if (task->ctx->parallel) {
    int rank = StarPart_getParallelCommRank(task->ctx);
    if (task->parallel)
      _execTask(task);
    else if (rank == 0) /* sequential task in parallel context */
      _execTask(task);
  }
  /* sequential context */
  else {
    assert(!task->parallel); /* error: parallel task in sequential context */
    _execTask(task);
  }
}

/* *********************************************************** */

void StarPart_submitTask(StarPart_Task* task)
{
  assert(task && task->ctx && task->ctx->queue);
  g_queue_push_tail(task->ctx->queue, task);
}

/* *********************************************************** */

void StarPart_cancelTask(StarPart_Task* task)
{
  assert(task && task->ctx && task->ctx->queue);
  bool r = g_queue_remove(task->ctx->queue, task);
  assert(r);
}

/* *********************************************************** */

void StarPart_submitAllTasks(StarPart_Context* ctx)
{
  assert(ctx && ctx->tasks && ctx->queue);

  GList* link = g_queue_peek_head_link(ctx->tasks);
  while (link) {
    StarPart_Task* task = (StarPart_Task*)link->data;
    StarPart_submitTask(task);
    link = g_list_next(link);
  }
}

/* *********************************************************** */

void StarPart_cancelAllTasks(StarPart_Context* ctx)
{
  assert(ctx && ctx->queue);
  g_queue_clear(ctx->queue);
}

/* *********************************************************** */

void StarPart_connectAllTasks(StarPart_Context* ctx, char* in, char* out)
{
  assert(ctx && ctx->queue);

  /* connect input of head task */
  if (in) {
    StarPart_Task* head = StarPart_getHeadTask(ctx);
    assert(head);
    if (StarPart_getTaskNbInputs(head) > 0) {
      if (StarPart_isDebug(ctx)) StarPart_print("connect task \"%s\": #in<%s\n", head->id, in);
      StarPart_connectTask(head, "#in", in);
    }
  }

  /* connect output of tail task */
  if (out) {
    StarPart_Task* tail = StarPart_getTailTask(ctx);
    assert(tail);
    if (StarPart_getTaskNbOutputs(tail) > 0) {
      if (StarPart_isDebug(ctx)) StarPart_print("connect task \"%s\": #out>%s\n", tail->id, out);
      StarPart_connectTask(tail, "#out", out);
    }
  }

  /* try to connect automatically other tasks in queue */
  char* data_id = in;  // last data ID connected in queue, used as defaut input
  char new_id[MAXLEN];
  int rank = 0;
  GList* link = g_queue_peek_head_link(ctx->queue);

  while (link) {
    StarPart_Task* task = (StarPart_Task*)link->data;
    StarPart_Method* method = StarPart_getTaskMethod(task);
    int nboutputs = StarPart_getMethodNbOutputs(method);
    GList* nextlink = g_list_next(link);
    if (!nextlink) break;  // last task in queue
    StarPart_Task* nexttask = (StarPart_Task*)nextlink->data;
    StarPart_Method* nextmethod = StarPart_getTaskMethod(nexttask);
    int nbinputs = StarPart_getMethodNbInputs(nextmethod);

    /* Rules: */
    /* 1) try to automatically connect the first output of current task to the first input of next task */
    /* 2) all pure ouputs are implicitly connected to a new data ID */

    /* for all outputs of current task... */
    for (int output = 0; output < nboutputs; output++) {
      // char* arg_id = StarPart_getMethodOutput(method, output);
      // int flags = StarPart_getMethodArgFlags(method, arg_id);
      StarPart_Data* arg = StarPart_getMethodOutput(method, output);
      if (task->parallel && (arg->flags & STARPART_MASTER) && (ctx->prank != 0)) continue;  // skip this output
      char* _data_id = StarPart_getTaskConn(task, arg->id);
      if (_data_id && output == 0) data_id = _data_id;  // only first ouput

      // not yet connected...
      if (!_data_id) {
        assert(!(arg->flags & STARPART_IN));  // check this arg is a pure output (not inout)
        generateNewDataID(new_id);
        if (StarPart_isDebug(ctx)) StarPart_print("connect task \"%s\": #out%d>%s\n", task->id, output, new_id);
        StarPart_connectTask(task, arg->id, new_id);
        if (output == 0) data_id = new_id;
      }
    }

    // connect only first input of next task (if possible)
    for (int input = 0; input < nbinputs; input++) {
      // char* arg_id = StarPart_getMethodInput(nextmethod, input);
      // int flags = StarPart_getMethodArgFlags(nextmethod, arg_id);
      StarPart_Data* arg = StarPart_getMethodInput(nextmethod, input);
      if (nexttask->parallel && (arg->flags & STARPART_MASTER) && (ctx->prank != 0)) break;  // skip this input
      char* _data_id = StarPart_getTaskConn(nexttask, arg->id);
      if (_data_id) data_id = _data_id;  // already connected
      if (!data_id)
        StarPart_error("Task \"%s\" (rank %d in queue): arg \"%s\" (#in) cannot be automatically connected!\n",
                       nexttask->id, rank, arg->id);
      if (!_data_id) {  // not yet connected
        if (StarPart_isDebug(ctx)) StarPart_print("connect task \"%s\": #in%d<%s\n", nexttask->id, input, data_id);
        StarPart_connectTask(nexttask, arg->id, data_id);
      }
      if (input == 0) break;  // only consider first input!!!
    }

    link = nextlink;
    rank++;
  }
}

/* *********************************************************** */

StarPart_Task* StarPart_step(StarPart_Context* ctx)
{
  assert(ctx && ctx->queue);
  StarPart_Task* task = (StarPart_Task*)g_queue_pop_head(ctx->queue);
  if(!task) return NULL; // empty queue
  execTask(task);
  return task; // continue
}

/* *********************************************************** */

void StarPart_run(StarPart_Context* ctx)
{
  assert(ctx && ctx->queue);
  StarPart_Task* task = NULL;
  while ((task = (StarPart_Task*)g_queue_pop_head(ctx->queue))) { execTask(task); }
}

/* *********************************************************** */
