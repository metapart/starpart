#ifndef STARPART_AUX_H
#define STARPART_AUX_H

#include "starpart.h"

/**
 *
 * @defgroup private StarPart Private API
 * @{
 * @brief The private StarPart programming interface.
 */

/* *********************************************************** */
/*                       PRIVATE API                           */
/* *********************************************************** */

/**
 * @brief create a new \ref StarPart_Task within a given context
 *
 * @details See manual for details on the string representation format of arguments & connections.
 * @param ctx the context of the new created task
 * @param method a registred method
 * @param args a string representation of task arguments & connections passed to the new created task
 * @return a new \ref StarPart_Task
 */

StarPart_Task* StarPart_newTask(StarPart_Context* ctx, StarPart_Method* method, char* args);

/**
 * @brief free a \ref StarPart_Task
 * @param task the task to be freed
 */

void StarPart_freeTask(StarPart_Task* task);

/**
 * @brief create a new \ref StarPart_Hook
 * 
 * @param id 
 * @param pre 
 * @param post 
 * @param desc 
 * @return a new \ref StarPart_Hook
 */

StarPart_Hook* StarPart_newHook(char* id, StarPart_HookFunc pre, StarPart_HookFunc post, char* desc);


/**
 * @brief free a \ref StarPart_Hook
 * @param hook the hook to be freed
 */

void StarPart_freeHook(StarPart_Hook* hook);


/** @} */

#endif
