#define _GNU_SOURCE
#include <assert.h>
#include <glib.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef USE_MPI
#include <mpi.h>
#endif

#include "starpart.h"
#include "struct.h"
#include "aux.h"

/* *********************************************************** */

/** default StarPart stack context (global variable)*/
GQueue* _ctx_stack = NULL;

/* *********************************************************** */

GQueue* StarPart_getContextStack(void)
{
  return _ctx_stack;
}

/* *********************************************************** */

static void pushContext(StarPart_Context* ctx)
{
  if (!_ctx_stack) _ctx_stack = g_queue_new();
  assert(_ctx_stack);
  g_queue_push_head(_ctx_stack, ctx);
}

/* *********************************************************** */

static StarPart_Context* popContext(void)
{
  assert(_ctx_stack);
  StarPart_Context* ctx = g_queue_pop_head(_ctx_stack);
  if (g_queue_is_empty(_ctx_stack)) {
    g_queue_free(_ctx_stack);
    _ctx_stack = NULL;
  }
  return ctx;
}

/* *********************************************************** */

StarPart_Context* StarPart_getCurrentContext(void)
{
  if (!_ctx_stack) return NULL;
  return g_queue_peek_head(_ctx_stack);
}

/* *********************************************************** */

static void _free_method(gpointer ptr)
{
  StarPart_Method* method = (StarPart_Method*)ptr;
  StarPart_freeMethod(method);
}

/* *********************************************************** */

static void _free_hook(gpointer ptr)
{
  StarPart_Hook* hook = (StarPart_Hook*)ptr;
  StarPart_freeHook(hook);
}

/* *********************************************************** */

StarPart_Context* StarPart_newContext(void)
{
  // StarPart_print("new context\n");

  StarPart_Context* ctx = malloc(sizeof(StarPart_Context));
  assert(ctx);

  /* method & data table */
  ctx->packages = g_sequence_new(NULL);
  ctx->methodtable = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, _free_method);
  ctx->hooktable = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, _free_hook);
  ctx->datatable = StarPart_newDataTable();
  ctx->debug = false;
  ctx->tasks = g_queue_new();
  ctx->queue = g_queue_new();
  ctx->parallel = false;
  ctx->comm = MPI_COMM_NULL;
  ctx->prank = 0;
  ctx->psize = 1;

  /* push context on stack */
  pushContext(ctx);

  return ctx;
}

/* *********************************************************** */

StarPart_Context* StarPart_newParallelContext(MPI_Comm comm)
{
  StarPart_Context* ctx = malloc(sizeof(StarPart_Context));
  assert(ctx);

#ifdef USE_MPI
  /* method & data table */
  ctx->packages = g_sequence_new(NULL);
  ctx->methodtable = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, _free_method);
  ctx->hooktable = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, _free_hook);
  ctx->datatable = StarPart_newDataTable();
  ctx->debug = false;
  ctx->tasks = g_queue_new();
  ctx->queue = g_queue_new();
  ctx->parallel = true;
  ctx->comm = comm;
  MPI_Comm_rank(ctx->comm, &ctx->prank);
  MPI_Comm_size(ctx->comm, &ctx->psize);
  /* push context on stack */
  pushContext(ctx);
#else
  StarPart_error("Parallel context are only available with MPI support (set USE_MPI cmake option)!\n");
#endif

  return ctx;
}

/* *********************************************************** */

bool StarPart_isParallelContext(StarPart_Context* ctx)
{
  assert(ctx);
  return ctx->parallel;
}

/* *********************************************************** */

MPI_Comm StarPart_getParallelComm(StarPart_Context* ctx)
{
  assert(ctx);
  return ctx->comm;
}

/* *********************************************************** */

int StarPart_getParallelCommRank(StarPart_Context* ctx)
{
  assert(ctx);
  return ctx->prank;
}

/* *********************************************************** */

int StarPart_getParallelCommSize(StarPart_Context* ctx)
{
  assert(ctx);
  return ctx->psize;
}

/* *********************************************************** */

void StarPart_freeContext(StarPart_Context* ctx)
{
  assert(ctx);
  /* pop context from stack */
  assert(popContext() == ctx);
  /* free mem */
  g_sequence_free(ctx->packages);
  g_hash_table_destroy(ctx->methodtable);
  g_hash_table_destroy(ctx->hooktable);
  StarPart_freeDataTable(ctx->datatable);
  g_queue_free(ctx->tasks); // TODO: same as freeAllTasks()?
  g_queue_free(ctx->queue);
  free(ctx);
}

/* *********************************************************** */

void StarPart_freeAllTasks(StarPart_Context* ctx)
{
  assert(ctx && ctx->tasks);

  // TODO: use g_queue_free_full() ?

  StarPart_Task* task = NULL;
  while ((task = g_queue_pop_head(ctx->tasks))) {
    StarPart_freeTask(task);
  }
  g_queue_clear(ctx->queue);
}

/* *********************************************************** */

StarPart_Context* StarPart_newNestedContext(StarPart_Context* ctx)
{
  // StarPart_print("new nested context\n");
  StarPart_Context* _ctx = malloc(sizeof(StarPart_Context));
  assert(_ctx);

  /* method & data table */
  _ctx->packages = ctx->packages; /* share same packages */
  _ctx->methodtable = ctx->methodtable;     /* share same method table */
  _ctx->hooktable = ctx->hooktable;     /* share same hook table */
  _ctx->datatable = StarPart_newDataTable();
  _ctx->debug = ctx->debug;
  _ctx->tasks = g_queue_new();
  _ctx->queue = g_queue_new();
  _ctx->parallel = ctx->parallel;
  _ctx->comm = ctx->comm;
  _ctx->prank = ctx->prank;
  _ctx->psize = ctx->psize;

  /* push context on stack */
  pushContext(_ctx);

  return _ctx;
}

/* *********************************************************** */

void StarPart_freeNestedContext(StarPart_Context* ctx)
{
  assert(ctx);
  /* pop context from stack */
  assert(popContext() == ctx);
  /* free mem */
  StarPart_freeDataTable(ctx->datatable);
  g_queue_free(ctx->tasks);
  g_queue_free(ctx->queue);
  free(ctx);
}

/* *********************************************************** */

GHashTable* StarPart_getMethodatatable(StarPart_Context* ctx)
{
  assert(ctx);
  return ctx->methodtable;
}

/* *********************************************************** */

GHashTable* StarPart_getHookTable(StarPart_Context* ctx)
{
  assert(ctx);
  return ctx->hooktable;
}

/* *********************************************************** */

StarPart_DataTable* StarPart_getDataTable(StarPart_Context* ctx)
{
  assert(ctx);
  return ctx->datatable;
}

/* *********************************************************** */

/* char * StarPart_getCurrentMethodConnection(StarPart_Context * ctx, char * arg_id) */
/* { */
/*   assert(ctx); */
/*   char * data_id = (char*)g_hash_table_lookup(ctx->ctable, arg_id); */
/*   return data_id; */
/* } */

/* *********************************************************** */

/* bool StarPart_connectCurrentMethod(StarPart_Context * ctx, char * arg_id, char * data_id) */
/* { */
/*   assert(ctx); */
/*   gboolean r = g_hash_table_insert(ctx->ctable, (gpointer)strdup(arg_id),
 * (gpointer)strdup(data_id)); */
/*   return r; */
/* } */

/* *********************************************************** */

/* void StarPart_disconnectCurrentMethod(StarPart_Context * ctx) */
/* { */
/*   assert(ctx); */
/*   g_hash_table_remove_all(ctx->ctable); */
/* } */

/* *********************************************************** */

/* StarPart_Data * StarPart_getCurrentMethodArgument(StarPart_Context * ctx,  char * arg_id) */
/* { */
/*   assert(ctx && ctx->datatable); */
/*   if(!arg_id) return NULL; */
/*   char * data_id = StarPart_getCurrentMethodConnection(ctx, arg_id); */
/*   if(!data_id) return NULL; */
/*   StarPart_Data * data = StarPart_getData(ctx, data_id); */
/*   return data; */
/* } */

/* *********************************************************** */

/* GHashTable * StarPart_getCurrentUserArguments(StarPart_Context * ctx) */
/* { */
/*   assert(ctx); */
/*   if(!ctx->node) return NULL; */
/*   return StarPart_getNodeArgs(ctx->node);   */
/* } */

/* *********************************************************** */

StarPart_Task* StarPart_getHeadTask(StarPart_Context* ctx)
{
  assert(ctx && ctx->queue);
  return (StarPart_Task*)g_queue_peek_head(ctx->queue);
}

/* *********************************************************** */

StarPart_Task* StarPart_getTailTask(StarPart_Context* ctx)
{
  assert(ctx && ctx->queue);
  return (StarPart_Task*)g_queue_peek_tail(ctx->queue);
}

/* *********************************************************** */
