#define _GNU_SOURCE
#include <assert.h>
#include <glib.h>
#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libgraph.h"
#include "starpart.h"
#include "struct.h"

/* *********************************************************** */

char* data_flags_str[] = {"_", "in", "out", "inout", "local", "?"};
char* item_flags_str[] = {"_", "r", "w", "rw", "x", "+", "x+"};
char* type_str[] = {"void",   "strat",      "int",         "float",        "double",
                    "string", "int array",  "float array", "double array", "string array",
                    "graph",  "dist graph", "hypergraph",  "mesh",         "hybrid mesh"};

/* *********************************************************** */
/*                    DUP ROUTINES                             */
/* *********************************************************** */

static int* dupInt(int* x)
{
  if (!x) return NULL;
  int* p = malloc(sizeof(int));
  *p = *x;
  return p;
}

/* *********************************************************** */

static char* dupStr(char* x)
{
  if (!x) return NULL;
  char* p = malloc((strlen(x) + 1) * sizeof(int));
  strcpy(p, x);
  return p;
}

/* *********************************************************** */

static float* dupFloat(float* x)
{
  float* p = (float*)malloc(sizeof(float));
  *p = *x;
  return p;
}

/* *********************************************************** */

static double* dupDouble(double* x)
{
  double* p = (double*)malloc(sizeof(double));
  *p = *x;
  return p;
}

/* *********************************************************** */

static LibGraph_Array* dupArray(LibGraph_Array* x, bool deep)
{
  if (!x) return NULL;
  LibGraph_Array* p = malloc(sizeof(LibGraph_Array));
  LibGraph_dupArray(x, p, deep);
  return p;
}

/* *********************************************************** */

static LibGraph_Graph* dupGraph(LibGraph_Graph* oldg)
{
  if (!oldg) return NULL;
  LibGraph_Graph* newg = malloc(sizeof(LibGraph_Graph));
  assert(newg);
  LibGraph_dupGraph(oldg, newg);
  return newg;
}

/* *********************************************************** */

static LibGraph_DistGraph* dupDistGraph(LibGraph_DistGraph* dg)
{
  if (!dg) return NULL;
  LibGraph_DistGraph* newdg = malloc(sizeof(LibGraph_DistGraph));
  LibGraph_dupDistGraph(dg, newdg);
  return newdg;
}

/* *********************************************************** */

static LibGraph_Hypergraph* dupHypergraph(LibGraph_Hypergraph* oldhg)
{
  if (!oldhg) return NULL;
  LibGraph_Hypergraph* newhg = malloc(sizeof(LibGraph_Hypergraph));
  LibGraph_dupHypergraph(oldhg, newhg);
  return newhg;
}

/* *********************************************************** */

static LibGraph_Mesh* dupMesh(LibGraph_Mesh* m)
{
  if (!m) return NULL;
  LibGraph_Mesh* newm = malloc(sizeof(LibGraph_Mesh));
  LibGraph_dupMesh(m, newm);
  return newm;
}

/* *********************************************************** */

static LibGraph_HybridMesh* dupHybridMesh(LibGraph_HybridMesh* hm)
{
  if (!hm) return NULL;
  LibGraph_HybridMesh* newhm = malloc(sizeof(LibGraph_HybridMesh));
  LibGraph_dupHybridMesh(hm, newhm);
  return newhm;
}

/* *********************************************************** */

static void* dupVal(StarPart_Type type, void* val)
{
  if (!val) return NULL;
  void* newval = NULL;
  switch (type) {
    case STARPART_VOID: newval = NULL; break;
    case STARPART_STRAT: newval = dupStr(val); break;     /* deep copy */
    case STARPART_INT: newval = dupInt(val); break;       /* deep copy */
    case STARPART_FLOAT: newval = dupFloat(val); break;   /* deep copy */
    case STARPART_DOUBLE: newval = dupDouble(val); break; /* deep copy */
    case STARPART_STR: newval = dupStr(val); break;       /* deep copy */

    case STARPART_INTARRAY:
    case STARPART_FLOATARRAY:
    case STARPART_DOUBLEARRAY:
    case STARPART_STRARRAY: newval = dupArray(val, true); break; /* deep copy */

    case STARPART_GRAPH: newval = dupGraph(val); break;           /* deep copy */
    case STARPART_DISTGRAPH: newval = dupDistGraph(val); break;   /* deep copy */
    case STARPART_HYPERGRAPH: newval = dupHypergraph(val); break; /* deep copy */
    case STARPART_MESH: newval = dupMesh(val); break;             /* deep copy */
    case STARPART_HYBRIDMESH: newval = dupHybridMesh(val); break; /* deep copy */
    default: assert(0);                                           // error
  }
  return newval;
}

/* *********************************************************** */
/*                   FREE ROUTINES                             */
/* *********************************************************** */

static void freeVal(StarPart_Item* item)
{
  if (!item->val) return;
  if (item->type == STARPART_VOID) return;
  if (!item->owner) {
    item->val = NULL;
    return;
  }

  /* free internal structured types */
  switch (item->type) {
    case STARPART_VOID:
    case STARPART_STRAT:
    case STARPART_INT:
    case STARPART_FLOAT:
    case STARPART_DOUBLE:
    case STARPART_STR: break;

    case STARPART_INTARRAY:
    case STARPART_FLOATARRAY:
    case STARPART_DOUBLEARRAY:
    case STARPART_STRARRAY: LibGraph_freeArray(item->val); break;

    case STARPART_GRAPH: LibGraph_freeGraph(item->val); break;
    case STARPART_DISTGRAPH: LibGraph_freeDistGraph(item->val); break;
    case STARPART_HYPERGRAPH: LibGraph_freeHypergraph(item->val); break;
    case STARPART_MESH: LibGraph_freeMesh(item->val); break;
    case STARPART_HYBRIDMESH: LibGraph_freeHybridMesh(item->val); break;
    default: assert(0);
  }

  free(item->val);
  item->val = NULL;
}

/* *********************************************************** */
/*                  NEW ROUTINES                               */
/* *********************************************************** */

int* StarPart_newInt(int x)
{
  return dupInt(&x);
}

/* *********************************************************** */

float* StarPart_newFloat(float x)
{
  return dupFloat(&x);
}

/* *********************************************************** */

double* StarPart_newDouble(double x)
{
  return dupDouble(&x);
}

/* *********************************************************** */

char* StarPart_newStr(char* x)
{
  if (!x) return NULL;
  return strdup(x);
}

/* *********************************************************** */

char* StarPart_newStrat(char* x)
{
  if (!x) return NULL;
  return strdup(x);
}

/* *********************************************************** */

LibGraph_Array* StarPart_newIntArray(int length, int* array)
{
  LibGraph_Array* a = LibGraph_newArray();
  assert(a);
  LibGraph_wrapArray(a, length, 1, LIBGRAPH_INT, array, false);  // don't take ownership
  return a;
}

/* *********************************************************** */

LibGraph_Array* StarPart_newFloatArray(int length, float* array)
{
  LibGraph_Array* a = LibGraph_newArray();
  assert(a);
  LibGraph_wrapArray(a, length, 1, LIBGRAPH_FLOAT, array, false);  // don't take ownership
  return a;
}

/* *********************************************************** */

LibGraph_Array* StarPart_newDoubleArray(int length, double* array)
{
  LibGraph_Array* a = LibGraph_newArray();
  assert(a);
  LibGraph_wrapArray(a, length, 1, LIBGRAPH_DOUBLE, array, false);  // don't take ownership
  return a;
}

/* *********************************************************** */

LibGraph_Mesh* StarPart_newMesh(void)
{
  return LibGraph_newMesh();
}

/* *********************************************************** */

LibGraph_Graph* StarPart_newGraph(void)
{
  return LibGraph_newGraph();
}

/* *********************************************************** */

LibGraph_DistGraph* StarPart_newDistGraph(void)
{
  return LibGraph_newDistGraph();
}

/* *********************************************************** */

LibGraph_Hypergraph* StarPart_newHypergraph(void)
{
  return LibGraph_newHypergraph();
}

/* *********************************************************** */

LibGraph_HybridMesh* StarPart_newHybridMesh(int nb_components)
{
  return LibGraph_newHybridMesh(nb_components);
}

/* *********************************************************** */
/*                  NEW ROUTINES FROM STRING                   */
/* *********************************************************** */

char* StarPart_newStrFromStr(char* x)
{
  if (!x) return NULL;
  int len = strlen(x);
  if (len >= 2 && x[0] == '"' && x[len - 1] == '"')
    return strndup(x + 1, len - 2);
  else
    return strdup(x);
}

/* *********************************************************** */

char* StarPart_newStratFromStr(char* x)
{
  if (!x) return NULL;
  int len = strlen(x);
  assert(len >= 2 && x[0] == '{' && x[len - 1] == '}');
  return strndup(x + 1, len - 2);
}

/* *********************************************************** */

LibGraph_Array* StarPart_newIntArrayV(int length, ...)
{
  LibGraph_Array* a = LibGraph_newArray();
  assert(a);
  LibGraph_allocArray(a, length, 1, LIBGRAPH_INT);
  int* array = (int*)a->data;
  va_list ap;
  va_start(ap, length);
  for (int i = 0; i < length; i++) { array[i] = va_arg(ap, int); }
  va_end(ap);
  return a;
}

/* *********************************************************** */

/* str should be formatted as "(val1,val2,...,valN)" */
LibGraph_Array* StarPart_newIntArrayFromStr(char* str)
{
  assert(str);
  int len = strlen(str);
  assert(str[0] == '(' && str[len - 1] == ')');  // remove parentheses
  char* _str = calloc(len + 1, sizeof(char));
  strncpy(_str, str + 1, len - 2);
  gchar** tokens = g_strsplit(_str, ",", -1);
  free(_str);
  assert(tokens);
  int length = 0;
  char** token = tokens;
  while (*token) {
    length++;
    token++;
  }
  int* array = calloc(length, sizeof(int));
  assert(array);
  for (int k = 0; k < length; k++) array[k] = atoi(tokens[k]);
  g_strfreev(tokens);

  LibGraph_Array* a = LibGraph_newArray();
  assert(a);
  LibGraph_wrapArray(a, length, 1, LIBGRAPH_INT, array, true);  // take ownership
  return a;
}

/* *********************************************************** */

LibGraph_Array* StarPart_newFloatArrayFromStr(char* str)
{
  assert(0);  // TODO: to implement
}

/* *********************************************************** */

LibGraph_Array* StarPart_newDoubleArrayFromStr(char* str)
{
  assert(0);  // TODO: to implement
}

/* *********************************************************** */

LibGraph_Array* StarPart_newStrArray(int length, char** array)
{
  LibGraph_Array* a = LibGraph_newArray();
  assert(a);
  LibGraph_wrapArray(a, length, 1, LIBGRAPH_STR, array, false);  // don't take ownership
  return a;
}

/* *********************************************************** */

LibGraph_Array* StarPart_newStrArrayV(int length, ...)
{
  LibGraph_Array* a = LibGraph_newArray();
  assert(a);
  LibGraph_allocArray(a, length, 1, LIBGRAPH_STR);
  char** array = (char**)a->data;
  va_list ap;
  va_start(ap, length);
  for (int i = 0; i < length; i++) { array[i] = strdup(va_arg(ap, char*)); }
  va_end(ap);
  return a;
}

/* *********************************************************** */

/* str should be formatted as "(\"str1\",\"str2\",...,\"strN\")" or
 * "(str1,str2,str3,...)" */
LibGraph_Array* StarPart_newStrArrayFromStr(char* str)
{
  assert(str);
  int len = strlen(str);
  assert(str[0] == '(' && str[len - 1] == ')');  // remove parentheses
  char* _str = calloc(len + 1, sizeof(char));
  strncpy(_str, str + 1, len - 2);
  gchar** tokens = g_strsplit(_str, ",", -1);
  free(_str);
  assert(tokens);
  int length = 0;
  char** token = tokens;
  while (*token) {
    length++;
    token++;
  }
  char** array = calloc(length, sizeof(char*));
  assert(array);
  for (int k = 0; k < length; k++) {
    g_strstrip(tokens[k]);
    int len = strlen(tokens[k]);
    // assert(tokens[k][0] == '"' && tokens[k][len-1] == '"');
    if (tokens[k][0] == '"' && tokens[k][len - 1] == '"')
      array[k] = strndup(tokens[k] + 1, len - 2);  // remove "..."
    else
      array[k] = strdup(tokens[k]);
  }
  g_strfreev(tokens);

  LibGraph_Array* a = LibGraph_newArray();
  assert(a);
  LibGraph_wrapArray(a, length, 1, LIBGRAPH_STR, array, true);  // take ownership
  return a;
}

/* *********************************************************** */
/*                        DATA ITEM                            */
/* *********************************************************** */

StarPart_Item* StarPart_newItem(char* item_id, StarPart_Type type, void* val, bool owner, int flags)
{
  assert(item_id);
  StarPart_Item* item = malloc(sizeof(StarPart_Item));
  assert(item);
  item->id = dupStr(item_id);
  item->type = type;
  item->owner = owner;
  item->val = val;
  item->flags = flags;
  item->versions = g_queue_new();
  return item;
}

/* *********************************************************** */

StarPart_Item* StarPart_newItemFromStr(char* item_id, StarPart_Type type, char* str_val, int flags)
{
  void* new_val = NULL;
  if (type == STARPART_VOID) { new_val = NULL; }
  else if (type == STARPART_STR) {
    new_val = StarPart_newStrFromStr(str_val);
  }  // "..."
  else if (type == STARPART_STRAT) {
    new_val = StarPart_newStratFromStr(str_val);
  }  // "{...}"
  else if (type == STARPART_INT) {
    new_val = StarPart_newInt(atoi(str_val));
  }
  else if (type == STARPART_FLOAT) {
    new_val = StarPart_newFloat((float)atof(str_val));
  }
  else if (type == STARPART_DOUBLE) {
    new_val = StarPart_newDouble(atof(str_val));
  }
  else if (type == STARPART_INTARRAY) {
    new_val = StarPart_newIntArrayFromStr(str_val);
  }  // "(...)"
  else if (type == STARPART_FLOATARRAY) {
    new_val = StarPart_newFloatArrayFromStr(str_val);
  }
  else if (type == STARPART_DOUBLEARRAY) {
    new_val = StarPart_newDoubleArrayFromStr(str_val);
  }
  else if (type == STARPART_STRARRAY) {
    new_val = StarPart_newStrArrayFromStr(str_val);
  }
  else
    StarPart_error("item \"%s\" has invalid type!\n", item_id);
  StarPart_Item* item = StarPart_newItem(item_id, type, new_val, true, flags);
  return item;
}

/* *********************************************************** */

static void _free_item(gpointer data)
{
  assert(data);
  StarPart_Item* item = (StarPart_Item*)data;
  StarPart_freeItem(item);
}

/* *********************************************************** */

void StarPart_freeItem(StarPart_Item* item)
{
  assert(item);
  if (item->id) free(item->id);
  if (item->val) freeVal(item);
  if (item->versions) g_queue_free_full(item->versions, _free_item);
  free(item);
}

/* *********************************************************** */

StarPart_Item* StarPart_dupItem(StarPart_Item* item, bool deep)
{
  assert(item);
  void* newval = item->val;                         /* shallow copy */
  if (deep) newval = dupVal(item->type, item->val); /* deep copy */
  StarPart_Item* newitem = StarPart_newItem(item->id, item->type, newval, deep, item->flags);
  // item->versions are not duplicated at all...
  return newitem;
}

/* *********************************************************** */

inline char* StarPart_getItemID(StarPart_Item* item)
{
  assert(item);
  return item->id;
}

/* *********************************************************** */

inline StarPart_Type StarPart_getItemType(StarPart_Item* item)
{
  assert(item);
  return item->type;
}

/* *********************************************************** */

inline void* StarPart_getItemAddr(StarPart_Item* item)
{
  assert(item);
  return item->val;
}

/* *********************************************************** */

inline int StarPart_getItemFlags(StarPart_Item* item)
{
  assert(item);
  return item->flags;
}

/* *********************************************************** */

inline void StarPart_setItemFlags(StarPart_Item* item, int flags)
{
  assert(item);
  item->flags = flags;
}

/* *********************************************************** */

inline void StarPart_setItemType(StarPart_Item* item, StarPart_Type type)
{
  assert(item);
  item->type = type;
}

/* *********************************************************** */

inline void StarPart_setItemID(StarPart_Item* item, char* item_id)
{
  assert(item);
  if (item->id) free(item->id);
  item->id = strdup(item_id);
}

/* *********************************************************** */

StarPart_Item* StarPart_getItemVersion(StarPart_Item* item, int version)
{
  assert(item && item->versions);
  assert(version >= 0);
  return g_queue_peek_nth(item->versions, version);  // return NULL if version is off end of queue
  return NULL;
}

/* *********************************************************** */

unsigned int StarPart_storeItemVersion(StarPart_Item* item)
{
  assert(item && item->versions);
  StarPart_Item* newitem = StarPart_dupItem(item, true);  // deep copy
  g_queue_push_tail(item->versions, newitem);
  int rank = g_queue_index(item->versions, newitem);
  assert(g_queue_get_length(item->versions) == rank + 1);
  return rank;
}

/* *********************************************************** */

unsigned int StarPart_getItemNbVersions(StarPart_Item* item)
{
  assert(item);
  assert(item && item->versions);
  return g_queue_get_length(item->versions);
}

/* *********************************************************** */
/*                        GETTER                               */
/* *********************************************************** */

char* StarPart_getStrat(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found!\n", item_id);
  if (item->type != STARPART_STRAT) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_READ))
    StarPart_error("item \"%s\" has invalid read mode!\n", item_id);
  // if(!item->val) StarPart_error("item \"%s\" is NULL!\n", item_id);
  return (char*)item->val;
}

/* *********************************************************** */

char* StarPart_getStr(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found!\n", item_id);
  if (item->type != STARPART_STR) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_READ))
    StarPart_error("item \"%s\" has invalid read mode!\n", item_id);
  // if(!item->val) StarPart_error("item \"%s\" is NULL!\n", item_id);
  return (char*)item->val;
}

/* *********************************************************** */

int StarPart_getInt(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found!\n", item_id);
  if (item->type != STARPART_INT) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_READ))
    StarPart_error("item \"%s\" has invalid read mode!\n", item_id);
  if (!item->val) StarPart_error("item \"%s\" is NULL!\n", item_id);
  return *((int*)item->val);
}

/* *********************************************************** */

float StarPart_getFloat(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found!\n", item_id);
  if (item->type != STARPART_FLOAT) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_READ))
    StarPart_error("item \"%s\" has invalid read mode!\n", item_id);
  if (!item->val) StarPart_error("item \"%s\" is NULL!\n", item_id);
  return *((float*)item->val);
}

/* *********************************************************** */

double StarPart_getDouble(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found!\n", item_id);
  if (item->type != STARPART_DOUBLE) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_READ))
    StarPart_error("item \"%s\" has invalid read mode!\n", item_id);
  if (!item->val) StarPart_error("item \"%s\" is NULL!\n", item_id);
  return *((double*)item->val);
}

/* *********************************************************** */

// int* StarPart_getIntAddr(StarPart_Data* data, char* item_id)
// {
//   assert(data && item_id);
//   StarPart_Item* item = StarPart_getItem(data, item_id);
//   if (!item) StarPart_error("item \"%s\" not found!\n", item_id);
//   if (item->type != STARPART_INT) StarPart_error("item \"%s\" has invalid type!\n", item_id);
//   if (!(item->flags == 0 || item->flags & STARPART_READ)) StarPart_error("item \"%s\" has invalid read mode!\n",
//   item_id);
//   // if(!item->val) StarPart_error("item \"%s\" is NULL!\n", item_id);
//   return (int*)item->val;
// }

/* *********************************************************** */

// double* StarPart_getDoubleAddr(StarPart_Data* data, char* item_id)
// {
//   assert(data && item_id);
//   StarPart_Item* item = StarPart_getItem(data, item_id);
//   if (!item) StarPart_error("item \"%s\" not found!\n", item_id);
//   if (item->type != STARPART_DOUBLE) StarPart_error("item \"%s\" has invalid type!\n", item_id);
//   if (!(item->flags == 0 || item->flags & STARPART_READ)) StarPart_error("item \"%s\" has invalid read mode!\n",
//   item_id);
//   // if(!item->val) StarPart_error("item \"%s\" is NULL!\n", item_id);
//   return (double*)item->val;
// }

/* *********************************************************** */

LibGraph_Array* StarPart_getArray(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found!\n", item_id);
  if ((item->type != STARPART_INTARRAY) && (item->type != STARPART_FLOATARRAY) &&
      (item->type != STARPART_DOUBLEARRAY) && (item->type != STARPART_STRARRAY))
    StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_READ))
    StarPart_error("item \"%s\" has invalid read mode!\n", item_id);
  // if(!item->val) StarPart_error("item \"%s\" is NULL!\n", item_id);
  if (!item->val) return NULL;
  LibGraph_Array* array = (LibGraph_Array*)item->val;
  if ((item->type == STARPART_INTARRAY && array->typecode != LIBGRAPH_INT) ||
      (item->type == STARPART_FLOATARRAY && array->typecode != LIBGRAPH_FLOAT) ||
      (item->type == STARPART_DOUBLEARRAY && array->typecode != LIBGRAPH_DOUBLE) ||
      (item->type == STARPART_STRARRAY && array->typecode != LIBGRAPH_STR))
    StarPart_error("item \"%s\" has incompatible type and array typecode!\n", item_id);
  return array;
}

/* *********************************************************** */

int StarPart_getArrayLength(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  LibGraph_Array* array = StarPart_getArray(data, item_id);
  if (!array) return 0;
  return array->nb_tuples * array->nb_components;
}

/* *********************************************************** */

int* StarPart_getIntArray(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  LibGraph_Array* array = StarPart_getArray(data, item_id);
  if (!array) return NULL;
  if (array->typecode != LIBGRAPH_INT) StarPart_error("item \"%s\" has invalid array typecode!\n", item_id);
  return array->data;
}

/* *********************************************************** */

float* StarPart_getFloatArray(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  LibGraph_Array* array = StarPart_getArray(data, item_id);
  if (!array) return NULL;
  if (array->typecode != LIBGRAPH_FLOAT) StarPart_error("item \"%s\" has invalid array typecode!\n", item_id);
  return array->data;
}

/* *********************************************************** */

double* StarPart_getDoubleArray(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  LibGraph_Array* array = StarPart_getArray(data, item_id);
  if (!array) return NULL;
  if (array->typecode != LIBGRAPH_DOUBLE) StarPart_error("item \"%s\" has invalid array typecode!\n", item_id);
  return array->data;
}

/* *********************************************************** */

char** StarPart_getStrArray(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  LibGraph_Array* array = StarPart_getArray(data, item_id);
  if (!array) return NULL;
  if (array->typecode != LIBGRAPH_STR) StarPart_error("item \"%s\" has invalid array typecode!\n", item_id);
  return array->data;
}

/* *********************************************************** */

LibGraph_Graph* StarPart_getGraph(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found!\n", item_id);
  if (item->type != STARPART_GRAPH) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_READ))
    StarPart_error("item \"%s\" has invalid read mode!\n", item_id);
  // if(!item->val) StarPart_error("item \"%s\" is NULL!\n", item_id);
  if (!item->val) return NULL;
  return (LibGraph_Graph*)item->val;
}

/* *********************************************************** */

LibGraph_DistGraph* StarPart_getDistGraph(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found!\n", item_id);
  if (item->type != STARPART_DISTGRAPH) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_READ))
    StarPart_error("item \"%s\" has invalid read mode!\n", item_id);
  // if(!item->val) StarPart_error("item \"%s\" is NULL!\n", item_id);
  if (!item->val) return NULL;
  return (LibGraph_DistGraph*)item->val;
}

/* *********************************************************** */

LibGraph_Hypergraph* StarPart_getHypergraph(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found!\n", item_id);
  if (item->type != STARPART_HYPERGRAPH) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_READ))
    StarPart_error("item \"%s\" has invalid read mode!\n", item_id);
  // if(!item->val) StarPart_error("item \"%s\" is NULL!\n", item_id);
  if (!item->val) return NULL;
  return (LibGraph_Hypergraph*)item->val;
}

/* *********************************************************** */

LibGraph_Mesh* StarPart_getMesh(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found!\n", item_id);
  if (item->type != STARPART_MESH) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_READ))
    StarPart_error("item \"%s\" has invalid read mode!\n", item_id);
  // if(!item->val) StarPart_error("item \"%s\" is NULL!\n", item_id);
  if (!item->val) return NULL;
  return item->val;
}

/* *********************************************************** */

LibGraph_HybridMesh* StarPart_getHybridMesh(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found!\n", item_id);
  if (item->type != STARPART_HYBRIDMESH) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_READ))
    StarPart_error("item \"%s\" has invalid read mode!\n", item_id);
  // if(!item->val) StarPart_error("item \"%s\" is NULL!\n", item_id);
  if (!item->val) return NULL;
  return item->val;
}

/* *********************************************************** */
/*                         CHECK                               */
/* *********************************************************** */

void StarPart_checkStr(StarPart_Data* data, char* item_id, char* checklist)
{
  assert(data && item_id && checklist);
  char* str = StarPart_getStr(data, item_id);
  bool check = false;
  char** vals = g_strsplit(checklist, ",", -1);  // val0,val1,val2,...
  char** val = vals;

  while (*val) {
    g_strstrip(*val);
    if (strcmp(*val, str) == 0) {
      check = true;
      break;
    }
    val++;
  }
  g_strfreev(vals);
  if (!check) StarPart_error("check item %s/%s = \"%s\" not in list \"%s\"!\n", data->id, item_id, str, checklist);
}

void StarPart_checkStrArray(StarPart_Data* data, char* item_id, char* checklist)
{
  assert(data && item_id && checklist);

  char** str_array = StarPart_getStrArray(data, item_id);
  int nb_strs = StarPart_getArrayLength(data, item_id);

  char** vals = g_strsplit(checklist, ",", -1);  // val0,val1,val2,...

  for (int s = 0; s < nb_strs; s++) {
    char* str = str_array[s];
    bool check = false;
    char** val = vals;
    while (*val) {
      g_strstrip(*val);
      if (strcmp(*val, str) == 0) {
        check = true;
        break;
      }
      val++;
    }
    if (!check) {
      StarPart_error("check item %s/%s = \"%s\" not in list \"%s\"!\n", data->id, item_id, str, checklist);
      break;
    }
  }
  g_strfreev(vals);
}

/* *********************************************************** */

void StarPart_checkArray(StarPart_Data* data, char* item_id, int length)
{
  assert(data && item_id);
  LibGraph_Array* array = StarPart_getArray(data, item_id);
  if (!array && !length) return;
  if (!array) StarPart_error("array \"%s\" is NULL!\n", item_id);
  if (length != array->nb_tuples * array->nb_components) StarPart_error("invalid array length of \"%s\"!\n", item_id);
  if (!array->data) StarPart_error("array address of \"%s\" is NULL!\n", item_id);
}

/* *********************************************************** */
/*                        SETTER                               */
/* *********************************************************** */

void StarPart_setStr(StarPart_Data* data, char* item_id, char* str)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);
  if (item->type != STARPART_STR) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_WRITE))
    StarPart_error("item \"%s\" has invalid write mode!\n", item_id);
  if (item->val) freeVal(item);
  item->val = dupStr(str);
  item->owner = true;
}

/* *********************************************************** */

void StarPart_setInt(StarPart_Data* data, char* item_id, int val)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);
  if (item->type != STARPART_INT) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_WRITE))
    StarPart_error("item \"%s\" has invalid write mode!\n", item_id);
  if (item->val) freeVal(item);
  item->val = StarPart_newInt(val);  // TODO: is it the better option? maybe just update value?
  item->owner = true;
}

/* *********************************************************** */

void StarPart_setFloat(StarPart_Data* data, char* item_id, float val)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);
  if (item->type != STARPART_FLOAT) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_WRITE))
    StarPart_error("item \"%s\" has invalid write mode!\n", item_id);
  if (item->val) freeVal(item);
  item->val = StarPart_newFloat(val);
  item->owner = true;
}

/* *********************************************************** */

void StarPart_setDouble(StarPart_Data* data, char* item_id, double val)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);
  if (item->type != STARPART_DOUBLE) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_WRITE))
    StarPart_error("item \"%s\" has invalid write mode!\n", item_id);
  if (item->val) freeVal(item);
  item->val = StarPart_newDouble(val);
  item->owner = true;
}

/* *********************************************************** */

void StarPart_setIntArray(StarPart_Data* data, char* item_id, int length, int* array, bool owner)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);
  if (item->type != STARPART_INTARRAY) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_WRITE))
    StarPart_error("item \"%s\" has invalid write mode!\n", item_id);
  if (item->val) freeVal(item);
  item->val = StarPart_newIntArray(length, array);
  item->owner = owner;
}

/* *********************************************************** */

void StarPart_setFloatArray(StarPart_Data* data, char* item_id, int length, float* array, bool owner)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);
  if (item->type != STARPART_FLOATARRAY) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_WRITE))
    StarPart_error("item \"%s\" has invalid write mode!\n", item_id);
  if (item->val) freeVal(item);
  item->val = StarPart_newFloatArray(length, array);
  item->owner = owner;
}

/* *********************************************************** */

void StarPart_setDoubleArray(StarPart_Data* data, char* item_id, int length, double* array, bool owner)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);
  if (item->type != STARPART_DOUBLEARRAY) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_WRITE))
    StarPart_error("item \"%s\" has invalid write mode!\n", item_id);
  if (item->val) freeVal(item);
  item->val = StarPart_newDoubleArray(length, array);
  item->owner = owner;
}

/* *********************************************************** */

void StarPart_setStrArray(StarPart_Data* data, char* item_id, int length, char** array, bool owner)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);
  if (item->type != STARPART_STRARRAY) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_WRITE))
    StarPart_error("item \"%s\" has invalid write mode!\n", item_id);
  if (item->val) freeVal(item);
  item->val = StarPart_newStrArray(length, array);
  item->owner = owner;
}

/* *********************************************************** */

void StarPart_setGraph(StarPart_Data* data, char* item_id, LibGraph_Graph* g, bool owner)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);
  if (item->type != STARPART_GRAPH) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_WRITE))
    StarPart_error("item \"%s\" has invalid write mode!\n", item_id);
  if (item->val) freeVal(item);
  item->val = g;
  item->owner = owner;
}

/* *********************************************************** */

void StarPart_setDistGraph(StarPart_Data* data, char* item_id, LibGraph_DistGraph* dg, bool owner)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);
  if (item->type != STARPART_DISTGRAPH) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_WRITE))
    StarPart_error("item \"%s\" has invalid write mode!\n", item_id);
  if (item->val) freeVal(item);
  item->val = dg;
  item->owner = owner;
}

/* *********************************************************** */

void StarPart_setHypergraph(StarPart_Data* data, char* item_id, LibGraph_Hypergraph* hg, bool owner)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);
  if (item->type != STARPART_HYPERGRAPH) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_WRITE))
    StarPart_error("item \"%s\" has invalid write mode!\n", item_id);
  if (item->val) freeVal(item);
  item->val = hg;
  item->owner = owner;
}

/* *********************************************************** */

void StarPart_setMesh(StarPart_Data* data, char* item_id, LibGraph_Mesh* m, bool owner)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);
  if (item->type != STARPART_MESH) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_WRITE))
    StarPart_error("item \"%s\" has invalid write mode!\n", item_id);
  if (item->val) freeVal(item);
  item->val = m;
  item->owner = owner;
}

/* *********************************************************** */

void StarPart_setHybridMesh(StarPart_Data* data, char* item_id, LibGraph_HybridMesh* m, bool owner)
{
  assert(data && item_id);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);
  if (item->type != STARPART_HYBRIDMESH) StarPart_error("item \"%s\" has invalid type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_WRITE))
    StarPart_error("item \"%s\" has invalid write mode!\n", item_id);
  if (item->val) freeVal(item);
  item->val = m;
  item->owner = owner;
}

/* *********************************************************** */

void StarPart_setArray(StarPart_Data* data, char* item_id, LibGraph_Array* array, bool owner)
{
  assert(data && item_id);
  assert(array);

  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);
  if ((item->type != STARPART_INTARRAY) && (item->type != STARPART_FLOATARRAY) &&
      (item->type != STARPART_DOUBLEARRAY) && (item->type != STARPART_STRARRAY))
    StarPart_error("item \"%s\" has not an array type!\n", item_id);
  if (!(item->flags == 0 || item->flags & STARPART_WRITE))
    StarPart_error("item \"%s\" has invalid write mode!\n", item_id);
  if (item->val) freeVal(item);
  item->val = array;
  item->owner = owner;
}

/* *********************************************************** */

void* StarPart_allocArray(StarPart_Data* data, char* item_id, int length)
{
  assert(data && item_id);
  assert(length > 0);  // TODO: or >= 0
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found in data!\n", item_id);

  LibGraph_Array* array = LibGraph_newArray();
  assert(array);

  switch (item->type) {
    case STARPART_INTARRAY: LibGraph_allocArray(array, length, 1, LIBGRAPH_INT); break;
    case STARPART_FLOATARRAY: LibGraph_allocArray(array, length, 1, LIBGRAPH_FLOAT); break;
    case STARPART_DOUBLEARRAY: LibGraph_allocArray(array, length, 1, LIBGRAPH_DOUBLE); break;
    case STARPART_STRARRAY: LibGraph_allocArray(array, length, 1, LIBGRAPH_STR); break;
    default: StarPart_error("item \"%s\" has not an array type!\n", item_id); break;
  }

  StarPart_setArray(data, item_id, array, true);  // StarPart takes memory ownership

  return array->data;
}

/* *********************************************************** */

void StarPart_initArray(StarPart_Data* data, char* item_id, int val)
{
  assert(data && item_id);
  LibGraph_Array* array = StarPart_getArray(data, item_id);
  int length = StarPart_getArrayLength(data, item_id);
  assert(array);

  /* set default value */
  switch (array->typecode) {
    case LIBGRAPH_INT:
      for (int i = 0; i < length; i++) ((int*)array->data)[i] = val;
      break;
    case LIBGRAPH_FLOAT:
      for (int i = 0; i < length; i++) ((float*)array->data)[i] = (float)val;
      break;
    case LIBGRAPH_DOUBLE:
      for (int i = 0; i < length; i++) ((double*)array->data)[i] = (double)val;
      break;
    case LIBGRAPH_STR: /* unused */ break;
    default: break;
  }
}

/* *********************************************************** */
/*                         DATA                                */
/* *********************************************************** */

char* StarPart_getDataID(StarPart_Data* data)
{
  assert(data);
  return data->id;
}

/* *********************************************************** */

int StarPart_getDataFlags(StarPart_Data* data)
{
  assert(data);
  return data->flags;
}

/* *********************************************************** */

GHashTable* StarPart_getDataItems(StarPart_Data* data)
{
  assert(data);
  return data->table;
}

/* *********************************************************** */

static void free_data_item(gpointer ptr)
{
  StarPart_Item* item = (StarPart_Item*)ptr;
  StarPart_freeItem(item);
}

/* *********************************************************** */

StarPart_Data* StarPart_newData(char* data_id, int flags)
{
  assert(data_id);
  StarPart_Data* data = malloc(sizeof(StarPart_Data));
  assert(data);
  data->id = dupStr(data_id);  // strdup(id);
  data->table = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, free_data_item);
  data->flags = flags;
  data->ref = 0;
  return data;
}

/* *********************************************************** */

void StarPart_dupItems(StarPart_Data* src, StarPart_Data* dst, bool deep)
{
  assert(src && dst);
  GHashTableIter iter;
  gpointer item_id, val;
  g_hash_table_iter_init(&iter, src->table);
  while (g_hash_table_iter_next(&iter, &item_id, &val)) {
    StarPart_Item* src_item = (StarPart_Item*)val;
    assert(src_item);
    StarPart_Item* dst_item = StarPart_dupItem(src_item, deep);
    StarPart_addItem(dst, dst_item);
  }
}

/* *********************************************************** */

StarPart_Data* StarPart_dupData(StarPart_Data* data, bool deep)
{
  assert(data);
  StarPart_Data* _data = StarPart_newData(data->id, data->flags);
  StarPart_dupItems(data, _data, deep);
  return _data;
}

/* *********************************************************** */

void StarPart_freeData(StarPart_Data* data)
{
  assert(data);
  // printf("[STARPART] free data \"%s\"\n", data->id);
  assert(data->ref >= 0);
  if (data->ref <= 0) {
    if (data->id) free(data->id);
    if (data->table) g_hash_table_destroy(data->table);  // call StarPart_freeItem() for each item
    free(data);
  }
}

/* *********************************************************** */

StarPart_Item* StarPart_addNewItem(StarPart_Data* data, char* item_id, StarPart_Type type, void* val, bool owner,
                                   int flags)
{
  assert(data && item_id);
  assert(flags == 0 || flags & STARPART_READ || flags & STARPART_WRITE);
  StarPart_Item* item = StarPart_newItem(item_id, type, val, owner, flags);
  g_hash_table_replace(data->table, item->id, (gpointer)item);
  return item;
}

/* *********************************************************** */

void StarPart_addItem(StarPart_Data* data, StarPart_Item* item)
{
  assert(data && item && item->id);
  g_hash_table_replace(data->table, item->id, (gpointer)item);
}

/* *********************************************************** */

StarPart_Item* StarPart_getItem(StarPart_Data* data, char* item_id)
{
  assert(item_id);
  if (!data) return NULL;
  StarPart_Item* item = (StarPart_Item*)g_hash_table_lookup(data->table, item_id);
  if (!item) return NULL;
  return item;
}

/* *********************************************************** */

bool StarPart_isItem(StarPart_Data* data, char* item_id)
{
  if (!data) return false;
  return g_hash_table_contains(data->table, item_id);
}

/* *********************************************************** */

bool StarPart_hasItemValue(StarPart_Data* data, char* item_id)
{
  assert(data);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  if (!item) StarPart_error("item \"%s\" not found!\n", item_id);
  return (item->val != NULL);
}

/* *********************************************************** */

void StarPart_removeItem(StarPart_Data* data, char* item_id)
{
  assert(data && item_id);
  gboolean r = g_hash_table_remove(data->table, item_id);  // free automatically memory
  assert(r);
}

/* *********************************************************** */

void StarPart_printItems(StarPart_Data* data, char* prefix)
{
  GHashTableIter iter;
  gpointer key, val;
  g_hash_table_iter_init(&iter, data->table);
  while (g_hash_table_iter_next(&iter, &key, &val)) {
    StarPart_Item* item = (StarPart_Item*)val;
    assert(item);
    StarPart_printItem(item, false, prefix);
  }
}

/* *********************************************************** */

void StarPart_printData(StarPart_Data* data)
{
  assert(data);

  StarPart_print("+ [%s] %s\n", data_flags_str[data->flags], data->id);  // TODO: bug here on flags

  GHashTableIter iter;
  gpointer key, val;
  g_hash_table_iter_init(&iter, data->table);
  while (g_hash_table_iter_next(&iter, &key, &val)) {
    StarPart_Item* item = (StarPart_Item*)val;
    assert(item);
    StarPart_printItem(item, false, "    +");
  }
}

/* *********************************************************** */

void StarPart_listAllData(StarPart_Context* ctx)
{
  assert(ctx && ctx->datatable);
  GHashTableIter data_iter;
  gpointer data_key, data_val;
  g_hash_table_iter_init(&data_iter, ctx->datatable);
  StarPart_print("Data:\n");
  while (g_hash_table_iter_next(&data_iter, &data_key, &data_val)) {
    StarPart_print("  + %s (%p)\n", (char*)data_key, (void*)data_val);
  }
}

/* *********************************************************** */

void StarPart_printAllData(StarPart_Context* ctx)
{
  assert(ctx && ctx->datatable);
  GHashTableIter data_iter;
  gpointer data_key, data_val;
  g_hash_table_iter_init(&data_iter, ctx->datatable);
  while (g_hash_table_iter_next(&data_iter, &data_key, &data_val)) {
    StarPart_Data* data = (StarPart_Data*)data_val;
    StarPart_printData(data);
  }
}

/* *********************************************************** */

void StarPart_printItem(StarPart_Item* item, bool dump, char* prefix)
{
  assert(item);
  if (!prefix) prefix = "*";
  char* flags = item_flags_str[item->flags];
  char* type = type_str[item->type];

  if (item->type == STARPART_INT && item->val)
    StarPart_print("%s [%2s] %s [%s] = %d\n", prefix, flags, item->id, type, *(int*)item->val);
  else if (item->type == STARPART_FLOAT && item->val)
    StarPart_print("%s [%2s] %s [%s] = %f\n", prefix, flags, item->id, type, *(float*)item->val);
  else if (item->type == STARPART_DOUBLE && item->val)
    StarPart_print("%s [%2s] %s [%s] = %lf\n", prefix, flags, item->id, type, *(double*)item->val);
  else if (item->type == STARPART_VOID)
    StarPart_print("%s [%2s] %s [%s]\n", prefix, flags, item->id, type);
  else if (item->type == STARPART_STRAT && item->val)
    StarPart_print("%s [%2s] %s [%s] =  {%s}\n", prefix, flags, item->id, type, (char*)item->val);
  else if (item->type == STARPART_STR && item->val)
    StarPart_print("%s [%2s] %s [%s] = \"%s\"\n", prefix, flags, item->id, type, (char*)item->val);
  else if ((item->type == STARPART_INTARRAY || item->type == STARPART_FLOATARRAY ||
            item->type == STARPART_DOUBLEARRAY || item->type == STARPART_STRARRAY) &&
           item->val) {
    LibGraph_Array* array = (LibGraph_Array*)item->val;
    StarPart_print("%s [%s] %s [%s] = {%d elements} (@%p, owner=%d)\n", prefix, flags, item->id, type,
                   array->nb_tuples * array->nb_components, array->data, item->owner);
    if (dump) LibGraph_printArray(array, dump);
  }
  else if (item->type == STARPART_GRAPH && item->val) {
    LibGraph_Graph* g = (LibGraph_Graph*)item->val;
    StarPart_print("%s [%2s] %s [%s] = {%d vertices, %d arcs} (@%p, owner=%d)\n", prefix, flags, item->id, type, g->nvtxs,
                   g->narcs, g, item->owner);
    if (dump) LibGraph_printGraph(g);
  }
  else if (item->type == STARPART_DISTGRAPH && item->val) {
    LibGraph_DistGraph* dg = (LibGraph_DistGraph*)item->val;
    StarPart_print("%s [%2s] %s [%s] = {%d vertices, %d arcs} (@%p, owner=%d)\n", prefix, flags, item->id, type, dg->nvtxs,
                   dg->narcs, dg, item->owner);
    if (dump) LibGraph_printDistGraph(dg);
  }
  else if (item->type == STARPART_HYPERGRAPH && item->val) {
    LibGraph_Hypergraph* hg = (LibGraph_Hypergraph*)item->val;
    StarPart_print("%s [%2s] %s [%s] = {%d vertices, %d hyperedges} (@%p, owner=%d)\n", prefix, flags, item->id, type,
                   hg->nvtxs, hg->nhedges, hg, item->owner);
    if (dump) LibGraph_printHypergraph(hg);
  }
  else if (item->type == STARPART_MESH && item->val) {
    LibGraph_Mesh* m = (LibGraph_Mesh*)item->val;
    StarPart_print("%s [%2s] %s [%s] = {%d cells, %d nodes} (@%p, owner=%d)\n", prefix, flags, item->id, type, m->nb_cells,
                   m->nb_nodes, m, item->owner);
    if (dump) LibGraph_printMesh(m);
  }
  else if (item->type == STARPART_HYBRIDMESH && item->val) {
    LibGraph_HybridMesh* hm = (LibGraph_HybridMesh*)item->val;
    int nb_cells = LibGraph_getHybridMeshNbCells(hm);
    int nb_nodes = LibGraph_getHybridMeshNbNodes(hm);
    StarPart_print("%s [%2s] %s [%s] = {%d cells, %d nodes} (@%p, owner=%d)\n", prefix, flags, item->id, type, nb_cells,
                   nb_nodes, hm, item->owner);
    if (dump) LibGraph_printHybridMesh(hm);
  }
  else
    StarPart_print("%s [%2s] %s [%s] = null (owner=%d)\n", prefix, flags, item->id, type, item->owner);
}

/* *********************************************************** */

StarPart_Data* StarPart_registerData(StarPart_Context* ctx, StarPart_Data* data)
{
  assert(ctx && data);
  if (data->id[0] != '@') StarPart_error("invalid data ID that must start by '@'");
  StarPart_Data* _data = StarPart_addDataInTable(ctx->datatable, data);
  return _data;
}

/* *********************************************************** */

StarPart_Data* StarPart_registerNewData(StarPart_Context* ctx, char* data_id, int flags)
{
  assert(ctx);
  assert(data_id && data_id[0] == '@');
  StarPart_Data* _data = StarPart_addNewDataInTable(ctx->datatable, data_id, flags);
  return _data;
}

/* *********************************************************** */

StarPart_Data* StarPart_unregisterData(StarPart_Context* ctx, char* data_id)
{
  assert(ctx);
  StarPart_Data* _data = StarPart_removeDataFromTable(ctx->datatable, data_id);
  return _data;
}

/* *********************************************************** */

StarPart_Data* StarPart_getData(StarPart_Context* ctx, char* data_id)
{
  assert(ctx && ctx->datatable);
  if (!data_id) return NULL;
  StarPart_Data* data = (StarPart_Data*)g_hash_table_lookup(ctx->datatable, data_id);
  if (!data) return NULL;  // not found
  return data;
}

/* *********************************************************** */
/*                       DATA TABLE                            */
/* *********************************************************** */

static void _free_data(gpointer ptr)
{
  StarPart_Data* data = (StarPart_Data*)ptr;
  if (data) StarPart_freeData(data);
}

/* *********************************************************** */

StarPart_DataTable* StarPart_newDataTable(void)
{
  GHashTable* table = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, _free_data);
  return table;
}

/* *********************************************************** */

static void _unref_data(gpointer key, gpointer value, gpointer user_data)
{
  StarPart_Data* data = (StarPart_Data*)value;
  if (data) data->ref--;
}

/* *********************************************************** */

void StarPart_freeDataTable(StarPart_DataTable* table)
{
  assert(table);
  g_hash_table_foreach(table, _unref_data, NULL);  // unref all data
  g_hash_table_destroy(table);
}

/* *********************************************************** */

bool StarPart_isDataFromethodtable(StarPart_DataTable* table, char* data_id)
{
  if (!table) return false;
  return g_hash_table_contains(table, data_id);
}

/* *********************************************************** */

StarPart_Data* StarPart_getDataFromTable(StarPart_DataTable* table, char* data_id)
{
  if (!table) return NULL;
  if (!data_id) return NULL;
  StarPart_Data* data = (StarPart_Data*)g_hash_table_lookup(table, data_id);
  if (!data) return NULL;  // not found
  return data;
}

/* *********************************************************** */

StarPart_Data* StarPart_addDataInTable(StarPart_DataTable* table, StarPart_Data* data)
{
  assert(table);
  gboolean r = g_hash_table_insert(table, (gpointer)data->id, (gpointer)data);
  if (!r) StarPart_error("fail to add data \"%s\" in data table!\n", data->id);
  data->ref++;
  return data;
}

/* *********************************************************** */

StarPart_Data* StarPart_addNewDataInTable(StarPart_DataTable* table, char* data_id, int flags)
{
  assert(table && data_id);
  StarPart_Data* data = StarPart_newData(data_id, flags);
  gboolean r = g_hash_table_insert(table, (gpointer)data->id, (gpointer)data);
  if (!r) StarPart_error("fail to add new data \"%s\" in data table!\n", data_id);
  data->ref++;
  return data;
}

/* *********************************************************** */

StarPart_Data* StarPart_removeDataFromTable(StarPart_DataTable* table, char* data_id)
{
  assert(table && data_id);
  gpointer key = NULL, data = NULL;
  g_hash_table_lookup_extended(table, data_id, &key, &data);
  g_hash_table_steal(table, data_id);  // don't free key and value
  StarPart_Data* _data = data;
  if (_data) _data->ref--;
  return _data;
}

/* *********************************************************** */
