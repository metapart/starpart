#define _GNU_SOURCE
#include <assert.h>
#include <glib.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef USE_MPI
#include <mpi.h>
#endif

#include "starpart.h"
#include "struct.h"

/* *********************************************************** */

#define RED "\e[31m"
#define BLUE "\e[34m"
#define BLACK "\e[0m"

/* *********************************************************** */

static void StarPart_fprintc(FILE* stream, char* color, const char* format, va_list ap)
{
  fprintf(stream, "%s", color);

  StarPart_Context* ctx = StarPart_getCurrentContext();
  if (ctx->parallel)
    fprintf(stream, "[STARPART#%d] ", ctx->prank);
  else
    fprintf(stream, "[STARPART] ");

  // char * mid = NULL;
  // if(ctx) mid = StarPart_getCurrentMethodID(ctx);
  // int lvl = g_queue_get_length(_ctx_stack);
  // if(mid) fprintf(stream, "[STARPART#%d@%s] ", lvl, mid);
  // else if(ctx) fprintf(stream, "[STARPART#%d] ", lvl);

  vfprintf(stream, format, ap);
  fprintf(stream, "%s", BLACK);
  fflush(stream);
}

/* *********************************************************** */

void StarPart_print(const char* format, ...)
{
  va_list ap;
  va_start(ap, format);
  StarPart_fprintc(stdout, BLACK, format, ap);
  va_end(ap);
}

/* *********************************************************** */

void StarPart_printRed(const char* format, ...)
{
  va_list ap;
  va_start(ap, format);
  StarPart_fprintc(stdout, RED, format, ap);
  va_end(ap);
}

/* *********************************************************** */

void StarPart_printBlue(const char* format, ...)
{
  va_list ap;
  va_start(ap, format);
  StarPart_fprintc(stdout, BLUE, format, ap);
  va_end(ap);
}

/* *********************************************************** */

void StarPart_warning(const char* format, ...)
{
  va_list ap;
  va_start(ap, format);
  StarPart_fprintc(stdout, BLUE, format, ap);
  va_end(ap);
}

/* *********************************************************** */

void StarPart_error(const char* format, ...)
{
  va_list ap;
  va_start(ap, format);
  StarPart_fprintc(stdout, RED, format, ap);
  va_end(ap);
  abort();
}

/* *********************************************************** */

void StarPart_setDebug(StarPart_Context* ctx, bool debug)
{
  assert(ctx);
  ctx->debug = debug;
}

/* *********************************************************** */

bool StarPart_isDebug(StarPart_Context* ctx)
{
  assert(ctx);
  return ctx->debug;
}

/* *********************************************************** */
