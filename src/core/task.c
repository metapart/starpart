#define _GNU_SOURCE
#include <assert.h>
#include <ctype.h>
#include <glib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "starpart.h"
#include "struct.h"

/* *********************************************************** */
/*                        TASK                                 */
/* *********************************************************** */

StarPart_Context* StarPart_getTaskContext(StarPart_Task* task)
{
  assert(task);
  return task->ctx;
}

/* *********************************************************** */

char* StarPart_getTaskID(StarPart_Task* task)
{
  assert(task);
  return task->id;
}

/* *********************************************************** */

// StarPart_DataTable* StarPart_getTaskArgs(StarPart_Task* task)
// {
//   assert(task);
//   return task->userargs;
// }

/* *********************************************************** */

StarPart_Method* StarPart_getTaskMethod(StarPart_Task* task)
{
  assert(task);
  return task->method;
}

/* *********************************************************** */

StarPart_Task* StarPart_newTask(StarPart_Context* ctx, StarPart_Method* method, char* args)
{
  assert(ctx && method);
  StarPart_Task* task = (StarPart_Task*)malloc(sizeof(StarPart_Task));
  assert(task);
  task->ctx = ctx;
  task->id = strdup(method->id);
  task->method = method;
  task->parallel = ctx->parallel;
  assert(task->method);
  task->conn = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
  task->userargs = StarPart_newDataTable();
  if (args) StarPart_insertTaskArgs(task, args);
  g_queue_push_tail(ctx->tasks, task);
  return task;
}

/* *********************************************************** */

StarPart_Task* StarPart_newTaskByName(StarPart_Context* ctx, char* id, char* args)
{
  assert(ctx && id);
  StarPart_Method* m = StarPart_getMethod(ctx, id);
  return StarPart_newTask(ctx, m, args);
}

/* *********************************************************** */

void StarPart_freeTask(StarPart_Task* task)
{
  assert(task);
  StarPart_Context* ctx = task->ctx;
  g_queue_remove(ctx->queue, task);
  g_queue_remove(ctx->tasks, task);
  g_hash_table_destroy(task->conn);
  StarPart_freeDataTable(task->userargs);
  free(task->id);
  free(task);
}

/* *********************************************************** */

void StarPart_setParallelTask(StarPart_Task* task, bool parallel)
{
  assert(task);
  task->parallel = parallel;
}

/* *********************************************************** */

bool StarPart_isParallelTask(StarPart_Task* task)
{
  assert(task);
  return task->parallel;
}

/* *********************************************************** */

char* StarPart_getTaskConn(StarPart_Task* task, char* arg_id)
{
  assert(task && arg_id);
  arg_id = StarPart_getAlias(task->method, arg_id);
  if (!arg_id) return NULL;
  // int flags = StarPart_getMethodArgFlags(task->method, arg_id);
  StarPart_Data* arg = StarPart_getMethodArg(task->method, arg_id);
  if (task->parallel && (arg->flags & STARPART_MASTER) && (task->ctx->prank != 0)) return NULL;  // or error ?
  char* data_id = (char*)g_hash_table_lookup(task->conn, arg_id);
  return data_id;
}

/* *********************************************************** */

StarPart_Data* StarPart_getTaskData(StarPart_Task* task, char* arg_id)
{
  assert(task && task->ctx);
  // int flags = StarPart_getMethodArgFlags(task->method, arg_id);
  StarPart_Data* arg = StarPart_getMethodArg(task->method, arg_id);
  if (task->parallel && (arg->flags & STARPART_MASTER) && (task->ctx->prank != 0)) return NULL;  // or error ?
  char* data_id = StarPart_getTaskConn(task, arg_id);
  return StarPart_getData(task->ctx, data_id);
}

/* *********************************************************** */

int StarPart_getTaskNbInputs(StarPart_Task* task)
{
  assert(task);
  StarPart_Method* method = StarPart_getTaskMethod(task);
  return StarPart_getMethodNbInputs(method);
}

/* *********************************************************** */

int StarPart_getTaskNbOutputs(StarPart_Task* task)
{
  assert(task);
  StarPart_Method* method = StarPart_getTaskMethod(task);
  return StarPart_getMethodNbOutputs(method);
}

/* *********************************************************** */

// int StarPart_getTaskNbLocals(StarPart_Task* task)
// {
//   assert(task);
//   StarPart_Method* method = StarPart_getTaskMethod(task);
//   return StarPart_getMethodNbLocals(method);
// }

/* *********************************************************** */

StarPart_Data* StarPart_getTaskInput(StarPart_Task* task, int rank)
{
  assert(task && rank >= 0 && rank < STARPART_MAX_INPUTS);
  StarPart_Data* arg = StarPart_getMethodInput(task->method, rank);
  if (!arg) return NULL;
  return StarPart_getTaskData(task, arg->id);
}

/* *********************************************************** */

StarPart_Data* StarPart_getTaskOutput(StarPart_Task* task, int rank)
{
  assert(task && rank >= 0 && rank < STARPART_MAX_OUTPUTS);
  StarPart_Data* arg = StarPart_getMethodOutput(task->method, rank);
  if (!arg) return NULL;
  return StarPart_getTaskData(task, arg->id);
}

/* *********************************************************** */

StarPart_Data* StarPart_getTaskLocal(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* arg = StarPart_getMethodLocal(task->method);
  if (!arg) return NULL;
  return StarPart_getTaskData(task, arg->id);
}

/* *********************************************************** */

void StarPart_connectTask(StarPart_Task* task, char* arg_id, char* data_id)
{
  assert(task && arg_id && data_id);
  arg_id = StarPart_getAlias(task->method, arg_id);
  // StarPart_print("connect task \"%s\": %s -> %s\n", task->id, arg_id, data_id);
  assert(arg_id);
  // int flags = StarPart_getMethodArgFlags(task->method, arg_id);
  StarPart_Data* arg = StarPart_getMethodArg(task->method, arg_id);
  if (task->parallel && (arg->flags & STARPART_MASTER) && (task->ctx->prank != 0)) return;  // skip connection
  // StarPart_error("fail to connect task %s: arg \"%s\" -> data \"%s\" (not master proc.))\n", task->id, arg_id,
  // data_id);
  gboolean r = g_hash_table_insert(task->conn, (gpointer)strdup(arg_id), (gpointer)strdup(data_id));
  if (!r) StarPart_error("fail to connect task %s: arg \"%s\" -> data \"%s\"\n", task->id, arg_id, data_id);
  // already connected?
}

/* *********************************************************** */

void StarPart_disconnectTask(StarPart_Task* task)
{
  g_hash_table_remove_all(task->conn);
}

/* *********************************************************** */

void StarPart_printTask(StarPart_Task* task, int flags)
{
  assert(task && task->ctx);
  // int nblocals = StarPart_getMethodNbLocals(task->method);
  int nbinputs = StarPart_getMethodNbInputs(task->method);
  int nboutputs = StarPart_getMethodNbOutputs(task->method);
  if (flags == 0) StarPart_print("task \"%s\"\n", task->id);
  // StarPart_print("  + locals (%d)\n", nblocals);
  if (flags == 0 || flags & STARPART_LOCAL) {
    // for (int i = 0; i < nblocals; i++) {
    // char* arg_id = StarPart_getMethodLocal(task->method);
    StarPart_Data* arg = StarPart_getMethodLocal(task->method);
    StarPart_Data* data = StarPart_getTaskLocal(task);
    StarPart_print("#local: arg \"%s\" -> \"%s\"\n", arg->id, data ? data->id : NULL);
    if (data) StarPart_printItems(data, "  + ");
    //}
  }
  // StarPart_print("  + inputs (%d)\n", nbinputs);
  if (flags == 0 || flags & STARPART_IN) {
    for (int i = 0; i < nbinputs; i++) {
      // char* arg_id = StarPart_getMethodInput(task->method, i);
      StarPart_Data* arg = StarPart_getMethodInput(task->method, i);
      StarPart_Data* data = StarPart_getTaskInput(task, i);
      StarPart_print("#in%d: arg \"%s\" -> \"%s\"\n", i, arg->id, data ? data->id : NULL);
      if (data) StarPart_printItems(data, "  + ");
    }
  }
  // StarPart_print("  + outputs (%d)\n", nboutputs);
  if (flags == 0 || flags & STARPART_OUT) {
    for (int i = 0; i < nboutputs; i++) {
      // char* arg_id = StarPart_getMethodOutput(task->method, i);
      StarPart_Data* arg = StarPart_getMethodOutput(task->method, i);
      StarPart_Data* data = StarPart_getTaskOutput(task, i);
      StarPart_print("#out%d: arg \"%s\" -> \"%s\"\n", i, arg->id, data ? data->id : NULL);
      if (data) StarPart_printItems(data, "  + ");
    }
  }
}

/* *********************************************************** */

StarPart_Item* StarPart_insertTaskArgByValue(StarPart_Task* task, char* arg_id, char* item_id, void* val, bool owner)
{
  char* _arg_id = StarPart_getAlias(task->method, arg_id);
  StarPart_Data* arg_data = StarPart_getMethodArg(task->method, _arg_id);  // default args
  if (!arg_data) StarPart_error("invalid arg %s/%s for task %s\n", _arg_id, item_id, task->id);
  StarPart_Item* arg_item = StarPart_getItem(arg_data, item_id);
  if (!arg_item) StarPart_error("invalid arg %s/%s for task %s\n", _arg_id, item_id, task->id);
  StarPart_Type arg_type = StarPart_getItemType(arg_item);
  StarPart_Data* user_data = StarPart_getDataFromTable(task->userargs, _arg_id);
  if (!user_data) user_data = StarPart_addNewDataInTable(task->userargs, _arg_id, 0);
  StarPart_Item* user_item = StarPart_addNewItem(user_data, item_id, arg_type, val, owner, 0);
  return user_item;
}

/* *********************************************************** */
