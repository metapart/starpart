#ifndef STARPART_STRUCT_H
#define STARPART_STRUCT_H

#include <glib.h>
#include <stdbool.h>
#include "starpart.h"

/* *********************************************************** */

extern GQueue* _ctx_stack;

/* *********************************************************** */

struct _StarPart_Item
{
  char* id;           /* item ID */
  StarPart_Type type; /* item type */
  void* val;          /* item raw address */
  bool owner;         /* true if StarPart takes memory ownership */
  int flags;          /* item special flags (see \ref StarPart_ItemFlags) */
  GQueue* versions;   /* item versions (queue of StarPart_Item*) */
};

/* *********************************************************** */

struct _StarPart_Data
{
  char* id;          /* data ID */
  GHashTable* table; /* item table (table of StarPart_Item) */
  int flags;         /* data special flags (see \ref StarPart_DataFlags) */
  int ref;           /* reference count */
};

/* *********************************************************** */

struct _StarPart_Context
{
  GSequence* packages;           /* sequence of packages (char*) */
  GHashTable* methodtable;       /* method table (table of StarPart_Method*) */
  GHashTable* hooktable;         /* hook table (table of StarPart_Hook*) */
  StarPart_DataTable* datatable; /* data table (table of StarPart_Data*) */
  GQueue* tasks;                 /* list of tasks (queue of StarPart_Task*) */
  GQueue* queue;                 /* task queue (queue of StarPart_Task*) */
  bool debug;                    /* debug */
  bool parallel;                 /* true if context is parallel */
  int prank;                     /* parallel process rank */
  int psize;                     /* parallel process size */
  MPI_Comm comm;                 /* MPI Communicator */
};

/* *********************************************************** */

#define STARPART_MAX_INPUTS 4
#define STARPART_MAX_OUTPUTS 4
#define STARPART_LOCAL_ID "local"

struct _StarPart_Method
{
  StarPart_Context* ctx;                        /* context */
  char* id;                                     /* method ID of form "package/name" */
  char* package;                                /* method package */
  char* name;                                   /* method name */
  char* desc;                                   /* method description */
  StarPart_MethodInitFunc init;                 /* init function */
  StarPart_MethodCallFunc call;                 /* call function */
  StarPart_DataTable* args;                     /* default argument table */
  int nbinputs;                                 /* nb of input args */
  int nboutputs;                                /* nb of output args */
  StarPart_Data* inputs[STARPART_MAX_INPUTS];   /* array of input arguments */
  StarPart_Data* outputs[STARPART_MAX_OUTPUTS]; /* array of output arguments */
  StarPart_Data* local;                         /* single local argument */
};

/* *********************************************************** */

struct _StarPart_Task
{
  StarPart_Context* ctx;        /* context */
  char* id;                     /* ID */
  StarPart_Method* method;      /* method */
  bool parallel;                /* parallel or sequential task */
  GHashTable* conn;             /* connection table (table of arg_id [char*] -> data_id [char*]) */
  StarPart_DataTable* userargs; /* user argument table */
};

/* *********************************************************** */

struct _StarPart_Hook
{
  char* id;               /* hook ID */
  char* desc;             /* hook description */
  StarPart_HookFunc pre;  /* pre-processing function */
  StarPart_HookFunc post; /* post-processing function */
};

/* *********************************************************** */

extern char* type_str[];
extern char* data_flags_str[];
extern char* item_flags_str[];

/* *********************************************************** */

#endif
