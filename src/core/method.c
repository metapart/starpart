#define _GNU_SOURCE  // required for strdup()
#include <assert.h>
#include <glib.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "starpart.h"
#include "struct.h"
#include "aux.h"

/* *********************************************************** */
/*                          PACKAGE                            */
/* *********************************************************** */

static gint cmpPackage(gconstpointer a, gconstpointer b, gpointer user_data)
{
  return strcmp((char*)a, (char*)b);
}

/* *********************************************************** */

static bool addPackage(StarPart_Context* ctx, char* package)
{
  assert(ctx && package);
  GSequenceIter* iter = g_sequence_lookup(ctx->packages, package, cmpPackage, NULL);
  if (iter)
    return false;
  else
    g_sequence_insert_sorted(ctx->packages, package, cmpPackage, NULL);
  return true;
}

/* *********************************************************** */

void StarPart_listAllPackages(StarPart_Context* ctx)
{
  printf("Packages:\n");
  GSequenceIter* begin = g_sequence_get_begin_iter(ctx->packages);
  GSequenceIter* end = g_sequence_get_end_iter(ctx->packages);
  for (GSequenceIter* iter = begin; iter != end; iter = g_sequence_iter_next(iter)) {
    char* package = g_sequence_get(iter);
    printf("  + %s\n", package);
  }
}

/* *********************************************************** */
/*                       METHOD                                */
/* *********************************************************** */

// static char* getMethodInputName(StarPart_Method* method, int rank)
// {
//   assert(method && rank >= 0 && rank < STARPART_MAX_INPUTS);
//   if (rank < method->nbinputs) return method->inputs[rank];
//   return NULL;
// }

// /* *********************************************************** */

// static char* getMethodOutputName(StarPart_Method* method, int rank)
// {
//   assert(method && rank >= 0 && rank < STARPART_MAX_OUTPUTS);
//   if (rank < method->nboutputs) return method->outputs[rank];
//   return NULL;
// }

// /* *********************************************************** */

// static char* getMethodLocalName(StarPart_Method* method)
// {
//   assert(method && method->local);
//   return method->local->id;
// }

/* *********************************************************** */

char* StarPart_getAlias(StarPart_Method* method, char* alias)
{
  assert(method && alias);
  StarPart_Data* arg = NULL;

  // TODO: write a loop depending of MACRO constants
  if (strcmp(alias, "#local") == 0)
    arg = StarPart_getMethodLocal(method);
  else if (strcmp(alias, "#in") == 0)
    arg = StarPart_getMethodInput(method, 0);
  else if (strcmp(alias, "#in0") == 0)
    arg = StarPart_getMethodInput(method, 0);
  else if (strcmp(alias, "#in1") == 0)
    arg = StarPart_getMethodInput(method, 1);
  else if (strcmp(alias, "#in2") == 0)
    arg = StarPart_getMethodInput(method, 2);
  else if (strcmp(alias, "#in3") == 0)
    arg = StarPart_getMethodInput(method, 3);
  else if (strcmp(alias, "#out") == 0)
    arg = StarPart_getMethodOutput(method, 0);
  else if (strcmp(alias, "#out0") == 0)
    arg = StarPart_getMethodOutput(method, 0);
  else if (strcmp(alias, "#out1") == 0)
    arg = StarPart_getMethodOutput(method, 1);
  else if (strcmp(alias, "#out2") == 0)
    arg = StarPart_getMethodOutput(method, 2);
  else if (strcmp(alias, "#out3") == 0)
    arg = StarPart_getMethodOutput(method, 3);
  else
    return alias;

  if(arg) return arg->id;
  return NULL;
}

/* *********************************************************** */

StarPart_Data* StarPart_getMethodLocal(StarPart_Method* method)
{
  assert(method);
  return method->local;
}

/* *********************************************************** */

StarPart_Data* StarPart_getMethodInput(StarPart_Method* method, int rank)
{
  assert(method && rank >= 0 && rank < STARPART_MAX_INPUTS);
  if (rank < method->nbinputs) return method->inputs[rank];
  return NULL;
}

/* *********************************************************** */

StarPart_Data* StarPart_getMethodOutput(StarPart_Method* method, int rank)
{
  assert(method && rank >= 0 && rank < STARPART_MAX_OUTPUTS);
  if (rank < method->nboutputs) return method->outputs[rank];
  return NULL;
}

/* *********************************************************** */

int StarPart_getMethodNbInputs(StarPart_Method* method)
{
  assert(method);
  return method->nbinputs;
}

/* *********************************************************** */

int StarPart_getMethodNbOutputs(StarPart_Method* method)
{
  assert(method);
  return method->nboutputs;
}

/* *********************************************************** */

char* StarPart_getMethodID(StarPart_Method* method)
{
  assert(method);
  return method->id;
}

/* *********************************************************** */

char* StarPart_getMethodPackage(StarPart_Method* method)
{
  assert(method);
  return method->package;
}

/* *********************************************************** */

char* StarPart_getMethodName(StarPart_Method* method)
{
  assert(method);
  return method->name;
}

/* *********************************************************** */

char* StarPart_getMethodDesc(StarPart_Method* method)
{
  assert(method);
  return method->desc;
}

/* *********************************************************** */

StarPart_MethodCallFunc StarPart_getCallFunc(StarPart_Method* method)
{
  assert(method);
  return method->call;
}

/* *********************************************************** */

StarPart_MethodInitFunc StarPart_getInitFunc(StarPart_Method* method)
{
  assert(method);
  return method->init;
}

/* *********************************************************** */

StarPart_Data* StarPart_getMethodArg(StarPart_Method* method, char* arg_id)
{
  assert(method);
  if (!arg_id) return NULL;
  arg_id = StarPart_getAlias(method, arg_id);
  StarPart_Data* data = StarPart_getDataFromTable(method->args, arg_id);
  return data;
}

/* *********************************************************** */

// int StarPart_getMethodArgFlags(StarPart_Method* method, char* arg_id)
// {
//   StarPart_Data* data = StarPart_getMethodArg(method, arg_id);
//   assert(data);
//   return StarPart_getDataFlags(data);
// }

/* *********************************************************** */

// static void _free_data(gpointer ptr)
// {
//   StarPart_Data * data = (StarPart_Data*)ptr;
//   if(data) StarPart_freeData(data);
// }

/* *********************************************************** */

StarPart_Context* StarPart_getMethodContext(StarPart_Method* method)
{
  assert(method);
  return method->ctx;
}

/* *********************************************************** */

StarPart_Method* StarPart_newMethod(StarPart_Context* ctx, char* id, StarPart_MethodCallFunc call,
                                    StarPart_MethodInitFunc init, char* desc)
{
  StarPart_Method* method = (StarPart_Method*)malloc(sizeof(StarPart_Method));
  assert(method);
  method->ctx = ctx;
  method->id = strdup(id);
  char* sep = strstr(id, "/");
  if (!sep) StarPart_error("malformed method ID \"%s\"\n", id);
  int len = sep - id;  // package length
  method->package = calloc(len + 1, sizeof(char));
  assert(method->package);
  strncpy(method->package, id, len);
  method->name = strdup(id + len + 1);
  method->desc = strdup(desc);
  method->call = call;
  method->init = init;
  method->args = StarPart_newDataTable();
  method->nbinputs = 0;
  method->nboutputs = 0;
  for (int i = 0; i < STARPART_MAX_INPUTS; i++) method->inputs[i] = NULL;
  for (int i = 0; i < STARPART_MAX_OUTPUTS; i++) method->outputs[i] = NULL;
  method->local = StarPart_newData(STARPART_LOCAL_ID, STARPART_LOCAL);
  StarPart_addDataInTable(method->args, method->local);
  return method;
}

/* *********************************************************** */

void StarPart_freeMethod(StarPart_Method* method)
{
  assert(method);
  free(method->id);
  free(method->package);
  free(method->name);
  free(method->desc);
  StarPart_freeDataTable(method->args);
  free(method);
}

/* *********************************************************** */

StarPart_DataTable* StarPart_getMethodArgTable(StarPart_Method* method)
{
  assert(method);
  return method->args;
}

/* *********************************************************** */

StarPart_Data* StarPart_addMethodArg(StarPart_Method* method, char* arg_id, int flags)
{
  assert(method);
  assert(flags & STARPART_IN || flags & STARPART_OUT || !(flags & STARPART_LOCAL));  // || flags == 0 ???
  StarPart_Data* data = StarPart_newData(arg_id, flags);
  StarPart_addDataInTable(method->args, data);
  if (flags & STARPART_IN) {
    assert(method->nbinputs < STARPART_MAX_INPUTS);
    method->inputs[method->nbinputs] = data;
    method->nbinputs++;
  }
  if (flags & STARPART_OUT) {
    assert(method->nboutputs < STARPART_MAX_OUTPUTS);
    method->outputs[method->nboutputs] = data;
    method->nboutputs++;
  }
  return data;
}

/* *********************************************************** */

void StarPart_printMethodDesc(StarPart_Method* method)
{
  assert(method);
  StarPart_print("%s: \"%s\"\n", method->id, method->desc);

  // print all data used by method
  GHashTableIter arg_iter;
  gpointer arg_key, arg_val;
  g_hash_table_iter_init(&arg_iter, method->args);
  while (g_hash_table_iter_next(&arg_iter, &arg_key, &arg_val)) {
    StarPart_Data* arg = (StarPart_Data*)arg_val;
    StarPart_printData(arg);
  }
}

/* *********************************************************** */

StarPart_Method* StarPart_registerNewMethod(StarPart_Context* ctx, char* id, StarPart_MethodCallFunc call,
                                            StarPart_MethodInitFunc init, char* desc)
{
  assert(ctx && id);
  assert(ctx->methodtable);
  StarPart_Method* m = StarPart_newMethod(ctx, id, call, init, desc);
  addPackage(ctx, m->package);
  gboolean r = g_hash_table_insert(ctx->methodtable, (gpointer)m->id, (gpointer)m);
  if (!r) StarPart_error("fail to register method \"%s\"\n", id);
  init(m);  // initialize method arguments
  return m;
}

/* *********************************************************** */

StarPart_Method* StarPart_unregisterMethod(StarPart_Context* ctx, char* method_id)
{
  assert(ctx && method_id);
  gpointer key = NULL, method = NULL;
  g_hash_table_lookup_extended(ctx->methodtable, method_id, &key, &method);
  g_hash_table_steal(ctx->methodtable, method_id);  // don't free key and value
  return method;
}

/* *********************************************************** */

StarPart_Method* StarPart_getMethod(StarPart_Context* ctx, const char* method_id)
{
  assert(ctx);
  assert(ctx->methodtable);
  StarPart_Method* m = (StarPart_Method*)g_hash_table_lookup(ctx->methodtable, method_id);
  if (!m) return NULL;  // not found
  return m;
}

/* *********************************************************** */

StarPart_Method* StarPart_getMethodFromPackage(StarPart_Context* ctx, const char* method_package,
                                               const char* method_name)
{
  assert(ctx);
  assert(ctx->methodtable);
  char* method_id = calloc(strlen(method_package) + strlen(method_name) + 2, sizeof(char));
  strcat(method_id, method_package);
  strcat(method_id, "/");
  strcat(method_id, method_name);
  StarPart_Method* m = (StarPart_Method*)g_hash_table_lookup(ctx->methodtable, method_id);
  free(method_id);
  if (!m) return NULL;  // not found
  return m;
}

/* *********************************************************** */

void StarPart_listAllMethods(StarPart_Context* ctx)
{
  assert(ctx);
  assert(ctx->methodtable);

  GSequenceIter* begin = g_sequence_get_begin_iter(ctx->packages);
  GSequenceIter* end = g_sequence_get_end_iter(ctx->packages);
  for (GSequenceIter* iter = begin; iter != end; iter = g_sequence_iter_next(iter)) {
    char* package = g_sequence_get(iter);
    printf("  + %s\n", package);

    GHashTableIter method_iter;
    gpointer method_key, method_val;
    g_hash_table_iter_init(&method_iter, ctx->methodtable);
    while (g_hash_table_iter_next(&method_iter, &method_key, &method_val)) {
      StarPart_Method* m = (StarPart_Method*)method_val;
      if (strcmp(StarPart_getMethodPackage(m), package) == 0) printf("    + %s: \"%s\"\n", m->id, m->desc);
    }
  }
}

/* *********************************************************** */

/* void StarPart_listAllMethods2(StarPart_Context * ctx) */
/* { */
/*   assert(ctx); */
/*   assert(ctx->methodtable); */

/*   GHashTableIter method_iter; */
/*   gpointer method_key, method_val; */
/*   g_hash_table_iter_init(&method_iter, ctx->methodtable); */
/*   printf("Methods:\n"); */
/*   while (g_hash_table_iter_next (&method_iter, &method_key, &method_val)) { */
/*     printf("  + %s\n", (char*) method_key); */
/*     StarPart_Method * m = (StarPart_Method*)method_val; */
/*     GHashTableIter arg_iter; */
/*     gpointer arg_key, arg_val; */
/*     g_hash_table_iter_init(&arg_iter, m->args); */
/*     int k = 0; */
/*     // int nargs = g_hash_table_size(m->args); */
/*     while (g_hash_table_iter_next (&arg_iter, &arg_key, &arg_val)) { */
/*       printf("    + %s\n", (char*) arg_key); */
/*       StarPart_Data * arg = StarPart_getMethodDefaultArgument(ctx, (char*)method_key,
 * (char*)arg_key); */
/*       GHashTableIter item_iter; */
/*       gpointer item_key, item_val; */
/*       g_hash_table_iter_init (&item_iter, arg->table); */
/*       while (g_hash_table_iter_next (&item_iter, &item_key, &item_val)) { */
/* 	StarPart_Item * item = (StarPart_Item*)item_val; */
/* 	assert(item); */
/* 	// StarPart_printItem(item, false, arg->id); */
/* 	printf("        + %s [%s, %s, %s]\n", item->id, typestr[item->type], modestr[item->mode],
 * lifestr[item->life]); */
/*       } */

/*       // if(k < (nargs-1)) printf(","); */
/*       k++; */
/*     } */
/*     // printf("]\n"); */
/*   } */
/* } */

/* *********************************************************** */

/* char* StarPart_getCurrentMethodID(StarPart_Context * ctx) */
/* { */
/*   assert(ctx); */
/*   return ctx->mid; */
/* } */

/* *********************************************************** */

/* void StarPart_setCurrentMethodID(StarPart_Context * ctx, char * method_id) */
/* { */
/*   assert(ctx); */
/*   ctx->mid = method_id; */
/* } */

/* *********************************************************** */

/* StarPart_Method * StarPart_getCurrentMethod(StarPart_Context * ctx) */
/* { */
/*   assert(ctx); */
/*   return StarPart_getMethod(ctx, ctx->mid); */
/* } */

/* *********************************************************** */

#ifdef POUET
void StarPart_printMethod(StarPart_Context* ctx, char* method)
{
  assert(ctx && method);
  // StarPart_print("method \"%s\":\n", method);

  // print all data used by method
  StarPart_Method* m = StarPart_getMethod(ctx, method);
  GHashTableIter arg_iter;
  gpointer arg_key, arg_val;
  g_hash_table_iter_init(&arg_iter, m->args);
  while (g_hash_table_iter_next(&arg_iter, &arg_key, &arg_val)) {
    StarPart_Data* arg = (StarPart_Data*)arg_val;
    StarPart_Data* data = StarPart_getCurrentMethodArgument(ctx, arg->id);
    if (!data) {
      StarPart_print("arg \"%s\" -> not connected to any data!\n", arg->id);
      continue;
    }
    StarPart_print("arg \"%s\" -> data \"%s\"\n", arg->id, data->id);
    StarPart_printData(data);
    // print all items of actual data
    /* GHashTableIter item_iter; */
    /* gpointer item_key, item_val; */
    /* g_hash_table_iter_init (&item_iter, data->table); */
    /* while (g_hash_table_iter_next (&item_iter, &item_key, &item_val)) { */
    /*   StarPart_Item * item = (StarPart_Item*)item_val; */
    /*   assert(item); */
    /*   StarPart_printItem(item, false, arg->id); */
    /* } */
  }
}

#endif

/* *********************************************************** */
/*                             HOOK                             */
/* *********************************************************** */

StarPart_Hook* StarPart_registerNewHook(StarPart_Context* ctx, char* id, StarPart_HookFunc pre, StarPart_HookFunc post,
                                      char* desc)
{
  assert(ctx && id);
  assert(ctx->hooktable);
  StarPart_Hook* hook = StarPart_newHook(id, pre, post, desc);
  gboolean r = g_hash_table_insert(ctx->hooktable, (gpointer)hook->id, (gpointer)hook);
  if (!r) StarPart_error("fail to register hook \"%s\"\n", id);
  return hook;
}

/* *********************************************************** */

StarPart_Hook* StarPart_unregisterHook(StarPart_Context* ctx, char* id)
{
  assert(ctx && id);
  gpointer key = NULL, hook = NULL;
  g_hash_table_lookup_extended(ctx->hooktable, id, &key, &hook);
  g_hash_table_steal(ctx->hooktable, id);  // don't free key and value
  return hook;
}

/* *********************************************************** */

StarPart_Hook* StarPart_getHook(StarPart_Context* ctx, const char* hook_id)
{
  assert(ctx);
  assert(ctx->hooktable);
  StarPart_Hook* hook = (StarPart_Hook*)g_hash_table_lookup(ctx->hooktable, hook_id);
  if (!hook) return NULL;  // not found
  return hook;
}

/* *********************************************************** */

StarPart_Hook* StarPart_newHook(char* id, StarPart_HookFunc pre, StarPart_HookFunc post, char* desc)
{
  StarPart_Hook* hook = malloc(sizeof(StarPart_Hook));
  assert(hook);
  hook->id = strdup(id);
  hook->desc = strdup(desc);
  hook->pre = pre;
  hook->post = post;
  return hook;
}

/* *********************************************************** */

void StarPart_freeHook(StarPart_Hook* hook)
{
  assert(hook);
  free(hook->id);
  free(hook->desc);
  free(hook);
}

/* *********************************************************** */
