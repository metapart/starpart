#define _GNU_SOURCE
#include <assert.h>
#include <ctype.h>
#include <glib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "starpart.h"
#include "struct.h"

/* *********************************************************** */

// "A[...,x={AA[xx={...}];BB},...];B[...];C[y={CC;DD}]""
// ->
// "A[...,x={},...];B[...];C[y={}]" with queue: "AA[xx={...}];BB" and "CC;DD"

static char* scanSubStrat(char* strat, GQueue* substrats)
{
  assert(strat && substrats);
  int len = strlen(strat);
  char* _strat = calloc(len + 1, sizeof(char));
  char* src = strat;
  char* dst = _strat;

  int cnt = 0;
  char* start = src;
  while (*src != 0) {
    // concatenate regular strat pieces into dst
    if (*src == '{' && cnt == 0) {
      int l = src - start + 1;
      assert(l > 0);
      strncat(dst, start, l);
      start = src;
    }

    if (*src == '{') {
      cnt++;
    }
    else if (*src == '}') {
      cnt--;
    }

    // push nested strat into queue
    if (*start == '{' && *src == '}' && cnt == 0) {
      int l = src - start + 1;
      assert(l > 0);
      char* substrat = calloc(l + 1, sizeof(char));
      strncpy(substrat, start, l);
      // printf("substrat: %s (%d)\n", substrat, l);
      g_queue_push_tail(substrats, substrat);
      start = src;
    }

    src++;
  }

  int l = src - start;
  assert(l > 0);
  strncat(dst, start, l);
  start = src;

  // printf("=> strat: %s\n", _strat);

  return _strat;
}

/* *********************************************************** */

// "A[x=(1,2,3),...];B[y=("a","b","c")]"
// ->
// "A[x=(),...];B[y=()]" + with queue: "(1,2,3) and ("a","b","c")

static char* scanArrays(char* args, GQueue* arrays)
{
  assert(args && arrays);
  int len = strlen(args);
  char* _args = calloc(len + 1, sizeof(char));

  char* src = args;
  char* dst = _args;

  char* start = src;
  while (*src != 0) {
    if (*src == '(') {
      int l = src - start + 1;
      assert(l > 0);
      strncat(dst, start, l);
      start = src;
    }

    // push array into queue
    if (*start == '(' && *src == ')') {
      int l = src - start + 1;
      assert(l > 0);
      char* array = calloc(l + 1, sizeof(char));
      strncpy(array, start, l);
      // printf("array: %s (%d)\n", array, l);
      g_queue_push_tail(arrays, array);
      start = src;
    }

    src++;
  }

  int l = src - start;
  assert(l >= 0);
  strncat(dst, start, l);
  start = src;

  // printf("=> args: %s\n", _args);

  return _args;
}

/* *********************************************************** */

static StarPart_Type getTypeFromStr(char* val)
{
  // select type
  StarPart_Type type = -1;
  int len = 0;
  char* d_endptr;
  char* l_endptr;
  char* la_endptr;
  if (val) {
    len = strlen(val);
    strtod(val, &d_endptr);           // try to convert in double
    strtol(val, &l_endptr, 10);       // try to convert in long
    strtol(val + 1, &la_endptr, 10);  // try to convert in long
  }

  if (!val)
    type = STARPART_VOID;
  else if (len >= 2 && val[0] == '"' && val[len - 1] == '"') {
    type = STARPART_STR;
  }
  else if (len >= 2 && val[0] == '{' && val[len - 1] == '}') {
    type = STARPART_STRAT;
  }
  else if (len >= 3 && val[0] == '(' && val[len - 1] == ')' && val[1] == '"') {
    type = STARPART_STRARRAY;
  }
  else if (len >= 3 && val[0] == '(' && val[len - 1] == ')' && la_endptr > val + 1) {
    type = STARPART_INTARRAY;
  }
  else if (len >= 2 && val[0] == '(' && val[len - 1] == ')') {
    type = STARPART_STRARRAY;
  }
  else if (*l_endptr == 0) {
    type = STARPART_INT;
  }
  else if (*d_endptr == 0) {
    type = STARPART_DOUBLE;
  }
  else
    type = STARPART_STR;  // or type = -1;
  return type;
}

/* *********************************************************** */

static bool handleConnection(StarPart_Task* task, char* key, char symbol, char* val)
{
  char* _key = key;
  StarPart_Data* arg_data = StarPart_getMethodArg(task->method, _key);
  char _keyin[] = "#in?";
  char _keyout[] = "#out?";
  if (!arg_data && (strlen(key) <= 1)) { /* try with prefix #in/#out */
    if ((symbol == '<') && *key == 0) {
      _keyin[3] = 0;
      _key = _keyin;
    }
    else if ((symbol == '<') && isdigit(*key)) {
      _keyin[3] = *key;
      _key = _keyin;
    }
    else if ((symbol == '>') && *key == 0) {
      _keyout[4] = 0;
      _key = _keyout;
    }
    else if ((symbol == '>') && isdigit(*key)) {
      _keyout[4] = *key;
      _key = _keyout;
    }
    else
      _key = NULL;
    if (_key) arg_data = StarPart_getMethodArg(task->method, _key);
  }
  if (!arg_data) return false;                                                // bad key
  if (symbol == '<') assert(StarPart_getDataFlags(arg_data) & STARPART_IN);   // bad direction
  if (symbol == '>') assert(StarPart_getDataFlags(arg_data) & STARPART_OUT);  // bad direction
  StarPart_connectTask(task, _key, val);

  return true;
}

/* *********************************************************** */

static StarPart_Data* lookupItem(StarPart_Method* method, char* item_id, int flags)
{
  assert(method);

  // lookup item in local
  if (flags & STARPART_LOCAL || flags == 0) {
    // char* arg_id = StarPart_getMethodLocal(method);
    // StarPart_Data* arg = StarPart_getMethodArg(method, arg_id);
    StarPart_Data* arg = StarPart_getMethodLocal(method);
    if (arg && StarPart_isItem(arg, item_id)) return arg;
  }

  // lookup item in inputs
  if (flags & STARPART_IN || flags == 0) {
    int nbinputs = StarPart_getMethodNbInputs(method);
    for (int i = 0; i < nbinputs; i++) {
      // char* arg_id = StarPart_getMethodInput(method, i);
      // StarPart_Data* arg = StarPart_getMethodArg(method, arg_id);
      StarPart_Data* arg = StarPart_getMethodInput(method, i);
      if (arg && StarPart_isItem(arg, item_id)) return arg;
    }
  }

  // lookup item in outputs
  if (flags & STARPART_OUT || flags == 0) {
    int nboutputs = StarPart_getMethodNbOutputs(method);
    for (int i = 0; i < nboutputs; i++) {
      // char* arg_id = StarPart_getMethodOutput(method, i);
      // StarPart_Data* arg = StarPart_getMethodArg(method, arg_id);
      StarPart_Data* arg = StarPart_getMethodOutput(method, i);
      if (arg && StarPart_isItem(arg, item_id)) return arg;
    }
  }

  return NULL;
}

/* *********************************************************** */

static bool handleArgument(StarPart_Task* task, char* key, char symbol, char* val)
{
  char* data_id = NULL;
  char* item_id = NULL;
  StarPart_Data* arg_data = NULL;
  StarPart_Item* arg_item = NULL;

  // key is data_id/item_id
  // char * _key = strdup(key);
  char* slash = strchr(key, '/');
  if (slash) {
    *slash = 0;
    data_id = key;
    item_id = slash + 1;
    data_id = StarPart_getAlias(task->method, data_id);
    arg_data = StarPart_getMethodArg(task->method, data_id);
    // if (!arg_data) return false;
    if (arg_data) arg_item = StarPart_getItem(arg_data, item_id);
    // if (!arg_item) return false;
  }
  else { /* lookup for items in all available args, starting by #local, #in0, #in1, ... #out0, #out1, ... */
    data_id = NULL;
    item_id = key;
    arg_data = lookupItem(task->method, key, STARPART_LOCAL | STARPART_IN | STARPART_OUT);
    if (arg_data) {
      data_id = arg_data->id;
      arg_item = StarPart_getItem(arg_data, item_id);
      assert(arg_item);
      StarPart_warning("parser: found item \"%s\" in argument \"%s\"\n", key, arg_data->id);
    }
  }

  if (!data_id) data_id = STARPART_LOCAL_ID;  // default ID if item is not found to add item
  assert(data_id && item_id);

  // insert "user arg" in task->userargs table
  StarPart_Data* data = StarPart_getDataFromTable(task->userargs, data_id);
  if (!data) data = StarPart_addNewDataInTable(task->userargs, data_id, 0);  // TODO:data flags?
  StarPart_Type type = getTypeFromStr(val);
  // if(type == -1) StarPart_error("arg \"%s\" has bad value \"%s\" with unknown type!\n", key, val);
  if (type == -1) {
    StarPart_warning("invalid type for data_id/item_id: \"%s/%s\"!\n", data_id, item_id);
    return false;
  }
  int flags = 0;
  if (arg_item) {
    flags = arg_item->flags;
    StarPart_Type arg_type = StarPart_getItemType(arg_item);
    if (arg_type != type) {
      StarPart_warning("arg \"%s/%s\" has bad type \"%s\"!\n", data_id, item_id, type_str[type]);
      return false;
    }
  }
  // printf("key=\"%s/%s\" val=%s (%s)\n", data_id, item_id, val, typestr[type]);

  // StarPart_print("parsing arg \"%s/%s\" = %s [%s]\n", data_id, item_id, val, type_str[type]);

  StarPart_Item* item = StarPart_newItemFromStr(item_id, type, val, flags);
  StarPart_addItem(data, item);
  // if (type == STARPART_STRAT || type == STARPART_INTARRAY || type == STARPART_STRARRAY) free(val);

  // free(_key);

  return true;
}

/* *********************************************************** */

static bool insertTaskArg(StarPart_Task* task, char* keyval, GQueue* substrats, GQueue* arrays)
{
  assert(task && keyval && substrats && arrays);
  // printf("keyval: \"%s\"\n", keyval);
  // scan key=val || key>val || key < val
  char symbol = ' ';
  char* key = strdup(keyval);
  char* val = NULL;
  char* sup = strchr(key, '>');
  char* inf = strchr(key, '<');
  char* equal = strchr(key, '=');
  if (sup) {
    *sup = 0;
    symbol = '>';
    val = sup + 1;
  }
  else if (inf) {
    *inf = 0;
    symbol = '<';
    val = inf + 1;
  }
  else if (equal) {
    *equal = 0;
    symbol = '=';
    val = equal + 1;
  }

  // removes leading and trailing whitespaces
  g_strstrip(key);
  if (val) g_strstrip(val);

  // performs substitution for (array) or {strat}
  char* _val = val;
  if (val && strcmp(val, "{}") == 0)
    _val = g_queue_pop_head(substrats);
  else if (val && strcmp(val, "()") == 0)
    _val = g_queue_pop_head(arrays);

  // StarPart_error("arg \"%s\" must be of the form key | key=val | key>val | key<val\n", keyval);

  // token is key>val |  key<val
  if ((symbol == '>') || (symbol == '<')) {
    bool success = handleConnection(task, key, symbol, _val);
    if (!success) {
      StarPart_warning("parser: fail to handle connection \"%s\" for task \"%s\" !\n", keyval, task->id);
      return false;
    }
  }

  // token is key or key=value
  if ((symbol == '=') || (symbol == ' ')) {
    bool success = handleArgument(task, key, symbol, _val);
    if (!success) {
      StarPart_warning("parser: fail to handle argument \"%s\" for task \"%s\" !\n", keyval, task->id);
      return false;
    }
  }

  free(key);
  if (val != _val) free(_val);

  return true;
}

/* *********************************************************** */

static int insertTaskArgs(StarPart_Task* task, char* args, GQueue* substrats)
{
  assert(task && args && substrats);

  GQueue* arrays = g_queue_new();
  char* _args = scanArrays(args, arrays);  // key=(val0,val1,...)

  int nbargs = 0;
  char** keyvals = g_strsplit(_args, ",", -1);  // keyval,keyval,keyval,...
  char** keyval = keyvals;

  while (*keyval) {
    bool r = insertTaskArg(task, *keyval, substrats, arrays);
    if (!r)
      StarPart_warning("parser: invalid argument \"%s\" for method \"%s\"!\n", *keyval, task->id);
    else
      nbargs++;
    keyval++;
  }

  g_strfreev(keyvals);
  g_queue_free(arrays);
  free(_args);

  return nbargs;
}

/* *********************************************************** */

int StarPart_insertTaskArgs(StarPart_Task* task, char* args)
{
  assert(task);
  if (!args) return 0;
  GQueue* substrats = g_queue_new();
  char* _args = scanSubStrat(args, substrats);
  int nbargs = insertTaskArgs(task, _args, substrats);
  g_queue_free(substrats);
  free(_args);
  return nbargs;
}

/* *********************************************************** */

// STRAT = METHOD0 ; METHOD1 ; ...
// METHOD = PACKAGE/NAME[ARGS] or !PACKAGE/NAME[ARGS]
// ARGS = KEY0=VAL0,KEY1=VAL1, ...

void StarPart_parseStrat(StarPart_Context* ctx, char* strat)
{
  assert(ctx && strat);

  /* some preprocessing */

  /* remove space characters */
  char *temp1_strat=strat, *temp2_strat=strat;
  do {
    while(isspace(*temp1_strat))
      temp1_strat++;
  } while((*temp2_strat++ = *temp1_strat++));

  GQueue* substrats = g_queue_new();
  char* _strat = scanSubStrat(strat, substrats);

  char** methods = g_strsplit(_strat, ";", -1);
  char** method = methods;
  while (*method) {
    // printf("method: \"%s\"\n", *method);
    char* bracket0 = strchr(*method, '[');   // first occurence
    char* bracket1 = strrchr(*method, ']');  // last occurrence
    if ((bracket0 != NULL) != (bracket1 != NULL)) StarPart_error("invalid brackets in \"%s\"\n", *method);

    // char * name = NULL; strncpy(name, *method, )
    char* name = *method;
    bool taskseq = false;
    if (name[0] == '!') {
      taskseq = true;
      name++;
    }
    if (bracket0) *bracket0 = 0;
    char* args = NULL;
    if (bracket0 && bracket1) {
      args = bracket0 + 1;
      *bracket1 = 0;
    }
    // printf("method: \"%s\" and args: \"%s\"\n", name, args);
    // StarPart_print("parsing method: \"%s\" with args \"%s\"\n", name, args);
    StarPart_Method* m = StarPart_getMethod(ctx, name);
    if (!m) StarPart_error("unknown method \"%s\"\n", name);

    int prank = StarPart_getParallelCommRank(ctx);

    /* sequential context */
    if (!ctx->parallel) {
      StarPart_Task* task = StarPart_newTaskByName(ctx, name, NULL);
      StarPart_setParallelTask(task, false);
      if (args) insertTaskArgs(task, args, substrats);
    }
    /* parallel context & parallel task*/
    else if (ctx->parallel && !taskseq) {
      StarPart_Task* task = StarPart_newTaskByName(ctx, name, NULL);
      if (args) insertTaskArgs(task, args, substrats);
      StarPart_setParallelTask(task, true);
    }
    /* parallel context & sequential task on prank 0*/
    else if (ctx->parallel && taskseq && prank == 0) {
      StarPart_Task* task = StarPart_newTaskByName(ctx, name, NULL);
      if (args) insertTaskArgs(task, args, substrats);
      StarPart_setParallelTask(task, false);
    }

    method++;
  }

  g_strfreev(methods);
  g_queue_free(substrats);
  free(_strat);
}

/* *********************************************************** */
