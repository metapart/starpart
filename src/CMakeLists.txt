
# check libgraph submodule 
if(NOT EXISTS "${CMAKE_SOURCE_DIR}/src/libgraph/.git" )
 message(FATAL_ERROR "Submodule src/libgraph not initialized, run: git submodule update --init")
endif()

add_subdirectory(libgraph)
add_subdirectory(core)
add_subdirectory(all)
