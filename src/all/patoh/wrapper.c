/* Wrapper library for PATOH */

#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "all.h"
#include "libgraph.h"
#include "patoh.h"
#include "starpart.h"
#include "wrapper.h"

static void partitionHypergraphRPaToH(LibGraph_Hypergraph* hg, int nparts, int ubfactor, int* part, int* cut);
static void partitionHypergraphFixedPaToH(LibGraph_Hypergraph* hg, int nparts, int ubfactor, int* part, int* cut);
static void partitionMultiConstHypergraphPaToH(LibGraph_Hypergraph* hg, int nconst, int nparts, int ubfactor, int* part,
                                               int* cut);

/* *********************************************************** */

void StarPart_PATOH_PART_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "hypergraph", STARPART_HYPERGRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_PATOH_PART_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Hypergraph* hg = StarPart_getHypergraph(data, "hypergraph");

  int* part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  int nparts = StarPart_getInt(data, "nparts");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  assert(hg);
  assert(nparts > 0);

  if (part)
    StarPart_checkArray(data, "part", hg->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", hg->nvtxs);
    StarPart_initArray(data, "part", -1);
  }
  if (fixed) StarPart_checkArray(data, "fixed", hg->nvtxs);

  int cut = 0;
  partitionHypergraphRPaToH(hg, nparts, ubfactor, part, &cut);
}

/* *********************************************************** */

static void partitionHypergraphRPaToH(LibGraph_Hypergraph* hg, int nparts, int ubfactor, int* part, int* edgecut)
{
  /* PATOH <-> HMETIS */

  int _c = hg->nvtxs;
  int _n = hg->nhedges;
  int _nconst = 1;         /* nb of constraints */
  int* cwghts = hg->vwgts; /* [in] array of size _c*_nconst */
  int* nwghts = hg->hewgts;
  int* xpins = hg->eptr;
  int* pins = hg->eind;
  int* partvec = part;     /* [out] array of size _c */
  int* cut = edgecut;      /* [out] */
  int* partweights = NULL; /* [out] array of size pargs._k*_nconst */

  /* initialization */

  PaToH_Parameters pargs;
  PaToH_Initialize_Parameters(&pargs, PATOH_CONPART, PATOH_SUGPARAM_SPEED);
  /* or PATOH_CUTPART */
  /* PATOH_SUGPARAM_QUALITY or PATOH_SUGPARAM_DEFAULT or PATOH_SUGPARAM_SPEED */

  // increase MemMul_Pins parameter

  pargs._k = nparts;
  pargs.init_imbal = ubfactor / 100.0;
  pargs.final_imbal = ubfactor / 100.0;
  partweights = (int*)malloc(pargs._k * _nconst * sizeof(int));

  /* debug */
  // PaToH_Print_Parameters(&pargs);

  PaToH_Alloc(&pargs, _c, _n, _nconst, cwghts, nwghts, xpins, pins);

  PaToH_Partition(&pargs, _c, _n, cwghts, nwghts, xpins, pins, partvec, partweights, cut);

  free(partweights);
  PaToH_Free();
}

/* *********************************************************** */

static void partitionMultiConstHypergraphPaToH(LibGraph_Hypergraph* hg, int nconst, int nparts, int ubfactor, int* part,
                                               int* edgecut)
{
  /* PATOH <-> HMETIS */

  int _c = hg->nvtxs;
  int _n = hg->nhedges;
  int _nconst = nconst;    /* nb of constraints */
  int* cwghts = hg->vwgts; /* [in] array of size _c*_nconst */
  int* nwghts = NULL;      /* not used for multi-constraint! */
  int* xpins = hg->eptr;
  int* pins = hg->eind;
  int* partvec = part;     /* [out] array of size _c */
  int* cut = edgecut;      /* [out] */
  int* partweights = NULL; /* [out] array of size pargs._k*_nconst */

  /* initialization */

  PaToH_Parameters pargs;
  PaToH_Initialize_Parameters(&pargs, PATOH_CONPART,   /* or PATOH_CUTPART */
                              PATOH_SUGPARAM_DEFAULT); /* PATOH_SUGPARAM_QUALITY or
                                                          PATOH_SUGPARAM_DEFAULT or
                                                          PATOH_SUGPARAM_SPEED */

  pargs._k = nparts;
  pargs.init_imbal = ubfactor / 100.0;
  pargs.final_imbal = ubfactor / 100.0;
  partweights = (int*)malloc(pargs._k * _nconst * sizeof(int));

  PaToH_Alloc(&pargs, _c, _n, _nconst, cwghts, nwghts, xpins, pins);

  /*PaToH_Print_Parameters(&pargs); /\* debug */

  PaToH_MultiConst_Partition(&pargs, _c, _n, _nconst, cwghts, xpins, pins, partvec, partweights, cut);

  free(partweights);
  PaToH_Free();
}

/* *********************************************************** */

static void partitionHypergraphFixedPaToH(LibGraph_Hypergraph* hg, int nparts, int ubfactor, int* part, int* edgecut)
{
  /* PATOH <-> HMETIS */

  int _c = hg->nvtxs;
  int _n = hg->nhedges;
  int _nconst = 1;         /* nb of constraints */
  int* cwghts = hg->vwgts; /* [in] array of size _c*_nconst */
  int* nwghts = hg->hewgts;
  int* xpins = hg->eptr;
  int* pins = hg->eind;
  int* partvec = part;     /* [out] array of size _c */
  int* cut = edgecut;      /* [out] */
  int* partweights = NULL; /* [out] array of size pargs._k*_nconst */

  /* check */
  for (int i = 0; i < _c; i++) { assert(partvec[i] >= -1 && partvec[i] < nparts); }

  /* initialization */

  PaToH_Parameters pargs;
  PaToH_Initialize_Parameters(&pargs, PATOH_CONPART, /* or PATOH_CUTPART */
                              PATOH_SUGPARAM_SPEED); /* or PATOH_SUGPARAM_QUALITY or
                                                        PATOH_SUGPARAM_DEFAULT or
                                                        PATOH_SUGPARAM_SPEED */

  pargs._k = nparts;
  pargs.init_imbal = ubfactor / 100.0;
  pargs.final_imbal = ubfactor / 100.0;
  partweights = (int*)malloc(pargs._k * _nconst * sizeof(int));

  /* For coupling hyperedge partitioning */
  // pargs.crs_alg = pargs.crs_useafteralg = PATOH_CRS_ABS;
  // pargs.crs_coarsentokmult = 8;
  // pargs.initp_refalg = PATOH_REFALG_FMKL;
  // pargs.ref_alg = PATOH_REFALG_FMKL;
  // pargs.ref_passcnt = 5;

  PaToH_Alloc(&pargs, _c, _n, _nconst, cwghts, nwghts, xpins, pins);

  // PaToH_Print_Parameters(&pargs); // debug

  /* Seed: 4294967295   #Parts: 100  Coarsen to:   100 */
  /* CoarToKM:  20  Coarsen %:   9  Coar.WeightMult: 0.25 */
  /* DON'T Permute input hypergraph. */
  /* Fixed net Size trsh (FS):    0  Netsize Trsh (NT): 100000.000  Netsize part.Mul.trsh(NM):
   * 100000 */
  /* remove IDentical nets after level (ID): 1000  check Iden. netsize Tresh (IT): 4 */
  /* Visit Order : Random */
  /* Matching Algorithm = ABSHCC  : Absorbtion Clustering using Nets */
  /* Use matching = ABSHCC  : Absorbtion Clustering using Nets  after level = 10000 */
  /* Part.Alg: GHGP             : Greedy Hypergraph Growing Partition */
  /* Refinement After InitPart: D_BFM    : Boundary FM with Dynamic Locking */
  /*  # Initpart Runs:   4  TryBalance in GHG part: 1 */
  /* Refinement: D_BFM    : Boundary FM with Dynamic Locking */
  /* Bal=Strict I.Bal: 0.050  F.Bal: 0.050  DI: 0.200  DF: 0.020 */
  /* total #Runs:   1   #Instances: 1   bigV:   1   smallV:   1 */
  /* Dynamic LoCk:   1   RefPass:   2   RefmaxNegMove:  100 */

  /* printf("MemMul_Pins = %d \n", pargs.MemMul_Pins); */
  /* printf("MemMul_CellNet = %d \n", pargs.MemMul_CellNet); */
  /* printf("MemMul_General = %d \n", pargs.MemMul_General); */

  PaToH_Partition_with_FixCells(&pargs, _c, _n, cwghts, nwghts, xpins, pins, partvec, partweights, cut);
  free(partweights);
  PaToH_Free();
}

/* *********************************************************** */
