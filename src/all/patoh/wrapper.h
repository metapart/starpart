#ifndef PATOH_WRAPPER_H
#define PATOH_WRAPPER_H

#include "starpart.h"

//@{
    
#define StarPart_PATOH_PART_desc "hypergraph partitioning routine of PaToH"
void StarPart_PATOH_PART_init(StarPart_Method* method);
void StarPart_PATOH_PART_call(StarPart_Task* task);

//@}

#endif
