/** @file hun.h */

#ifndef BASIC_HUN_H
#define BASIC_HUN_H

#include "starpart.h"

#define StarPart_HUN_desc "Hungarian method for post-processing partition with fixed vertices"
void StarPart_HUN_call(StarPart_Task* task);
void StarPart_HUN_init(StarPart_Method* method);

#endif
