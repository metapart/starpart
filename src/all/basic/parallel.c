#include <assert.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>

#include "all.h"
#include "libgraph.h"
#include "parallel.h"
#include "starpart.h"

/* *********************************************************** */

void StarPart_MPISPAWN_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "np", STARPART_INT, NEW_INT(2), true, STARPART_READ);
  StarPart_addNewItem(local, "debug", STARPART_INT, NEW_INT(0), true, STARPART_READ);
  StarPart_addNewItem(local, "strat", STARPART_STRAT, NULL, true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_MPISPAWN_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  int np = StarPart_getInt(local, "np");
  int debug = StarPart_getInt(local, "debug");
  char* strat = StarPart_getStrat(local, "strat");
  assert(strat);

  // MPI_Init(NULL, NULL); // called by starpart-run "master"

  MPI_Comm comm = MPI_COMM_WORLD;
  int rank, size;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &size);
  // printf("=> Master MPI rank %d / %d\n", rank, size);

  /* set cwd */
  MPI_Info info = MPI_INFO_NULL;
  // MPI_Info_create(&info);
  // MPI_Info_set(info, "wdir", code->cwd); /* MPI-2 Standard (?) */

  int root = 0;
  int errcodes[np];
  MPI_Comm intercomm;
  char* program = "starpart-run";
  char* argv[] = {"-g", "-s", "-r", strat, NULL};
  char** _argv = debug ? argv : argv + 1;

  int err = MPI_Comm_spawn(program, _argv, /* MPI_ARGV_NULL */
                           np,             /* nb of proc. */
                           info,           /* or MPI_INFO_NULL */
                           root,           /* root rank 0 for spawn */
                           comm,           /* or MPI_COMM_WORLD or MPI_COMM_SELF */
                           &intercomm,     /* inter-communicator */
                           errcodes);      /* or MPI_ERRCODES_IGNORE */

  assert(err == MPI_SUCCESS);

  // printf("end of spawn\n");

  MPI_Barrier(intercomm);  // synchronize with master stapart-run

  // MPI_Finalize(); // called by starpart-run "master"
}

/* *********************************************************** */

void StarPart_SCATTER_init(StarPart_Method* method)
{
  assert(method);
  StarPart_Context* ctx = StarPart_getMethodContext(method);
  assert(StarPart_isParallelContext(ctx));
  // int prank = StarPart_getParallelCommRank(ctx);
  // if (prank == 0) {
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_IN | STARPART_MASTER);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  // }
  StarPart_Data* distdata = StarPart_addMethodArg(method, "distdata", STARPART_OUT);
  StarPart_addNewItem(distdata, "distgraph", STARPART_DISTGRAPH, NULL, true, STARPART_WRITE);
  // StarPart_addNewItem(distdata, "graph", STARPART_GRAPH, NULL, true, STARPART_WRITE);
  // StarPart_addNewItem(distdata, "vtxdist", STARPART_INTARRAY, NULL, true, STARPART_WRITE);
}

/* *********************************************************** */

/* scatter graph */
void StarPart_SCATTER_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  assert(StarPart_isParallelContext(ctx));
  MPI_Comm comm = StarPart_getParallelComm(ctx);
  int prank = StarPart_getParallelCommRank(ctx);
  int psize = StarPart_getParallelCommSize(ctx);
  assert(psize >= 2);

  StarPart_Data* data = NULL;
  LibGraph_Graph* g = NULL;

  if (prank == 0) {
    data = StarPart_getTaskData(task, "data");
    g = StarPart_getGraph(data, "graph");
  }

  StarPart_Data* distdata = StarPart_getTaskData(task, "distdata");
  LibGraph_DistGraph* gg = StarPart_newDistGraph();  // distributed graph
  StarPart_setDistGraph(distdata, "distgraph", gg, true);
  // LibGraph_Graph* gg = StarPart_newGraph();  // distributed graph
  // StarPart_setGraph(distdata, "graph", gg, true);

  /* Distribute vertices */

  int* vtxdist = malloc((psize + 1) * sizeof(int));
  if (prank == 0) {
    for (int i = 0; i < psize + 1; i++) vtxdist[i] = g->nvtxs * i / psize;
  }
  MPI_Bcast(vtxdist, psize + 1, MPI_INT, 0, comm);
  int nvtxs = vtxdist[prank + 1] - vtxdist[prank];

  // StarPart_setIntArray(distdata, "vtxdist", psize + 1, vtxdist, true);

  /* Distribute xadj */
  /* Cannot use MPI_Scatterv because of overlapping */
  int* xadj = (int*)malloc((nvtxs + 1) * sizeof(int));
  if (prank == 0) {
    for (int i = 1; i < psize; i++)
      MPI_Send(&g->xadj[vtxdist[i]], vtxdist[i + 1] - vtxdist[i] + 1, MPI_INT, i, 0, comm);
    memcpy(xadj, g->xadj, (nvtxs + 1) * sizeof(int));
  }
  else {
    MPI_Recv(xadj, nvtxs + 1, MPI_INT, 0, 0, comm, MPI_STATUS_IGNORE);
    int offset = xadj[0];
    for (int i = 0; i < nvtxs + 1; i++) xadj[i] -= offset;
  }

  /* Distribute adjncy */
  int* adjncy = malloc(xadj[nvtxs] * sizeof(int));
  if (prank == 0) {
    int sendcounts[psize];
    int displs[psize];
    for (int i = 0; i < psize; i++) {
      sendcounts[i] = g->xadj[vtxdist[i + 1]] - g->xadj[vtxdist[i]];
      displs[i] = g->xadj[vtxdist[i]];
    }
    MPI_Scatterv(g->adjncy, sendcounts, displs, MPI_INT, adjncy, xadj[nvtxs], MPI_INT, 0, comm);
  }
  else
    MPI_Scatterv(NULL, NULL, NULL, MPI_INT, adjncy, xadj[nvtxs], MPI_INT, 0, comm);

  /* Distribute vertex weights */
  int* vwgts = (int*)malloc(nvtxs * sizeof(int));
  if (prank == 0) {
    if (g->vwgts == NULL) {
      g->vwgts = malloc(g->nvtxs * sizeof(int));
      for (int i = 0; i < g->nvtxs; i++) g->vwgts[i] = 1;
    }
    int sendcounts[psize];
    for (int i = 0; i < psize; i++) sendcounts[i] = vtxdist[i + 1] - vtxdist[i];
    MPI_Scatterv(g->vwgts, sendcounts, vtxdist, MPI_INT, vwgts, nvtxs, MPI_INT, 0, comm);
  }
  else {
    MPI_Scatterv(NULL, NULL, NULL, MPI_INT, vwgts, nvtxs, MPI_INT, 0, comm);
  }

  // set distributed graph gg
  gg->nvtxs = nvtxs;
  gg->narcs = xadj[nvtxs];
  gg->xadj = xadj;
  gg->adjncy = adjncy;
  gg->vwgts = vwgts;
  gg->ewgts = NULL;
  gg->vtxdist = vtxdist;

  // debug
  // for (int i = 0; i < psize; i++) {
  //   if (prank == i) { printGraph(gg); }
  //   MPI_Barrier(comm);
  // }
}

/* *********************************************************** */

/* gather graph (and optionnally partition or arrays) */
void StarPart_GATHER_init(StarPart_Method* method)
{
  assert(method);
  StarPart_Context* ctx = StarPart_getMethodContext(method);
  assert(StarPart_isParallelContext(ctx));
  // int prank = StarPart_getParallelCommRank(ctx);

  StarPart_Data* distdata = StarPart_addMethodArg(method, "distdata", STARPART_IN);
  StarPart_addNewItem(distdata, "distgraph", STARPART_DISTGRAPH, NULL, true, STARPART_READ);
  // StarPart_addNewItem(distdata, "vtxdist", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(distdata, "part", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(distdata, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(distdata, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);

  // if (prank == 0) {
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_OUT | STARPART_MASTER);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NULL, true, STARPART_WRITE);
  // }
}

/* *********************************************************** */

void StarPart_GATHER_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  assert(StarPart_isParallelContext(ctx));
  MPI_Comm comm = StarPart_getParallelComm(ctx);
  int prank = StarPart_getParallelCommRank(ctx);
  int psize = StarPart_getParallelCommSize(ctx);
  assert(psize >= 2);

  /* distributed data (input) */
  StarPart_Data* distdata = StarPart_getTaskData(task, "distdata");
  LibGraph_DistGraph* g = StarPart_getDistGraph(distdata, "distgraph");
  int nvtxs = g->nvtxs;
  int* part = StarPart_getIntArray(distdata, "part");

  int* vtxdist = g->vtxdist;
  int total_nvtxs = vtxdist[psize];
  assert(nvtxs == vtxdist[prank + 1] - vtxdist[prank]);

  /* sequential data (output) */

  StarPart_Data* data = NULL;
  int* gpart = NULL;
  if (prank == 0) {
    data = StarPart_getTaskData(task, "data");
    if (part) {
        gpart = StarPart_allocArray(data, "part", total_nvtxs);
    }
  }

  int* xadj = NULL;
  int* vwgts = NULL;
  int* adjncy = NULL;
  int* ewgts = NULL;
  int recvcounts[psize];
  int displs[psize];

  /* Compute recvcounts for arrays about vtxs */
  if (prank == 0) {
      xadj = (int*)malloc(sizeof(int[total_nvtxs+1]));
      for (int i = 0; i < psize; i++) {
          recvcounts[i] = vtxdist[i+1]-vtxdist[i];
      }
      xadj[0] = 0;
  }

  /* Gather */
  /* xadj */
  MPI_Gatherv(g->xadj+1, nvtxs, MPI_INT, xadj+1, recvcounts, vtxdist, MPI_INT, 0, comm);

  /* part */
  int partsize = StarPart_getArrayLength(distdata, "part");

  /* Share partition size */
  int *partsizes = (int *)malloc(sizeof(int[psize]));
  MPI_Allgather(&partsize, 1, MPI_INT, partsizes, 1, MPI_INT, comm);

  int *partdispls = (int *)malloc(sizeof(int[psize+1]));
  partdispls[0] = 0;
  for (int p = 0; p < psize; ++p) {
    partdispls[p+1] = partdispls[p]+partsizes[p];
  }

  if (partdispls[psize]) {
    MPI_Gatherv(part, partsize, MPI_INT,
        gpart, partsizes, partdispls, MPI_INT, 0, comm);
  }
  free(partsizes);
  free(partdispls);

  /* vwgts */
  /* Check if wgts exists somewhere */
  int hasvwgts;
  MPI_Allreduce(&g->vwgts, &hasvwgts, 1, MPI_INT, MPI_SUM, comm);
  if (hasvwgts) {
    if (prank == 0) {
      vwgts = (int*)malloc(sizeof(int[total_nvtxs]));
    }
    MPI_Gatherv(g->vwgts, nvtxs, MPI_INT, vwgts, recvcounts, vtxdist, MPI_INT,
        0, comm);
  }

  /* Compute recvcounts and offsets for arrays about arcs */
  int narcs = -1;
  if (prank == 0) {
      int offset = 0;
      for (int i = 0; i < psize; i++) {
          for (int j = vtxdist[i]+1; j <= vtxdist[i+1]; j++) {
              xadj[j] += offset;
          }
          offset = xadj[vtxdist[i+1]];
      }

      narcs = xadj[total_nvtxs];

      adjncy = (int*)malloc(sizeof(int[narcs]));

      for (int i = 0; i < psize; i++) {
          recvcounts[i] = xadj[vtxdist[i+1]]-xadj[vtxdist[i]];
          displs[i] = xadj[vtxdist[i]];
      }
  }

  /* narcs size arrays */
  MPI_Gatherv(g->adjncy, g->narcs, MPI_INT, adjncy, recvcounts, displs, MPI_INT, 0, comm);

  int hasewgts;
  MPI_Allreduce(&g->ewgts, &hasewgts, 1, MPI_INT, MPI_SUM, comm);
  if (hasewgts) {
      if (prank == 0) {
          ewgts = (int*)malloc(sizeof(int[narcs]));
      }
      MPI_Gatherv(g->ewgts, g->narcs, MPI_INT, ewgts, recvcounts, displs, MPI_INT, 0, comm);
  }


  if (prank == 0) {
    /* set gathered graph */
    LibGraph_Graph* gg = StarPart_newGraph();  // graph
    StarPart_setGraph(data, "graph", gg, true);
    gg->nvtxs = total_nvtxs;
    gg->narcs = narcs;
    gg->xadj = xadj;
    gg->adjncy = adjncy;
    gg->vwgts = vwgts;
    gg->ewgts = ewgts;

    if (StarPart_hasItemValue(distdata, "nparts")) {
      int nparts = StarPart_getInt(distdata, "nparts");
      int ubfactor = StarPart_getInt(distdata, "ubfactor");
      StarPart_setInt(data, "ubfactor", ubfactor);
      StarPart_setInt(data, "nparts", nparts);
    }
  }
}

/* *********************************************************** */

void StarPart_REDIST_init(StarPart_Method* method)
{
  assert(method);
  StarPart_Context* ctx = StarPart_getMethodContext(method);
  assert(StarPart_isParallelContext(ctx));
  // int prank = StarPart_getParallelCommRank(ctx);

  StarPart_Data* distdata = StarPart_addMethodArg(method, "distdata", STARPART_INOUT);
  StarPart_addNewItem(distdata, "distgraph", STARPART_DISTGRAPH, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(distdata, "part", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(distdata, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(distdata, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
  StarPart_addNewItem(distdata, "inverse", STARPART_INT, NEW_INT(0), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_REDIST_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  assert(StarPart_isParallelContext(ctx));
  MPI_Comm comm = StarPart_getParallelComm(ctx);
  int prank = StarPart_getParallelCommRank(ctx);
  int psize = StarPart_getParallelCommSize(ctx);
  assert(psize >= 2);

  /* distributed data */
  StarPart_Data* distdata = StarPart_getTaskData(task, "distdata");
  LibGraph_DistGraph* g = StarPart_getDistGraph(distdata, "distgraph");
  int nvtxs = g->nvtxs;
  int* part = StarPart_getIntArray(distdata, "part");
  int inverse = StarPart_getInt(distdata, "inverse");

  if (!inverse) {
    StarPart_checkArray(distdata, "part", nvtxs);
  }

  int* vtxdist = g->vtxdist;
  int total_nvtxs = vtxdist[psize];
  assert(nvtxs == vtxdist[prank+1]-vtxdist[prank]);

  int *new_xadj = NULL;
  int *new_adjncy = NULL;
  int *new_ewgts = NULL;
  int *new_vwgts = NULL;
  int new_nvtxs = -1;
  int new_narcs;

  /**************************/
  /* Permutation of indices */
  /* First we need to gather part */
  int* gpart = NULL;
  int recv_npart[psize];
  int *recv_partdispls = (int *)malloc(sizeof(int[psize+1]));
  int partsize = StarPart_getArrayLength(distdata, "part");
  MPI_Allgather(&partsize, 1, MPI_INT, recv_npart, 1, MPI_INT, comm);
  recv_partdispls[0] = 0;
  for (int p = 0; p < psize; p++) {
    recv_partdispls[p+1] = recv_partdispls[p]+recv_npart[p];
  }

  gpart = (int*)malloc(sizeof(int[vtxdist[psize]]));
  MPI_Allgatherv(part, partsize, MPI_INT, gpart, recv_npart, recv_partdispls, MPI_INT, comm);

  /* Build lists of new indices */
  GArray* new_indices[psize]; /* list of vertices */
  for (int p = 0; p < psize; p++) {
    new_indices[p] = g_array_sized_new(FALSE, FALSE, sizeof(int), (total_nvtxs+psize-1)/psize);
  }
  for (int v = 0; v < total_nvtxs; v++) {
    g_array_append_val(new_indices[gpart[v]], v);
  }
  free(gpart);

  /* Merge arrays to make one permutation array */
  /* Change vtxdist */
  assert(total_nvtxs > 0);
  int *permutations = (int *)malloc(sizeof(int[total_nvtxs]));
  for (int p = 0; p < psize; p++) {
    int n = new_indices[p]->len;
    memcpy(permutations+vtxdist[p], new_indices[p]->data, sizeof(int[n]));
    vtxdist[p+1] = vtxdist[p]+n;
    g_array_free(new_indices[p], TRUE);
  }
  assert(vtxdist[psize] == total_nvtxs);

  /* Permutations was the inverse one, so it is okay only for inverse case */
  if (!inverse) {
    int *inverse_permut = (int *)malloc(sizeof(int[total_nvtxs]));
    for (int v = 0; v < total_nvtxs; ++v) {
      inverse_permut[permutations[v]] = v;
    }
    free(permutations);
    permutations = inverse_permut;
  }

  /* In case of inverse, since we use gather to share data, we have to permut
   * received vertices to follow permutations */
  int *recv_permutations = NULL;
  if (!inverse) {
    free(recv_partdispls);

  } else {
    /* First we set new vtxdist, then we can find part used for distributing vertices */
    part = (int *)malloc(sizeof(int[nvtxs]));
    int *old_vtxdist = g->vtxdist;
    g->vtxdist = recv_partdispls;
    vtxdist = g->vtxdist;
    int new_nvtxs = vtxdist[prank+1]-vtxdist[prank];
    int need_recv_permutations = 0;

    recv_permutations = (int *)malloc(sizeof(int[new_nvtxs]));

    for (int p = 0; p < psize; ++p) {
      int min = vtxdist[p];
      int max = vtxdist[p+1];
      int idx = 0;
      for (int v = 0; v < nvtxs; v++) {
        if (permutations[v+old_vtxdist[prank]] >= min && permutations[v+old_vtxdist[prank]] < max) {
          part[v] = p;
        }
      }
      for (int v = 0; v < total_nvtxs; v++) {
        if (permutations[v] >= min && permutations[v] < max) {
          /* we build permutation to apply on reception buffer */
          if (p == prank) {
            recv_permutations[permutations[v]-min] = idx;
            if (idx != permutations[v]-min)
              need_recv_permutations = 1;
            idx++;
          }
        }
      }
    }
    if (!need_recv_permutations) {
      free(recv_permutations);
      recv_permutations = NULL;
    }
  }

  /**************************/
  /* Each rank receive its data given by part */
  for (int p = 0; p < psize; p++) {
    GArray* sent_adjncy = g_array_sized_new (FALSE, FALSE, sizeof(int), g->narcs);
    GArray* sent_adjcnt = g_array_sized_new (FALSE, FALSE, sizeof(int), nvtxs);
    GArray* sent_vwgts = NULL;
    GArray* sent_ewgts = NULL;

    if (g->vwgts) {
      sent_vwgts = g_array_sized_new (FALSE, FALSE, sizeof(int), nvtxs);
    }
    if (g->ewgts) {
       sent_ewgts = g_array_sized_new (FALSE, FALSE, sizeof(int), g->narcs);
    }

    /* From part, we build arrays to send */
    for (int i = 0; i < nvtxs; i++) {
      if (part[i] == p) {
        int adjcnt = g->xadj[i+1]-g->xadj[i];
        g_array_append_val(sent_adjcnt, adjcnt);
        g_array_append_vals(sent_adjncy, &g->adjncy[g->xadj[i]], adjcnt);

        if (sent_vwgts) {
          g_array_append_val(sent_vwgts, g->vwgts[i]);
        }
        if (sent_ewgts) {
          g_array_append_vals(sent_ewgts, &g->ewgts[g->xadj[i]], adjcnt);
        }
      }
    }

    /* Receive adjcnt */
    /* First we need to compute recvcounts (recv_nvtxs) and displs (recv_displvtxs) */
    int recv_nvtxs[psize];
    int sent_nvtxs = sent_adjcnt->len;
    MPI_Gather(&sent_nvtxs, 1, MPI_INT, recv_nvtxs, 1, MPI_INT, p, comm);

    int recv_displvtxs[psize+1];
    int *recv_adjcnt = NULL;
    if (prank == p) {
      recv_displvtxs[0] = 0;
      for (int i = 0; i < psize; i++) {
        recv_displvtxs[i+1] = recv_displvtxs[i]+recv_nvtxs[i];
      }

      new_nvtxs = recv_displvtxs[psize];
      recv_adjcnt = (int*)malloc(sizeof(int[new_nvtxs]));

      if (sent_vwgts) {
        new_vwgts = (int*)malloc(sizeof(int[new_nvtxs]));
      }
    }

    MPI_Gatherv(sent_adjcnt->data, sent_nvtxs, MPI_INT, recv_adjcnt,
        recv_nvtxs, recv_displvtxs, MPI_INT, p, comm);
    g_array_free(sent_adjcnt, TRUE);

    /* vwgts */
    /* Check if wgts exists somewhere */
    int hasvwgts;
    MPI_Allreduce(&g->vwgts, &hasvwgts, 1, MPI_INT, MPI_SUM, comm);
    if (hasvwgts) {
      MPI_Gatherv(sent_vwgts->data, sent_nvtxs, MPI_INT, new_vwgts,
          recv_nvtxs, recv_displvtxs, MPI_INT, p, comm);
      g_array_free(sent_vwgts, TRUE);
    }

    /* Compute new_xadj */
    if (prank == p) {
      new_xadj = (int*)malloc(sizeof(int[new_nvtxs+1]));
      new_xadj[0] = 0;
      for (int i = 0; i < new_nvtxs; i++) {
        new_xadj[i+1] = new_xadj[i]+recv_adjcnt[i];
      }
    }
    free(recv_adjcnt);

    /* Receive adjncy */
    /* First we need to compute recvcounts (recv_nadjncy) and displs (recv_displadjncy) */
    int recv_nadjncy[psize];
    int recv_displadjncy[psize+1];
    if (prank == p) {
      /* Compute recv_nadjncy */
      recv_displadjncy[0] = 0;
      for (int i = 0; i < psize; i++) {
        recv_displadjncy[i+1] = new_xadj[recv_displvtxs[i+1]];
        recv_nadjncy[i] = recv_displadjncy[i+1]-recv_displadjncy[i];
      }
    }

    if (prank == p) {
      new_adjncy = (int *)malloc(sizeof(int[recv_displadjncy[psize]]));
    }
    MPI_Gatherv(sent_adjncy->data, sent_adjncy->len, MPI_INT, new_adjncy,
        recv_nadjncy, recv_displadjncy, MPI_INT, p, comm);
    g_array_free(sent_adjncy, TRUE);

    if (sent_ewgts) {
      if (prank == p) {
        new_ewgts = (int *)malloc(sizeof(int[recv_displadjncy[psize]]));
      }
      MPI_Gatherv(sent_ewgts->data, sent_adjncy->len, MPI_INT, new_ewgts,
          recv_nadjncy, recv_displadjncy, MPI_INT, p, comm);
      g_array_free(sent_ewgts, TRUE);
    }
  }

  if (inverse) {
    free(part);
  }

  /* Apply permutation in adjncy */
  assert(new_xadj);
  new_narcs = new_xadj[new_nvtxs];
  for (int e = 0; e < new_narcs; e++) {
    new_adjncy[e] = permutations[new_adjncy[e]];
  }

  /* For inverse */
  if (recv_permutations) {
    int *tmp_xadj = (int *)malloc(sizeof(int[new_nvtxs+1]));
    int *tmp_narcs = (int *)malloc(sizeof(int[new_nvtxs]));
    int *tmp_vwgts = NULL;
    if (new_vwgts) {
      tmp_vwgts = (int *)malloc(sizeof(int[new_nvtxs]));
    }

    for (int v = 0; v < new_nvtxs; ++v) {
      int dest = recv_permutations[v];
      // for now, only size, fixed after that
      tmp_narcs[v] = new_xadj[dest+1]-new_xadj[dest];
      if (new_vwgts) {
        tmp_vwgts[v] = new_vwgts[dest];
      }
    }

    tmp_xadj[0] = 0;
    for (int v = 0; v < new_nvtxs; ++v) {
      tmp_xadj[v+1] = tmp_xadj[v]+tmp_narcs[v];
    }

    int *tmp_adjncy = (int *)malloc(sizeof(int[new_narcs]));
    int *tmp_ewgts = NULL;
    if (new_ewgts) {
      tmp_ewgts = (int *)malloc(sizeof(int[new_narcs]));
    }
    for (int v = 0; v < new_nvtxs; ++v) {
      int dest = recv_permutations[v];
      memcpy(tmp_adjncy+tmp_xadj[v],
          new_adjncy+new_xadj[dest],
          sizeof(int[tmp_narcs[v]]));
      if (new_ewgts) {
        memcpy(tmp_ewgts+tmp_xadj[v],
            new_ewgts+new_xadj[dest],
            sizeof(int[tmp_narcs[v]]));
      }
    }
    free(tmp_narcs);

    free(new_xadj);
    new_xadj = tmp_xadj;
    free(new_adjncy);
    new_adjncy = tmp_adjncy;

    if (new_vwgts) {
      free(new_vwgts);
      new_vwgts = tmp_vwgts;
    }
    if (new_ewgts) {
      free(new_ewgts);
      new_ewgts = tmp_ewgts;
    }

    free(recv_permutations);
  }

  /* Copy data back to graph */
  g->nvtxs = new_nvtxs;
  g->narcs = new_narcs;
  free(g->xadj);
  g->xadj = new_xadj;
  free(g->adjncy);
  g->adjncy = new_adjncy;
  g->ewgts = new_ewgts;
  g->vwgts = new_vwgts;

  free(permutations);
}

/* *********************************************************** */

void StarPart_GATHERP_init(StarPart_Method* method)
{
  assert(method);
  StarPart_Context* ctx = StarPart_getMethodContext(method);
  assert(StarPart_isParallelContext(ctx));

  StarPart_Data* distdata = StarPart_addMethodArg(method, "distdata", STARPART_IN);
  StarPart_addNewItem(distdata, "part", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(distdata, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(distdata, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);

  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_OUT | STARPART_MASTER);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NULL, true, STARPART_WRITE);
}

/* Gather partition only */
void StarPart_GATHERP_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  assert(StarPart_isParallelContext(ctx));
  MPI_Comm comm = StarPart_getParallelComm(ctx);
  int prank = StarPart_getParallelCommRank(ctx);
  int psize = StarPart_getParallelCommSize(ctx);
  assert(psize >= 2);

  /* distributed data (input) */
  StarPart_Data* distdata = StarPart_getTaskData(task, "distdata");
  int* part = StarPart_getIntArray(distdata, "part");
  int nparts = StarPart_getInt(distdata, "nparts");
  int partSize = StarPart_getArrayLength(distdata, "part");

  /* Get recvcounts and displs */
  int *recv_partSize = NULL;
  if (prank == 0) {
    recv_partSize = (int *)malloc(sizeof(int[psize]));
  }
  MPI_Gather(&partSize, 1, MPI_INT, recv_partSize, 1, MPI_INT, 0, comm);
  /* Build displs */
  int *recv_displs = NULL;
  if (prank == 0) {
    recv_displs = (int *)malloc(sizeof(int[psize+1]));
    recv_displs[0] = 0;
    for (int p = 0; p < psize; ++p) {
      recv_displs[p+1] = recv_displs[p]+recv_partSize[p];
    }
  }

  /* Gather partition on root */
  int *parts = NULL;
  if (prank == 0) {
    parts = (int *)malloc(sizeof(int[recv_displs[psize]]));
  }
  MPI_Gatherv(part, partSize, MPI_INT, parts, recv_partSize, recv_displs, MPI_INT,
      0, comm);


  if (prank == 0) {
    StarPart_Data* data = StarPart_getTaskData(task, "data");
    StarPart_setIntArray(data, "part", recv_displs[psize], parts, true);
    StarPart_setInt(data, "nparts", nparts);
  }

}

/* *********************************************************** */

void StarPart_RANDOMP_init(StarPart_Method* method)
{
  assert(method);
  StarPart_Context* ctx = StarPart_getMethodContext(method);
  assert(StarPart_isParallelContext(ctx));

  StarPart_Data* data = StarPart_addMethodArg(method, "distdata", STARPART_INOUT);
  StarPart_addNewItem(data, "distgraph", STARPART_DISTGRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_WRITE);
}

/* *********************************************************** */

void StarPart_RANDOMP_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  assert(StarPart_isParallelContext(ctx));
  int prank = StarPart_getParallelCommRank(ctx);
  int psize = StarPart_getParallelCommSize(ctx);
  assert(psize >= 2);

  StarPart_Data* data = StarPart_getTaskData(task, "distdata");  // distributed data
  LibGraph_DistGraph* g = StarPart_getDistGraph(data, "distgraph");
  int* part = StarPart_getIntArray(data, "part");


  int nparts;
  if (StarPart_hasItemValue(data, "nparts")) {
    nparts = StarPart_getInt(data, "nparts");
  } else {
    nparts = psize;
  }

  assert(g && nparts > 0);

  StarPart_setInt(data, "nparts", nparts);
  StarPart_setInt(data, "ubfactor", 100);

  if (part) {
    StarPart_checkArray(data, "part", g->nvtxs);
  } else {
    if (g->nvtxs) {
      part = StarPart_allocArray(data, "part", g->nvtxs);
      StarPart_initArray(data, "part", -1);
    } else {
      part = NULL;
    }
  }

  int nvtxs = g->vtxdist[prank+1]-g->vtxdist[prank];
  assert(g->nvtxs == nvtxs);

  for (int i = 0; i < nvtxs; ++i) {
    part[i] = rand()%psize;
  }
}

void StarPart_GRAPHTODISTGRAPH_init(StarPart_Method* method)
{
  assert(method);
  StarPart_Context* ctx = StarPart_getMethodContext(method);
  assert(StarPart_isParallelContext(ctx));
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_IN | STARPART_MASTER);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);

  StarPart_Data* distdata = StarPart_addMethodArg(method, "distdata", STARPART_OUT);
  StarPart_addNewItem(distdata, "distgraph", STARPART_DISTGRAPH, NULL, true, STARPART_WRITE);
}

/* *********************************************************** */

/* graph to distgraph with all vertices in root */
void StarPart_GRAPHTODISTGRAPH_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  assert(StarPart_isParallelContext(ctx));

  MPI_Comm comm = StarPart_getParallelComm(ctx);
  int prank = StarPart_getParallelCommRank(ctx);
  int psize = StarPart_getParallelCommSize(ctx);
  assert(psize >= 2);

  StarPart_Data* data = NULL;
  LibGraph_Graph* g = NULL;
  if (prank == 0) {
    data = StarPart_getTaskData(task, "data");
    g = StarPart_getGraph(data, "graph");
  }

  StarPart_Data* distdata = StarPart_getTaskData(task, "distdata");
  LibGraph_DistGraph* gg = StarPart_newDistGraph();  // distributed graph
  StarPart_setDistGraph(distdata, "distgraph", gg, true);

  int* vtxdist = (int *)malloc(sizeof(int[psize+1]));
  if (prank == 0) {
    gg->nvtxs = g->nvtxs;
    gg->narcs = g->narcs;

    gg->xadj = (int *)malloc(sizeof(int[g->nvtxs+1]));
    memcpy(gg->xadj, g->xadj, sizeof(int[g->nvtxs+1]));
    gg->adjncy = (int *)malloc(sizeof(int[g->narcs]));
    memcpy(gg->adjncy, g->adjncy, sizeof(int[g->narcs]));
    if (g->vwgts) {
      gg->vwgts = (int *)malloc(sizeof(int[g->nvtxs]));
      memcpy(gg->vwgts, g->vwgts, g->nvtxs);
    } else {
      gg->vwgts = NULL;
    }

    if (g->ewgts) {
      gg->ewgts = (int *)malloc(sizeof(int[g->narcs]));
      memcpy(gg->ewgts,g->ewgts, g->narcs);
    } else {
      gg->ewgts = NULL;
    }

     vtxdist[0] = 0;
     for (int p = 0; p < psize; p++) {
       vtxdist[p+1] = g->nvtxs;
     }

  } else {
    gg->nvtxs = 0;
    gg->narcs = 0;
    gg->xadj = NULL;
    gg->adjncy = NULL;
    gg->vwgts = NULL;
    gg->ewgts = NULL;
  }

  MPI_Bcast(vtxdist, psize+1, MPI_INT, 0, comm);
  gg->vtxdist = vtxdist;
}

/* *********************************************************** */

void StarPart_BARRIER_init(StarPart_Method* method)
{
}

void StarPart_BARRIER_call(StarPart_Task* task)
{
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  MPI_Comm comm = StarPart_getParallelComm(ctx);
  MPI_Barrier(comm);
}

/* *********************************************************** */

void StarPart_LOADP_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "filename", STARPART_STR, NULL, true, STARPART_READ);
  StarPart_addNewItem(local, "coords", STARPART_STR, NULL, true, STARPART_READ);
  StarPart_addNewItem(local, "format", STARPART_STR, NEW_STR("metis"), true, STARPART_READ);
  StarPart_Data* distdata = StarPart_addMethodArg(method, "distdata", STARPART_OUT);
  StarPart_addNewItem(distdata, "distgraph", STARPART_DISTGRAPH, NULL, true, STARPART_WRITE);
}

/* *********************************************************** */

void StarPart_LOADP_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  assert(StarPart_isParallelContext(ctx));
  int prank = StarPart_getParallelCommRank(ctx);
  int psize = StarPart_getParallelCommSize(ctx);

  StarPart_Data* local = StarPart_getTaskLocal(task);
  char* filename = StarPart_getStr(local, "filename");
  char* coordfile = StarPart_getStr(local, "coords");
  char* format = StarPart_getStr(local, "format");
  StarPart_checkStr(local, "format", "metis,scotch,mm,rb,mesh");
  StarPart_Data* distdata = StarPart_getTaskData(task, "distdata");

  LibGraph_DistGraph* g = NULL;

  double *coords = NULL;
  int r = LibGraph_loadParallelGraph(filename, coordfile, format, prank, psize, &g, &coords);
  assert(r);

  StarPart_setDistGraph(distdata, "distgraph", g, true);
  if (coords) {
    StarPart_addNewItem(distdata, "coords", STARPART_DOUBLEARRAY, NULL, true, STARPART_WRITE);
    StarPart_setDoubleArray(distdata, "coords", 3*g->nvtxs, coords, true);
  }
}
