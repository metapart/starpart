#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "all.h"
#include "hun.h"
#include "libgraph.h"
#include "starpart.h"

/* *********************************************************** */

void StarPart_HUN_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "pstrat", STARPART_STRAT, NULL, true, STARPART_READ);
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_HUN_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  char* pstrat = StarPart_getStrat(local, "pstrat");
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int nparts = StarPart_getInt(data, "nparts");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int* fixed = StarPart_getIntArray(data, "fixed");
  int* part = StarPart_getIntArray(data, "part");

  assert(g && fixed);
  StarPart_checkArray(data, "fixed", g->nvtxs);
  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else
    part = StarPart_allocArray(data, "part", g->nvtxs);
  for (int i = 0; i < g->nvtxs; i++) part[i] = fixed[i];

  /* ********* remove fixed vertices ********* */

  LibGraph_Graph gg;
  int nfreevtxs = 0;
  int* freevtxs = malloc(g->nvtxs * sizeof(int));  // max size
  assert(freevtxs);
  for (int i = 0; i < g->nvtxs; i++)
    if (part[i] == -1) freevtxs[nfreevtxs++] = i;  // freevtxs[new_vtx_index] = former_vtx_index;
  LibGraph_subGraphFromSet(g, nfreevtxs, freevtxs, &gg);
  assert(gg.nvtxs == nfreevtxs && gg.nvtxs <= g->nvtxs);
  assert(gg.narcs <= g->narcs);
  // LibGraph_checkGraph(&gg, true); // debug

  /* ********* partition it ********* */

  // partition free vertices (here, we do not take fixed vertices into account)

  int* _part = malloc(gg.nvtxs * sizeof(int));
  assert(_part);
  assert(gg.nvtxs == nfreevtxs);
  for (int i = 0; i < gg.nvtxs; i++) _part[i] = -1;  // all is free

  /* process data in nested context */

  StarPart_Data* _data = StarPart_newData("@0", 0);
  StarPart_addNewItem(_data, "graph", STARPART_GRAPH, &gg, false, STARPART_READ);
  StarPart_addNewItem(_data, "part", STARPART_INTARRAY, NEW_INTARRAY(gg.nvtxs, _part), true, STARPART_READWRITE);
  StarPart_addNewItem(_data, "nparts", STARPART_INT, NEW_INT(nparts), true, STARPART_READ);
  StarPart_addNewItem(_data, "ubfactor", STARPART_INT, NEW_INT(ubfactor), true, STARPART_READ);

  StarPart_Context* ctx = StarPart_getTaskContext(task);
  StarPart_Context* _ctx = StarPart_newNestedContext(ctx);
  StarPart_registerData(_ctx, _data);
  StarPart_parseStrat(_ctx, pstrat);
  StarPart_submitAllTasks(_ctx);
  StarPart_connectAllTasks(_ctx, "@0", NULL);
  StarPart_run(_ctx);
  StarPart_freeAllTasks(_ctx);
  StarPart_freeNestedContext(_ctx);

  /* ********* solve bipartite graph matching ********* */

  // compute bipartite graph:
  // special quotient graph with 2*nparts vertices, that represents both free and fixed parts...

  for (int i = 0; i < g->nvtxs; i++) assert(part[i] >= -1 && part[i] < nparts);   // check
  for (int i = 0; i < gg.nvtxs; i++) assert(_part[i] >= 0 && _part[i] < nparts);  // check

  LibGraph_Graph q;
  int* tmp = malloc(g->nvtxs * sizeof(int));
  for (int i = 0; i < g->nvtxs; i++)               // for fixed vtxs
    if (part[i] != -1) tmp[i] = part[i] + nparts;  // shift index for fixed vtxs
  for (int i = 0; i < gg.nvtxs; i++) {             // for free vtxs
    int ii = freevtxs[i];                          // vtx i in gg is mapped to vtx ii in g
    assert(ii >= 0 && ii < g->nvtxs);
    tmp[ii] = _part[i];
  }
  for (int i = 0; i < g->nvtxs; i++) assert(tmp[i] >= 0 && tmp[i] < 2 * nparts);  // check
  LibGraph_quotientGraph(g, 2 * nparts, tmp, &q);
  assert(q.ewgts);
  // LibGraph_checkGraph(&q); // debug
  // printf("Q: "); LibGraph_printGraph(&q); // debug

  const int maximum = 1;
  int* perm = LibGraph_bipartiteGraphMatching(&q, nparts, maximum);

  /* ********* perform permutation ********* */

  // perform the permutation on free vtxs (only)
  for (int i = 0; i < gg.nvtxs; i++) {
    int ii = freevtxs[i];       // vtx i in subg is mapped to vtx ii in g
    part[ii] = perm[_part[i]];  // apply permutation
  }

  /* free */
  LibGraph_freeGraph(&gg);
  LibGraph_freeGraph(&q);
  free(perm);
  free(freevtxs);
  free(_part);
  free(tmp);
}

/* *********************************************************** */
