#define _GNU_SOURCE
#include <assert.h>
#include <fcntl.h>
#include <glib.h>
#include <math.h>
#include <setjmp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "all.h"
#include "basic.h"
#include "libgraph.h"
#include "starpart.h"

#define TIMEOUT 60  // in seconds
#define LOG_FILE "bench.log"
// #define LOG_FILE "/dev/null"

/* *********************************************************** */
/*                         BENCH                               */
/* *********************************************************** */

static void _bench(char* file, char* strat, int ubfactor, int nparts, int seed, LibGraph_Graph* g,
                   LibGraph_Hypergraph* hg, int* part, double* ub, int* ec, double* exectime)
{
  /* input data */
  StarPart_Data* _data = StarPart_newData("@0", 0);
  StarPart_addNewItem(_data, "graph", STARPART_GRAPH, g, false, 0);
  StarPart_addNewItem(_data, "hypergraph", STARPART_HYPERGRAPH, hg, false, 0);
  StarPart_addNewItem(_data, "part", STARPART_INTARRAY, NEW_INTARRAY(g->nvtxs, part), true, 0);
  StarPart_addNewItem(_data, "nparts", STARPART_INT, NEW_INT(nparts), true, 0);
  StarPart_addNewItem(_data, "ubfactor", STARPART_INT, NEW_INT(ubfactor), true, 0);
  StarPart_addNewItem(_data, "seed", STARPART_INT, NEW_INT(seed), true, 0);

  /* reset part */
  for (int i = 0; i < g->nvtxs; i++) part[i] = -1;

  /* intialize random seed */
  srand(seed);

  /* process strat */
  StarPart_Context* ctx = StarPart_getCurrentContext();  // BUG: why this?
  StarPart_Context* _ctx = StarPart_newNestedContext(ctx);
  StarPart_registerData(_ctx, _data);
  char* id = StarPart_getDataID(_data);

  // StarPart_newTask(_ctx, strat, "#in<@0");
  StarPart_parseStrat(_ctx, strat);
  StarPart_submitAllTasks(_ctx);
  StarPart_connectAllTasks(_ctx, id, NULL);

  struct timeval start;
  gettimeofday(&start, NULL); /* start timer */
  StarPart_run(_ctx);
  struct timeval stop;
  gettimeofday(&stop, NULL); /* stop timer */
  StarPart_freeAllTasks(_ctx);
  StarPart_freeNestedContext(_ctx);

  /* check */
  for (int i = 0; i < g->nvtxs; i++) assert(part[i] >= 0 && part[i] < nparts);

  /* statistic */
  *ub = LibGraph_computeGraphMaxUnbalance(g, nparts, part) * 100.0;
  *ec = LibGraph_computeGraphEdgeCut(g, nparts, part);
  *exectime = ((stop.tv_sec * 1000.0 * 1000.0 + stop.tv_usec) / 1000.0) -
              ((start.tv_sec * 1000.0 * 1000.0 + start.tv_usec) / 1000.0);  // in ms
}

/* *********************************************************** */

static sigjmp_buf buf;
struct sigaction sa;
char* str_error[] = {"OK", "SIGNAL", "TIMEOUT"};

static void handler(int sig)
{
  fprintf(stderr, "Signal %s received!\n", g_strsignal(sig));
  // if(sig == SIGABRT) sigaction(SIGABRT, &sa, NULL); // special abort behaviour!!!
  if (sig == SIGALRM) siglongjmp(buf, 2);
  siglongjmp(buf, 1);
}

/* *********************************************************** */

static void bench(FILE* mystdout, char* file, char* strat, int ubfactor, int nparts, int seed, LibGraph_Graph* g,
                  LibGraph_Hypergraph* hg, int* part)
{
  /* error management */
  sa.sa_handler = handler;
  sa.sa_flags = 0;
  sigemptyset(&sa.sa_mask);
  sigaction(SIGSEGV, &sa, NULL);
  sigaction(SIGFPE, &sa, NULL);
  sigaction(SIGABRT, &sa, NULL);
  sigaction(SIGBUS, &sa, NULL);
  sigaction(SIGQUIT, &sa, NULL);
  sigaction(SIGALRM, &sa, NULL);
  sigprocmask(SIG_SETMASK, &sa.sa_mask, NULL);

  /* call bench routine */
  double ub = NAN;
  int ec = -1;
  double exectime = NAN;
  int error = sigsetjmp(buf, 1);  // return 0 if OK, 1 if signal and 2 if timeout
  alarm(TIMEOUT);
  if (!error) { _bench(file, strat, ubfactor, nparts, seed, g, hg, part, &ub, &ec, &exectime); }

  /* print results */
  fprintf(mystdout, "%s %s %d %d %d %.2f %d %.2f %s\n", file, strat, ubfactor, nparts, seed, ub, ec, exectime,
          str_error[error]);
}

/* *********************************************************** */

void StarPart_BENCH_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "files", STARPART_STRARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(local, "strats", STARPART_STRARRAY, NEW_STRARRAYV(1, "BASIC/RANDOM"), true, STARPART_READ);
  StarPart_addNewItem(local, "seeds", STARPART_INTARRAY, NEW_INTARRAYV(5, 1, 2, 3, 4, 5), true, STARPART_READ);
  StarPart_addNewItem(local, "ubfactors", STARPART_INTARRAY, NEW_INTARRAYV(1, 5), true, STARPART_READ);
  StarPart_addNewItem(local, "nparts", STARPART_INTARRAY, NEW_INTARRAYV(4, 2, 4, 8, 16), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_BENCH_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  char** files_array = StarPart_getStrArray(local, "files");
  char** strats_array = StarPart_getStrArray(local, "strats");
  int* seeds_array = StarPart_getIntArray(local, "seeds");
  int* ubfactors_array = StarPart_getIntArray(local, "ubfactors");
  int* nparts_array = StarPart_getIntArray(local, "nparts");
  int files_length = StarPart_getArrayLength(local, "files");
  int strats_length = StarPart_getArrayLength(local, "strats");
  int seeds_length = StarPart_getArrayLength(local, "seeds");
  int ubfactors_length = StarPart_getArrayLength(local, "ubfactors");
  int nparts_length = StarPart_getArrayLength(local, "nparts");

  /* run all exp in a child process */
  pid_t pid = fork();
  if (pid == 0) {
    /* redirect stdout & stderr */
    int myout = dup(1);
    int redirect = open(LOG_FILE, O_WRONLY | O_CREAT | O_TRUNC, 0644);
    assert(redirect > 0);
    dup2(redirect, 1);
    dup2(redirect, 2);
    close(redirect);
    FILE* mystdout = fdopen(myout, "w");

    fprintf(mystdout, "# stdout & stderr redirected in \"%s\"\n", LOG_FILE);
    fprintf(mystdout, "# file strat ubfactor nparts seed ub ec time(ms) error\n");

    /* for all graph files */
    for (int i0 = 0; i0 < files_length; i0++) {
      LibGraph_Graph* g = StarPart_newGraph();
      assert(g);
      int r = LibGraph_loadMetisGraph(files_array[i0], g);
      assert(r);
      LibGraph_Hypergraph* hg = StarPart_newHypergraph();
      assert(hg);
      LibGraph_graph2HypergraphV(g, hg);
      int* part = malloc(g->nvtxs * sizeof(int));
      assert(part);
      /* for all partitioning strategies */
      for (int i1 = 0; i1 < strats_length; i1++) {
        /* for all imbalance factors */
        for (int i2 = 0; i2 < ubfactors_length; i2++) {
          /* for all desired nb parts */
          for (int i3 = 0; i3 < nparts_length; i3++) {
            /* for all random seeds */
            for (int i4 = 0; i4 < seeds_length; i4++) {
              bench(mystdout, files_array[i0], strats_array[i1], ubfactors_array[i2], nparts_array[i3], seeds_array[i4],
                    g, hg, part);
            }
          }
        }
      }
      LibGraph_freeGraph(g);
      free(g);
      g = NULL;
      LibGraph_freeHypergraph(hg);
      free(hg);
      hg = NULL;
      free(part);
    }

    fclose(mystdout);
    /* free memory in child process */
    StarPart_freeAllTasks(ctx);
    StarPart_freeContext(ctx);

    exit(EXIT_SUCCESS);
  }

  /* wait child process */
  int wstatus;
  pid_t p = waitpid(pid, &wstatus, 0);
  assert(p == pid && WIFEXITED(wstatus) && WEXITSTATUS(wstatus) == EXIT_SUCCESS);  // alright
}

/* *********************************************************** */
