#ifndef STARPART_BASIC_H
#define STARPART_BASIC_H

#include "starpart.h"

/** initialize StarPart context with basic methods and tasks */
void StarPart_registerBasic(StarPart_Context* ctx);

#endif
