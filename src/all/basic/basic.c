#define _GNU_SOURCE
#include <assert.h>
#include <glib.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "all.h"
#include "basic.h"
#include "libgraph.h"
#include "starpart.h"

/* *********************************************************** */

/* -1:no seed, -2: random seed, >=0: user seed */
static void setRandomSeed(int seed, void* arg)
{
  // printf("seed = %d\n", seed);
  if (seed >= 0)
    srand(seed);
  else if (seed == -1) {
    /* no seed */
  }
  else if (seed == -2) {
    srand((size_t)arg);  // TODO change this ugly stuff
    // srand(time(NULL));
  }
}

/* *********************************************************** */
/*                         BASIC                               */
/* *********************************************************** */

void StarPart_NOP_init(StarPart_Method* method)
{
  /* nop */
}

/* *********************************************************** */

void StarPart_NOP_call(StarPart_Task* task)
{
  /* nop */
}

/* *********************************************************** */

void StarPart_DUP_init(StarPart_Method* method)
{
  StarPart_addMethodArg(method, "in", STARPART_IN);
  StarPart_addMethodArg(method, "out", STARPART_OUT);
}

/* *********************************************************** */

void StarPart_DUP_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* in = StarPart_getTaskData(task, "in");
  StarPart_Data* out = StarPart_getTaskData(task, "out");
  StarPart_dupItems(in, out, true);  // deep copy of each item
}

/* *********************************************************** */

void StarPart_DATA_init(StarPart_Method* method)
{
  StarPart_addMethodArg(method, "data", STARPART_OUT);
}

/* *********************************************************** */

void StarPart_DATA_call(StarPart_Task* task)
{
  assert(task);
  // StarPart_Data * data = StarPart_getTaskData(task, "data");
}

/* *********************************************************** */

void StarPart_INT_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "key", STARPART_STR, NEW_STR("nparts"), true, STARPART_READ);
  StarPart_addNewItem(local, "val", STARPART_INT, NEW_INT(4), true, STARPART_READ);
  StarPart_addMethodArg(method, "data", STARPART_INOUT);
}

/* *********************************************************** */

void StarPart_INT_call(StarPart_Task* task)
{
  StarPart_Data* local = StarPart_getTaskLocal(task);
  char* key = StarPart_getStr(local, "key");
  int val = StarPart_getInt(local, "val");
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  StarPart_addNewItem(data, key, STARPART_INT, NEW_INT(val), true, 0);
}

/* *********************************************************** */

void StarPart_GRID_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "dimx", STARPART_INT, NEW_INT(100), true, STARPART_READ);
  StarPart_addNewItem(local, "dimy", STARPART_INT, NEW_INT(100), true, STARPART_READ);
  StarPart_addNewItem(local, "dimz", STARPART_INT, NEW_INT(0), true, STARPART_READ);
  StarPart_addNewItem(local, "gcoords", STARPART_INT, NEW_INT(0), true, STARPART_READ);
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_OUT);
  StarPart_addNewItem(data, "mesh", STARPART_MESH, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_WRITE);
}

/* *********************************************************** */

void StarPart_GRID_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  int dimx = StarPart_getInt(local, "dimx");
  int dimy = StarPart_getInt(local, "dimy");
  int dimz = StarPart_getInt(local, "dimz");
  int gcoords = StarPart_getInt(local, "gcoords");
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Mesh* m = StarPart_newMesh();
  StarPart_setMesh(data, "mesh", m, true);
  LibGraph_Graph* g = StarPart_newGraph();
  StarPart_setGraph(data, "graph", g, true);
  if (dimz == 0)
    LibGraph_generateGrid2D(dimx, dimy, 0.0, 1.0, 1.0, m);
  else
    LibGraph_generateGrid3D(dimx, dimy, dimz, 1.0, 1.0, 1.0, m);
  LibGraph_mesh2Graph(m, g);

  if (gcoords == 1) {
    StarPart_addNewItem(data, "coords", STARPART_DOUBLEARRAY, NULL, true, STARPART_WRITE);
    double* coords = StarPart_allocArray(data, "coords", 3 * g->nvtxs);
    /* Compute coords */
    LibGraph_cellCenters(m, coords);
  }

  // LibGraph_graph2HypergraphV(g, hg); /* WARNING: It should be explicitly asked!!! */
}

/* *********************************************************** */

void StarPart_RANDOM_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "seed", STARPART_INT, NEW_INT(-2), true, STARPART_READ);
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NEW_INT(2), true, STARPART_READ);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_RANDOM_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  int seed = StarPart_getInt(local, "seed");

  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  // int ubfactor = StarPart_getInt(data, "ubfactor");
  int nparts = StarPart_getInt(data, "nparts");

  assert(g && g->nvtxs > 0);
  assert(nparts > 0);

  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else
    part = StarPart_allocArray(data, "part", g->nvtxs);
  if (fixed) StarPart_checkArray(data, "fixed", g->nvtxs);
  for (int i = 0; i < g->nvtxs; i++) part[i] = fixed ? fixed[i] : -1;

  LibGraph_Array* part_array = StarPart_getArray(data, "part");
  if (part_array) LibGraph_setArrayFlags(part_array, LIBGRAPH_GRAPH_VERTEX_VAR);

  setRandomSeed(seed, task);
  int* perm = malloc(g->nvtxs * sizeof(int));
  LibGraph_generateRandomOrder(perm, g->nvtxs);
  for (int i = 0; i < g->nvtxs; i++) {
    int v = perm[i];
    if (fixed && fixed[v] != -1)
      part[v] = fixed[v];
    else
      part[v] = i % nparts;
  }
  free(perm);
}

/* *********************************************************** */

void StarPart_CMP_init(StarPart_Method* method)
{
  StarPart_Data* left = StarPart_addMethodArg(method, "left", STARPART_IN);
  StarPart_addNewItem(left, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(left, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(left, "part", STARPART_INTARRAY, NULL, true, STARPART_READ);

  StarPart_Data* right = StarPart_addMethodArg(method, "right", STARPART_IN);
  StarPart_addNewItem(right, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(right, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(right, "part", STARPART_INTARRAY, NULL, true, STARPART_READ);

  StarPart_addMethodArg(method, "out", STARPART_OUT);
}

/* *********************************************************** */

void StarPart_CMP_call(StarPart_Task* task)
{
  assert(task);

  StarPart_Data* left = StarPart_getTaskData(task, "left");
  StarPart_Data* right = StarPart_getTaskData(task, "right");
  StarPart_Data* out = StarPart_getTaskData(task, "out");

  LibGraph_Graph* g0 = StarPart_getGraph(left, "graph");
  int nparts0 = StarPart_getInt(left, "nparts");
  int* part0 = StarPart_getIntArray(left, "part");

  LibGraph_Graph* g1 = StarPart_getGraph(right, "graph");
  int nparts1 = StarPart_getInt(right, "nparts");
  int* part1 = StarPart_getIntArray(right, "part");

  int ec0 = LibGraph_computeGraphEdgeCut(g0, nparts0, part0);
  int ec1 = LibGraph_computeGraphEdgeCut(g1, nparts1, part1);

  if (ec0 <= ec1)
    StarPart_dupItems(left, out, false); /* shallow copy */
  else
    StarPart_dupItems(right, out, false); /* shallow copy */
}

/* *********************************************************** */

void StarPart_DIAG_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_IN);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_DIAG_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  int nparts = StarPart_getInt(data, "nparts");
  int* part = StarPart_getIntArray(data, "part");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  // printf("StarPart diagnostic for data \"%s\"\n",
  // StarPart_getCurrentDataID(ctx));
  LibGraph_printGraphDiagnostic(g, nparts, part);
  if (StarPart_isItem(data, "exectime")) {
    double exectime = StarPart_getDouble(data, "exectime");
    printf("  - execution time = %lf (in ms)\n", exectime);
  }
}

/* *********************************************************** */

void StarPart_G2HG_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "hypergraph", STARPART_HYPERGRAPH, NULL, true, STARPART_READWRITE);
}

/* *********************************************************** */

void StarPart_G2HG_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  LibGraph_Hypergraph* hg = StarPart_getHypergraph(data, "hypergraph");
  assert(g && !hg);  // hg should be NULL, else memory leak
  if (!hg) {
    hg = StarPart_newHypergraph();
    StarPart_setHypergraph(data, "hypergraph", hg, true);
  }
  LibGraph_graph2HypergraphV(g, hg);
}

/* *********************************************************** */

void StarPart_BFS_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "seed", STARPART_INT, NEW_INT(-2), true, STARPART_READ);
  StarPart_addNewItem(local, "scheme", STARPART_STR, NEW_STR("geom"), true, STARPART_READ);

  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NEW_INT(2), true, STARPART_READ);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_BFS_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  int seed = StarPart_getInt(local, "seed");
  char* scheme = StarPart_getStr(local, "scheme");
  StarPart_checkStr(local, "scheme", "random,geom");

  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int nparts = StarPart_getInt(data, "nparts");

  assert(g && g->nvtxs > 0);
  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else
    part = StarPart_allocArray(data, "part", g->nvtxs);
  if (fixed) StarPart_checkArray(data, "fixed", g->nvtxs);

  for (int i = 0; i < g->nvtxs; i++) part[i] = fixed ? fixed[i] : -1;

  // if (!fixed) part[0] = 0; /* seed */
  setRandomSeed(seed, task);
  if (strcmp(scheme, "geom") == 0)
    LibGraph_computeGraphSeeds(g, nparts, 0, part);
  else if (strcmp(scheme, "random") == 0)
    for (int i = 0; i < nparts; i++) part[rand() % g->nvtxs] = i;

  int w = 0; /* total weight */
  for (int i = 0; i < g->nvtxs; i++) w += g->vwgts ? g->vwgts[i] : 1;
  int maxpartsize = ceil((w / nparts) * (1.0 + ubfactor / 100.0));

  /* first pass: the output part can still contain free vertices */
  LibGraph_levelset1(g, nparts, maxpartsize, part);
  /* this second pass will remove all free vertices, but will probably break the ubfactor constraint */
  LibGraph_levelset1(g, nparts, -1, part);

  LibGraph_Array* part_array = StarPart_getArray(data, "part");
  if (part_array) LibGraph_setArrayFlags(part_array, LIBGRAPH_GRAPH_VERTEX_VAR);
}

/* *********************************************************** */

void StarPart_SPLIT_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_IN);
  StarPart_Data* left = StarPart_addMethodArg(method, "left", STARPART_OUT);
  StarPart_Data* right = StarPart_addMethodArg(method, "right", STARPART_OUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "mesh", STARPART_MESH, NULL, true, STARPART_READ);
  StarPart_addNewItem(left, "graph", STARPART_GRAPH, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(left, "mesh", STARPART_MESH, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(left, "map", STARPART_INTARRAY, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(right, "graph", STARPART_GRAPH, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(right, "mesh", STARPART_MESH, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(right, "map", STARPART_INTARRAY, NULL, true, STARPART_WRITE);
}

/* *********************************************************** */

void StarPart_SPLIT_call(StarPart_Task* task)
{
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  StarPart_Data* left = StarPart_getTaskData(task, "left");
  StarPart_Data* right = StarPart_getTaskData(task, "right");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  LibGraph_Mesh* m = StarPart_getMesh(data, "mesh");

  assert(g);

  // TODO: use an imput bi-partition...
  /* compute bisection with BFS */
  int* part = malloc(g->nvtxs * sizeof(int));
  assert(part);
  for (int i = 0; i < g->nvtxs; i++) part[i] = -1;
  part[0] = 0; /* seed */
  LibGraph_levelset1(g, 1, g->nvtxs / 2, part);
  for (int i = 0; i < g->nvtxs; i++)
    if (part[i] == -1) part[i] = 1;

  /* split graph in two sub-graphs */
  int* map = malloc(g->nvtxs * sizeof(int));
  assert(map);
  LibGraph_Graph* lg = StarPart_newGraph();
  LibGraph_Graph* rg = StarPart_newGraph();
  StarPart_setGraph(left, "graph", lg, true);
  StarPart_setGraph(right, "graph", rg, true);
  LibGraph_splitGraph(g, part, lg, rg, map);

  /* compute mapping */
  int lnvtxs = 0, rnvtxs = 0;
  for (int i = 0; i < g->nvtxs; i++) {
    if (part[i] == 0)
      lnvtxs++;
    else if (part[i] == 1)
      rnvtxs++;
  }
  assert(g->nvtxs == lnvtxs + rnvtxs); /* check */

  int* lmap = StarPart_allocArray(left, "map", lnvtxs);
  int* rmap = StarPart_allocArray(right, "map", rnvtxs);

  for (int i = 0, lk = 0, rk = 0; i < g->nvtxs; i++) {
    if (part[i] == 0)
      lmap[lk++] = i;
    else
      rmap[rk++] = i;
  }

  // TODO: how to set flags directly from StarPart... bug here on RW access to map array
  // LibGraph_Array* lmap_array = StarPart_getArray(left, "map");
  // if (lmap_array) LibGraph_setArrayFlags(lmap_array, LIBGRAPH_GRAPH_VERTEX_VAR);
  // LibGraph_Array* rmap_array = StarPart_getArray(right, "map");
  // if (rmap_array) LibGraph_setArrayFlags(rmap_array, LIBGRAPH_GRAPH_VERTEX_VAR);

  /* split mesh in two sub-meshes */
  if (m) {
    LibGraph_Mesh* lm = StarPart_newMesh();
    LibGraph_Mesh* rm = StarPart_newMesh();
    LibGraph_subMeshFromSet(m, lnvtxs, lmap, lm);
    LibGraph_subMeshFromSet(m, rnvtxs, rmap, rm);
    StarPart_setMesh(left, "mesh", lm, true);
    StarPart_setMesh(right, "mesh", rm, true);
  }

  free(part);
  free(map);
}

/* *********************************************************** */

void StarPart_PART_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "strat", STARPART_STRAT, NULL, true, STARPART_READ);
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NEW_INT(2), true, STARPART_READ);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
}

/* *********************************************************** */

void StarPart_PART_call(StarPart_Task* task)
{
  assert(task);

  StarPart_Data* local = StarPart_getTaskLocal(task);
  char* strat = StarPart_getStrat(local, "strat");
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int nparts = StarPart_getInt(data, "nparts");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  assert(g && nparts > 0 && strat && ubfactor >= 0);
  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else
    part = StarPart_allocArray(data, "part", g->nvtxs);
  if (fixed) StarPart_checkArray(data, "fixed", g->nvtxs);
  for (int i = 0; i < g->nvtxs; i++) part[i] = fixed ? fixed[i] : -1;

  LibGraph_Array* part_array = StarPart_getArray(data, "part");
  if (part_array) LibGraph_setArrayFlags(part_array, LIBGRAPH_GRAPH_VERTEX_VAR);

  StarPart_Context* ctx = StarPart_getTaskContext(task);
  StarPart_Context* _ctx = StarPart_newNestedContext(ctx);
  StarPart_registerData(_ctx, data);
  StarPart_parseStrat(_ctx, strat);
  StarPart_submitAllTasks(_ctx);
  char* id = StarPart_getDataID(data);
  StarPart_connectAllTasks(_ctx, id, NULL);
  StarPart_run(_ctx);
  StarPart_freeAllTasks(_ctx);
  StarPart_freeNestedContext(_ctx);

  /* check */
  for (int i = 0; i < g->nvtxs; i++) assert(part[i] >= 0 && part[i] < nparts);
}

void StarPart_PERMUT_init(StarPart_Method* method)
{
  assert(method);
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "inverse", STARPART_INT, NEW_INT(0), true, STARPART_READ);

  StarPart_Data* graphData = StarPart_addMethodArg(method, "graphData", STARPART_INOUT);
  StarPart_addNewItem(graphData, "graph", STARPART_GRAPH, NULL, true, STARPART_READWRITE);

  StarPart_Data* partData = StarPart_addMethodArg(method, "partData", STARPART_IN);
  StarPart_addNewItem(partData, "part", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(partData, "nparts", STARPART_INT, NULL, true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_PERMUT_call(StarPart_Task* task)
{
  assert(task);

  StarPart_Data* local = StarPart_getTaskLocal(task);
  int inverse = StarPart_getInt(local, "inverse");

  StarPart_Data* graphData = StarPart_getTaskData(task, "graphData");
  LibGraph_Graph* g = StarPart_getGraph(graphData, "graph");
  int nvtxs = g->nvtxs;

  StarPart_Data* partData = StarPart_getTaskData(task, "partData");
  int* part = StarPart_getIntArray(partData, "part");
  int nparts = StarPart_getInt(partData, "nparts");

  StarPart_checkArray(partData, "part", nvtxs);

  int* new_xadj = NULL;
  int* new_adjncy = NULL;
  int* new_ewgts = NULL;
  int* new_vwgts = NULL;

  /* Build lists of new indices */
  GArray* new_indices[nparts]; /* list of vertices */
  for (int p = 0; p < nparts; p++) {
    new_indices[p] = g_array_sized_new(FALSE, FALSE, sizeof(int), (nvtxs + nparts - 1) / nparts);
  }
  for (int v = 0; v < nvtxs; v++) { g_array_append_val(new_indices[part[v]], v); }

  /* Merge arrays to make one permutation array */
  /* Change vtxdist */
  assert(nvtxs > 0);
  int* permutations = (int*)malloc(sizeof(int[nvtxs]));
  int offset = 0;
  for (int p = 0; p < nparts; p++) {
    int n = new_indices[p]->len;
    memcpy(permutations + offset, new_indices[p]->data, sizeof(int[n]));
    offset += n;
    g_array_free(new_indices[p], TRUE);
  }

  /* Inverse permut */
  int* inverse_permut = (int*)malloc(sizeof(int[nvtxs]));
  for (int v = 0; v < nvtxs; ++v) { inverse_permut[permutations[v]] = v; }

  if (inverse) {
    int* temp = inverse_permut;
    inverse_permut = permutations;
    permutations = temp;
  }

  /* Compute the number of arcs by vertex */
  int* narcs_by_vertex = (int*)malloc(sizeof(int[nvtxs]));
  for (int v = 0; v < nvtxs; ++v) { narcs_by_vertex[v] = g->xadj[v + 1] - g->xadj[v]; }

  /* Permut data */
  new_xadj = (int*)malloc(sizeof(int[nvtxs + 1]));
  new_adjncy = (int*)malloc(sizeof(int[g->xadj[nvtxs]]));
  if (g->vwgts) { new_vwgts = (int*)malloc(sizeof(int[nvtxs])); }
  else {
    new_vwgts = NULL;
  }
  if (g->ewgts) { new_ewgts = (int*)malloc(sizeof(int[g->narcs])); }
  else {
    new_ewgts = NULL;
  }
  new_xadj[0] = 0;
  for (int v = 0; v < nvtxs; ++v) {
    int old_v = permutations[v];
    int narcs = narcs_by_vertex[old_v];
    new_xadj[v + 1] = new_xadj[v] + narcs;

    for (int i = 0; i < narcs; ++i) { new_adjncy[new_xadj[v] + i] = inverse_permut[g->adjncy[g->xadj[old_v] + i]]; }

    if (new_vwgts) { new_vwgts[v] = g->vwgts[old_v]; }
    if (new_ewgts) { memcpy(new_ewgts + new_xadj[v], g->ewgts + g->xadj[old_v], sizeof(int[narcs])); }
  }

  /* Copy data back to graph */
  free(g->xadj);
  g->xadj = new_xadj;
  free(g->adjncy);
  g->adjncy = new_adjncy;
  g->ewgts = new_ewgts;
  g->vwgts = new_vwgts;

  free(permutations);
  free(inverse_permut);
  free(narcs_by_vertex);
}

/* *********************************************************** */

void StarPart_INTARRAY_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "filename", STARPART_STR, NULL, true, 0);
  StarPart_addNewItem(data, "first", STARPART_INT, NEW_INT(0), true, 0);
  StarPart_addNewItem(data, "last", STARPART_INT, NULL, true, 0);
  StarPart_addNewItem(data, "array", STARPART_INTARRAY, NULL, true, 0);
}

/* *********************************************************** */

void StarPart_INTARRAY_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  char* filename = StarPart_getStr(data, "filename");
  int length = 0;
  int* array = NULL;

  if (filename) {
    FILE* file = fopen(filename, "r");
    if (!file) StarPart_error("fail to load \"%s\"!\n", filename);
    int tmp;
    while (fscanf(file, "%d", &tmp) > 0) length++;
    // printf("length = %d\n", length);
    array = calloc(length, sizeof(int));
    assert(array);
    rewind(file);
    for (int i = 0; i < length; i++) fscanf(file, "%d", array + i);
    // for(int i = 0 ; i < length; i++) printf("%d ", array[i]);
    // printf("\n");
    fclose(file);
  }
  else {
    int first = StarPart_getInt(data, "first");
    int last = StarPart_getInt(data, "last");
    assert(last > 0 && last >= first);
    length = last - first + 1;
    array = calloc(length, sizeof(int));
    assert(array);
    for (int i = first, k = 0; i <= last; i++) array[k++] = i;
  }

  StarPart_setIntArray(data, "array", length, array, true);
}

/* *********************************************************** */

void StarPart_FIXED_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "bubblesize", STARPART_INT, NEW_INT(20), true, STARPART_READ); /* 20% of each part */
  StarPart_addNewItem(local, "scheme", STARPART_STR, NEW_STR("bfs"), true, STARPART_READ);
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  // StarPart_addNewItem(data, "part",  STARPART_INTARRAY, NULL, true,
  // STARPART_READWRITE);
  StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
}

/* *********************************************************** */

void StarPart_FIXED_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  char* scheme = StarPart_getStr(local, "scheme");
  StarPart_checkStr(local, "scheme", "bfs,bubble");
  int bubblesize = StarPart_getInt(local, "bubblesize");

  StarPart_Data* data = StarPart_getTaskData(task, "data");
  // int * part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int nparts = StarPart_getInt(data, "nparts");
  assert(g && nparts > 0);

  if (fixed)
    StarPart_checkArray(data, "fixed", g->nvtxs);
  else {
    fixed = StarPart_allocArray(data, "fixed", g->nvtxs);
    StarPart_initArray(data, "fixed", -1);
  }

  /* bfs */
  if (!strcmp(scheme, "bfs")) { LibGraph_computeGraphSeeds(g, nparts, 0, fixed); }
  /* bubble */
  else if (!strcmp(scheme, "bubble")) {
    assert(bubblesize > 0 && bubblesize <= 100);
    LibGraph_computeGraphSeeds(g, nparts, 0, fixed);
    int maxpartsize = (int)round(g->nvtxs / (nparts * 100.0 / bubblesize));
    // printf("INFO: initial bubble size = %d\n", maxpartsize);
    LibGraph_levelset1(g, nparts, maxpartsize, fixed);
  }
  else {
    StarPart_error("fixed scheme \"%s\" unknown!\n", scheme);
  }

  LibGraph_Array* fixed_array = StarPart_getArray(data, "fixed");
  if (fixed_array) LibGraph_setArrayFlags(fixed_array, LIBGRAPH_GRAPH_VERTEX_VAR);

  /* set vertices as fixed */
  // for(int i = 0; i < g->nvtxs; i++) part[i] = fixed[i];
  // if(part[i] != -1) fixed[i] = 1;

  /* output */
  // StarPart_setIntArray(data, "fixed", g->nvtxs, fixed, true);
  // StarPart_setIntArray(data, "part", g->nvtxs, part, true);
}

/* *********************************************************** */

void StarPart_WEIGHT_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "type", STARPART_STR, NEW_STR("vertex"), true, STARPART_READ);
  StarPart_addNewItem(local, "scheme", STARPART_STR, NEW_STR("uniform"), true, STARPART_READ);
  StarPart_addNewItem(local, "value", STARPART_INT, NEW_INT(1), true, STARPART_READ);
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_WEIGHT_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  char* type = StarPart_getStr(local, "type");
  StarPart_checkStr(local, "type", "vertex,edge,both");
  char* scheme = StarPart_getStr(local, "scheme");
  StarPart_checkStr(local, "scheme", "uniform,linear");
  int value = StarPart_getInt(local, "value");

  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");

  if (!strcmp(type, "vertex") || !strcmp(type, "both")) {
    g->vwgts = (int*)malloc(sizeof(int[g->nvtxs]));

    if (!strcmp(scheme, "uniform")) {
      for (int v = 0; v < g->nvtxs; v++) { g->vwgts[v] = value; }
    }
    else if (!strcmp(scheme, "linear")) {
      for (int v = 0; v < g->nvtxs; v++) { g->vwgts[v] = v; }
    }
  }

  if (!strcmp(type, "edge") || !strcmp(type, "both")) {
    g->ewgts = (int*)malloc(sizeof(int[g->narcs]));
    for (int e = 0; e < g->narcs; e++) { g->ewgts[e] = e; }
  }
}

void StarPart_EQUAL_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "strict", STARPART_INT, NEW_INT(0), true, STARPART_READ);
  StarPart_addNewItem(local, "items", STARPART_STRARRAY, NULL, true, STARPART_READ);

  StarPart_addMethodArg(method, "data1", STARPART_IN);
  StarPart_addMethodArg(method, "data2", STARPART_IN);
}

/* *********************************************************** */

void StarPart_EQUAL_call(StarPart_Task* task)
{
  assert(task);

  StarPart_Data* local = StarPart_getTaskLocal(task);
  int strict = StarPart_getInt(local, "strict");
  char** item_ids = StarPart_getStrArray(local, "items");
  int num_item_ids = StarPart_getArrayLength(local, "items");

  StarPart_Data* data1 = StarPart_getTaskData(task, "data1");
  StarPart_Data* data2 = StarPart_getTaskData(task, "data2");

  bool all = 0;
  /* if no items are given, we process all shared items */
  if (!item_ids) {
    all = 1;
    /* if strict: we check the two data have same number of items */
    if (strict) {
      int num_item_ids1 = g_hash_table_size(StarPart_getDataItems(data1));
      int num_item_ids2 = g_hash_table_size(StarPart_getDataItems(data2));
      assert(num_item_ids1 == num_item_ids2);
    }

    int max_item_ids = g_hash_table_size(StarPart_getDataItems(data1));
    item_ids = (char**)malloc(sizeof(char * [max_item_ids]));
    num_item_ids = 0;

    GHashTableIter iter;
    gpointer item_id, val;
    g_hash_table_iter_init(&iter, StarPart_getDataItems(data1));
    while (g_hash_table_iter_next(&iter, &item_id, &val)) {
      if (StarPart_isItem(data2, item_id)) {
        item_ids[num_item_ids] = strdup(item_id);
        num_item_ids++;
      }
      else {
        /* item in other data flow does not exist */
        if (strict) assert(0);
      }
    }
  }
  else {
    for (int i = 0; i < num_item_ids; i++) {
      assert(StarPart_isItem(data1, item_ids[i]) && StarPart_isItem(data2, item_ids[i]));
    }
  }

  /* Check items */
  for (int i = 0; i < num_item_ids; i++) {
    char* item_id = item_ids[i];
    StarPart_Item* item1 = StarPart_getItem(data1, item_id);
    StarPart_Item* item2 = StarPart_getItem(data2, item_id);

    StarPart_Type type1 = StarPart_getItemType(item1);
    StarPart_Type type2 = StarPart_getItemType(item2);
    assert(type1 == type2);

    if (STARPART_INTARRAY == type1 || STARPART_FLOATARRAY == type1 || STARPART_DOUBLEARRAY == type1 ||
        STARPART_STRARRAY == type1) {
      LibGraph_Array* array1 = StarPart_getArray(data1, item_id);
      LibGraph_Array* array2 = StarPart_getArray(data2, item_id);
      assert(LibGraph_compareArray(array1, array2));
    }
    else if (STARPART_GRAPH == type1) {
      LibGraph_Graph* g1 = StarPart_getGraph(data1, item_id);
      LibGraph_Graph* g2 = StarPart_getGraph(data2, item_id);
      assert(LibGraph_compareGraph(g1, g2));
    }
    else if (STARPART_MESH == type1) {
      LibGraph_Mesh* m1 = StarPart_getMesh(data1, item_id);
      LibGraph_Mesh* m2 = StarPart_getMesh(data2, item_id);
      assert(LibGraph_compareMesh(m1, m2));
    }
    else {
      assert(0);  // TODO implement
    }
  }

  if (all) {
    for (int i = 0; i < num_item_ids; i++) { free(item_ids[i]); }
    free(item_ids);
  }
}

/* *********************************************************** */

void StarPart_CHECK_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_IN);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NULL, true, STARPART_READ);
}

/* *********************************************************** */

#define TOLERANCE 1.001

void StarPart_CHECK_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  int* part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int nparts = StarPart_getInt(data, "nparts");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  assert(part);

  /* check fixed vertices */
  if (fixed) {
    for (int i = 0; i < g->nvtxs; i++)
      if (fixed[i] != -1) assert(part[i] == fixed[i]);
  }
  /* check partition */
  double maxub, minub;
  LibGraph_computeGraphMinMaxUnbalance(g, nparts, part, &minub, &maxub);
  int edgecut = LibGraph_computeGraphEdgeCut(g, nparts, part);
  /* printf("maxub = %.2f\n", maxub*100.0); */
  /* printf("minub = %.2f\n", minub*100.0); */
  /* printf("edgecut = %d\n", edgecut); */
  /* check */
  assert(maxub * 100.0 <= ubfactor * TOLERANCE);
  // assert(minub*100.0 <= ubfactor*TOLERANCE);
  assert(edgecut > 0);
}

/* *********************************************************** */

void StarPart_CHECKITEMS_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "strict", STARPART_INT, NEW_INT(0), true, STARPART_READ);
  StarPart_addNewItem(local, "items", STARPART_STRARRAY, NULL, true, STARPART_READ);

  StarPart_addMethodArg(method, "data", STARPART_IN);
}

/* *********************************************************** */

void StarPart_CHECKITEMS_call(StarPart_Task* task)
{
  assert(task);

  StarPart_Data* local = StarPart_getTaskLocal(task);
  int strict = StarPart_getInt(local, "strict");
  char** item_ids = StarPart_getStrArray(local, "items");
  int num_item_ids = StarPart_getArrayLength(local, "items");

  StarPart_Data* data = StarPart_getTaskData(task, "data");

  if (strict) {
    int num_data_item_ids = g_hash_table_size(StarPart_getDataItems(data));
    assert(num_data_item_ids == num_item_ids);
  }

  for (int i = 0; i < num_item_ids; i++) {
    char* item_id = item_ids[i];
    assert(StarPart_isItem(data, item_id));
  }
}

/* *********************************************************** */

void StarPart_LOOP_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "first", STARPART_INT, NEW_INT(0), true, STARPART_READ);
  StarPart_addNewItem(local, "last", STARPART_INT, NEW_INT(9), true, STARPART_READ);
  StarPart_addNewItem(local, "inc", STARPART_INT, NEW_INT(1), true, STARPART_READ);
  StarPart_addNewItem(local, "var", STARPART_STR, NEW_STR("k"), true, STARPART_READ);
  StarPart_addNewItem(local, "wrap", STARPART_INT, NEW_INT(0), true, STARPART_READ);  // duplicate data by default
  StarPart_addNewItem(local, "strat", STARPART_STRAT, NULL, true, STARPART_READ);

  StarPart_addMethodArg(method, "data", STARPART_INOUT);
}

/* *********************************************************** */

void StarPart_LOOP_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  int first = StarPart_getInt(local, "first");
  int last = StarPart_getInt(local, "last");
  int inc = StarPart_getInt(local, "inc");
  int wrap = StarPart_getInt(local, "wrap");  // default false (duplicate data)
  char* strat = StarPart_getStrat(local, "strat");
  char* var = StarPart_getStr(local, "var");
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  char* id = StarPart_getDataID(data);

  /* the loop */
  for (int k = first; k <= last; k += inc) {
    StarPart_Context* ctx = StarPart_getTaskContext(task);
    StarPart_Context* _ctx = StarPart_newNestedContext(ctx);
    StarPart_Data* _data = StarPart_dupData(data, !wrap);
    StarPart_addNewItem(_data, var, STARPART_INT, &k, false, 0);
    StarPart_registerData(_ctx, _data);
    StarPart_parseStrat(_ctx, strat);
    StarPart_submitAllTasks(_ctx);
    StarPart_connectAllTasks(_ctx, id, NULL);
    StarPart_run(_ctx);
    StarPart_freeAllTasks(_ctx);
    StarPart_freeNestedContext(_ctx);
  }
}

/* *********************************************************** */

void StarPart_PRINT_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "var", STARPART_STR, NULL, true, STARPART_READ);
  StarPart_addNewItem(local, "dump", STARPART_INT, NEW_INT(0), true, STARPART_READ);
  StarPart_addMethodArg(method, "data", STARPART_IN);
}

/* *********************************************************** */

void StarPart_PRINT_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  char* var = StarPart_getStr(local, "var");
  int dump = StarPart_getInt(local, "dump");
  assert(var);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  StarPart_Item* item = StarPart_getItem(data, var);
  StarPart_printItem(item, dump, NULL);
}

/* *********************************************************** */

void StarPart_PRINTALL_init(StarPart_Method* method)
{
}

/* *********************************************************** */

void StarPart_PRINTALL_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  StarPart_printAllData(ctx);
}

/* *********************************************************** */

void StarPart_NESTED_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "strat", STARPART_STRAT, NULL, true, STARPART_READ);
  StarPart_addMethodArg(method, "data", STARPART_INOUT);
}

/* *********************************************************** */

void StarPart_NESTED_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  char* strat = StarPart_getStrat(local, "strat");
  // int wrap = StarPart_getInt(local, "wrap");
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  char* id = StarPart_getDataID(data);

  StarPart_Context* ctx = StarPart_getTaskContext(task);
  StarPart_Context* _ctx = StarPart_newNestedContext(ctx);
  StarPart_registerData(_ctx, data);  // ref++
  StarPart_parseStrat(_ctx, strat);
  StarPart_submitAllTasks(_ctx);
  StarPart_connectAllTasks(_ctx, id, NULL);
  StarPart_run(_ctx);
  StarPart_freeAllTasks(_ctx);
  StarPart_freeNestedContext(_ctx);
}

/* *********************************************************** */

// void StarPart_FORALL_init(StarPart_Context * ctx, StarPart_Method * method)
// {
//   StarPart_Data * param = StarPart_addMethodArg(method, "param",
//   STARPART_LOCAL); StarPart_addNewItem(local, "strat",  STARPART_STRAT, NULL,
//   true, STARPART_READ);
// }

/* *********************************************************** */

// void StarPart_FORALL_call(StarPart_Context * ctx, StarPart_Task * task)
// {
//   assert(task);
//   StarPart_Data * param = StarPart_getTaskData(task, "param");
//   char * strat = StarPart_getStrat(local, "strat");
//
//   /* for all data */
//   GHashTable * datatable = StarPart_getDataTable(ctx);
//   GHashTableIter iter;
//   gpointer key, val;
//   g_hash_table_iter_init(&iter, datatable);
//   while (g_hash_table_iter_next (&iter, &key, &val)) {
//     StarPart_Data * data = (StarPart_Data*)val;
//     printf("FORALL: process data %s\n", StarPart_getDataID(data));
//     StarPart_printData(data);
//     StarPart_Data * _data = StarPart_newData("@0", 0); // BUG: how to be
//     generic??? StarPart_dupItems(data, _data, false); // wrap data
//     StarPart_Context * _ctx = StarPart_newNestedContext(ctx);
//     StarPart_registerData(_ctx, _data);
//     StarPart_parseStrat(_ctx, strat);
//     StarPart_submitAllTasks(_ctx);
//     StarPart_run(_ctx);
//     StarPart_freeAllTasks(_ctx);
//     StarPart_freeNestedContext(_ctx);
//   }
//
// }

/* *********************************************************** */
/*                           HOOK                               */
/* *********************************************************** */

void StarPart_DEBUG_pre(StarPart_Task* task)
{
  // StarPart_Data * local = StarPart_getTaskLocal(task);
  GQueue* ctx_stack = StarPart_getContextStack();
  int lvl = g_queue_get_length(ctx_stack);
  char* id = StarPart_getTaskID(task);
  StarPart_printRed("=> start task \"%s\" (level %d)\n", id, lvl);
  // StarPart_printTask(ctx, task);
}

/* *********************************************************** */

void StarPart_DEBUG_post(StarPart_Task* task)
{
  // StarPart_Data * local = StarPart_getTaskLocal(task);
  GQueue* ctx_stack = StarPart_getContextStack();
  int lvl = g_queue_get_length(ctx_stack);
  char* id = StarPart_getTaskID(task);
  StarPart_printRed("<= end task \"%s\" (level %d)\n", id, lvl);
  // StarPart_printTask(ctx, task);
}

/* *********************************************************** */

void StarPart_TIME_pre(StarPart_Task* task)
{
  // StarPart_Data * local = StarPart_getTaskLocal(task);
  /* start timer */
  StarPart_Data* data = StarPart_getTaskData(task, "#in");  // BUG: why "#in"?
  assert(data);
  struct timeval start;
  gettimeofday(&start, NULL);
  double exectime = (start.tv_sec * 1000.0 * 1000.0 + start.tv_usec) / 1000.0;  // in ms
  if (!StarPart_isItem(data, "exectime"))
    StarPart_addNewItem(data, "exectime", STARPART_DOUBLE, NEW_DOUBLE(exectime), true, 0);
}

/* *********************************************************** */

void StarPart_TIME_post(StarPart_Task* task)
{
  // StarPart_Data * local = StarPart_getTaskLocal(task);
  /* stop timer */
  struct timeval stop;
  gettimeofday(&stop, NULL);
  double exectime = (stop.tv_sec * 1000.0 * 1000.0 + stop.tv_usec) / 1000.0;  // in ms
  StarPart_Data* data = StarPart_getTaskData(task, "#in");                    // BUG: why "#in"?
  double _exectime = StarPart_getDouble(data, "exectime");
  StarPart_setDouble(data, "exectime", exectime - _exectime);
}

/* *********************************************************** */
