
/** @file vtk.h */

#ifndef BASIC_VTK_H
#define BASIC_VTK_H

#include "starpart.h"

#define StarPart_VTK_desc "save mesh as VTK output file"
void StarPart_VTK_call(StarPart_Task* task);
void StarPart_VTK_init(StarPart_Method* method);

#define StarPart_HVTK_desc "save hynrid mesh as VTK output file"
void StarPart_HVTK_call(StarPart_Task* task);
void StarPart_HVTK_init(StarPart_Method* method);

#define StarPart_STORE_desc "store arrays as mesh variable for visualization purpose"
void StarPart_STORE_post(StarPart_Task* task);

#endif
