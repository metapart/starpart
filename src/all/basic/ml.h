/** @file ml.h */

#ifndef BASIC_ML_H
#define BASIC_ML_H

#include "starpart.h"

#define StarPart_ML_desc "multilevel partitioning framework"
void StarPart_ML_call(StarPart_Task* task);
void StarPart_ML_init(StarPart_Method* method);

#define StarPart_KFM_desc "k-way partition refinement"
void StarPart_KFM_init(StarPart_Method* method);
void StarPart_KFM_call(StarPart_Task* task);

#define StarPart_COARSEN_desc "graph coarsening"
void StarPart_COARSEN_call(StarPart_Task* task);
void StarPart_COARSEN_init(StarPart_Method* method);

#define StarPart_REMAP_desc "remap the partition of a graph to a coarsen graph"
void StarPart_REMAP_call(StarPart_Task* task);
void StarPart_REMAP_init(StarPart_Method* method);

#endif
