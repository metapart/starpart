/** @file basic.h */

#ifndef BASIC_BASIC_H
#define BASIC_BASIC_H

#include "starpart.h"

/**
 * @defgroup basic Basic Methods
 * @{
 * @brief Basic StarPart methods.
 */

/* *********************************************************** */
/*                      BASIC METHODS                          */
/* *********************************************************** */

#define StarPart_NOP_desc "null operation"
void StarPart_NOP_call(StarPart_Task* task);
void StarPart_NOP_init(StarPart_Method* method);

#define StarPart_DUP_desc "duplicate input data to output"
void StarPart_DUP_call(StarPart_Task* task);
void StarPart_DUP_init(StarPart_Method* method);

#define StarPart_GRID_desc "generate grid mesh"
void StarPart_GRID_call(StarPart_Task* task);
void StarPart_GRID_init(StarPart_Method* method);

#define StarPart_CMP_desc "compare two graph partitions"
void StarPart_CMP_call(StarPart_Task* task);
void StarPart_CMP_init(StarPart_Method* method);

#define StarPart_DIAG_desc "print graph partition diagnostic"
void StarPart_DIAG_call(StarPart_Task* task);
void StarPart_DIAG_init(StarPart_Method* method);

#define StarPart_DATA_desc "new empty data"
void StarPart_DATA_call(StarPart_Task* task);
void StarPart_DATA_init(StarPart_Method* method);

#define StarPart_INT_desc "add an integer"
void StarPart_INT_call(StarPart_Task* task);
void StarPart_INT_init(StarPart_Method* method);

#define StarPart_G2HG_desc "convert graph to hypergraph"
void StarPart_G2HG_call(StarPart_Task* task);
void StarPart_G2HG_init(StarPart_Method* method);

#define StarPart_INTARRAY_desc ""
void StarPart_INTARRAY_call(StarPart_Task* task);
void StarPart_INTARRAY_init(StarPart_Method* method);

#define StarPart_FIXED_desc "set fixed vertices"
void StarPart_FIXED_call(StarPart_Task* task);
void StarPart_FIXED_init(StarPart_Method* method);

#define StarPart_WEIGHT_desc "add wieghts"
void StarPart_WEIGHT_call(StarPart_Task* task);
void StarPart_WEIGHT_init(StarPart_Method* method);

#define StarPart_LOOP_desc ""
void StarPart_LOOP_call(StarPart_Task* task);
void StarPart_LOOP_init(StarPart_Method* method);

#define StarPart_EQUAL_desc "check if two graphs are equal"
void StarPart_EQUAL_init(StarPart_Method* method);
void StarPart_EQUAL_call(StarPart_Task* task);

#define StarPart_CHECK_desc "check graph partition"
void StarPart_CHECK_call(StarPart_Task* task);
void StarPart_CHECK_init(StarPart_Method* method);

#define StarPart_CHECKITEMS_desc "check items are present in data"
void StarPart_CHECKITEMS_call(StarPart_Task* task);
void StarPart_CHECKITEMS_init(StarPart_Method* method);

#define StarPart_PRINT_desc ""
void StarPart_PRINT_call(StarPart_Task* task);
void StarPart_PRINT_init(StarPart_Method* method);

#define StarPart_PRINTALL_desc ""
void StarPart_PRINTALL_call(StarPart_Task* task);
void StarPart_PRINTALL_init(StarPart_Method* method);

#define StarPart_RANDOM_desc "random graph partition"
void StarPart_RANDOM_call(StarPart_Task* task);
void StarPart_RANDOM_init(StarPart_Method* method);

#define StarPart_BFS_desc "bisection using BFS method"
void StarPart_BFS_call(StarPart_Task* task);
void StarPart_BFS_init(StarPart_Method* method);

#define StarPart_SPLIT_desc "split an input graph in two subgraphs"
void StarPart_SPLIT_call(StarPart_Task* task);
void StarPart_SPLIT_init(StarPart_Method* method);

#define StarPart_NESTED_desc "run a strategy within a nested context"
void StarPart_NESTED_call(StarPart_Task* task);
void StarPart_NESTED_init(StarPart_Method* method);

#define StarPart_PART_desc "run a partition strategy within a nested context"
void StarPart_PART_call(StarPart_Task* task);
void StarPart_PART_init(StarPart_Method* method);

#define StarPart_PERMUT_desc "permut a partition partition"
void StarPart_PERMUT_call(StarPart_Task* task);
void StarPart_PERMUT_init(StarPart_Method* method);

// #define StarPart_FORALL_desc ""
// void StarPart_FORALL_call(StarPart_Context * ctx, StarPart_Task * task);
// void StarPart_FORALL_init(StarPart_Context * ctx, StarPart_Method * method);

/* *********************************************************** */

#define StarPart_DEBUG_desc "debug hook"
void StarPart_DEBUG_pre(StarPart_Task* task);
void StarPart_DEBUG_post(StarPart_Task* task);

#define StarPart_TIME_desc "timer hook"
void StarPart_TIME_pre(StarPart_Task* task);
void StarPart_TIME_post(StarPart_Task* task);

/* *********************************************************** */

/** @} */

#endif
