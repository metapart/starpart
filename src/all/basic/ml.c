#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "all.h"
#include "libgraph.h"
#include "ml.h"
#include "starpart.h"

/* *********************************************************** */

void StarPart_KFM_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);

  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "maxnegmove", STARPART_INT, NEW_INT(STARPART_MAXNEGMOVE), true, STARPART_READ);
  StarPart_addNewItem(local, "bandsize", STARPART_INT, NEW_INT(0), true, STARPART_READ);
  StarPart_addNewItem(local, "nrefpass", STARPART_INT, NEW_INT(STARPART_NREFPASS), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_KFM_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int nparts = StarPart_getInt(data, "nparts");
  StarPart_Data* local = StarPart_getTaskLocal(task);
  int nrefpass = StarPart_getInt(local, "nrefpass");
  int maxnegmove = StarPart_getInt(local, "maxnegmove");
  int bandsize = StarPart_getInt(local, "bandsize");

  assert(g && part);

  int* locked = NULL;
  if (fixed) {
    locked = malloc(g->nvtxs * sizeof(int));
    assert(locked);
    for (int i = 0; i < g->nvtxs; i++) locked[i] = (fixed[i] >= 0);  // 0 or 1
  }

  if (bandsize > 0)
    LibGraph_refineBandKFM(g, nparts, ubfactor, part, locked, nrefpass, maxnegmove, bandsize);
  else
    LibGraph_refineKFM(g, nparts, ubfactor, part, locked, nrefpass, maxnegmove);

  if (locked) free(locked);
}

/* *********************************************************** */
/*                        WRAPPER                              */
/* *********************************************************** */

static void _partitionGraph(LibGraph_Graph* g, int nparts, int* part, int* fixed, int ubfactor, void* opt)
{
  char* strat = (char*)opt;

  StarPart_Data* _data = StarPart_newData("@0", 0);
  StarPart_addNewItem(_data, "graph", STARPART_GRAPH, g, false, STARPART_READ);
  StarPart_addNewItem(_data, "part", STARPART_INTARRAY, NEW_INTARRAY(g->nvtxs, part), true, STARPART_READ);
  StarPart_addNewItem(_data, "fixed", STARPART_INTARRAY, NEW_INTARRAY(g->nvtxs, fixed), true, STARPART_READ);
  StarPart_addNewItem(_data, "nparts", STARPART_INT, NEW_INT(nparts), true, STARPART_READ);
  StarPart_addNewItem(_data, "ubfactor", STARPART_INT, NEW_INT(ubfactor), true, STARPART_READ);

  StarPart_Context* ctx = StarPart_getCurrentContext();
  StarPart_Context* _ctx = StarPart_newNestedContext(ctx);
  StarPart_registerData(_ctx, _data);
  StarPart_parseStrat(_ctx, strat);
  StarPart_submitAllTasks(_ctx);
  StarPart_connectAllTasks(_ctx, "@0", NULL);
  StarPart_run(_ctx);
  StarPart_freeAllTasks(_ctx);
  StarPart_freeNestedContext(_ctx);
}

/* *********************************************************** */

static void _refineGraph(LibGraph_Graph* g, int nparts, int* part, int* fixed, int ubfactor, void* opt)
{
  char* strat = (char*)opt;

  StarPart_Data* _data = StarPart_newData("@0", 0);
  StarPart_addNewItem(_data, "graph", STARPART_GRAPH, g, false, STARPART_READ);
  StarPart_addNewItem(_data, "part", STARPART_INTARRAY, NEW_INTARRAY(g->nvtxs, part), true, STARPART_READ);
  StarPart_addNewItem(_data, "fixed", STARPART_INTARRAY, NEW_INTARRAY(g->nvtxs, fixed), true, STARPART_READ);
  StarPart_addNewItem(_data, "nparts", STARPART_INT, NEW_INT(nparts), true, STARPART_READ);
  StarPart_addNewItem(_data, "ubfactor", STARPART_INT, NEW_INT(ubfactor), true, STARPART_READ);

  StarPart_Context* ctx = StarPart_getCurrentContext();
  StarPart_Context* _ctx = StarPart_newNestedContext(ctx);
  StarPart_registerData(_ctx, _data);
  StarPart_parseStrat(_ctx, strat);
  StarPart_submitAllTasks(_ctx);
  StarPart_connectAllTasks(_ctx, "@0", NULL);
  StarPart_run(_ctx);
  StarPart_freeAllTasks(_ctx);
  StarPart_freeNestedContext(_ctx);
}

/* *********************************************************** */

static void _coarsenGraph(LibGraph_Graph* g, LibGraph_Graph* cg, int* map, int* fixed, void* opt)
{
  char* strat = (char*)opt;

  StarPart_Data* _in = StarPart_newData("@0", 0);
  StarPart_Data* _out = StarPart_newData("@1", 0);
  StarPart_addNewItem(_in, "graph", STARPART_GRAPH, g, false, STARPART_READ);
  StarPart_addNewItem(_in, "fixed", STARPART_INTARRAY, NEW_INTARRAY(g->nvtxs, fixed), true, STARPART_READ);
  StarPart_addNewItem(_in, "map", STARPART_INTARRAY, NEW_INTARRAY(g->nvtxs, map), true, STARPART_WRITE);
  StarPart_addNewItem(_out, "graph", STARPART_GRAPH, cg, false, STARPART_WRITE);

  StarPart_Context* ctx = StarPart_getCurrentContext();
  StarPart_Context* _ctx = StarPart_newNestedContext(ctx);
  StarPart_registerData(_ctx, _in);
  StarPart_registerData(_ctx, _out);
  StarPart_parseStrat(_ctx, strat);
  StarPart_submitAllTasks(_ctx);
  StarPart_connectAllTasks(_ctx, "@0", "@1");
  StarPart_run(_ctx);
  StarPart_freeAllTasks(_ctx);
  StarPart_freeNestedContext(_ctx);
}

/* *********************************************************** */

void StarPart_ML_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);

  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "ninitpass", STARPART_INT, NEW_INT(STARPART_NINITPASS), true, STARPART_READ);
  StarPart_addNewItem(local, "nrefpass", STARPART_INT, NEW_INT(STARPART_NREFPASS), true, STARPART_READ);
  StarPart_addNewItem(local, "cpartsize", STARPART_INT, NEW_INT(STARPART_CPARTSIZE), true, STARPART_READ);
  StarPart_addNewItem(local, "cstrat", STARPART_STRAT, NEW_STRAT("BASIC/COARSEN"), true, STARPART_READ);
  StarPart_addNewItem(local, "pstrat", STARPART_STRAT, NULL, true, STARPART_READ);
  StarPart_addNewItem(local, "rstrat", STARPART_STRAT, NEW_STRAT("BASIC/KFM"), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_ML_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int nparts = StarPart_getInt(data, "nparts");
  StarPart_Data* local = StarPart_getTaskLocal(task);
  int ninitpass = StarPart_getInt(local, "ninitpass");
  int nrefpass = StarPart_getInt(local, "nrefpass");
  int cpartsize = StarPart_getInt(local, "cpartsize");
  char* cstrat = StarPart_getStrat(local, "cstrat");
  char* pstrat = StarPart_getStrat(local, "pstrat");
  char* rstrat = StarPart_getStrat(local, "rstrat");
  int usefixed = fixed ? 1 : 0;
  assert(pstrat);

  assert(g);
  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else
    part = StarPart_allocArray(data, "part", g->nvtxs);
  if (fixed) StarPart_checkArray(data, "fixed", g->nvtxs);

  /* set free vertices in input partition */
  for (int i = 0; i < g->nvtxs; i++) part[i] = fixed ? fixed[i] : -1;

  LibGraph_CoarsenMethod coarsen = _coarsenGraph;
  LibGraph_PartitionMethod partition = _partitionGraph;
  LibGraph_RefineMethod refine = _refineGraph;

  LibGraph_partitionGraphMultiLevel(g, nparts, part, ubfactor, usefixed, ninitpass, nrefpass, cpartsize, coarsen,
                                    partition, refine, cstrat, pstrat, rstrat);
}

/* *********************************************************** */

void StarPart_COARSEN_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "scheme", STARPART_STR, NEW_STR("hem"), true, STARPART_READ);
  StarPart_Data* in = StarPart_addMethodArg(method, "in", STARPART_IN);
  StarPart_addNewItem(in, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(in, "mesh", STARPART_MESH, NULL, true, STARPART_READ);
  StarPart_addNewItem(in, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(in, "map", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_Data* out = StarPart_addMethodArg(method, "out", STARPART_OUT);
  StarPart_addNewItem(out, "graph", STARPART_GRAPH, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(out, "mesh", STARPART_MESH, NULL, true, STARPART_READWRITE);
  // StarPart_addNewItem(out, "imap", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
}

/* *********************************************************** */

void StarPart_COARSEN_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  StarPart_Data* in = StarPart_getTaskData(task, "in");
  StarPart_Data* out = StarPart_getTaskData(task, "out");
  char* scheme = StarPart_getStr(local, "scheme");
  StarPart_checkStr(local, "scheme", "hem,rm");
  LibGraph_Graph* g = StarPart_getGraph(in, "graph");
  LibGraph_Mesh* m = StarPart_getMesh(in, "mesh");
  int* fixed = StarPart_getIntArray(in, "fixed");
  int* map = StarPart_getIntArray(in, "map");
  LibGraph_Graph* cg = StarPart_getGraph(out, "graph");
  LibGraph_Mesh* cm = StarPart_getMesh(out, "mesh");

  assert(g && scheme);

  int ctype = -1;
  if (!strcmp(scheme, "hem"))
    ctype = LIBGRAPH_MATCH_HEM;
  else if (!strcmp(scheme, "rm"))
    ctype = LIBGRAPH_MATCH_RM;
  else
    StarPart_error("coarsening scheme \"%s\" unknown!\n", scheme);

  if (map)
    StarPart_checkArray(in, "map", g->nvtxs);
  else {
    map = StarPart_allocArray(in, "map", g->nvtxs);
    StarPart_initArray(in, "map", -1); // useful?
  }

  if (fixed) StarPart_checkArray(in, "fixed", g->nvtxs);

  if (!cg) {
    cg = StarPart_newGraph();
    StarPart_setGraph(out, "graph", cg, true);
  }

  /* coarsen graph */
  LibGraph_coarsenGraph(g, ctype, cg, map, fixed);

  /* coarsen mesh */
  if (!cm && m) {
    cm = StarPart_newMesh();
    StarPart_setMesh(out, "mesh", cm, true);
  }

  if (m && cm) LibGraph_coarsenGraph2Mesh(m, cg, map, cm);

  /* map part and fixed? */
}

/* *********************************************************** */

void StarPart_REMAP_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "inverse", STARPART_INT, NEW_INT(0), true, STARPART_READ);

  /* data (larger graph)*/
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  /* the map array is added by BASIC/COARSEN to the larger graph (array of size g->nvtxs) */
  StarPart_addNewItem(data, "map", STARPART_INTARRAY, NULL, true, STARPART_READ);

  /* coarsen data (smaller graph) */
  StarPart_Data* cdata = StarPart_addMethodArg(method, "cdata", STARPART_INOUT);
  StarPart_addNewItem(cdata, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(cdata, "ubfactor", STARPART_INT, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(cdata, "nparts", STARPART_INT, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(cdata, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(cdata, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
}

/* *********************************************************** */

void StarPart_REMAP_call(StarPart_Task* task)
{
  assert(task);

  StarPart_Data* local = StarPart_getTaskLocal(task);
  int inverse = StarPart_getInt(local, "inverse");

  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  int* map = StarPart_getIntArray(data, "map");
  // int* nparts = StarPart_getIntAddr(data, "nparts");
  // int* ubfactor = StarPart_getIntAddr(data, "ubfactor");

  StarPart_Data* cdata = StarPart_getTaskData(task, "cdata");
  LibGraph_Graph* cg = StarPart_getGraph(cdata, "graph");
  int* cpart = StarPart_getIntArray(cdata, "part");
  int* cfixed = StarPart_getIntArray(cdata, "fixed");
  // int* cnparts = StarPart_getIntAddr(cdata, "nparts");
  // int* cubfactor = StarPart_getIntAddr(cdata, "ubfactor");

  /* check */
  assert(g && cg);
  assert(g->nvtxs >= cg->nvtxs);
  assert(inverse == 0 || inverse == 1);
  assert(map);
  StarPart_checkArray(data, "map", g->nvtxs);

  if (!inverse) {
    assert(part);
    // assert(nparts && *nparts > 0);
    // assert(ubfactor && *ubfactor > 0);
    int nparts = StarPart_getInt(data, "nparts");
    int ubfactor = StarPart_getInt(data, "ubfactor");

    StarPart_checkArray(data, "part", g->nvtxs);
    if (!cpart) {
      cpart = StarPart_allocArray(cdata, "part", cg->nvtxs);
      StarPart_initArray(cdata, "part", -1);  // useful?
    }
    else
      StarPart_checkArray(cdata, "part", cg->nvtxs);
    StarPart_setInt(cdata, "nparts", nparts);
    StarPart_setInt(cdata, "ubfactor", ubfactor);
  }
  else {
    assert(cpart);
    // assert(cnparts && *cnparts > 0);
    // assert(cubfactor && *cubfactor > 0);
    int cnparts = StarPart_getInt(cdata, "nparts");
    int cubfactor = StarPart_getInt(cdata, "ubfactor");
    StarPart_checkArray(cdata, "part", cg->nvtxs);
    if (!part) {
      part = StarPart_allocArray(data, "part", g->nvtxs);
      StarPart_initArray(data, "part", -1);
    }
    else
      StarPart_checkArray(data, "part", g->nvtxs);
    StarPart_setInt(data, "nparts", cnparts);
    StarPart_setInt(data, "ubfactor", cubfactor);
  }

  // remap in/part to out/part
  /* the last vertex i in mapping overwrites the others */
  if (!inverse)
    for (int i = 0; i < g->nvtxs; i++) cpart[map[i]] = part[i];
  else
    for (int i = 0; i < g->nvtxs; i++) part[i] = cpart[map[i]];

  // TODO: add other functions SUM, MIN, MAX, LAST, FIRST, AVG?

  if (fixed) {
    StarPart_checkArray(data, "fixed", g->nvtxs);
    if (!cfixed) {
      cfixed = StarPart_allocArray(cdata, "fixed", cg->nvtxs);
      StarPart_initArray(cdata, "fixed", -1); // useful?
    }
    else
      StarPart_checkArray(cdata, "fixed", cg->nvtxs);
    if (!inverse)
      for (int i = 0; i < g->nvtxs; i++) cfixed[map[i]] = fixed[i];
    else
      for (int i = 0; i < g->nvtxs; i++) fixed[i] = cfixed[map[i]];
  }
}

/* *********************************************************** */
