/** @file bench.h */

#ifndef BASIC_BENCH_H
#define BASIC_BENCH_H

#include "starpart.h"

/**
 * @defgroup basic Basic Methods
 * @{
 * @brief Basic StarPart methods.
 */

#define StarPart_BENCH_desc "benchmarking of partitioning routines"
void StarPart_BENCH_call(StarPart_Task* task);
void StarPart_BENCH_init(StarPart_Method* method);

/** @} */

#endif
