#include <assert.h>
#include <glib.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "all.h"
#include "libgraph.h"
#include "starpart.h"
#include "vtk.h"

/* *********************************************************** */

static LibGraph_Variables* makeVariables(StarPart_Data* data)
{
  GHashTable* items = StarPart_getDataItems(data);
  int maxsize = g_hash_table_size(items);
  LibGraph_Variables* vars = LibGraph_newVariables(maxsize);
  char* data_id = StarPart_getDataID(data);  // data->id;
  /* for all items in data */
  GHashTableIter item_iter;
  gpointer item_key, item_val;
  g_hash_table_iter_init(&item_iter, items);
  while (g_hash_table_iter_next(&item_iter, &item_key, &item_val)) {
    StarPart_Item* item = (StarPart_Item*)item_val;
    char* item_id = StarPart_getItemID(item);
    StarPart_Type item_type = StarPart_getItemType(item);
    if (item_type != STARPART_INTARRAY && item_type != STARPART_DOUBLEARRAY) continue;  // skip if not numeric array
    LibGraph_Array* array = StarPart_getArray(data, item_id);
    if (!array) continue;  // skip null array
    // if (array->flags & LIBGRAPH_GRAPH_VERTEX_VAR)
    //   array->flags |= LIBGRAPH_MESH_CELL_VAR;  // TODO: handle line mesh case...
    // if (!(array->flags & LIBGRAPH_MESH_CELL_VAR) && !(array->flags & LIBGRAPH_MESH_NODE_VAR)) continue;
    char id[strlen(data_id) + strlen(item_id) + 2];
    sprintf(id, "%s/%s", data_id, item_id);
    LibGraph_addVariable(vars, "part", array);
    printf("=> add mesh variable %s\n", id);
  }
  return vars;
}

/* *********************************************************** */

void StarPart_VTK_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_IN);
  StarPart_addNewItem(data, "mesh", STARPART_MESH, NULL, true, STARPART_READ);
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "filename", STARPART_STR, NEW_STR(STARPART_VTK_OUTPUT), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_VTK_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Mesh* m = StarPart_getMesh(data, "mesh");
  if (!m) StarPart_error("fail to find mesh in data flow!\n");
  StarPart_Data* local = StarPart_getTaskLocal(task);
  char* filename = StarPart_getStr(local, "filename");
  LibGraph_Variables* vars = makeVariables(data);
  int r = LibGraph_saveVTKMesh(filename, m, vars);
  if (!r) StarPart_error("fail to save \"%s\"!\n", filename);
  LibGraph_freeVariables(vars, false);  // don't free arrays
  free(vars);
}

/* *********************************************************** */

void StarPart_HVTK_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "hmesh", STARPART_HYBRIDMESH, NULL, true, 0);
  StarPart_addNewItem(data, "filename", STARPART_STR, NEW_STR(STARPART_VTK_OUTPUT), true, 0);  
}

/* *********************************************************** */

void StarPart_HVTK_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_HybridMesh* hm = StarPart_getHybridMesh(data, "hmesh");
  if (!hm) StarPart_error("fail to find hybrid mesh in data flow!\n");
  char* filename = StarPart_getStr(data, "filename");
  int r = LibGraph_saveVTKHybridMesh(filename, hm);  // TODO: update this
  if (!r) StarPart_error("fail to save \"%s\"!\n", filename);
}

/* *********************************************************** */
/*                           HOOK                              */
/* *********************************************************** */

static void storeItem(StarPart_Data* data, char* arg_id, char* item_id)
{
  assert(data);
  char* data_id = StarPart_getDataID(data);
  StarPart_Item* item = StarPart_getItem(data, item_id);
  unsigned int version = StarPart_storeItemVersion(item);
  printf("=> store %s/%s in data %s as version %d\n", arg_id, item_id, data_id, version);
}

/* *********************************************************** */

static void storeItems(StarPart_Data* data, char* arg_id, char* var)
{
  assert(data);

  LibGraph_Mesh* m = NULL;
  if (StarPart_isItem(data, "mesh")) m = StarPart_getMesh(data, "mesh");
  LibGraph_HybridMesh* hm = NULL;
  if (StarPart_isItem(data, "hmesh")) hm = StarPart_getHybridMesh(data, "hmesh");
  if (!m && !hm) return; /* no mesh nor hmesh*/

  // if (!prefix) prefix = StarPart_getDataID(data);  // data->id;

  GHashTable* items = StarPart_getDataItems(data);

  /* for all items in data */
  GHashTableIter item_iter;
  gpointer item_key, item_val;
  g_hash_table_iter_init(&item_iter, items);
  while (g_hash_table_iter_next(&item_iter, &item_key, &item_val)) {
    StarPart_Item* item = (StarPart_Item*)item_val;
    char* item_id = StarPart_getItemID(item);
    char* id = malloc(strlen(arg_id) + strlen(item_id) + 2);
    sprintf(id, "%s/%s", arg_id, item_id);
    if (!var || strcmp(id, var) == 0) storeItem(data, arg_id, item_id);
    free(id);

    // StarPart_Type item_type = StarPart_getItemType(item);
    // if (item_type != STARPART_INTARRAY) continue;  // skip item that are not an array
    // if (var && strcmp(var, item_id)) continue;     // if var is used, skip item that doesn't match this ID...

    // int* array = StarPart_getIntArray(data, item_id);
    // if (!array) continue;
    // int length = StarPart_getArrayLength(data, item_id);
    // char name[strlen(prefix) + 20];
    // sprintf(name, "%s_%s_%s", prefix, arg_id, item_id);

    //   if (m) {
    //     if (m->elm_type == LIBGRAPH_LINE || m->elm_type == LIBGRAPH_POINT) {
    //       if (length == m->nb_nodes) LibGraph_addMeshVariable(m, name, LIBGRAPH_NODE_INTEGER, 1, array);
    //     }
    //     else {
    //       if (length == m->nb_cells) LibGraph_addMeshVariable(m, name, LIBGRAPH_CELL_INTEGER, 1, array);
    //     }
    //   }
    //   if (hm) {
    //     LibGraph_Mesh* cm = NULL;
    //     bool nodeVariable = 0;
    //     for (int i = 0; i < hm->nb_components; i++) {
    //       cm = LibGraph_getHybridMeshComponent(hm, i);
    //       if (cm->elm_type == LIBGRAPH_LINE || cm->elm_type == LIBGRAPH_POINT) {
    //         nodeVariable = 1;
    //         break;
    //       }
    //     }
    //     if (nodeVariable) {
    //       if (length == LibGraph_getHybridMeshNbNodes(hm))
    //         LibGraph_addHybridMeshVariable(hm, name, LIBGRAPH_NODE_INTEGER, 1, array);
    //     }
    //     else {
    //       if (length == LibGraph_getHybridMeshNbCells(hm))
    //         LibGraph_addHybridMeshVariable(hm, name, LIBGRAPH_CELL_INTEGER, 1, array);
    //     }
    //   }
  }
}

/* *********************************************************** */

/* var=[NULL | <arg_id> | <arg_id>/<item_id> | <item_id> ] | <var>,... */
static void storeData(StarPart_Context* ctx, StarPart_Task* task, char* var)
{
  // char* task_id = StarPart_getTaskID(task);
  GHashTableIter arg_iter;
  gpointer arg_key, arg_val;
  StarPart_Method* m = StarPart_getTaskMethod(task);
  StarPart_DataTable* args = StarPart_getMethodArgTable(m);
  g_hash_table_iter_init(&arg_iter, args);
  while (g_hash_table_iter_next(&arg_iter, &arg_key, &arg_val)) {
    StarPart_Data* arg = (StarPart_Data*)arg_val;
    char* arg_id = StarPart_getDataID(arg);
    StarPart_Data* data = StarPart_getTaskData(task, arg_id);
    storeItems(data, arg_id, var);
  }
}

/* *********************************************************** */

/* store mesh data for VTK output */
void StarPart_STORE_post(StarPart_Task* task)
{
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  StarPart_Item* item = StarPart_getItem(local, "store");
  assert(item);
  char* var = NULL;
  if (StarPart_getItemType(item) == STARPART_STR) var = StarPart_getStr(local, "store");
  storeData(ctx, task, var);
}

/* *********************************************************** */
