#include <assert.h>
#include <glib.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "io.h"
#include "libgraph.h"
#include "starpart.h"
// #include "all.h"

/* *********************************************************** */

void StarPart_LOAD_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "filename", STARPART_STR, NULL, true, STARPART_READ);
  StarPart_addNewItem(local, "coords", STARPART_STR, NULL, true, STARPART_READ);
  StarPart_addNewItem(local, "format", STARPART_STR, NEW_STR("metis"), true, STARPART_READ);
  StarPart_addNewItem(local, "gcoords", STARPART_INT, NEW_INT(0), true, STARPART_READ);
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_OUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(data, "hypergraph", STARPART_HYPERGRAPH, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(data, "mesh", STARPART_MESH, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(data, "coords", STARPART_DOUBLEARRAY, NULL, true, STARPART_WRITE);
}

/* *********************************************************** */

void StarPart_LOAD_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  char* filename = StarPart_getStr(local, "filename");
  char* coordfile = StarPart_getStr(local, "coords");
  char* format = StarPart_getStr(local, "format");
  int gcoords = StarPart_getInt(local, "gcoords");
  StarPart_checkStr(local, "format", "metis,scotch,mm,rb,mesh");
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  // LibGraph_Graph * g = StarPart_getGraph(data, "graph");
  // LibGraph_Mesh * m = StarPart_getMesh(data, "mesh");
  // assert(!g && !m);
  // LibGraph_Hypergraph * hg = StarPart_getHypergraph(data, "hypergraph");
  // assert(!g && !m && !hg);

  LibGraph_Graph* g = NULL;
  LibGraph_Mesh* m = NULL;

  int r = LibGraph_loadGraphMesh(filename, coordfile, format, &g, &m);
  assert(r);
  assert(g || m);

  /* convert to hypergraph */
  // hg = StarPart_newHypergraph();
  // graph2HypergraphV(g, hg); /* WARNING: It should be explicitly asked!!! */

  StarPart_setMesh(data, "mesh", m, true);
  // StarPart_setHypergraph(data, "hypergraph", hg, true);
  StarPart_setGraph(data, "graph", g, true);

  if (gcoords) {
    assert(coordfile || !strcmp(format, "mesh"));
    if (m->elm_type == LIBGRAPH_LINE) {
      /* we can use coords from mesh  */
      assert(g->nvtxs == m->nb_nodes);
      StarPart_setDoubleArray(data, "coords", 3*g->nvtxs, m->nodes, false);
    } else {
      double *coords = (double *)malloc(sizeof(double[3*g->nvtxs]));
      /* Compute coords */
      LibGraph_cellCenters(m, coords);
      StarPart_setDoubleArray(data, "coords", 3*g->nvtxs, coords, true);
    }
  }
}

/* *********************************************************** */

void StarPart_SAVE_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "filename", STARPART_STR, NEW_STR("output.txt"), true, STARPART_READ);
  StarPart_addNewItem(local, "coordfilename", STARPART_STR, NULL, true, STARPART_READ);
  // StarPart_addNewItem(local, "format",  STARPART_STRSEQ, NEW_STRSEQ("mesh","metis|mesh"), true,
  // STARPART_READ);
  StarPart_addNewItem(local, "format", STARPART_STR, NEW_STR("mesh"), true, STARPART_READ);
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_IN);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "mesh", STARPART_MESH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "coords", STARPART_DOUBLEARRAY, NULL, true, STARPART_READ);
  // StarPart_addNewItem(data, "hypergraph",  STARPART_HYPERGRAPH, NULL, true, 0);
}

/* *********************************************************** */

void StarPart_SAVE_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  char* filename = StarPart_getStr(local, "filename");
  char* coordfilename = StarPart_getStr(local, "coordfilename");
  // char * format = StarPart_getStrSeq(local, "format");
  char* format = StarPart_getStr(local, "format");
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  LibGraph_Mesh* m = StarPart_getMesh(data, "mesh");
  double *coords = StarPart_getDoubleArray(data, "coords");
  // LibGraph_Hypergraph * hg = StarPart_getHypergraph(data, "hypergraph");
  // assert(!g && !m && !hg);
  int r = 0;
  if (strcmp(format, "mesh") == 0) {
    assert(m);
    r = LibGraph_saveMesh(filename, m);
  }
  else if (strcmp(format, "metis") == 0 || strcmp(format, "graph") == 0) {
    assert(g);
    /* Save graph */
    r = LibGraph_saveGraph(filename, g);  // metis graph format

    /* Coords */
    if (coordfilename) {
      /* If coords is not an item */
      if (!coords) {
        assert(m->nb_cells == g->nvtxs);
        coords = (double *)malloc(sizeof(double[3*m->nb_cells]));
        /* Compute coords */
        LibGraph_cellCenters(m, coords);
        /* Save coords */
        LibGraph_saveCoordinates(coordfilename, g->nvtxs, coords);
        free(coords);
      /* If we have coords as item */
      } else {
        LibGraph_saveCoordinates(coordfilename, g->nvtxs, coords);
      }
    }
  }
  assert(r);
}

/* *********************************************************** */

void StarPart_HLOAD_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);  // TODO: use STARPART_OUT
  StarPart_addNewItem(data, "filename1", STARPART_STR, NULL, true, 0);          
  StarPart_addNewItem(data, "filename2", STARPART_STR, NULL, true, 0);          
  StarPart_addNewItem(data, "filename3", STARPART_STR, NULL, true, 0);          
  StarPart_addNewItem(data, "filename4", STARPART_STR, NULL, true, 0);          
  StarPart_addNewItem(data, "nb_components", STARPART_INT, NEW_INT(1), true, 0);
  StarPart_addNewItem(data, "coords", STARPART_STR, NULL, true, 0);              
  StarPart_addNewItem(data, "format", STARPART_STR, NEW_STR("metis"), true, 0);  
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, 0);
  StarPart_addNewItem(data, "mesh", STARPART_MESH, NULL, true, 0);
  StarPart_addNewItem(data, "hmesh", STARPART_HYBRIDMESH, NULL, true, 0);
}

/* *********************************************************** */

void StarPart_HLOAD_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  char* filename1 = StarPart_getStr(data, "filename1");
  char* filename2 = StarPart_getStr(data, "filename2");
  char* filename3 = StarPart_getStr(data, "filename3");
  char* filename4 = StarPart_getStr(data, "filename4");
  char* coordfile = StarPart_getStr(data, "coords");
  char* format = StarPart_getStr(data, "format");
  StarPart_checkStr(data, "format", "metis,scotch,mm,rb,mesh");
  int nb_components = StarPart_getInt(data, "nb_components");
  LibGraph_Graph* g = NULL;
  LibGraph_Mesh* m = NULL;
  LibGraph_HybridMesh* hm = NULL;

  switch (nb_components) {
    case 1:
      assert(filename1);
      int r = LibGraph_loadGraphMesh(filename1, coordfile, format, &g, &m);
      if (!r) exit(EXIT_FAILURE);
      assert(g || m);
      break;
    case 2:
      assert(filename1 && filename2);
      g = StarPart_newGraph();
      assert(g);
      hm = StarPart_newHybridMesh(nb_components);
      assert(hm);
      LibGraph_loadHybridMeshFiles(hm, 1, nb_components, filename1, filename2);
      LibGraph_hybridMesh2Graph(hm, g);
      break;
    case 3:
      assert(filename1 && filename2 && filename3);
      g = StarPart_newGraph();
      assert(g);
      hm = StarPart_newHybridMesh(nb_components);
      assert(hm);
      LibGraph_loadHybridMeshFiles(hm, 1, nb_components, filename1, filename2, filename3);
      LibGraph_hybridMesh2Graph(hm, g);
      assert(g);
      break;
    case 4:
      assert(filename1 && filename2 && filename3 && filename4);
      g = StarPart_newGraph();
      assert(g);
      hm = StarPart_newHybridMesh(nb_components);
      assert(hm);
      LibGraph_loadHybridMeshFiles(hm, 1, nb_components, filename1, filename2, filename3, filename4);
      LibGraph_hybridMesh2Graph(hm, g);
      assert(g);
      break;
    default: printf("[HLOAD] * ERROR: The number of components is not valid.\n");
  }

  StarPart_setGraph(data, "graph", g, true);
  if (m) StarPart_setMesh(data, "mesh", m, true);
  if (hm) StarPart_setHybridMesh(data, "hmesh", hm, true);
}

/* *********************************************************** */
 
