/** @file parallel.h */

#ifndef PARALLEL_H
#define PARALLEL_H

#include "starpart.h"

#ifdef USE_MPI

#define StarPart_MPISPAWN_desc "spawn MPI processes and run a strategy in parallel"
void StarPart_MPISPAWN_call(StarPart_Task* task);
void StarPart_MPISPAWN_init(StarPart_Method* method);

#define StarPart_SCATTER_desc "scatter a graph"
void StarPart_SCATTER_init(StarPart_Method* method);
void StarPart_SCATTER_call(StarPart_Task* task);

#define StarPart_GATHER_desc "gather a distributed graph"
void StarPart_GATHER_init(StarPart_Method* method);
void StarPart_GATHER_call(StarPart_Task* task);

#define StarPart_REDIST_desc "given a new partition, redistribute a distributed graph"
void StarPart_REDIST_init(StarPart_Method* method);
void StarPart_REDIST_call(StarPart_Task* task);

#define StarPart_GATHERP_desc "gather a distributed partition"
void StarPart_GATHERP_init(StarPart_Method* method);
void StarPart_GATHERP_call(StarPart_Task* task);

#define StarPart_RANDOMP_desc "make a random partition"
void StarPart_RANDOMP_init(StarPart_Method* method);
void StarPart_RANDOMP_call(StarPart_Task* task);

#define StarPart_GRAPHTODISTGRAPH_desc "convert a graph to a distgraph (not distributer"
void StarPart_GRAPHTODISTGRAPH_init(StarPart_Method* method);
void StarPart_GRAPHTODISTGRAPH_call(StarPart_Task* task);

#define StarPart_BARRIER_desc "wait other processes"
void StarPart_BARRIER_init(StarPart_Method* method);
void StarPart_BARRIER_call(StarPart_Task* task);

#endif

#define StarPart_LOADP_desc "load a graph in parallel"
void StarPart_LOADP_init(StarPart_Method* method);
void StarPart_LOADP_call(StarPart_Task* task);

#endif
