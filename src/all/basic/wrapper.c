#include <stdlib.h>

#include "basic.h"
#include "bench.h"
#include "hun.h"
#include "io.h"
#include "ml.h"
#include "vtk.h"
#include "wrapper.h"
#ifdef USE_MPI
#include "parallel.h"
#endif

#include "starpart.h"

/* *********************************************************** */

void StarPart_registerBasic(StarPart_Context* ctx)
{
  /* ******************** METHODS ******************** */

  /* BASIC */
  StarPart_registerNewMethod(ctx, "BASIC/NOP", StarPart_NOP_call, StarPart_NOP_init, StarPart_NOP_desc);
  StarPart_registerNewMethod(ctx, "BASIC/CMP", StarPart_CMP_call, StarPart_CMP_init, StarPart_CMP_desc);
  StarPart_registerNewMethod(ctx, "BASIC/G2HG", StarPart_G2HG_call, StarPart_G2HG_init, StarPart_G2HG_desc);
  StarPart_registerNewMethod(ctx, "BASIC/RANDOM", StarPart_RANDOM_call, StarPart_RANDOM_init, StarPart_RANDOM_desc);
  StarPart_registerNewMethod(ctx, "BASIC/BFS", StarPart_BFS_call, StarPart_BFS_init, StarPart_BFS_desc);
  StarPart_registerNewMethod(ctx, "BASIC/SPLIT", StarPart_SPLIT_call, StarPart_SPLIT_init, StarPart_SPLIT_desc);
  StarPart_registerNewMethod(ctx, "BASIC/LOAD", StarPart_LOAD_call, StarPart_LOAD_init, StarPart_LOAD_desc);
  StarPart_registerNewMethod(ctx, "BASIC/SAVE", StarPart_SAVE_call, StarPart_SAVE_init, StarPart_SAVE_desc);
  StarPart_registerNewMethod(ctx, "BASIC/FIXED", StarPart_FIXED_call, StarPart_FIXED_init, StarPart_FIXED_desc);
  StarPart_registerNewMethod(ctx, "BASIC/WEIGHT", StarPart_WEIGHT_call, StarPart_WEIGHT_init, StarPart_WEIGHT_desc);
  StarPart_registerNewMethod(ctx, "BASIC/EQUAL", StarPart_EQUAL_call, StarPart_EQUAL_init, StarPart_EQUAL_desc);
  StarPart_registerNewMethod(ctx, "BASIC/CHECK", StarPart_CHECK_call, StarPart_CHECK_init, StarPart_CHECK_desc);
  StarPart_registerNewMethod(ctx, "BASIC/CHECKITEMS", StarPart_CHECKITEMS_call, StarPart_CHECKITEMS_init, StarPart_CHECKITEMS_desc);
  StarPart_registerNewMethod(ctx, "BASIC/PART", StarPart_PART_call, StarPart_PART_init, StarPart_PART_desc);
  StarPart_registerNewMethod(ctx, "BASIC/REMAP", StarPart_REMAP_call, StarPart_REMAP_init, StarPart_REMAP_desc);
  StarPart_registerNewMethod(ctx, "BASIC/LOOP", StarPart_LOOP_call, StarPart_LOOP_init, StarPart_LOOP_desc);
  StarPart_registerNewMethod(ctx, "BASIC/NESTED", StarPart_NESTED_call, StarPart_NESTED_init, StarPart_NESTED_desc);
  // StarPart_registerNewMethod(ctx, "BASIC/FORALL", StarPart_FORALL_call, StarPart_FORALL_init,
  // StarPart_FORALL_desc);

  /* DATA */
  StarPart_registerNewMethod(ctx, "BASIC/DUP", StarPart_DUP_call, StarPart_DUP_init, StarPart_DUP_desc);
  StarPart_registerNewMethod(ctx, "BASIC/GRID", StarPart_GRID_call, StarPart_GRID_init, StarPart_GRID_desc);
  StarPart_registerNewMethod(ctx, "BASIC/DATA", StarPart_DATA_call, StarPart_DATA_init, StarPart_DATA_desc);
  StarPart_registerNewMethod(ctx, "BASIC/INT", StarPart_INT_call, StarPart_INT_init, StarPart_INT_desc);
  // StarPart_registerNewMethod(ctx, "BASIC/INTARRAY", StarPart_INTARRAY_call,
  // StarPart_INTARRAY_init, StarPart_INTARRAY_desc);

  /* BENCH */
  StarPart_registerNewMethod(ctx, "BASIC/BENCH", StarPart_BENCH_call, StarPart_BENCH_init, StarPart_BENCH_desc);

  /* ML */
  StarPart_registerNewMethod(ctx, "BASIC/KFM", StarPart_KFM_call, StarPart_KFM_init, StarPart_KFM_desc);
  StarPart_registerNewMethod(ctx, "BASIC/COARSEN", StarPart_COARSEN_call, StarPart_COARSEN_init, StarPart_COARSEN_desc);
  StarPart_registerNewMethod(ctx, "BASIC/ML", StarPart_ML_call, StarPart_ML_init, StarPart_ML_desc);

  /* HUN */
  StarPart_registerNewMethod(ctx, "BASIC/HUN", StarPart_HUN_call, StarPart_HUN_init, StarPart_HUN_desc);

  /* HYBRID*/
  StarPart_registerNewMethod(ctx, "BASIC/HLOAD", StarPart_HLOAD_call, StarPart_HLOAD_init, StarPart_HLOAD_desc);
  StarPart_registerNewMethod(ctx, "BASIC/HVTK", StarPart_HVTK_call, StarPart_HVTK_init, StarPart_HVTK_desc);

  /* PRINT */
  StarPart_registerNewMethod(ctx, "BASIC/PRINT", StarPart_PRINT_call, StarPart_PRINT_init, StarPart_PRINT_desc);
  StarPart_registerNewMethod(ctx, "BASIC/PRINTALL", StarPart_PRINTALL_call, StarPart_PRINTALL_init,
                             StarPart_PRINTALL_desc);
  StarPart_registerNewMethod(ctx, "BASIC/DIAG", StarPart_DIAG_call, StarPart_DIAG_init, StarPart_DIAG_desc);
  StarPart_registerNewMethod(ctx, "BASIC/VTK", StarPart_VTK_call, StarPart_VTK_init, StarPart_VTK_desc);

  StarPart_registerNewMethod(ctx, "BASIC/PERMUT", StarPart_PERMUT_call, StarPart_PERMUT_init, StarPart_PERMUT_desc);

/* PARALLEL */
#ifdef USE_MPI
  if (StarPart_isParallelContext(ctx)) {
    StarPart_registerNewMethod(ctx, "BASIC/MPISPAWN", StarPart_MPISPAWN_call, StarPart_MPISPAWN_init,
                               StarPart_MPISPAWN_desc);
    StarPart_registerNewMethod(ctx, "BASIC/SCATTER", StarPart_SCATTER_call, StarPart_SCATTER_init,
                               StarPart_SCATTER_desc);
    StarPart_registerNewMethod(ctx, "BASIC/GATHER", StarPart_GATHER_call, StarPart_GATHER_init, StarPart_GATHER_desc);
    StarPart_registerNewMethod(ctx, "BASIC/REDIST", StarPart_REDIST_call, StarPart_REDIST_init, StarPart_REDIST_desc);
    StarPart_registerNewMethod(ctx, "BASIC/GATHERP", StarPart_GATHERP_call, StarPart_GATHERP_init, StarPart_GATHERP_desc);
    StarPart_registerNewMethod(ctx, "BASIC/RANDOMP", StarPart_RANDOMP_call, StarPart_RANDOMP_init, StarPart_RANDOMP_desc);
    StarPart_registerNewMethod(ctx, "BASIC/GRAPHTODISTGRAPH", StarPart_GRAPHTODISTGRAPH_call, StarPart_GRAPHTODISTGRAPH_init, StarPart_GRAPHTODISTGRAPH_desc);
    StarPart_registerNewMethod(ctx, "BASIC/BARRIER", StarPart_BARRIER_call, StarPart_BARRIER_init, StarPart_BARRIER_desc);
    StarPart_registerNewMethod(ctx, "BASIC/LOADP", StarPart_LOADP_call, StarPart_LOADP_init, StarPart_LOADP_desc);
  }
#endif

  /* ******************** HOOKS ******************** */

  StarPart_registerNewHook(ctx, "debug", StarPart_DEBUG_pre, StarPart_DEBUG_post, StarPart_DEBUG_desc);
  StarPart_registerNewHook(ctx, "store", NULL, StarPart_STORE_post, StarPart_STORE_desc);
  StarPart_registerNewHook(ctx, "time", StarPart_TIME_pre, StarPart_TIME_post, StarPart_TIME_desc);
}

/* *********************************************************** */
