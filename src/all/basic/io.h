/** @file io.h */

#ifndef BASIC_IO_H
#define BASIC_IO_H

#include "starpart.h"

#define StarPart_LOAD_desc "load graph/hypergraph/mesh from file"
void StarPart_LOAD_call(StarPart_Task* task);
void StarPart_LOAD_init(StarPart_Method* method);

#define StarPart_SAVE_desc "save graph/hypergraph/mesh to file"
void StarPart_SAVE_call(StarPart_Task* task);
void StarPart_SAVE_init(StarPart_Method* method);

#define StarPart_HLOAD_desc ""
void StarPart_HLOAD_call(StarPart_Task* task);
void StarPart_HLOAD_init(StarPart_Method* method);

#endif
