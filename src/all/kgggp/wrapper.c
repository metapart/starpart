/* Wrapper library for KGGGP */

#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "all.h"
#include "kgggp.h"
#include "libgraph.h"
#include "starpart.h"
#include "wrapper.h"

/* *********************************************************** */

void StarPart_KGGGP_PART_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "gainscheme", STARPART_STR, NEW_STR("classic"), true, STARPART_READ);
  StarPart_addNewItem(local, "npass", STARPART_INT, NEW_INT(1), true, STARPART_READ);
  StarPart_addNewItem(local, "internaledgefactor", STARPART_INT, NEW_INT(KGGGP_INTERNALEDGEFACTOR), true, STARPART_READ);
  StarPart_addNewItem(local, "maxbestcandidates", STARPART_INT, NEW_INT(KGGGP_MAXBESTCANDIDATES), true, STARPART_READ);
  StarPart_addNewItem(local, "shrinking", STARPART_INT, NEW_INT(KGGGP_SHRINKING_NO), true, STARPART_READ);
  StarPart_addNewItem(local, "useseeds", STARPART_INT, NEW_INT(KGGGP_USE_SEEDS), true, STARPART_READ);
  StarPart_addNewItem(local, "greedy", STARPART_STR, NEW_STR("local"), true, STARPART_READ);
  StarPart_addNewItem(local, "connectivity", STARPART_INT, NEW_INT(KGGGP_CONNECTIVITY_NO), true, STARPART_READ);

  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_KGGGP_PART_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  int connectivity = StarPart_getInt(local, "connectivity");
  int useseeds = StarPart_getInt(local, "useseeds");
  int npass = StarPart_getInt(local, "npass");
  int internaledgefactor = StarPart_getInt(local, "internaledgefactor");
  int maxbestcandidates = StarPart_getInt(local, "maxbestcandidates");
  int shrinking = StarPart_getInt(local, "shrinking");
  char* _greedy = StarPart_getStr(local, "greedy");
  char* _gainscheme = StarPart_getStr(local, "gainscheme");
  StarPart_checkStr(local, "gainscheme", "classic,diff,hybrid");
  StarPart_checkStr(local, "greedy", "local,global");

  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  int nparts = StarPart_getInt(data, "nparts");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  assert(g);
  assert(nparts > 0);

  int greedy = -1;
  if (!strcmp(_greedy, "local"))
    greedy = KGGGP_GREEDY_LOCAL;
  else if (!strcmp(_greedy, "global"))
    greedy = KGGGP_GREEDY_GLOBAL;
  assert(greedy != -1);
  int gainscheme = -1;
  if (!strcmp(_gainscheme, "classic"))
    gainscheme = KGGGP_GAIN_CLASSIC;
  else if (!strcmp(_gainscheme, "diff"))
    gainscheme = KGGGP_GAIN_DIFF;
  else if (!strcmp(_gainscheme, "hybrid"))
    gainscheme = KGGGP_GAIN_HYBRID;
  assert(gainscheme != -1);

  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", g->nvtxs);
    StarPart_initArray(data, "part", -1);
  }

  if (fixed) StarPart_checkArray(data, "fixed", g->nvtxs);

  /* set free vertices in input partition */
  for (int i = 0; i < g->nvtxs; i++) part[i] = fixed ? fixed[i] : -1;

  LibGraph_kgggp(g, nparts, ubfactor, gainscheme, greedy, connectivity, useseeds, npass, internaledgefactor,
                 maxbestcandidates, shrinking, part, NULL);
}

/* *********************************************************** */
