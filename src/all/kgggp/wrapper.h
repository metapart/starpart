#ifndef KGGGP_WRAPPER_H
#define KGGGP_WRAPPER_H

#include "starpart.h"

//@{

#define StarPart_KGGGP_PART_desc "initial graph partitioning routine based on k-way greedy growing algorithm"
void StarPart_KGGGP_PART_init(StarPart_Method* method);
void StarPart_KGGGP_PART_call(StarPart_Task* task);

//@}

#endif
