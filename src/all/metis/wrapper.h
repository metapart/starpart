#ifndef STARPART_METIS_H
#define STARPART_METIS_H

#include "starpart.h"

//@{

void StarPart_registerMetis(StarPart_Context* ctx);

#define StarPart_METIS_RPART_desc "partition graph with multilevel RB Metis"
void StarPart_METIS_RPART_call(StarPart_Task* task);
void StarPart_METIS_RPART_init(StarPart_Method* method);

#define StarPart_METIS_KPART_desc "partition graph with multilevel KW Metis"
void StarPart_METIS_KPART_call(StarPart_Task* task);
void StarPart_METIS_KPART_init(StarPart_Method* method);

#define StarPart_METIS_ORDER_desc "graph odering with Metis"
void StarPart_METIS_ORDER_call(StarPart_Task* task);
void StarPart_METIS_ORDER_init(StarPart_Method* method);

//@}

#endif
