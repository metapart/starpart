/* Wrapper library for Metis */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "all.h"
#include "libgraph.h"
#include "metis.h"
#include "starpart.h"
#include "wrapper.h"

/* *********************************************************** */

void StarPart_METIS_RPART_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "nrefpass", STARPART_INT, NEW_INT(STARPART_NREFPASS), true, STARPART_READ);
  StarPart_addNewItem(local, "npass", STARPART_INT, NEW_INT(1), true, STARPART_READ);
  StarPart_addNewItem(local, "seed", STARPART_INT, NEW_INT(0), true, STARPART_READ);
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
}

/* *********************************************************** */

#ifdef DOPATCH
void libmetis__InitRandom(idx_t seed)
{ /* srand ignore */
}
#endif

/* *********************************************************** */

void StarPart_METIS_RPART_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  int nrefpass = StarPart_getInt(local, "nrefpass");
  int npass = StarPart_getInt(local, "npass");
  int seed = StarPart_getInt(local, "seed");
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int nparts = StarPart_getInt(data, "nparts");

  assert(g && nparts > 0);

  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", g->nvtxs);
    StarPart_initArray(data, "part", -1);
  }

  assert(sizeof(idx_t) == sizeof(int32_t));  // check 32 bits

  /* Default Options for METIS_PartRecursive. */
  idx_t options[METIS_NOPTIONS];
  METIS_SetDefaultOptions(options);
  options[METIS_OPTION_NUMBERING] = 0;            /* C numbering scheme */
  options[METIS_OPTION_SEED] = seed;              /* set random seed */
  options[METIS_OPTION_NCUTS] = npass;            /* nb of multilevel cycles */
  options[METIS_OPTION_NITER] = nrefpass;         /* nb of refinement pass */
  options[METIS_OPTION_CTYPE] = METIS_CTYPE_SHEM; /* coarsening */
  options[METIS_OPTION_RTYPE] = METIS_RTYPE_FM;   /* FM refinement */
  options[METIS_OPTION_UFACTOR] = ubfactor * 10;  /* unbalance factor */

  if (nparts == 1) { /* Bug with Metis in this case! */
    for (int i = 0; i < g->nvtxs; i++) part[i] = 0;
    return;
  }

  int ncon = 1;  // single constraint
  real_t ub = 1.0 + ubfactor / 100.0;
  idx_t edgecut;

  METIS_PartGraphRecursive((idx_t*)&g->nvtxs, (idx_t*)&ncon, (idx_t*)g->xadj, (idx_t*)g->adjncy, (idx_t*)g->vwgts, NULL,
                           (idx_t*)g->ewgts, (idx_t*)&(nparts), NULL, /* twgts */
                           &ub, options, &edgecut, (idx_t*)part);
}

/* *********************************************************** */

void StarPart_METIS_KPART_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "nrefpass", STARPART_INT, NEW_INT(STARPART_NREFPASS), true, STARPART_READ);
  StarPart_addNewItem(local, "npass", STARPART_INT, NEW_INT(1), true, STARPART_READ);
  StarPart_addNewItem(local, "seed", STARPART_INT, NEW_INT(0), true, STARPART_READ);
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_METIS_KPART_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  int nrefpass = StarPart_getInt(local, "nrefpass");
  int npass = StarPart_getInt(local, "npass");
  int seed = StarPart_getInt(local, "seed");
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int nparts = StarPart_getInt(data, "nparts");

  assert(g && nparts > 0);

  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", g->nvtxs);
    StarPart_initArray(data, "part", -1);
  }

  // check metis size of type
  assert(sizeof(idx_t) == sizeof(int32_t));
  assert(sizeof(real_t) == sizeof(float));

  if (nparts == 1) { /* Bug with Metis in this case! */
    for (int i = 0; i < g->nvtxs; i++) part[i] = 0;
    return;
  }

  /* Default Options for METIS_PartRecursive. */
  idx_t options[METIS_NOPTIONS];
  METIS_SetDefaultOptions(options);
  options[METIS_OPTION_NUMBERING] = 0;            /* C numbering scheme */
  options[METIS_OPTION_SEED] = 0;                 /* set random seed */
  options[METIS_OPTION_NCUTS] = 1;                /* nb of multilevel cycles */
  options[METIS_OPTION_NITER] = nrefpass;         /* nb of refinement pass */
  options[METIS_OPTION_CTYPE] = METIS_CTYPE_SHEM; /* coarsening */
  options[METIS_OPTION_RTYPE] = METIS_RTYPE_FM;   /* FM refinement */
  options[METIS_OPTION_UFACTOR] = ubfactor * 10;  /* unbalance factor */

  // all KMetis options
  /* options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_CUT; */
  /* options[METIS_OPTION_CTYPE ] = METIS_CTYPE_SHEM; // METIS_CTYPE_RM */
  /* options[METIS_OPTION_IPTYPE] = METIS_IPTYPE_GROW; // ... */
  /* options[METIS_OPTION_RTYPE] = 0; */
  /* options[METIS_OPTION_NO2HOP] = 0; */
  /* options[METIS_OPTION_NCUTS] = 0; */
  /* options[METIS_OPTION_NITER] = 0; */
  /* options[METIS_OPTION_UFACTOR] = 0; */
  /* options[METIS_OPTION_MINCONN] = 0; */
  /* options[METIS_OPTION_CONTIG] = 0; */
  /* options[METIS_OPTION_SEED] = 0; */
  /* options[METIS_OPTION_NUMBERING] = 0; */
  /* options[METIS_OPTION_DBGLVL] = 0; */

  int ncon = 1;  // single constraint
  real_t ub = 1.0 + ubfactor / 100.0;
  idx_t edgecut;
  METIS_PartGraphKway((idx_t*)&g->nvtxs, (idx_t*)&ncon, (idx_t*)g->xadj, (idx_t*)g->adjncy, (idx_t*)g->vwgts, NULL,
                      (idx_t*)g->ewgts, (idx_t*)&(nparts), NULL, /* twgts */
                      &ub, options, &edgecut, (idx_t*)part);
}

/* *********************************************************** */

void StarPart_METIS_ORDER_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "mesh", STARPART_MESH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "perm", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
}

/* *********************************************************** */

// This function computes fill reducing orderings of sparse matrices
// using the multilevel nested dissection algorithm.
// => https://gitlab.inria.fr/solverstack/pastix/blob/master/order/order_compute_metis.c

void StarPart_METIS_ORDER_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  assert(g);
  int* perm = StarPart_getIntArray(data, "perm");

  if (perm)
    StarPart_checkArray(data, "perm", g->nvtxs);
  else {
    perm = StarPart_allocArray(data, "perm", g->nvtxs);
    StarPart_initArray(data, "perm", -1);
  }

  assert(sizeof(idx_t) == sizeof(int32_t));  // check 32 bits

  /* Default Options for METIS_PartRecursive. */
  idx_t options[METIS_NOPTIONS];
  METIS_SetDefaultOptions(options);
  options[METIS_OPTION_NUMBERING] = 0; /* numbering scheme: C-style */
  options[METIS_OPTION_SEED] = 0;      /* set random seed */

  /* PASTIX DEFAULT OPTIONS */
  options[METIS_OPTION_CTYPE] = METIS_CTYPE_SHEM;      /* coarsen: sorted heavy edge matching */
  options[METIS_OPTION_RTYPE] = METIS_RTYPE_SEP1SIDED; /* refine: FM, ... */
  options[METIS_OPTION_NO2HOP] = 0;                    /* coarsen: disable 2-hop matching */
  options[METIS_OPTION_NSEPS] = 1;     /* nb of different separators computed at each level of ND (default 1) */
  options[METIS_OPTION_NITER] = 10;    /* nb of iterations for the refinement algorithms (default 10) */
  options[METIS_OPTION_UFACTOR] = 200; /* imbalance factor */
  options[METIS_OPTION_COMPRESS] = 1;  /* try to compress the graph */
  options[METIS_OPTION_CCORDER] = 0;   /* connected components are ordered separately */
  options[METIS_OPTION_PFACTOR] = 0;   /* minimum degree of the vertices that will be ordered last */
  options[METIS_OPTION_DBGLVL] = 0;    /* debug level */

  int* iperm = malloc(g->nvtxs * sizeof(int));  // inverse permutation (array of size g->nvtxs)

  int r = METIS_NodeND((idx_t*)&g->nvtxs, (idx_t*)g->xadj, (idx_t*)g->adjncy, (idx_t*)g->vwgts, options,
                       iperm,  // called perm in Metis Manual
                       perm    // called iperm in Metis Manual
  );

  assert(r == METIS_OK);

  free(iperm);
}

/* *********************************************************** */

void StarPart_registerMetis(StarPart_Context* ctx)
{
  StarPart_registerNewMethod(ctx, "METIS/RPART", StarPart_METIS_RPART_call, StarPart_METIS_RPART_init,
                             StarPart_METIS_RPART_desc);
  StarPart_registerNewMethod(ctx, "METIS/KPART", StarPart_METIS_KPART_call, StarPart_METIS_KPART_init,
                             StarPart_METIS_KPART_desc);
  StarPart_registerNewMethod(ctx, "METIS/ORDER", StarPart_METIS_ORDER_call, StarPart_METIS_ORDER_init,
                             StarPart_METIS_ORDER_desc);
}

/* *********************************************************** */
