#!/bin/bash

LIBRARY=$1
PREFIX=$2

# COMMAND bash -c "strip -w -K mtmetis* -K MTMETIS* ${CMAKE_CURRENT_BINARY_DIR}/lib/libmtmetis.a"
# COMMAND bash -c "ranlib ${CMAKE_CURRENT_BINARY_DIR}/lib/libmtmetis.a"
# COMMAND bash -c "objcopy --prefix-symbols=${MTMETIS_PREFIX} ${CMAKE_CURRENT_BINARY_DIR}/lib/libmtmetis.a"
# COMMAND bash -c "objcopy -G mtmetis_partition_explicit  -G mtmetis_init_options ${CMAKE_CURRENT_BINARY_DIR}/lib/libmtmetis.a"
# COMMAND bash -c "objcopy --redefine-sym ${MTMETIS_PREFIX}mtmetis_partition_explicit=mtmetis_partition_explicit --redefine-sym ${MTMETIS_PREFIX}mtmetis_init_options=mtmetis_init_options ${CMAKE_CURRENT_BINARY_DIR}/lib/libmtmetis.a"
# COMMAND bash -c "nm ${CMAKE_CURRENT_BINARY_DIR}/lib/libmtmetis.a | grep -e ' T ' -e ' t ' | cut -d ' ' -f 3 | sort | uniq > symbols.txt"
# COMMAND sed -e "s/\(.*\)\$/\\1 ${MTMETIS_PREFIX}\\1/" symbols.txt > rename.txt
# COMMAND bash -c "awk {print \$1 \" xyz_\" \$1} symbols.txt > rename.txt"
# COMMAND bash -c "objcopy --redefine-syms=rename.txt ${CMAKE_CURRENT_BINARY_DIR}/lib/libmtmetis.a"

# nm --defined $OBJFILE | cut -f 3 -d ' ' -s | sed "s/\(.*\)$/\1 $PREFIX\1/"  | sort | uniq > symbols.txt
nm --defined $LIBRARY  | cut -d ' ' -f 3 -s | sort | uniq > symbols.txt
# nm $LIBRARY  | grep -e ' T ' -e ' t ' | cut -d ' ' -f 3 | sort | uniq > symbols.txt
awk "{print \$1 \" $PREFIX\" \$1}" symbols.txt > rename.txt
objcopy --redefine-syms="rename.txt" $LIBRARY

# echo "done"

