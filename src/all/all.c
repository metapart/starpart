#include "all.h"
#include <stdlib.h>
#include "starpart.h"

#ifdef USE_BASIC
#include "basic/wrapper.h"
#endif

/* external */
#ifdef USE_METIS
#include "metis/wrapper.h"
#endif
#ifdef USE_MTMETIS
#include "mtmetis/wrapper.h"
#endif
#ifdef USE_PARMETIS
#include "parmetis/wrapper.h"
#endif
#ifdef USE_SCOTCH
#include "scotch/wrapper.h"
#endif
#ifdef USE_PTSCOTCH
#include "scotch/wrapper.h"
#endif
#ifdef USE_KGGGP
#include "kgggp/wrapper.h"
#endif
#ifdef USE_ZOLTAN
#include "zoltan/wrapper.h"
#endif
#ifdef USE_PATOH
#include "patoh/wrapper.h"
#endif
#ifdef USE_KAHIP
#include "kahip/wrapper.h"
#endif
#ifdef USE_MONDRIAAN
#include "mondriaan/wrapper.h"
#endif
#ifdef USE_PULP
#include "pulp/wrapper.h"
#endif

/* *********************************************************** */

void StarPart_registerAllMethods(StarPart_Context* ctx)
{
#ifdef USE_BASIC
  StarPart_registerBasic(ctx);
#endif

#ifdef USE_KGGGP
  StarPart_registerNewMethod(ctx, "KGGGP/PART", StarPart_KGGGP_PART_call, StarPart_KGGGP_PART_init,
                             StarPart_KGGGP_PART_desc);
#endif

#if defined(USE_SCOTCH) || defined(USE_PTSCOTCH)
  StarPart_registerScotch(ctx);
#endif

#ifdef USE_METIS
  StarPart_registerMetis(ctx);
#endif

#ifdef USE_PARMETIS
  StarPart_registerParmetis(ctx);
#endif

#ifdef USE_MTMETIS
  StarPart_registerNewMethod(ctx, "MTMETIS/PART", StarPart_MTMETIS_PART_call, StarPart_MTMETIS_PART_init,
                             StarPart_MTMETIS_PART_desc);
#endif

#ifdef USE_ZOLTAN
  if (StarPart_isParallelContext(ctx)) {
    StarPart_registerNewMethod(ctx, "ZOLTAN/PART", StarPart_ZOLTAN_PART_call, StarPart_ZOLTAN_PART_init,
                               StarPart_ZOLTAN_PART_desc);
  }
#endif

#ifdef USE_PATOH
  StarPart_registerNewMethod(ctx, "PATOH/PART", StarPart_PATOH_PART_call, StarPart_PATOH_PART_init,
                             StarPart_PATOH_PART_desc);
#endif

#ifdef USE_KAHIP
  StarPart_registerNewMethod(ctx, "KAHIP/PART", StarPart_KAHIP_PART_call, StarPart_KAHIP_PART_init,
                             StarPart_KAHIP_PART_desc);
#endif

#ifdef USE_MONDRIAAN
  StarPart_registerNewMethod(ctx, "MONDRIAAN/PART", StarPart_MONDRIAAN_PART_call, StarPart_MONDRIAAN_PART_init,
                             StarPart_MONDRIAAN_PART_desc);
#endif

#ifdef USE_PULP
  StarPart_registerNewMethod(ctx, "PULP/PART", StarPart_PULP_PART_call, StarPart_PULP_PART_init,
                             StarPart_PULP_PART_desc);
#endif
}

/* *********************************************************** */
