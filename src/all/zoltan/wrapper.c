/* Wrapper library for ZOLTAN */

#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "all.h"
#include "libgraph.h"
#include "starpart.h"
#include "wrapper.h"
#include "zoltan.h"

/* *********************************************************** */
/*                          Zoltan Callbacks                   */
/* *********************************************************** */

typedef struct ZoltanData
{
  LibGraph_Hypergraph* hg;
  int prank;
  int psize;
  int* vtxdist;  // of size psize+1
  LibGraph_Hypergraph* subhg;
  int* vtxmap;
  int* hedgemap;
  int* fixed;
} ZoltanData;

/* *********************************************************** */

/* A ZOLTAN_NUM_OBJ_FN query function returns the number of objects that are currently assigned to the processor. */
static int getNumberOfVertices(void* data, int* ierr)
{
  ZoltanData* _data = (ZoltanData*)data;
  // Hypergraph* hg = _data->hg;
  LibGraph_Hypergraph* subhg = _data->subhg;
  *ierr = ZOLTAN_OK;
  // return hg->nvtxs;  // global nb of vertices
  // int nvtxs = _data->vtxdist[_data->prank + 1] - _data->vtxdist[_data->prank];
  // return nvtxs;  // local nb of vertices owned by this process
  return subhg->nvtxs;
}

/* *********************************************************** */

/* A ZOLTAN_OBJ_LIST_FN query function fills two (three if weights are used) arrays with information about the objects
 * currently assigned to the processor. Both arrays are allocated (and subsequently freed) by Zoltan; their size is
 * determined by a call to a ZOLTAN_NUM_OBJ_FN query function to get the array size. For many algorithms, either a
 * ZOLTAN_OBJ_LIST_FN query function or a ZOLTAN_FIRST_OBJ_FN/ZOLTAN_NEXT_OBJ_FN query-function pair must be registered;
 * however, both query options need not be provided. The ZOLTAN_OBJ_LIST_FN is preferred for efficiency. */

static void getVertexList(void* data, int sizeGID, int sizeLID, ZOLTAN_ID_PTR globalID, ZOLTAN_ID_PTR localID,
                          int wgt_dim, float* obj_wgts, int* ierr)
{
  ZoltanData* _data = (ZoltanData*)data;
  // Hypergraph* hg = _data->hg;
  *ierr = ZOLTAN_OK;
  int nvtxs = _data->vtxdist[_data->prank + 1] - _data->vtxdist[_data->prank];
  // printf("proc #%d: sizeLID = %d, sizeGID = %d\n", _data->prank, sizeLID, sizeGID);
  assert(sizeGID == 1 && sizeLID == 1);  // ???

  // for (int i = 0; i < hg->nvtxs; i++) {
  //   globalID[i] = localID[i] = i;
  //   if (hg->vwgts) obj_wgts[i] = hg->vwgts[i];
  // }

  for (int i = 0; i < nvtxs; i++) {
    localID[i] = i;
    // globalID[i] = _data->vtxdist[_data->prank] + i;
    globalID[i] = _data->vtxmap[i];  // local-to-global mapping
  }
}

/* *********************************************************** */

// http://www.cs.sandia.gov/zoltan/ug_html/ug_query_lb.html#ZOLTAN_HG_SIZE_CS_FN

/* A ZOLTAN_HG_CS_FN returns a hypergraph in a compressed storage (CS) format. The size and format of the data to be
returned must have been supplied to Zoltan using a ZOLTAN_HG_SIZE_CS_FN_TYPE function.
When a hypergraph is distributed across multiple processes, Zoltan expects that all processes share a consistent global
numbering scheme for hyperedges and vertices. Also, no two processes should return the same pin (matrix non-zero) in
this query function. (Pin ownership is unique.) */

static void getHypergraphSize(void* data, int* num_lists, int* num_nonzeroes, int* format, int* ierr)
{
  ZoltanData* _data = (ZoltanData*)data;
  // Hypergraph* hg = _data->hg;
  LibGraph_Hypergraph* subhg = _data->subhg;
  *ierr = ZOLTAN_OK;
  *format = ZOLTAN_COMPRESSED_EDGE;              // classic CSR scheme
  *num_lists = subhg->nhedges;                   // number of hyperedges (local)
  *num_nonzeroes = subhg->eptr[subhg->nhedges];  // number of pins/nonzeroes (local)

  // *num_lists = hg->nhedges;                // number of hyperedges (global)
  // *num_nonzeroes = hg->eptr[hg->nhedges];  // number of pins/nonzeroes (global)
}

/* *********************************************************** */

static void getHypergraph(void* data, int sizeGID, int num_edges, int num_nonzeroes, int format, ZOLTAN_ID_PTR edgeGID,
                          int* vtxPtr, ZOLTAN_ID_PTR vtxGID, int* ierr)
{
  ZoltanData* _data = (ZoltanData*)data;
  // Hypergraph* hg = _data->hg;
  LibGraph_Hypergraph* subhg = _data->subhg;

  *ierr = ZOLTAN_OK;
  assert(format == ZOLTAN_COMPRESSED_EDGE);

  // if (num_edges != hg->nhedges || num_nonzeroes != hg->eptr[hg->nhedges] || format != ZOLTAN_COMPRESSED_EDGE) {
  //   *ierr = ZOLTAN_FATAL;
  //   return;
  // }
  printf("proc #%d: getHypergraph() num_edges=%d, num_nonzeroes=%d\n", _data->prank, num_edges, num_nonzeroes);

  for (int i = 0; i < num_edges; i++) {
    // edgeGID[i] = i;
    edgeGID[i] = _data->hedgemap[i];  // global hyperedge index
    vtxPtr[i] = subhg->eptr[i];
  }
  for (int i = 0; i < num_nonzeroes; i++) { vtxGID[i] = subhg->eind[i]; }  // local index ???
}

/* *********************************************************** */

// static void getNumWeightedHyperedges(void* data, int* num_edges, int* ierr)
// {
//   ZoltanData* _data = (ZoltanData*)data;
//   Hypergraph* hg = _data->hg;
//   *ierr = ZOLTAN_OK;
//   if (hg->hewgts)
//     *num_edges = hg->nhedges;
//   else
//     *num_edges = 0;
// }

/* *********************************************************** */

// static void getHyperedgeWeight(void* data, int num_gid_entries, int num_lid_entries, int num_edges, int
// edge_weight_dim,
//                                ZOLTAN_ID_PTR edge_GID, ZOLTAN_ID_PTR edge_LID, float* edge_weight, int* ierr)
// {
//   ZoltanData* _data = (ZoltanData*)data;
//   Hypergraph* hg = _data->hg;
//   *ierr = ZOLTAN_OK;
//   for (int i = 0; i < hg->nhedges; i++) {
//     edge_GID[i] = edge_LID[i] = i;
//     edge_weight[i] = hg->hewgts[i];
//   }
// }

/* *********************************************************** */

// static int getNumFixedVertices(void* data, int* ierr)
// {
//   ZoltanData* _data = (ZoltanData*)data;
//   Hypergraph* hg = _data->hg;
//   *ierr = ZOLTAN_OK;
//   int n = 0;
//   for (int i = 0; i < hg->nvtxs; i++)
//     if (_data->fixed[i] != -1) n++;
//   return n;
// }

/* *********************************************************** */

// static void getFixedVertexList(void* data, int num_fixed_obj, int num_gid_entries, ZOLTAN_ID_PTR fixed_gids,
//                                int* fixed_parts, int* ierr)
// {
//   ZoltanData* _data = (ZoltanData*)data;
//   Hypergraph* hg = _data->hg;
//   *ierr = ZOLTAN_OK;
//   for (int i = 0, k = 0; i < hg->nvtxs; i++)
//     if (_data->fixed[i] != -1) {
//       fixed_gids[k] = i;
//       fixed_parts[k] = _data->fixed[i];
//       ++k;
//     }
// }

/* *********************************************************** */
/*                Zoltan Load-Balancing Routines               */
/* *********************************************************** */

// http://www.cs.sandia.gov/Zoltan/ug_html/ug_alg_phg.html
// https://github.com/trilinos/Trilinos_tutorial/wiki/ZoltanSimplePHG

static void partitionHypergraphZoltan(LibGraph_Hypergraph* hg, int nparts, int ubfactor, int* part, int* edgecut,
                                      bool usefixed, MPI_Comm comm)
{
  float ver;
  Zoltan_Initialize(0, NULL, &ver);
  // struct Zoltan_Struct* zz = Zoltan_Create(MPI_COMM_SELF);
  struct Zoltan_Struct* zz = Zoltan_Create(comm);
  int prank, psize;
  MPI_Comm_rank(comm, &prank);
  MPI_Comm_size(comm, &psize);
  // assert(nparts == psize);  // really ?

  if (prank == 0) LibGraph_printHypergraph(hg);
  MPI_Barrier(comm);

  /* Distribute vertices */

  int* vtxdist = (int*)malloc((psize + 1) * sizeof(int));
  if (prank == 0) {
    for (int i = 0; i < psize + 1; i++)
      vtxdist[i] = hg->nvtxs * i / psize;  // naive slice distribution used for PTSCOTCH & PARMETIS
    // vtxdist[0] = 0;
    // vtxdist[1] = 4; /* 8; */
    // vtxdist[2] = 16;
  }
  MPI_Bcast(vtxdist, psize + 1, MPI_INT, 0, comm);
  int nvtxs = vtxdist[prank + 1] - vtxdist[prank];
  int vtxstart = vtxdist[prank];
  int vtxend = vtxdist[prank + 1] - 1;
  LibGraph_Hypergraph subhg;
  int* vtxmap = NULL;
  int* hedgemap = NULL;
  LibGraph_subHypergraphFromRange(hg, vtxstart, vtxend, &subhg, &vtxmap, &hedgemap);
  assert(vtxmap && hedgemap);
  for (int i = 0; i < psize; i++) {
    if (prank == i) {
      printf("proc #%d: vtx dist %d -> %d (nvtxs %d)\n", prank, vtxstart, vtxend, nvtxs);
      LibGraph_printHypergraph(&subhg);
      printf("proc #%d: vtx mapping [", prank);
      for (int i = 0; i < subhg.nvtxs; i++) printf("%d -> %d, ", i, vtxmap[i]);
      printf("]\n");
      printf("proc #%d: hyperedge mapping [", prank);
      for (int i = 0; i < subhg.nhedges; i++) printf("%d -> %d, ", i, hedgemap[i]);
      printf("]\n");
    }
    MPI_Barrier(comm);
  }

  assert(hg->vwgts == NULL && hg->hewgts == NULL);

  ZoltanData data = {hg, prank, psize, vtxdist, &subhg, vtxmap, hedgemap, usefixed ? part : NULL};

  /* Zoltan General parameters */

  Zoltan_Set_Param(zz, "DEBUG_LEVEL", "0");
  Zoltan_Set_Param(zz, "LB_METHOD", "HYPERGRAPH"); /* partitioning method */
  // Zoltan_Set_Param(zz, "LB_METHOD", "GRAPH");
  Zoltan_Set_Param(zz, "HYPERGRAPH_PACKAGE", "PHG");                 /* version of method */
  Zoltan_Set_Param(zz, "NUM_GID_ENTRIES", "1");                      /* global IDs are integers */
  Zoltan_Set_Param(zz, "NUM_LID_ENTRIES", "1");                      /* local IDs are integers */
  Zoltan_Set_Param(zz, "RETURN_LISTS", "PARTITION");                 /* export AND import lists */
  Zoltan_Set_Param(zz, "OBJ_WEIGHT_DIM", (hg->vwgts ? "1" : "0"));   /* use Zoltan default vertex weights */
  Zoltan_Set_Param(zz, "EDGE_WEIGHT_DIM", (hg->hewgts ? "1" : "0")); /* use Zoltan default hyperedge weights */
  char imbalance[1024];
  snprintf(imbalance, sizeof(imbalance), "%lf", 1.0 + ubfactor / 100.0);
  Zoltan_Set_Param(zz, "IMBALANCE_TOL", imbalance);
  char num_parts[1024];
  snprintf(num_parts, sizeof(num_parts), "%d", nparts);
  Zoltan_Set_Param(zz, "NUM_GLOBAL_PARTS", num_parts);
  // Zoltan_Set_Param(zz, "LB_APPROACH", "REPARTITION");
  Zoltan_Set_Param(zz, "LB_APPROACH", "PARTITION");
  Zoltan_Set_Param(zz, "PHG_REFINEMENT_QUALITY", "1");  // TODO: add as starpart option

  /* Zoltan query function */

  Zoltan_Set_Num_Obj_Fn(zz, getNumberOfVertices, &data);
  Zoltan_Set_Obj_List_Fn(zz, getVertexList, &data);
  Zoltan_Set_HG_Size_CS_Fn(zz, getHypergraphSize, &data);
  Zoltan_Set_HG_CS_Fn(zz, getHypergraph, &data);
  // if (hg->hewgts) {
  //   Zoltan_Set_HG_Size_Edge_Wts_Fn(zz, getNumWeightedHyperedges, &data);
  //   Zoltan_Set_HG_Edge_Wts_Fn(zz, getHyperedgeWeight, &data);
  // }
  // if (usefixed) {
  //   Zoltan_Set_Num_Fixed_Obj_Fn(zz, getNumFixedVertices, &data);
  //   Zoltan_Set_Fixed_Obj_List_Fn(zz, getFixedVertexList, &data);
  // }

  /* Zoltan_LB_Partition */
  int rc, changes, numGidEntries, numLidEntries, numImport, numExport;
  ZOLTAN_ID_PTR importGlobalGids, importLocalGids, exportGlobalGids, exportLocalGids;
  int *importProcs, *importToPart, *exportProcs, *exportToPart;

  // int changes, num_gid_entries, num_lid_entries, num_import, num_export;
  // ZOLTAN_ID_PTR import_gids, import_lids, export_gids, export_lids;
  // int *import_procs, *import_to_part, *export_procs, *export_to_part;

  rc = Zoltan_LB_Partition(zz,                /* input (all remaining fields are output) */
                           &changes,          /* 1 if partitioning was changed, 0 otherwise */
                           &numGidEntries,    /* Number of integers used for a global ID */
                           &numLidEntries,    /* Number of integers used for a local ID */
                           &numImport,        /* Number of vertices to be sent to me */
                           &importGlobalGids, /* Global IDs of vertices to be sent to me */
                           &importLocalGids,  /* Local IDs of vertices to be sent to me */
                           &importProcs,      /* Process rank for source of each incoming vertex */
                           &importToPart,     /* New partition for each incoming vertex */
                           &numExport,        /* Number of vertices I must send to other processes*/
                           &exportGlobalGids, /* Global IDs of the vertices I must send */
                           &exportLocalGids,  /* Local IDs of the vertices I must send */
                           &exportProcs,      /* Process to which I send each of the vertices */
                           &exportToPart);    /* Partition to which each vertex will belong */

  if (rc != ZOLTAN_OK) {
    printf("Zoltan sorry...\n");
    MPI_Finalize();
    Zoltan_Destroy(&zz);
    exit(EXIT_FAILURE);
  }

  for (int i = 0; i < numExport; i++) part[exportLocalGids[i]] = exportToPart[i];

  printf("proc #%d: changes=%d, numGID=%d, numLID=%d, numImport=%d, numExport=%d\n", prank, changes, numGidEntries,
         numLidEntries, numImport, numExport);

  printf("proc #%d: part [", prank);
  for (int i = 0; i < numExport; i++) printf("%d -> %d, ", exportGlobalGids[i], exportToPart[i]);
  printf("]\n");

  /* Migrate vertices to new parts */
  // if (changes) {
  //   rc = Zoltan_Migrate(zz, numImport, importGlobalGids, importLocalGids, importProcs, importToPart, numExport,
  //                       exportGlobalGids, exportLocalGids, exportProcs, exportToPart);
  // }

  Zoltan_LB_Free_Part(&importGlobalGids, &importLocalGids, &importProcs, &importToPart);
  Zoltan_LB_Free_Part(&exportGlobalGids, &exportLocalGids, &exportProcs, &exportToPart);

  Zoltan_Destroy(&zz);

  if (edgecut) *edgecut = LibGraph_computeHypergraphEdgeCut(hg, nparts, part);
}

/* *********************************************************** */
/*                        StarPart Method                      */
/* *********************************************************** */

void StarPart_ZOLTAN_PART_init(StarPart_Method* method)
{
  StarPart_Context* ctx = StarPart_getMethodContext(method);
  assert(StarPart_isParallelContext(ctx));
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "hypergraph", STARPART_HYPERGRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_ZOLTAN_PART_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Hypergraph* hg = StarPart_getHypergraph(data, "hypergraph");
  int* part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  int nparts = StarPart_getInt(data, "nparts");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  assert(hg);
  assert(nparts > 0);

  if (part)
    StarPart_checkArray(data, "part", hg->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", hg->nvtxs);
    StarPart_initArray(data, "part", -1);
  }
  if (fixed) {
    StarPart_checkArray(data, "fixed", hg->nvtxs);
    for (int i = 0; i < hg->nvtxs; i++) part[i] = fixed[i];
  }

  bool usefixed = false;
  if (fixed) usefixed = true;

  MPI_Comm comm = MPI_COMM_SELF;
  if (StarPart_isParallelContext(ctx)) comm = StarPart_getParallelComm(ctx);

  /* partition */

  partitionHypergraphZoltan(hg, nparts, ubfactor, part, NULL, usefixed, comm);
}

/* *********************************************************** */
