#ifndef ZOLTAN_WRAPPER_H
#define ZOLTAN_WRAPPER_H

#include "starpart.h"

//@{

#define StarPart_ZOLTAN_PART_desc "hypergraph partitioning routine of Zoltan"
void StarPart_ZOLTAN_PART_init(StarPart_Method* method);
void StarPart_ZOLTAN_PART_call(StarPart_Task* task);

//@}

#endif
