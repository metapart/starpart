#ifndef PULP_WRAPPER_H
#define PULP_WRAPPER_H

//@{

#include "starpart.h"

#define StarPart_PULP_PART_desc "graph partitioning routine of PULP"
void StarPart_PULP_PART_init(StarPart_Method* method);
void StarPart_PULP_PART_call(StarPart_Task* task);

//@}

#endif
