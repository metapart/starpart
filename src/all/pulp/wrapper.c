/* Wrapper library for PULP */

#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "all.h"
#include "libgraph.h"
#include "starpart.h"
#include "wrapper.h"

/* *********************************************************** */

// #include "pulp.h"

typedef struct
{
  int n;                 /* nb vertices */
  long m;                /* nb edges */
  int* out_array;        /* array of size m */
  long* out_degree_list; /* array of size n+1 */
  int* vertex_weights;
  int* edge_weights;
  long vertex_weights_sum;
} pulp_graph_t;

typedef struct
{
  double vert_balance;
  double edge_balance;
  bool do_lp_init;
  bool do_bfs_init;
  bool do_repart;
  bool do_edge_balance;
  bool do_maxcut_balance;
  bool verbose_output;
  int pulp_seed;
} pulp_part_control_t;

int pulp_run(pulp_graph_t* g, pulp_part_control_t* ppc, int* parts, int num_parts);

/* *********************************************************** */

void StarPart_PULP_PART_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_PULP_PART_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int nparts = StarPart_getInt(data, "nparts");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  assert(g);
  assert(nparts > 0);

  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", g->nvtxs);
    StarPart_initArray(data, "part", -1);
  }


  // assert(sizeof(long) == sizeof(int));

  int n = g->nvtxs;
  int m = g->narcs;
  int* out_array = g->adjncy;                          /* array of size m */
  long* out_degree_list = calloc(n + 1, sizeof(long)); /* array of size n+1 */
  int* vertex_weights = g->vwgts;
  int* edge_weights = g->ewgts;
  long vertex_weights_sum = 0;

  for (int i = 0; i < n + 1; i++) out_degree_list[i] = (long)g->xadj[i];
  for (int i = 0; i < n; i++) vertex_weights_sum += (g->vwgts ? g->vwgts[i] : 1);

  pulp_graph_t pg = {n, m, out_array, out_degree_list, vertex_weights, edge_weights, vertex_weights_sum};

  double vert_balance = 1.0 + ubfactor / 100.0;
  double edge_balance = 1.50;
  bool do_bfs_init = true;
  bool do_lp_init = false;
  bool do_edge_balance = false;
  bool do_maxcut_balance = false;
  bool verbose_output = false;
  int pulp_seed = rand();

  pulp_part_control_t ppc = {vert_balance,    edge_balance,      do_lp_init,     do_bfs_init,
                             do_edge_balance, do_maxcut_balance, verbose_output, pulp_seed};

  pulp_run(&pg, &ppc, part, nparts);

  free(out_degree_list);
}

/* *********************************************************** */
