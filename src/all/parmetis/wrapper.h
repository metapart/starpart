#ifndef STARPART_PARMETIS_H
#define STARPART_PARMETIS_H

#include "starpart.h"

//@{

#define StarPart_PARMETIS_PART_desc "partition graph in parallel with PARMETIS"
void StarPart_PARMETIS_PART_init(StarPart_Method* method);
void StarPart_PARMETIS_PART_call(StarPart_Task* task);

#ifndef USE_METIS
#define StarPart_METIS_KPART_desc "partition graph with multilevel KW Metis"
void StarPart_METIS_KPART_call(StarPart_Task* task);
void StarPart_METIS_KPART_init(StarPart_Method* method);
#endif

void StarPart_registerParmetis(StarPart_Context* ctx);

//@}

#endif
