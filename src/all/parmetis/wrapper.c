/* Wrapper library for Parmetis */

#include <assert.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "all.h"
#include "libgraph.h"
#include "metis.h"
#include "parmetis.h"
#include "starpart.h"
#include "wrapper.h"

/* *********************************************************** */
/*                         PARMETIS                            */
/* *********************************************************** */

void StarPart_PARMETIS_PART_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "distdata", STARPART_INOUT);
  StarPart_addNewItem(data, "distgraph", STARPART_DISTGRAPH, NULL, true, STARPART_READ);
  // StarPart_addNewItem(data, "vtxdist", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_PARMETIS_PART_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  StarPart_Data* data = StarPart_getTaskData(task, "distdata");  // distributed data
  LibGraph_DistGraph* g = StarPart_getDistGraph(data, "distgraph");
  // int* vtxdist = StarPart_getIntArray(data, "vtxdist");
  int* part = StarPart_getIntArray(data, "part");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int nparts = StarPart_getInt(data, "nparts");
  // int np = StarPart_getInt(data, "np");
  // int nrefpass = StarPart_getInt(data, "nrefpass");
  // int npass = StarPart_getInt(data, "npass");
  // int seed = StarPart_getInt(data, "seed");

  assert(g && nparts > 0);

  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", g->nvtxs);
    StarPart_initArray(data, "part", -1);
}

  // int edgecut = -1;
  MPI_Comm comm = StarPart_getParallelComm(ctx);
  int prank = StarPart_getParallelCommRank(ctx);
  int psize = StarPart_getParallelCommSize(ctx);
  assert(psize >= 2);

  int nvtxs = g->vtxdist[prank + 1] - g->vtxdist[prank];
  assert(g->nvtxs == nvtxs);

  assert(sizeof(idx_t) == sizeof(int));

  float* tpwgts = malloc(g->nvtxs * sizeof(int));
  for (int i = 0; i < nvtxs; i++) tpwgts[i] = 1.0f / nparts;
  // float ubvec[1] = {1.01};  // TODO: use ubfactor
  float ubvec[1] = {1.0 + ubfactor / 100.0};
  int wgtflag = 2; /* vertices only */
  int zero = 0;
  int cut;
  int one = 1;

  /* parallel partition from scratch */
  int options_part[3] = {0};
  ParMETIS_V3_PartKway(g->vtxdist, g->xadj, g->adjncy, g->vwgts, NULL, &wgtflag, &zero, &one, &nparts, tpwgts, ubvec,
                       options_part, &cut, part, &comm);

  /*
  ParMETIS_V3_PartKway(
           idx_t *vtxdist, idx_t *xadj, idx_t *adjncy, idx_t *vwgt,
           idx_t *adjwgt, idx_t *wgtflag, idx_t *numflag, idx_t *ncon, idx_t *nparts,
           real_t *tpwgts, real_t *ubvec, idx_t *options, idx_t *edgecut, idx_t *part,
           MPI_Comm *comm);
  */

  // MPI_Finalize();
}

/* *********************************************************** */
/*                         FORMER                              */
/* *********************************************************** */

#ifdef POUET
static void partitionGraphParMetis(
    Graph* g,     /* [in] graph struct */
    int nparts,   /* [in] nb of desired partitions */
    int repart,   /* [in] 0 for new partition, 1 for repartition */
    float itr,    /* [in] ratio of inter-processor communications compared to data redistribution */
    int* part,    /* [in,out] array of computed partitions (of size graph->nvtxs) */
    int* edgecut, /* [out] cut size */
    MPI_Comm comm /* [in] MPI communicator (communicator size must be at least 2) */
    )
{
  int rank, size;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &size);
  assert(size >= 2);

  /* Distribute vertices */
  int* dist = malloc((size + 1) * sizeof(int));
  if (rank == 0) {
    for (int i = 0; i < size + 1; i++) dist[i] = g->nvtxs * i / size;
  }
  MPI_Bcast(dist, size + 1, MPI_INT, 0, comm);
  int nvtxs = dist[rank + 1] - dist[rank];

  /* Distribute xadj */
  /* Cannot use MPI_Scatterv because of overlapping */
  int* xadj = malloc((nvtxs + 1) * sizeof(int));
  if (rank == 0) {
    for (int i = 1; i < size; i++) MPI_Send(&g->xadj[dist[i]], dist[i + 1] - dist[i] + 1, MPI_INT, i, 0, comm);
    memcpy(xadj, g->xadj, (nvtxs + 1) * sizeof(int));
  }
  else {
    MPI_Recv(xadj, nvtxs + 1, MPI_INT, 0, 0, comm, MPI_STATUS_IGNORE);
    int offset = xadj[0];
    for (int i = 0; i < nvtxs + 1; i++) xadj[i] -= offset;
  }

  /* Distribute adjncy */
  int* adjncy = malloc(xadj[nvtxs] * sizeof(int));
  if (rank == 0) {
    int sendcounts[size];
    int displs[size];
    for (int i = 0; i < size; i++) {
      sendcounts[i] = g->xadj[dist[i + 1]] - g->xadj[dist[i]];
      displs[i] = g->xadj[dist[i]];
    }
    MPI_Scatterv(g->adjncy, sendcounts, displs, MPI_INT, adjncy, xadj[nvtxs], MPI_INT, 0, comm);
  }
  else
    MPI_Scatterv(NULL, NULL, NULL, MPI_INT, adjncy, xadj[nvtxs], MPI_INT, 0, comm);

  /* Distribute vertex weights */
  int* vwgts = malloc(nvtxs * sizeof(int));
  if (rank == 0) {
    if (g->vwgts == NULL) {
      g->vwgts = malloc(g->nvtxs * sizeof(int));
      for (int i = 0; i < g->nvtxs; i++) g->vwgts[i] = 1;
    }
    int sendcounts[size];
    for (int i = 0; i < size; i++) sendcounts[i] = dist[i + 1] - dist[i];
    MPI_Scatterv(g->vwgts, sendcounts, dist, MPI_INT, vwgts, nvtxs, MPI_INT, 0, comm);
  }
  else
    MPI_Scatterv(NULL, NULL, NULL, MPI_INT, vwgts, nvtxs, MPI_INT, 0, comm);

  /* === Partition from scratch === */

  float* tpwgts = malloc(nvtxs * sizeof(int));
  for (int i = 0; i < nvtxs; i++) tpwgts[i] = 1.0f / nparts;
  float ubvec[1] = {1.01};
  int wgtflag = 2; /* vertices only */
  int zero = 0;
  int cut;
  int one = 1;

  int* p = malloc(nvtxs * sizeof(int));

  if (!repart) {
    int options_part[3] = {0};
    ParMETIS_V3_PartKway(dist, xadj, adjncy, vwgts, NULL, &wgtflag, &zero, &one, &nparts, tpwgts, ubvec, options_part,
                         &cut, p, &comm);

    /*
    ParMETIS_V3_PartKway(
             idx_t *vtxdist, idx_t *xadj, idx_t *adjncy, idx_t *vwgt,
             idx_t *adjwgt, idx_t *wgtflag, idx_t *numflag, idx_t *ncon, idx_t *nparts,
             real_t *tpwgts, real_t *ubvec, idx_t *options, idx_t *edgecut, idx_t *part,
             MPI_Comm *comm);
    */
  }
  /* === Repartition === */
  else {
    /* Distribute initial partition */
    if (rank == 0) {
      int sendcounts[size];
      for (int i = 0; i < size; i++) {
        sendcounts[i] = dist[i + 1] - dist[i];
      }
      MPI_Scatterv(part, sendcounts, dist, MPI_INT, p, nvtxs, MPI_INT, 0, comm);
    }
    else
      MPI_Scatterv(NULL, NULL, NULL, MPI_INT, p, nvtxs, MPI_INT, 0, comm);

    int* vsize = malloc(nvtxs * sizeof(int));
    for (int i = 0; i < nvtxs; i++) vsize[i] = 1;
    int options_repart[4] = {1, 0, 15, PARMETIS_PSR_UNCOUPLED};
    ParMETIS_V3_AdaptiveRepart(dist, xadj, adjncy, vwgts, vsize, NULL, &wgtflag, &zero, &one, &nparts, tpwgts, ubvec,
                               &itr, options_repart, &cut, p, &comm);
    free(vsize);
  }

  int* recvcounts;
  if (rank == 0) {
    *edgecut = cut;

    recvcounts = malloc(size * sizeof(int));
    for (int i = 0; i < size; i++) recvcounts[i] = dist[i + 1] - dist[i];
  }
  MPI_Gatherv(p, nvtxs, MPI_INT, part, recvcounts, dist, MPI_INT, 0, comm);

  if (rank == 0) free(recvcounts);

  free(dist);
  free(xadj);
  free(adjncy);
  free(tpwgts);
  free(vwgts);
  free(p);
}
#endif

/* *********************************************************** */
/*                        METIS                                */
/* *********************************************************** */

#ifndef USE_METIS

void StarPart_METIS_KPART_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodLocal(method);
  StarPart_addNewItem(local, "nrefpass", STARPART_INT, NEW_INT(STARPART_NREFPASS), true, STARPART_READ);
  StarPart_addNewItem(local, "npass", STARPART_INT, NEW_INT(1), true, STARPART_READ);
  StarPart_addNewItem(local, "seed", STARPART_INT, NEW_INT(0), true, STARPART_READ);
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_METIS_KPART_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* local = StarPart_getTaskLocal(task);
  int nrefpass = StarPart_getInt(local, "nrefpass");
  // int npass = StarPart_getInt(local, "npass");
  // int seed = StarPart_getInt(local, "seed");
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int nparts = StarPart_getInt(data, "nparts");

  assert(g && nparts > 0);

  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", g->nvtxs);
    StarPart_initArray(data, "part", -1);
}

  // check metis size of type
  assert(sizeof(idx_t) == sizeof(int32_t));
  assert(sizeof(real_t) == sizeof(float));

  if (nparts == 1) { /* Bug with Metis in this case! */
    for (int i = 0; i < g->nvtxs; i++) part[i] = 0;
    return;
  }

  /* Default Options for METIS_PartRecursive. */
  idx_t options[METIS_NOPTIONS];
  METIS_SetDefaultOptions(options);
  options[METIS_OPTION_NUMBERING] = 0;            /* C numbering scheme */
  options[METIS_OPTION_SEED] = 0;                 /* set random seed */
  options[METIS_OPTION_NCUTS] = 1;                /* nb of multilevel cycles */
  options[METIS_OPTION_NITER] = nrefpass;         /* nb of refinement pass */
  options[METIS_OPTION_CTYPE] = METIS_CTYPE_SHEM; /* coarsening */
  options[METIS_OPTION_RTYPE] = METIS_RTYPE_FM;   /* FM refinement */
  options[METIS_OPTION_UFACTOR] = ubfactor * 10;  /* unbalance factor */

  // all KMetis options
  /* options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_CUT; */
  /* options[METIS_OPTION_CTYPE ] = METIS_CTYPE_SHEM; // METIS_CTYPE_RM */
  /* options[METIS_OPTION_IPTYPE] = METIS_IPTYPE_GROW; // ... */
  /* options[METIS_OPTION_RTYPE] = 0; */
  /* options[METIS_OPTION_NO2HOP] = 0; */
  /* options[METIS_OPTION_NCUTS] = 0; */
  /* options[METIS_OPTION_NITER] = 0; */
  /* options[METIS_OPTION_UFACTOR] = 0; */
  /* options[METIS_OPTION_MINCONN] = 0; */
  /* options[METIS_OPTION_CONTIG] = 0; */
  /* options[METIS_OPTION_SEED] = 0; */
  /* options[METIS_OPTION_NUMBERING] = 0; */
  /* options[METIS_OPTION_DBGLVL] = 0; */

  int ncon = 1;  // single constraint
  real_t ub = 1.0 + ubfactor / 100.0;
  idx_t edgecut;
  METIS_PartGraphKway((idx_t*)&g->nvtxs, (idx_t*)&ncon, (idx_t*)g->xadj, (idx_t*)g->adjncy, (idx_t*)g->vwgts, NULL,
                      (idx_t*)g->ewgts, (idx_t*)&(nparts), NULL, &ub, options, &edgecut, (idx_t*)part);
}

#endif

/* *********************************************************** */
/*                  REGISTER PARMETIS METHODS                  */
/* *********************************************************** */

void StarPart_registerParmetis(StarPart_Context* ctx)
{
  if (StarPart_isParallelContext(ctx)) {
    StarPart_registerNewMethod(ctx, "PARMETIS/PART", StarPart_PARMETIS_PART_call, StarPart_PARMETIS_PART_init,
                               StarPart_PARMETIS_PART_desc);
  }
#ifndef USE_METIS
  StarPart_registerNewMethod(ctx, "METIS/KPART", StarPart_METIS_KPART_call, StarPart_METIS_KPART_init,
                             StarPart_METIS_KPART_desc);
#endif
}

/* *********************************************************** */
