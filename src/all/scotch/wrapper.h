#ifndef STARPART_SCOTCH_H
#define STARPART_SCOTCH_H

#include "starpart.h"

//@{

void StarPart_registerScotch(StarPart_Context* ctx);

void StarPart_SCOTCH_init(StarPart_Method* method);

#define StarPart_SCOTCH_DEFAULT_desc "default SCOTCH partitioning routine"
void StarPart_SCOTCH_DEFAULT_call(StarPart_Task* task);

#define StarPart_SCOTCH_KPART_desc "SCOTCH k-way multilevel partitioning routine"
void StarPart_SCOTCH_KPART_call(StarPart_Task* task);

#define StarPart_SCOTCH_RB_desc "SCOTCH initial partitioning routine based on RB"
void StarPart_SCOTCH_RB_call(StarPart_Task* task);

#define StarPart_SCOTCH_KFM_desc "SCOTCH refinement routine based on KFM"
void StarPart_SCOTCH_KFM_call(StarPart_Task* task);

#define StarPart_SCOTCH_ORDER_desc "SCOTCH ordering method"
void StarPart_SCOTCH_ORDER_call(StarPart_Task* task);

/* *********************************************************** */

#ifdef USE_SCOTCH_ML
#define StarPart_SCOTCH_ML_desc "SCOTCH multilevel framework"
void StarPart_SCOTCH_ML_call(StarPart_Task* task);
#endif

/* *********************************************************** */
/*                           PTSCOTCH                          */
/* *********************************************************** */

#ifdef USE_PTSCOTCH

#define StarPart_PTSCOTCH_SCATTER_desc "scatter a graph for PTSCOTCH"
void StarPart_PTSCOTCH_SCATTER_init(StarPart_Method* method);
void StarPart_PTSCOTCH_SCATTER_call(StarPart_Task* task);

#define StarPart_PTSCOTCH_GATHER_desc "gather a distributed graph for PTSCOTCH"
void StarPart_PTSCOTCH_GATHER_init(StarPart_Method* method);
void StarPart_PTSCOTCH_GATHER_call(StarPart_Task* task);

#define StarPart_PTSCOTCH_PART_desc "partition graph in parallel with PTSCOTCH"
void StarPart_PTSCOTCH_PART_init(StarPart_Method* method);
void StarPart_PTSCOTCH_PART_call(StarPart_Task* task);

#endif

/* *********************************************************** */

//@}

#endif
