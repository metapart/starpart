/* Wrapper library for Scotch */

#define _GNU_SOURCE
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "all.h"
#include "libgraph.h"
#include "scotch.h"
#include "starpart.h"
#include "wrapper.h"

#ifdef USE_PTSCOTCH
#include <mpi.h>
#include "ptscotch.h"
#endif

#define MAXSTR 8192

/* *********************************************************** */

void StarPart_SCOTCH_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_Data* local = StarPart_getMethodLocal(method);
  char* id = StarPart_getMethodID(method);

  if (!strcmp(id, "SCOTCH/DEFAULT")) {
    StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
    StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
  }
  else if (!strcmp(id, "SCOTCH/KPART")) {
    StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
    StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
    StarPart_addNewItem(local, "ubfactorc", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
    StarPart_addNewItem(local, "nbisectpass", STARPART_INT, NEW_INT(STARPART_NINITPASS), true, STARPART_READ);
    StarPart_addNewItem(local, "nrefpass", STARPART_INT, NEW_INT(STARPART_NREFPASS), true, STARPART_READ);
    StarPart_addNewItem(local, "maxnegmove", STARPART_INT, NEW_INT(STARPART_MAXNEGMOVE), true, STARPART_READ);
    StarPart_addNewItem(local, "cpartsize", STARPART_INT, NEW_INT(STARPART_CPARTSIZE), true, STARPART_READ);
  }
  else if (!strcmp(id, "SCOTCH/RB")) {
    StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
    StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
    StarPart_addNewItem(local, "nbisectpass", STARPART_INT, NEW_INT(STARPART_NINITPASS), true, STARPART_READ);
    StarPart_addNewItem(local, "nrefpass", STARPART_INT, NEW_INT(STARPART_NREFPASS), true, STARPART_READ);
    StarPart_addNewItem(local, "maxnegmove", STARPART_INT, NEW_INT(STARPART_MAXNEGMOVE), true, STARPART_READ);
  }
  else if (!strcmp(id, "SCOTCH/KFM")) {
    StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
    StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
    StarPart_addNewItem(local, "nrefpass", STARPART_INT, NEW_INT(STARPART_NREFPASS), true, STARPART_READ);
    StarPart_addNewItem(local, "maxnegmove", STARPART_INT, NEW_INT(STARPART_MAXNEGMOVE), true, STARPART_READ);
    StarPart_addNewItem(local, "bandsize", STARPART_INT, NEW_INT(0), true, STARPART_READ);
  }
#ifdef USE_SCOTCH_ML
  else if (!strcmp(id, "SCOTCH/ML")) {
    StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
    StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
    StarPart_addNewItem(local, "ninitpass", STARPART_INT, NEW_INT(STARPART_NINITPASS), true, STARPART_READ);
    StarPart_addNewItem(local, "nrefpass", STARPART_INT, NEW_INT(STARPART_NREFPASS), true, STARPART_READ);
    StarPart_addNewItem(local, "maxnegmove", STARPART_INT, NEW_INT(STARPART_MAXNEGMOVE), true, STARPART_READ);
    StarPart_addNewItem(local, "cpartsize", STARPART_INT, NEW_INT(STARPART_CPARTSIZE), true, STARPART_READ);
    StarPart_addNewItem(local, "pstrat", STARPART_STRAT, NEW_STRAT("SCOTCH_RB"), true, STARPART_READ);
  }
#endif
  else if (!strcmp(id, "SCOTCH/ORDER")) {
    StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
    StarPart_addNewItem(data, "perm", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
    StarPart_addNewItem(data, "blk", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
    StarPart_addNewItem(local, "maxlvl", STARPART_INT, NEW_INT(INT_MAX), true, STARPART_READ);
    StarPart_addNewItem(local, "minsize", STARPART_INT, NEW_INT(120), true, STARPART_READ);
    StarPart_addNewItem(local, "leaf", STARPART_STR, NEW_STR("minfill"), true, STARPART_READ);
    StarPart_addNewItem(local, "sepbal", STARPART_INT, NEW_INT(20), true, STARPART_READ);  // in %
    StarPart_addNewItem(local, "sepnpass", STARPART_INT, NEW_INT(10), true, STARPART_READ);
    StarPart_addNewItem(local, "bandsize", STARPART_INT, NEW_INT(0), true, STARPART_READ);  // or size 3
    StarPart_addNewItem(local, "cprratio", STARPART_INT, NEW_INT(0), true,
                        STARPART_READ);  // in % (0 for no compression or 70)
  }
}

/* *********************************************************** */
/*                         PARTITION                           */
/* *********************************************************** */

void StarPart_SCOTCH_DEFAULT_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  // StarPart_Data * param = StarPart_getTaskData(task, "param");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int nparts = StarPart_getInt(data, "nparts");
  assert(g);
  assert(nparts > 0);

  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", g->nvtxs);
    StarPart_initArray(data, "part", -1);
  }

  if (fixed) StarPart_checkArray(data, "fixed", g->nvtxs);

  /* set free vertices in input partition*/
  for (int i = 0; i < g->nvtxs; i++)
    if (!fixed || !fixed[i]) part[i] = -1;

  assert(sizeof(SCOTCH_Idx) == sizeof(int32_t));  // check 32 bits

  SCOTCH_Graph sg;
  SCOTCH_graphInit(&sg);

  // int
  // SCOTCH_graphBuild (
  // SCOTCH_Graph * const        grafptr,              /* Graph structure to fill             */
  // const SCOTCH_Num            baseval,              /* Base value                          */
  // const SCOTCH_Num            vertnbr,              /* Number of vertices                  */
  // const SCOTCH_Num * const    verttab,              /* Vertex array [vertnbr or vertnbr+1] */
  // const SCOTCH_Num * const    vendtab,              /* Vertex end array [vertnbr]          */
  // const SCOTCH_Num * const    velotab,              /* Vertex load array                   */
  // const SCOTCH_Num * const    vlbltab,              /* Vertex label array                  */
  // const SCOTCH_Num            edgenbr,              /* Number of edges (arcs)              */
  // const SCOTCH_Num * const    edgetab,              /* Edge array [edgenbr]                */
  // const SCOTCH_Num * const    edlotab)              /* Edge load array                     */

  SCOTCH_graphBuild(&sg, 0, g->nvtxs, g->xadj, g->xadj + 1, g->vwgts, NULL, g->narcs, g->adjncy, g->ewgts);
  // SCOTCH_graphCheck (&sg);

  SCOTCH_Strat strat;
  SCOTCH_stratInit(&strat);

  SCOTCH_stratGraphMapBuild(&strat, SCOTCH_STRATDEFAULT | SCOTCH_STRATSAFETY, nparts, ubfactor / 100.0);
  // printf("=> Scotch Partitioning strategy: SCOTCH_STRATDEFAULT | SCOTCH_STRATSAFETY\n");

  int r = -1;
  if (!fixed)
    r = SCOTCH_graphPart(&sg, nparts, &strat, part);
  else
    r = SCOTCH_graphPartFixed(&sg, nparts, &strat, part);
  assert(r == 0);  // success

  SCOTCH_stratExit(&strat);
  SCOTCH_graphExit(&sg);  // remarks: arrays passed to graphBuild() are not free!
}

/* *********************************************************** */

static void _SCOTCHML(StarPart_Task* task, bool multilevel)
{
  assert(task);
  // StarPart_Context* ctx = StarPart_getTaskContext(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  StarPart_Data* local = StarPart_getTaskLocal(task);
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  int nparts = StarPart_getInt(data, "nparts");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int ubfactorc = ubfactor;
  if (multilevel) ubfactorc = StarPart_getInt(local, "ubfactorc");
  int nrefpass = StarPart_getInt(local, "nrefpass");
  int maxnegmove = StarPart_getInt(local, "maxnegmove");
  int nbisectpass = StarPart_getInt(local, "nbisectpass");

  assert(g);
  assert(nparts > 0);
  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", g->nvtxs);
    StarPart_initArray(data, "part", -1);
  }
  if (fixed) StarPart_checkArray(data, "fixed", g->nvtxs);

  /* set free vertices in input partition*/
  for (int i = 0; i < g->nvtxs; i++)
    if (!fixed || !fixed[i]) part[i] = -1;

  assert(sizeof(SCOTCH_Idx) == sizeof(int32_t));  // check 32 bits

  SCOTCH_Graph sg;
  SCOTCH_graphInit(&sg);
  SCOTCH_graphBuild(&sg, 0, g->nvtxs, g->xadj, g->xadj + 1, g->vwgts, NULL, g->narcs, g->adjncy, g->ewgts);
  SCOTCH_graphCheck(&sg);

  SCOTCH_Strat strat;
  SCOTCH_stratInit(&strat);

  if (maxnegmove < 0) maxnegmove = INT_MAX;

  /* initial partitioning: RB */
  char s[MAXSTR];
  snprintf(s, sizeof(s), "r{bal=%lf,sep=h{pass=%d}f{bal=%lf,move=100}}f{bal=%lf,pass=%d,move=%d}",
           ubfactorc / 100.0,  // <- ubfactor
           nbisectpass,        // <- nb init pass for each bisection
           ubfactorc / 100.0,  // <- ubfactor
           ubfactorc / 100.0,  // <- ubfactor
           nrefpass,           // <- max nb of ref pass
           maxnegmove          // <- maxnegmove for refinement (or -1)
           );

  char* sss = s;

  /* no multilevel */
  char ss[MAXSTR];

  if (multilevel) {
    int cpartsize = StarPart_getInt(local, "cpartsize");
    int cnvtxs = cpartsize * nparts;

    snprintf(ss, sizeof(ss), "m{vert=%d,low=%s,asc=f{bal=%lf,move=%d,pass=%d}}",
             cnvtxs,            // <- max size of coarsest graph
             s,                 // <- initial method
             ubfactor / 100.0,  // <- ubfactor
             maxnegmove,        // <- maxnegmove for refinement (or -1)
             nrefpass           // <- max nb of pass (or -1)
             );
    sss = ss;
  }

  SCOTCH_stratGraphMap(&strat, sss);
  PRINT("=> Scotch Partitioning strategy: %s\n", sss);

  int r = -1;
  if (!fixed)
    r = SCOTCH_graphPart(&sg, nparts, &strat, part);
  else
    r = SCOTCH_graphPartFixed(&sg, nparts, &strat, part);
  assert(r == 0);  // success

  SCOTCH_stratExit(&strat);
  SCOTCH_graphExit(&sg);
}

/* *********************************************************** */

void StarPart_SCOTCH_KPART_call(StarPart_Task* task)
{
  assert(task);
  _SCOTCHML(task, true); /* multilevel=true */
}

/* *********************************************************** */

void StarPart_SCOTCH_RB_call(StarPart_Task* task)
{
  assert(task);
  _SCOTCHML(task, false); /* multilevel=false */
}

/* *********************************************************** */
/*                        REFINEMENT                           */
/* *********************************************************** */

void StarPart_SCOTCH_KFM_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  StarPart_Data* local = StarPart_getTaskLocal(task);
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  int nparts = StarPart_getInt(data, "nparts");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int nrefpass = StarPart_getInt(local, "nrefpass");
  int maxnegmove = StarPart_getInt(local, "maxnegmove");
  int bandsize = StarPart_getInt(local, "bandsize");

  assert(g && part);
  assert(nparts > 0);
  assert(sizeof(SCOTCH_Idx) == sizeof(int32_t));  // check 32 bits
  assert(nrefpass >= -1);
  StarPart_checkArray(data, "part", g->nvtxs);
  if (fixed) StarPart_checkArray(data, "fixed", g->nvtxs);

  if (maxnegmove < 0) maxnegmove = INT_MAX;
  /* nrefpass = -1 for infinite nb passes, move = 10000 by default for scotch */

  /* initialize output partition */
  int* partout = calloc(g->nvtxs, sizeof(int));
  for (int i = 0; i < g->nvtxs; i++)
    if (fixed && fixed[i])
      partout[i] = part[i];
    else
      partout[i] = -1;

  // int edgecut_initial = computeGraphEdgeCut(g, nparts, part);

  SCOTCH_Graph sg;
  SCOTCH_graphInit(&sg);
  SCOTCH_graphBuild(&sg, 0, g->nvtxs, g->xadj, g->xadj + 1, g->vwgts, NULL, g->narcs, g->adjncy, g->ewgts);
  // SCOTCH_graphCheck (&sg);

  SCOTCH_Strat strat;
  SCOTCH_stratInit(&strat);

  // char s0[] = "c{type=p}f{move=%3$d,pass=%2$d,bal=%1$lf}"; /* heavy strat
  // (full KFM) */
  char str0[] = "cf{move=%3$d,pass=%2$d,bal=%1$lf}"; /* heavy strat (full KFM) */
  // Warning: dont use band strategy with high degree vertices, like we do in
  // repart strategy with fixed vertices !!!
  char str1[] =
      "cb{width=%4$d,bnd=f{move=%3$d,pass=%2$d,bal=%1$lf},org=f{move=%3$d,pass="
      "%2$d,bal=%1$lf}}"; /* band FM */
  // char s2[] =
  // "cb{width=3,bnd=d{pass=80,dif=1,rem=0},org=f{move=%3$d,pass=%2$d,bal=%1$lf}}f{move=%3$d,pass=%2$d,bal=%1$lf}";
  // char s3[] =
  // "cb{width=3,bnd=d{pass=80,dif=1,rem=0}f{move=%3$d,pass=%2$d,bal=%1$lf},org=f{move=%3$d,pass=%2$d,bal=%1$lf}}";

  char str[8192];
  if (bandsize > 0)
    snprintf(str, sizeof(str), str1, ubfactor / 100.0, nrefpass, maxnegmove, bandsize);
  else
    snprintf(str, sizeof(str), str0, ubfactor / 100.0, nrefpass, maxnegmove, bandsize);
  PRINT("=> Scotch Refinement strategy: %s\n", str);

  SCOTCH_stratGraphMap(&strat, str);

  const double migcost = 0.0; /* because we just want refinement! */

  int r = -1;
  if (!fixed)
    r = SCOTCH_graphRepart(&sg, nparts, part, migcost, NULL, &strat, partout);
  else
    r = SCOTCH_graphRepartFixed(&sg, nparts, part, migcost, NULL, &strat, partout);
  assert(r == 0);

  SCOTCH_stratExit(&strat);
  SCOTCH_graphExit(&sg);

  memcpy(part, partout, g->nvtxs * sizeof(int));
  free(partout);
}

/* *********************************************************** */
/*                        MULTILEVEL                           */
/* *********************************************************** */

#ifdef USE_SCOTCH_ML

void StarPart_SCOTCH_ML_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  StarPart_Data* local = StarPart_getTaskLocal(task);
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int* fixed = StarPart_getIntArray(data, "fixed");
  int nparts = StarPart_getInt(data, "nparts");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int maxnegmove = StarPart_getInt(local, "maxnegmove");
  int ninitpass = StarPart_getInt(local, "ninitpass");
  int nrefpass = StarPart_getInt(local, "nrefpass");
  int cpartsize = StarPart_getInt(local, "cpartsize");
  char* pstrat = StarPart_getStrat(local, "pstrat");

  assert(g);
  assert(nparts > 0);

  if (maxnegmove < 0) maxnegmove = INT_MAX;

  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", g->nvtxs);
    StarPart_initArray(data, "part", -1);
  }
  if (fixed) StarPart_checkArray(data, "fixed", g->nvtxs);

  /* set free vertices in input partition*/
  for (int i = 0; i < g->nvtxs; i++)
    if (!fixed || !fixed[i]) part[i] = -1;

  SCOTCH_Graph sg;
  SCOTCH_graphInit(&sg);
  SCOTCH_graphBuild(&sg, 0, g->nvtxs, g->xadj, g->xadj + 1, g->vwgts, NULL, g->narcs, g->adjncy, g->ewgts);
  // SCOTCH_graphCheck (&sg);

  SCOTCH_Strat strat;
  SCOTCH_stratInit(&strat);

  char s[MAXSTR];
  snprintf(s, sizeof(s),
           "z{bal=%lf,pass=%d,method=\"%s\"}f{bal=%lf,pass=%d,move=%d}",  // initial
           // part
           ubfactor / 100.0,  // <- ubfactor
           ninitpass,         // <- nb init pass
           pstrat,            // <- initial partitioning strat
           ubfactor / 100.0,  // <- ubfactor
           nrefpass,          // <- nb ref pass (or -1)
           maxnegmove         // <- maxnegmove for refinement (or -1)
           );

  int cnvtxs = cpartsize * nparts;

  char ss[MAXSTR];
  snprintf(ss, sizeof(ss), "m{vert=%d,low=%s,asc=f{bal=%lf,move=%d,pass=%d}}",
           cnvtxs,            // <- max size of coarsest graph
           s,                 // <- init part + FM refinement
           ubfactor / 100.0,  // <- ubfactor
           maxnegmove,        // <- maxnegmove for refinement (or -1)
           nrefpass           // <- nb of ref pass (or -1)
           );

  PRINT("=> Scotch Partitioning strategy: %s\n", ss);

  SCOTCH_stratGraphMap(&strat, ss);

  int r = -1;
  if (!fixed)
    r = SCOTCH_graphPart(&sg, nparts, &strat, part);
  else
    r = SCOTCH_graphPartFixed(&sg, nparts, &strat, part);
  assert(r == 0);  // success

  SCOTCH_stratExit(&strat);
  SCOTCH_graphExit(&sg);
}
#endif

/* *********************************************************** */
/*                         ORDERING                            */
/* *********************************************************** */

void StarPart_SCOTCH_ORDER_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  StarPart_Data* local = StarPart_getTaskLocal(task);
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* perm = StarPart_getIntArray(data, "perm");
  int* blk = StarPart_getIntArray(data, "blk");

  assert(g);
  if (perm)
    StarPart_checkArray(data, "perm", g->nvtxs);
  else {
    perm = StarPart_allocArray(data, "perm", g->nvtxs);
    StarPart_initArray(data, "perm", -1);
  }
  if (blk)
    StarPart_checkArray(data, "blk", g->nvtxs);
  else {
    blk = StarPart_allocArray(data, "blk", g->nvtxs);
    StarPart_initArray(data, "blk", -1);
  }

  assert(sizeof(SCOTCH_Idx) == sizeof(int32_t));  // check 32 bits
  assert(sizeof(SCOTCH_Num) == sizeof(int32_t));  // check 32 bits

  SCOTCH_Graph sg;
  SCOTCH_graphInit(&sg);
  SCOTCH_graphBuild(&sg, 0, g->nvtxs, g->xadj, g->xadj + 1, g->vwgts, NULL, g->narcs, g->adjncy, g->ewgts);
  // SCOTCH_graphCheck (&sg);

  SCOTCH_Strat strat;
  SCOTCH_stratInit(&strat);

  int maxlvl = StarPart_getInt(local, "maxlvl");
  int minsize = StarPart_getInt(local, "minsize");
  int bandsize = StarPart_getInt(local, "bandsize");
  int sepbal = StarPart_getInt(local, "sepbal");
  int sepnpass = StarPart_getInt(local, "sepnpass");
  int cprratio = StarPart_getInt(local, "cprratio");
  char* leaf = StarPart_getStr(local, "leaf");
  StarPart_checkStr(local, "leaf", "minfill,simple");

  /* separator FM refinement with/or/without band */
  char* s0;
  if (bandsize > 0)
    asprintf(&s0, "b{width=%d,bnd=f{bal=%lf},org=(|h{pass=%d})f{bal=%lf}}", bandsize, sepbal / 100.0, sepnpass,
             sepbal / 100.0);  // why org uses "h{}" and not only "f{}"?
  else
    asprintf(&s0, "f{bal=%lf}", sepbal / 100.0);

  /* separation strategy */
  char* s1;
  asprintf(&s1, "m{rat=0.8, vert=100, low=h{pass=%d}, asc=%s}", sepnpass, s0);  // or rat 0.7 in MUMPS

  /* nested dissection */
  char* OSE = "g";  // separator ordering = gibbs-poole-stockenmeyer: pass=3 in MAPHYS ?
  char* OLE;        // leaf ordering = minimum fill method: frat=0.0 in MUMPS ?
  if (!strcmp(leaf, "minfill")) {
    asprintf(&OLE, "f{cmin=15,cmax=100000,frat=0.08}");
  }
  else if (!strcmp(leaf, "simple")) {
    asprintf(&OLE, "s");
  }
  else {
    StarPart_error("leaf ordering method \"%s\" unknown!\n", leaf);
  }

  char* s2;
  asprintf(&s2, "n{sep=/((vert>%d)&(levl<%d))?(%s)|(%s);,ole=%s,ose=%s}", minsize, maxlvl, s1, s1, OLE, OSE);

  /* compression */
  char* s3;
  if (cprratio > 0)
    asprintf(&s3, "c{rat=%lf, cpr=%s, unc=%s}", cprratio / 100.0, s2, s2);
  else
    asprintf(&s3, "%s", s2);

  // PRINTINFO("=> Scotch Ordering strategy: %s\n", s3);

  SCOTCH_stratGraphOrder(&strat, s3);

  // int * permtab = malloc(g->nvtxs*sizeof(int)); // permutation array (of size
  // g->nvtxs)
  int* peritab = malloc(g->nvtxs * sizeof(int));  // inverse permutation (array of size g->nvtxs)
  assert(peritab);
  int cblknbr;                                        // nb of column blocks
  int* rangtab = malloc(g->nvtxs * sizeof(int) + 1);  // starting indices of permuted column blocks (max size = nvtxs+1)
  assert(rangtab);
  int* treetab = malloc(g->nvtxs * sizeof(int));  // separator tree structure: who is my
  // father block? (max size = nvtxs)
  assert(treetab);

  int r = SCOTCH_graphOrder(&sg, &strat, perm, peritab, &cblknbr, rangtab, treetab);
  assert(r == 0);  // success

  SCOTCH_stratExit(&strat);
  SCOTCH_graphExit(&sg);

  // compute blk
  for (int b = 0; b < cblknbr; b++)
    for (int i = rangtab[b]; i < rangtab[b + 1]; i++) blk[peritab[i]] = b;

  /* check */
  // for(int i = 0 ; i < g->nvtxs ; i++) assert(blk[i] >= 0 && blk[i] <=
  // nblk(maxlvl)); for(int i = 0 ; i < g->nvtxs ; i++) assert(perm[i] >= 0 &&
  // perm[i] < g->nvtxs);

  /* free */
  free(peritab);
  free(rangtab);
  free(treetab);
  free(s0);
  free(s1);
  free(s2);
  free(s3);
}

/* *********************************************************** */
/*                           PTSCOTCH                          */
/* *********************************************************** */

#ifdef USE_PTSCOTCH

void StarPart_PTSCOTCH_SCATTER_init(StarPart_Method* method)
{
  assert(method);
  StarPart_Context* ctx = StarPart_getMethodContext(method);
  assert(StarPart_isParallelContext(ctx));
  int prank = StarPart_getParallelCommRank(ctx);
  if (prank == 0) {
    StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_IN);
    StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  }
  StarPart_Data* distdata = StarPart_addMethodArg(method, "distdata", STARPART_OUT);
  StarPart_addNewItem(distdata, "graph", STARPART_GRAPH, NULL, true, STARPART_WRITE);
  StarPart_addNewItem(distdata, "vtxdist", STARPART_INTARRAY, NULL, true, STARPART_WRITE);
}

#endif

/* *********************************************************** */

#ifdef USE_PTSCOTCH

void StarPart_PTSCOTCH_SCATTER_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  assert(StarPart_isParallelContext(ctx));
  MPI_Comm comm = StarPart_getParallelComm(ctx);
  int prank = StarPart_getParallelCommRank(ctx);
  int psize = StarPart_getParallelCommSize(ctx);
  assert(psize >= 2);

  // StarPart_Data* data = NULL;
  // Graph* g = NULL;
  // if (prank == 0) {
  //   data = StarPart_getTaskData(task, "data");
  //   g = StarPart_getGraph(data, "graph");
  // }

  StarPart_Data* data = StarPart_getTaskData(task, "data");  // return NULL if DISABLED
  LibGraph_Graph* g = NULL;
  if (prank == 0 && data) {
    g = StarPart_getGraph(data, "graph");
  }

  /* wrap starpart graph into scotch graph */
  SCOTCH_Graph* sg = NULL;
  if (prank == 0) {
    sg = SCOTCH_graphAlloc();
    SCOTCH_graphInit(sg);
    SCOTCH_graphBuild(sg, 0, g->nvtxs, g->xadj, g->xadj + 1, g->vwgts, NULL, g->narcs, g->adjncy, g->ewgts);
  }

  /* scatter scotch graph */
  SCOTCH_Dgraph* dsg = SCOTCH_dgraphAlloc();
  int r0 = SCOTCH_dgraphInit(dsg, comm);
  assert(r0 == 0);  // success
  int r1 = SCOTCH_dgraphScatter(dsg, sg);
  assert(r1 == 0);  // success

  SCOTCH_dgraphCheck(dsg);

  assert(sizeof(SCOTCH_Num) == sizeof(int));
  int base = 0;
  int nbvertglb = 0;
  int nbvertloc = 0;
  int maxvertloc = 0;
  int nbedgeglb = 0;
  int nbedgeloc = 0;
  int edgelocsize = 0;
  int* vertloctab = NULL;
  int* edgeloctab = NULL;
  int* veloloctab = NULL;
  int* edloloctab = NULL;

  SCOTCH_dgraphData(dsg,          /* Graph structure to read */
                    &base,        /* Base value */
                    &nbvertglb,   /* Number of global vertices */
                    &nbvertloc,   /* Number of local vertices */
                    &maxvertloc,  /* Maximum number of local vertices */
                    NULL,         /* Number of local + ghost vertices */
                    &vertloctab,  /* Vertex local adjacency array [vertnbr+1] */
                    NULL,         /* Vertex array [vertnbr] */
                    &veloloctab,  /* Vertex load array */
                    NULL,         /* Vertex label array */
                    &nbedgeglb,   /* Number of global edges (arcs) */
                    &nbedgeloc,   /* Number of local edges (arcs) */
                    &edgelocsize, /* Size of local edge array */
                    &edgeloctab,  /* Local edge array [edgelocsiz]    */
                    NULL,         /* Ghost edge array [edgelocsiz]    */
                    &edloloctab,  /* Edge load array [edgelocsiz]     */
                    NULL);        /* MPI Communicator */

  StarPart_print("nb vert loc/glb = %d/%d, max vert loc = %d, nb edge loc/glb = %d/%d, edge loc size = %d\n", nbvertloc,
                 nbvertglb, maxvertloc, nbedgeloc, nbedgeglb, edgelocsize);

  // check scotch graph is compat
  assert(nbedgeloc == edgelocsize);  // nb arcs
  assert(nbvertloc == maxvertloc);   // nb local vtx

  /* wrap distributed scotch graph as starpart output */

  StarPart_Data* distdata = StarPart_getTaskData(task, "distdata");
  LibGraph_Graph* gg = StarPart_newGraph();  // distributed graph
  StarPart_setGraph(distdata, "graph", gg, true);

  // TODO: set distributed graph gg
  gg->nvtxs = nbvertloc;
  gg->narcs = nbedgeloc;
  gg->xadj = vertloctab;
  gg->adjncy = edgeloctab; /* with global indices */
  gg->vwgts = veloloctab;
  gg->ewgts = edloloctab;

  // debug
  // for (int i = 0; i < psize; i++) {
  //   if (prank == i) { printGraph(gg); }
  //   MPI_Barrier(comm);
  // }

  /* vertex distribution */

  int* nbvtxdist = calloc(psize, sizeof(int));
  MPI_Allgather(&nbvertloc, 1, MPI_INT, nbvtxdist, 1, MPI_INT, comm);

  // printf("nbvtxdist: [");
  // for (int i = 0; i < psize; i++) printf("%d ", nbvtxdist[i]);
  // printf("]\n");

  int* vtxdist = calloc((psize + 1), sizeof(int));
  vtxdist[0] = 0;
  for (int i = 1; i < psize + 1; i++) vtxdist[i] = vtxdist[i - 1] + nbvtxdist[i - 1];

  // printf("vtxdist: [");
  // for (int i = 0; i < psize + 1; i++) printf("%d ", vtxdist[i]);
  // printf("]\n");

  int nvtxs = vtxdist[prank + 1] - vtxdist[prank];
  StarPart_print("nb vert loc = %d, nvtxs = %d\n", nbvertloc, nvtxs);
  assert(nbvertloc == nvtxs);  // check scotch distribution

  StarPart_setIntArray(distdata, "vtxdist", psize + 1, vtxdist, true);

  /* free memory */
  if (prank == 0 && sg) {
    SCOTCH_graphExit(sg);
    free(sg);
  }

  // BUG: how to free memory of the distributed scotch graph???
}

#endif

/* *********************************************************** */

#ifdef USE_PTSCOTCH

void StarPart_PTSCOTCH_GATHER_init(StarPart_Method* method)
{
  // ???
}

#endif

/* *********************************************************** */

#ifdef USE_PTSCOTCH

void StarPart_PTSCOTCH_GATHER_call(StarPart_Task* task)
{
  // int SCOTCH_dgraphGather(const SCOTCH_Dgraph* const, SCOTCH_Graph* const);
}

#endif

/* *********************************************************** */

#ifdef USE_PTSCOTCH

void StarPart_PTSCOTCH_PART_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "distdata", STARPART_INOUT);
  StarPart_addNewItem(data, "distgraph", STARPART_DISTGRAPH, NULL, true, STARPART_READ);
  // StarPart_addNewItem(data, "vtxdist", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
}

#endif

/* *********************************************************** */

#ifdef USE_PTSCOTCH

void StarPart_PTSCOTCH_PART_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  StarPart_Data* data = StarPart_getTaskData(task, "distdata");  // distributed data
  LibGraph_DistGraph* g = StarPart_getDistGraph(data, "distgraph");
  // int* vtxdist = StarPart_getIntArray(data, "vtxdist");
  int* part = StarPart_getIntArray(data, "part");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int nparts = StarPart_getInt(data, "nparts");

  assert(g && nparts > 0);

  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", g->nvtxs);
    StarPart_initArray(data, "part", -1);
  }

  // int edgecut = -1;
  MPI_Comm comm = StarPart_getParallelComm(ctx);
  int prank = StarPart_getParallelCommRank(ctx);
  int psize = StarPart_getParallelCommSize(ctx);
  assert(psize >= 2);

  int nvtxs = g->vtxdist[prank + 1] - g->vtxdist[prank];
  assert(g->nvtxs == nvtxs);

  assert(sizeof(SCOTCH_Num) == sizeof(int));

  // debug
  // for (int i = 0; i < psize; i++) {
  //   if (prank == i) { printGraph(g); }
  //   MPI_Barrier(comm);
  // }

  // ParMETIS_V3_PartKway(vtxdist, g->xadj, g->adjncy, g->vwgts, NULL, &wgtflag, &zero, &one, &nparts, tpwgts, ubvec,
  //                      options_part, &cut, part, &comm);

  SCOTCH_Dgraph dsg;
  int r0 = SCOTCH_dgraphInit(&dsg, comm);
  assert(r0 == 0);

  // int SCOTCH_dgraphBuild(SCOTCH_Dgraph* const grafptr, /* Distributed graph structure to fill  */
  //                        const Gnum baseval,           /* Base for indexing                    */
  //                        const Gnum vertlocnbr,        /* Number of local vertices             */
  //                        const Gnum vertlocmax,        /* Maximum number of local vertices     */
  //                        Gnum* const vertloctab,       /* Local vertex begin array             */
  //                        Gnum* const vendloctab,       /* Local vertex end array               */
  //                        Gnum* const veloloctab,       /* Local vertex load array (if any)     */
  //                        Gnum* const vlblloctab,       /* Local vertex label array (if any)    */
  //                        const Gnum edgelocnbr,        /* Number of local edges                */
  //                        const Gnum edgelocsiz,        /* Size of local edge array             */
  //                        Gnum* const edgeloctab,       /* Local edge array                     */
  //                        Gnum* const edgegsttab,       /* Ghost edge array (if any); not const */
  //                        Gnum* const edloloctab)       /* Local edge load array (if any)       */

  int r1 = SCOTCH_dgraphBuild(&dsg, 0, g->nvtxs, g->nvtxs, g->xadj, g->xadj + 1, g->vwgts, NULL, g->narcs, g->narcs,
                              g->adjncy, NULL, g->ewgts);
  assert(r1 == 0);
  SCOTCH_dgraphCheck(&dsg);

  /*
  - SCOTCH STRATQUALITY: Privilege quality over speed. This is the default behavior of default strategy strings when
  they are used just after being initialized.
  - SCOTCH STRATSPEED: Privilege speed over quality.
  - SCOTCH STRATBALANCE: Enforce load balance as much as possible.
  - SCOTCH STRATSAFETY: Do not use methods that can lead to the occurrence of problematic events,
  such as floating point exceptions, which could not be properly handled by the
  calling software.
  - SCOTCH STRATSCALABILITY: Favor scalability as much as possible.
  */

  SCOTCH_Strat strat;
  SCOTCH_stratInit(&strat);  // default strategy
  int r2 = SCOTCH_stratDgraphMapBuild(&strat, SCOTCH_STRATDEFAULT, psize, nparts, ubfactor / 100.0);
  assert(r2 == 0);

  int r3 = SCOTCH_dgraphPart(&dsg, nparts, &strat, part);
  assert(r3 == 0);

  // SCOTCH_stratExit(&strat);
  // SCOTCH_dgraphExit(&dsg);  // remarks: arrays passed to dgraphBuild() are not free!
}

#endif

/* *********************************************************** */
/*                  REGISTER SCOTCH METHODS                    */
/* *********************************************************** */

void StarPart_registerScotch(StarPart_Context* ctx)
{
  StarPart_registerNewMethod(ctx, "SCOTCH/DEFAULT", StarPart_SCOTCH_DEFAULT_call, StarPart_SCOTCH_init,
                             StarPart_SCOTCH_DEFAULT_desc);
  StarPart_registerNewMethod(ctx, "SCOTCH/KPART", StarPart_SCOTCH_KPART_call, StarPart_SCOTCH_init,
                             StarPart_SCOTCH_KPART_desc);
  StarPart_registerNewMethod(ctx, "SCOTCH/RB", StarPart_SCOTCH_RB_call, StarPart_SCOTCH_init, StarPart_SCOTCH_RB_desc);
  StarPart_registerNewMethod(ctx, "SCOTCH/KFM", StarPart_SCOTCH_KFM_call, StarPart_SCOTCH_init,
                             StarPart_SCOTCH_KFM_desc);
#ifdef USE_SCOTCH_ML
  StarPart_registerNewMethod(ctx, "SCOTCH/ML", StarPart_SCOTCH_ML_call, StarPart_SCOTCH_init, StarPart_SCOTCH_ML_desc);
#endif
  StarPart_registerNewMethod(ctx, "SCOTCH/ORDER", StarPart_SCOTCH_ORDER_call, StarPart_SCOTCH_init,
                             StarPart_SCOTCH_ORDER_desc);

#ifdef USE_PTSCOTCH
  if (StarPart_isParallelContext(ctx)) {
    StarPart_registerNewMethod(ctx, "PTSCOTCH/SCATTER", StarPart_PTSCOTCH_SCATTER_call, StarPart_PTSCOTCH_SCATTER_init,
                               StarPart_PTSCOTCH_SCATTER_desc);
    // StarPart_registerNewMethod(ctx, "PTSCOTCH/GATHER", StarPart_PTSCOTCH_GATHER_call, StarPart_PTSCOTCH_GATHER_init,
    //                            StarPart_PTSCOTCH_GATHER_desc);
    StarPart_registerNewMethod(ctx, "PTSCOTCH/PART", StarPart_PTSCOTCH_PART_call, StarPart_PTSCOTCH_PART_init,
                               StarPart_PTSCOTCH_PART_desc);
  }
#endif
}

/* *********************************************************** */
