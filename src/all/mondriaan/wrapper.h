#ifndef MONDRIAAN_WRAPPER_H
#define MONDRIAAN_WRAPPER_H

#include "starpart.h"

//@{

#define StarPart_MONDRIAAN_PART_desc "hypergraph partitioning routine of Mondriaan"
void StarPart_MONDRIAAN_PART_init(StarPart_Method* method);
void StarPart_MONDRIAAN_PART_call(StarPart_Task* task);

//@}

#endif
