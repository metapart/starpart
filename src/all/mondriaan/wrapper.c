/* Wrapper library for MONDRIAAN */

#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Mondriaan.h"
#include "all.h"
#include "libgraph.h"
#include "starpart.h"
#include "wrapper.h"

static void partitionHypergraphRMondriaan(LibGraph_Hypergraph* hg, int nparts, int ubfactor, int* part);

/* *********************************************************** */

void StarPart_MONDRIAAN_PART_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "hypergraph", STARPART_HYPERGRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  // StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true,
  // STARPART_READ);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_MONDRIAAN_PART_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Hypergraph* hg = StarPart_getHypergraph(data, "hypergraph");

  int* part = StarPart_getIntArray(data, "part");
  // int * fixed = StarPart_getIntArray(data, "fixed");
  int nparts = StarPart_getInt(data, "nparts");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  assert(hg);
  assert(nparts > 0);

  if (part)
    StarPart_checkArray(data, "part", hg->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", hg->nvtxs);
    StarPart_initArray(data, "part", -1);
  }
  // if(fixed) StarPart_checkArray(data, "fixed", hg->nvtxs);

  partitionHypergraphRMondriaan(hg, nparts, ubfactor, part);
}

/* *********************************************************** */

static void partitionHypergraphRMondriaan(LibGraph_Hypergraph* hg, int nparts, int ubfactor, int* part)
{
  struct sparsematrix M;
  double* values = malloc(hg->eptr[hg->nhedges] * sizeof(double));
  for (int i = 0; i < hg->eptr[hg->nhedges]; i++) values[i] = 1.0;
  CRSSparseMatrixInit(&M, hg->nhedges, hg->nvtxs, hg->eptr[hg->nhedges], hg->eind, hg->eptr, values, 0);
  free(values);

  struct opts options;
  SetDefaultOptions(&options);
  options.SplitStrategy = OneDimCol;
  DistributeMatrixMondriaan(&M, nparts, ubfactor / 100.0, &options, NULL);

  for (int p = 0; p < nparts; p++)
    for (int i = M.Pstart[p]; i < M.Pstart[p + 1]; i++) part[M.j[i]] = p;

  MMDeleteSparseMatrix(&M);
}

/* *********************************************************** */
