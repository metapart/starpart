#ifndef STARPART_ALL_H
#define STARPART_ALL_H

#include "starpart.h"

/* *********************************************************** */
/*                       CONSTANTS                             */
/* *********************************************************** */

/* public constant */
// #define STARPART_COARSEN_HEM    "hem"
// #define STARPART_COARSEN_RM     "rm"

/* default values */
#define STARPART_UBFACTOR 5               // unbalance factor
#define STARPART_NREFPASS 10              // nb of refinement passes
#define STARPART_NINITPASS 4              // nb of initial partitioning passes
#define STARPART_MAXNEGMOVE 300           // max negative move allowed for FM refinement (-1 for infinite)
#define STARPART_CPARTSIZE 30             // max size per part of coarsest graph (default value)
#define STARPART_BANDSIZE 3               // band size used for band refinement
#define STARPART_VTK_OUTPUT "output.vtk"  // default vtk output

/* *********************************************************** */

// typedef struct _StarPart_Context StarPart_Context;

/** initialize StarPart context with default methods */
void StarPart_registerAllMethods(StarPart_Context* ctx);

#endif
