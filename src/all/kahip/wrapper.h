#ifndef KAHIP_WRAPPER_H
#define KAHIP_WRAPPER_H

#include "starpart.h"

//@{

#define StarPart_KAHIP_PART_desc "KAFFPA graph partitioning routine of KAHIP"
void StarPart_KAHIP_PART_init(StarPart_Method* method);
void StarPart_KAHIP_PART_call(StarPart_Task* task);

//@}

#endif
