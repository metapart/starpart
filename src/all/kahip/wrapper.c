/* Wrapper library for KAHIP */

#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "all.h"
#include "kahip.h"
#include "libgraph.h"
#include "starpart.h"
#include "wrapper.h"

/* *********************************************************** */

void StarPart_KAHIP_PART_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  // StarPart_addNewItem(data, "fixed", STARPART_INTARRAY, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_KAHIP_PART_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  // int * fixed = StarPart_getIntArray(data, "fixed");
  int nparts = StarPart_getInt(data, "nparts");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  assert(g);
  assert(nparts > 0);

  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", g->nvtxs);
    StarPart_initArray(data, "part", -1);
  }
  // if(fixed) StarPart_checkArray(data, "fixed", g->nvtxs);

  int edgecut = 0;
  int mode = FAST;  // FAST, ECO, STRONG, FASTSOCIAL, ECOSOCIAL, STRONGSOCIAL
  int seed = 0;
  bool suppress_output = true;
  double imbalance = ubfactor / 100.0;

  kaffpa(&g->nvtxs, g->vwgts, g->xadj, g->ewgts, g->adjncy, &nparts, &imbalance, suppress_output, seed, mode, &edgecut,
         part);
}

/* *********************************************************** */
