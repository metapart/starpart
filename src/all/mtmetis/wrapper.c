/* Wrapper library for MTMETIS */

#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "all.h"
#include "libgraph.h"
#include "starpart.h"
#include "wrapper.h"

// #define mtmetis_partition_explicit xyz_##mtmetis_partition_explicit
// #define mtmetis_init_options xyx_##mtmetis_init_options

// #pragma redefine_extname mtmetis_partition_explicit xyz_mtmetis_partition_explicit
// #pragma redefine_extname mtmetis_init_options xyx_mtmetis_init_options

#include "mtmetis.h"

static void partitionGraphMTMETIS(LibGraph_Graph* g, int nparts, int ubfactor, int nthreads, int* part, int* edgecut);

/* *********************************************************** */

void StarPart_MTMETIS_PART_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "graph", STARPART_GRAPH, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "part", STARPART_INTARRAY, NULL, true, STARPART_READWRITE);
  StarPart_addNewItem(data, "nparts", STARPART_INT, NULL, true, STARPART_READ);
  StarPart_addNewItem(data, "nthreads", STARPART_INT, NEW_INT(-1), true, STARPART_READ);  // -1 => omp_get_max_threads()
  StarPart_addNewItem(data, "ubfactor", STARPART_INT, NEW_INT(STARPART_UBFACTOR), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_MTMETIS_PART_call(StarPart_Task* task)
{
  assert(task);
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  LibGraph_Graph* g = StarPart_getGraph(data, "graph");
  int* part = StarPart_getIntArray(data, "part");
  int nparts = StarPart_getInt(data, "nparts");
  int nthreads = StarPart_getInt(data, "nthreads");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  assert(g);
  assert(nparts > 0);

  if (part)
    StarPart_checkArray(data, "part", g->nvtxs);
  else {
    part = StarPart_allocArray(data, "part", g->nvtxs);
    StarPart_initArray(data, "part", -1);
  }

  // nthreads = omp_get_max_threads()
  int edgecut = 0;
  partitionGraphMTMETIS(g, nparts, ubfactor, nthreads, part, &edgecut);
}

/* *********************************************************** */

static void partitionGraphMTMETIS(LibGraph_Graph* g, int nparts, int ubfactor, int nthreads, int* part, int* edgecut)
{
  // check type compatibility
  assert(sizeof(mtmetis_vtx_type) == sizeof(int));
  assert(sizeof(mtmetis_adj_type) == sizeof(int));
  assert(sizeof(mtmetis_wgt_type) == sizeof(int));
  assert(sizeof(mtmetis_pid_type) == sizeof(int));

  *edgecut = 0;

  /* Bug with Metis in this case! */
  if (nparts == 1) {
    for (int i = 0; i < g->nvtxs; i++) part[i] = 0;
    return;
  }

  mtmetis_real_type ub = 1.0 + ubfactor / 100.0;

  double* options = mtmetis_init_options();

  static const size_t DEFAULT_NCUTS = 1;
  static const size_t DEFAULT_NREFPASS = 8;
  // static const mtmetis_real_type DEFAULT_UBFACTOR = 1.03;
  static const size_t DEFAULT_NINITSOLUTIONS = 8;
  static const int DEFAULT_CTYPE = MTMETIS_CTYPE_SHEM;  // MTMETIS_CTYPE_RM;
  static const int DEFAULT_VERBOSITY = MTMETIS_VERBOSITY_NONE;
  // static const int DEFAULT_VERBOSITY = MTMETIS_VERBOSITY_LOW;
  // static const int DEFAULT_VERBOSITY = MTMETIS_VERBOSITY_MEDIUM;
  // static const int DEFAULT_VERBOSITY = MTMETIS_VERBOSITY_HIGH;
  // static const int DEFAULT_VERBOSITY = MTMETIS_VERBOSITY_MAXIMUM;
  static const int DEFAULT_DISTRIBUTION = MTMETIS_DISTRIBUTION_BLOCKCYCLIC;

  options[MTMETIS_OPTION_TIME] = MTMETIS_VAL_OFF;  // default (disable timer)
  options[MTMETIS_OPTION_NPARTS] = (double)nparts;
  options[MTMETIS_OPTION_NTHREADS] = MTMETIS_VAL_OFF;  // default (omp_get_max_threads)
  if (nthreads > 0) options[MTMETIS_OPTION_NTHREADS] = (double)nthreads;
  options[MTMETIS_OPTION_SEED] = MTMETIS_VAL_OFF;  // default (seed = time())
  options[MTMETIS_OPTION_NCUTS] = DEFAULT_NCUTS;
  options[MTMETIS_OPTION_NINITSOLUTIONS] = DEFAULT_NINITSOLUTIONS;
  options[MTMETIS_OPTION_NITER] = DEFAULT_NREFPASS;
  options[MTMETIS_OPTION_UBFACTOR] = ub;  // DEFAULT_UBFACTOR;
  options[MTMETIS_OPTION_CTYPE] = DEFAULT_CTYPE;
  options[MTMETIS_OPTION_VERBOSITY] = DEFAULT_VERBOSITY;
  options[MTMETIS_OPTION_DISTRIBUTION] = DEFAULT_DISTRIBUTION;  // not used

  int r = mtmetis_partition_explicit(g->nvtxs, (mtmetis_adj_type*)g->xadj, (mtmetis_vtx_type*)g->adjncy,
                                     (mtmetis_wgt_type*)g->vwgts, (mtmetis_wgt_type*)g->ewgts, options,
                                     (mtmetis_pid_type*)part, (mtmetis_wgt_type*)edgecut);

  assert(r == MTMETIS_SUCCESS);

  free(options);
}

/* *********************************************************** */
