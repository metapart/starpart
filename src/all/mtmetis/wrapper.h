#ifndef MTMETIS_WRAPPER_H
#define MTMETIS_WRAPPER_H

#include "starpart.h"

//@{

#define StarPart_MTMETIS_PART_desc "multithread partitioning routine of MT-Metis"
void StarPart_MTMETIS_PART_init(StarPart_Method* method);
void StarPart_MTMETIS_PART_call(StarPart_Task* task);

//@}

#endif
