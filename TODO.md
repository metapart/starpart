# Todo List

*Here are a list of some on-going/future developments for StarPart.*

## Ideas

- Recent Ideas
  - we need to introduce a real integer task->id, different from method name (method->id)... use naive order of taskAdd()
  - add graph vtx/edge weight arrays as data items (wrap memory) -> BASIC/GRID, BASIC/LOAD, ...
  - allow using multi-lines strategy files vs single-line strategy on stdin
  - how to support multithread execution of Task queue? how to process data items safely?
  - use prefix "dist" for all distributed items, e.g. distpart or distgraph?
- Data Model
  - use "pin" & "pout" in strategy for parallel input & output (vs "in" & "out" for sequential input & output)
  - add new primitive type STARPART_POINTER (maybe register it with a user-defined callback to dup/free memory) for opaque types
  - introduce a generic StarPart_Struct type for both StarPart_Graph, StarPart_Hypergraph, StarPart_Mesh, ...
  - introduce a generic data type... with a routine: StarPart_registerDataType(), in the same way as methods?
  - add STARPART_REQUIRED item flags and check this requirements?
  - check and update semantic for item access flags (R & W & RW): replace it by READ | WRITE for getter & CREATE for setter (and maybe REPLACE?)
  - how to enable RW access for getter/setter in hooks pre/post processing (if not allowed by method)?
  - add a reference counter to handle properly the raw data memory... For instance: item2=dupItem(item1,deep=false);freeItem(item1); item2 becomes invalid!
- Strategies
  - introduce operator A|B to explicitly auto-connect A to B
  - rework syntax of strategies, maybe using multi-pipes syntax...
- Integer support
  - how to take into accounts 32bits/64bits integer for LibGraph and external packages? In metis.h, you need to setup IDXTYPEWIDTH=32|64.
  - replace all 'int' in libgraph & starpart by int32_t, uint32_t, int64_t, int64_t => Graph32 & Graph64 ?

## Todo

- New 
  - freeContext() should involve freeAllTasks()?
- Bug Issues
  - how to cleanly handle concurrent contexts... we should probable not accept this? problem with global context stack.
  - doc generation is broken on gitlab pages
  - Python do not work with some external packages (shared libs issue)
- Minors / Majors
  - add method StarPart_checkStrat() and better handle errors in scanner/parser...
  - "make clean" should clean external projects!
  - add unit tests for API (data, context, ...) and to check memory leaks
- Packages
  - for all packages, rename \<package\>/wrapper.h into \<package\>/startpart-\<package\>.h
  - move packages from src/all/* to packages/*
  - how to document each method in packages? (idea: for each method XXX, write a file XXX.md)
  - update and test parallel packages for PARMETIS/XTRAPULP/PTSCOTCH/ZOLTAN/ZOLTAN2/...
  - split BASIC package into several packages (PARALLEL, DEBUG, ...)
  - add convention of "object renaming" using objcopy (PACKAGE_symbol) to avoid symbol conflicts when linking several packages or linking several times the same package (with different compilation options)...
  - update/add SCOTCHML and METISML methods/packages (how exactly? using callback or patches)
- Pre- & Post-processing
  - hook "store": implement strategy string store=dataid/itemid, ...
  - update VTK support
  - check if BASIC/SAVE save graph coords...
  - how to SAVE and LOAD Distributed Graph/Mesh efficiently method in H5 format. It should use a file already divided into pieces... see maybe in ParMetis.
  - benchmark post-processing using output format for R
- GUI
  - visual representation of connected tasks (DAG, data-flow, ...)
  - load a string strategy and visualize it as basic SVG drawing?
  - method toolbox: create and connect tasks and export the strategy
  - simple interaction with this representation to explore StarPart_Data & Items, ...
  - step by step execution of the task queue
  - how to handle MPI for parallel context in the GUI?
  - using Python & Python binding?
    - implement node editor similar to Blender's one
    - tkinter (native on python, based on TK) or wxPython (based on GTK) or PyQT (based on QT) or PyGObject (based on GTK3)
    - Cairo: vectorial drawing
- LibGraph
  - what about Distributed Mesh into Libgraph & StarPart?
  - add LIBGRAPH_COORD_VAR in LibGraph_ArrayFlags?
  - refactoring of the API with "LibGraph_" prefix or something else?
  - add documentation
  - add LibGraph_DistGraph structure: struct { Graph g; int prank; int psize; int vtxdist[psize+1]; }
  - add unit tests + covering

## Bug

- move bugs here...


What else?

---
