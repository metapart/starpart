# StarPart

[![build status](https://gitlab.inria.fr/metapart/starpart/badges/master/pipeline.svg)](https://gitlab.inria.fr/metapart/starpart/commits/master)
[![coverage report](https://gitlab.inria.fr/metapart/starpart/badges/master/coverage.svg)](https://gitlab.inria.fr/metapart/starpart/commits/master)

StarPart is a flexible and extensible framework that integrates state-of-the-art methods for graph partitioning and sparse matrix ordering.

More precisely, StarPart is a framework that offers a uniform API to manipulate graph, hypergraph and mesh structures.  It is designed to be easily extensible by adding new methods and to plug all these methods into a comprehensive framework.  It is initially designed to provide graph partitioning and sparse matrix ordering methods, that come from sate-of-the-art software such as Metis, Scotch, Patoh, Zoltan, etc. Besides, it provides some facilities for IO, diagnostic, benchmark, visualization (VTK, SVG, ...).

[StarPart](https://gitlab.inria.fr/metapart/starpart) is the core of the [MetaPart](https://gitlab.inria.fr/metapart) project. It is built upon the [LibGraph](https://gitlab.inria.fr/metapart/libgraph) library.

## Installation

Before starting the compilation process, you need to fulfill some dependencies. For instance, you can install the following packages on a Debian-like system.

```bash
sudo apt-get install cmake git gcc libglib2.0-dev libpng-dev gcovr valgrind doxygen graphviz openmpi-* python3
```

Additionally, you will need: vtk, paraview

### Download

You can download the last version of StarPart directly from the [Inria
Gitlab](https://gitlab.inria.fr/metapart/starpart) repository, as follow:

```bash
# for end-users
git clone --recursive https://gitlab.inria.fr/metapart/starpart.git
# for developpers only
git clone --recursive git@gitlab.inria.fr:metapart/starpart.git
```

Note the `--recursive` option that is required because StarPart uses a GIT submodules (libgraph).

### Compilation

StarPart can be built using [CMake](https://cmake.org/). StarPart
integrates several external packages, that can be installed
optionally. All these external packages are integrated in the StarPart
framework as [CMake External Projects](https://cmake.org/cmake/help/v3.0/module/ExternalProject.html).
Here are the table of packages that are already available in StarPart
or that will be integrated in the near future. Besides you can
enable/disable any available package using a *CMake option*.

| **Package**                                                                                  | **Version** | **Model**  | **Parallel** | **Open** | **Available** | **CMake Option**   |
| -------------------------------------------------------------------------------------------- | ----------- | ---------- | ------------ | -------- | ------------- | ------------------ |
| [KGGGP](https://gitlab.inria.fr/metapart/libgraph)                                           |             | graph      | no           | source   | **yes**       | `USE_KGGGP=ON`     |
| [Scotch](http://www.labri.fr/perso/pelegrin/scotch/)                                         | 6.0.4       | graph      | no           | source   | **yes**       | `USE_SCOTCH=ON`    |
| [PT-Scotch](http://www.labri.fr/perso/pelegrin/scotch/)                                      |             | graph      | MPI          | source   | **yes**       | `USE_PTSCOTCH=ON`  |
| [Metis](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview)                               | 5.1.0       | graph      | no           | source   | **yes**       | `USE_METIS=ON`     |
| [MT-Metis](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview)                            | 0.6         | graph      | OpenMP       | source   | **yes**       | `USE_MTMETIS=ON`   |
| [ParMetis](http://glaros.dtc.umn.edu/gkhome/metis/parmetis/overview)                         | 4.0.3       | graph      | MPI          | source   | **yes**       | `USE_PARMETIS=ON`  |
| [PuLP/XtraPuLP](https://github.com/HPCGraphAnalysis/PuLP)                                    | 0.2         | graph      | OpenMP/MPI   | source   | **yes**       | `USE_PULP=ON`      |
| [KaHIP](http://algo2.iti.kit.edu/kahip/)                                                     | 2.0         | graph      | OpenMP/MPI   | source   | **yes**       | `USE_KAHIP=ON`     |
| [PaToH](http://bmi.osu.edu/umit/software.html#zoltan)                                        | 3.2         | hypergraph | no           | binary   | **yes**       | `USE_PATOH=ON`     |
| [Mondriaan](http://www.staff.science.uu.nl/~bisse101/Mondriaan/mondriaan.html)               |             | hypergraph | no           | source   | **yes**       | `USE_MONDRIAAN=ON` |
| [ZOLTAN](http://www.cs.sandia.gov/zoltan/)                                                   | 3.83        | hypergraph | MPI          | source   | **yes**       | `USE_ZOLTAN=ON`    |
| [ZOLTAN2](https://github.com/trilinos/Trilinos/tree/master/packages/zoltan2)                 | beta        | hypergraph | MPI          | source   | not yet       | --                 |
| [hMetis](http://glaros.dtc.umn.edu/gkhome/metis/hmetis/overview)                             |             | hypergraph | no           | binary   | no            | --                 |
| [Chaco](https://cfwebprod.sandia.gov/cfdocs/CompResearch/templates/insert/softwre.cfm?sw=36) |             | graph      | no           | source   | no            | --                 |
| [JOSTLE](http://staffweb.cms.gre.ac.uk/~wc06/jostle/)                                        |             | graph      | no           | no       | no            | --                 |
| [MANGOOSE](https://github.com/scottkolo/mongoose/)                                           |             | graph      | no           | no       | not yet       | --                 |

<!-- PARTY? -->

Some other CMake options are available:

* basic internal routines: `USE_BASIC=[ON/OFF]`
* enable MPI: `USE_MPI=[ON/OFF]`
* enable OpenMP: `USE_OMP=[ON/OFF]`  
* download packages automatically: `PCK_DOWNLOAD=[ON/OFF]`
* URL where to download packages: `PCK_URL` (used only if `PCK_DOWNLOAD=ON`)
* directory where to find packages: `PCK_DIR` (used only if `PCK_DOWNLOAD=OFF`)
* build packages in verbose mode: `PCK_VERBOSE=[ON/OFF]`

In the standard way, the compilation process of StarPart will download automatically all external packages required:

```bash
cd starpart ; mkdir -p build ; cd build  ; cmake .. ; make
```

Besides, the build may be configured using a convenient interface like `ccmake`.

Alternatively, you can choose to manually download the external packages from a *mirror* server (<https://gitlab.inria.fr/metapart/mirror>)
before the build process, by using the `download.sh` script, located in `download` directory.

```bash
cd starpart
cd download ; ./download.sh ; cd -
mkdir -p build ; cd build
cmake -DPCK_DOWNLOAD=OFF -DPCK_DIR=../download .. ; make
```

### Documentation

The Doxygen documentation is available at <https://metapart.gitlabpages.inria.fr/starpart/doc/api.html>.

You can generate an HTML documentation (using Doxygen) with the following command:

```bash
make doc
```

### Test

To run all tests:

```bash
make test
```

To run all tests with coverage and memory check (by Valgrind):

```bash
make ExperimentalTest
make ExperimentalMemCheck
make ExperimentalCoverage
```

If you want to see the list of all available tests:

```bash
ctest -N
```

To run a specific test with verbose output:

```bash
ctest -V -R <testname>
```

To see the CMake dashboard (CDash) of the StarPart project: <http://cdash.inria.fr/CDash/index.php?project=StarPart>.

You can also find details on coverage at <https://metapart.gitlabpages.inria.fr/starpart/cov/>.

## StarPart Model

The StarPart framework uses a *task model* (`StarPart_Task`) to handle the processing of multiple data-flows. The definition of a task is called a *method* (`StarPart_Method`) and is provided by given an *init* function (declaration of arguments) and a *call* function (processing). Each method may have several input argument and several output argument, each of which is identified by an ID (e.g. "data") or an alias ("#in"). Besides, each method owns a special local argument (named "local", with alias "#local") that is a convenient way to passed some parameters to the task with a scope that is limited to the task itself. All arguments are represented by `StarPart_Data` with some specific flags (`STARPART_IN`, `STARPART_OUT` or `STARPART_INOUT`), which explains how the data are used by the method.

Given a `StarPart_Method` named "X", you can instantiate a `StarPart_Task`, that requires to connect its input & output arguments with some actual data (represesented by `StarPart_Data` as well). A task is executed within a context (`StarPart_Context`)...

![StarPart Task Model](misc/starpart-task-model.png?raw=true "StarPart Task Model")

### StarPart Data

The StarPart *data model* is based on a structured type (`StarPart_Data`), containing one or more *data items* (`Starart_Item`), each of which has an ID and a data type (`StarPart_Type`). The following table enumerates the basic StarPart types and the format of user arguments passed to task as a string.

| **typecode**           | **type**              | **description**   | **string representation**      |
| ---------------------- | --------------------- | ----------------- | ------------------------------ |
| `STARPART_VOID`        | `void`                | void              |                                |
| `STARPART_STRAT`       | `StarPart_Strat`      | strategy string   | {strat}                        |
| `STARPART_INT`         | `int`                 | integer           | 10                             |
| `STARPART_FLOAT`       | `float`               | float             | 2.0f                           |
| `STARPART_DOUBLE`      | `double`              | double            | 2.0                            |
| `STARPART_STR`         | `char*`               | string            | "abc" or abc                   |
| `STARPART_INTARRAY`    | `LibGraph_Array`      | integer array     | (1,2,3)                        |
| `STARPART_FLOATARRAY`  | `LibGraph_Array`      | float array       | (1.0f,2.0f,3.0f)               |
| `STARPART_DOUBLEARRAY` | `LibGraph_Array`      | double array      | (1.0,2.0,3.0)                  |
| `STARPART_STRARRAY`    | `LibGraph_Array`      | string array      | ("aa","bb","cc") or (aa,bb,cc) |
| `STARPART_GRAPH`       | `LibGraph_Graph`      | graph             |                                |
| `STARPART_DISTGRAPH`   | `LibGraph_DistGraph`  | distributed graph |                                |
| `STARPART_HYPERGRAPH`  | `LibGraph_Hypergraph` | hypergraph        |                                |
| `STARPART_MESH`        | `LibGraph_Mesh`       | mesh              |                                |
| `STARPART_HYBRIDMESH`  | `LibGraph_Hybridmesh` | hybrid mesh       |                                |

As StarPart is mainly dedicated for graph & mesh processing, it follows some conventions in the naming of data items, that are used by all methods (as argument name), that are already provided in different packages. In this convention, it is expected (but not required) that a `StarPart_Data` will only contain one instance of the following types: graph, hypergraph, mesh or hybrid mesh. Here is the list of item IDs with a predefined meaning in StarPart :

* *graph*: a default graph
* *distgraph*: a default distributed graph
* *hypergraph*: a default hypergraph
* *part*: a partition (integer) array for the default graph (or hypergraph)
* *nbparts*: the number of parts (integer) of the default graph (or hypergraph) partition
* *ubfactor*: the imbalance factor (integer, in percent) of the default graph (or hypergraph) partition
* *fixed*: a fixed-vertex (integer) array for the default graph (or hypergraph)
* *coords*: an array of 3d double coordinates for the default graph (or hypergraph)
* *mesh*: a default mesh (related to the default graph)
* *hmesh*: a default hybrid mesh (related to the default graph)

### Task Inputs & Outputs

At runtime, a task (`StarPart_Task`) processes some data (`StarPart_Data`) as input or output, according to its method (`StarPart_Method`) definition. Each method argument is declared with a flag (described in the table below), which explains how data are used.

| **data flags**   | **description**                                                         |
| ---------------- | ----------------------------------------------------------------------- |
| `STARPART_IN`    | a `StarPart_Data` used by a method as an input                          |
| `STARPART_OUT`   | a new `StarPart_Data` produced by a method as an output                 |
| `STARPART_INOUT` | a `StarPart_Data` used by a method as an input and updated as an output |

In the generic case, a task has both several input arguments and output arguments. It receives data from other tasks, modify the data in some way, and then deliver the modified data as output to be used by other tasks. In particular, a task with a single ouput argument (no inputs) is typically called a *source* task, that produced new data (file reader, mesh generator, ...). On the contrary, a task with a single input argument (no outputs) is typically dedicated to post-processing (diagnostic, file saving, ...). Finally, a task with a single `STARPART_INOUT` argument is typically called a *filter* task, when it will read some items and modify some others (e.g. update values of an array).

In addition, the definition of an argument specifies which items (contained in the data) are used and how they will be processed by a method/task. The following table summarizes the different possibilities.

|        | **item flags**       | **description**                                                          |
| ------ | -------------------- | ------------------------------------------------------------------------ |
| `[ ]`  | `STARPART_ALL`       | enable all access flags (default)                                        |
| `[r]`  | `STARPART_READ`      | enable to read item                                                      |
| `[w]`  | `STARPART_WRITE`     | enable to write item                                                     |
| `[rw]` | `STARPART_READWRITE` | enable both & read flags                                                 |
| `[+]`  | `STARPART_CREATE`    | if item does not exist, enable to create it (involve read & write flags) |
| `[*]`  | `STARPART_REPLACE`   | if item exists, enable to replace it (involve read & write flags)        |

While `STARPART_WRITE` flag just eanbles to change the values of an item, the `STARPART_REPLACE` flag allows to modify its associated memory area (as for instance, the reallocation of an array).

For all other argument items, that are not declared to be used, the default access flag `STARPART_ALL` (value 0) granted all privileges. It is particularly useful for methods that processes items dynamically (for instance, duplicate all items of a `StarPart_Data`).

Here is a list of additional rules:

* A `STARPART_OUT` argument involves that all its items have only the flag `STARPART_CREATE`.
* A `STARPART_IN` argument involves that all its items have only the flag `STARPART_READ`.
* A `STARPART_INOUT` argument implies that at least one item has the flag `STARPART_READ` (or involve it) and that at least one other item has the flag `STARPART_WRITE` (or involve it).
* An error occurs if an item with flag `STARPART_CREATE` already exists in the data passed as argument.
* An error occurs if an item with flag `STARPART_READ` or `STARPART_WRITE` or `STARPART_REPLACE` does not exist in the data passed as argument.
* An item with flag `STARPART_WRITE` or `STARPART_REPLACE` must necessarily be passed in a `STARPART_INOUT` argument.

Some combinations of flags are possible:

* `STARPART_READ|STARPART_WRITE` accesses an item in *read-write* mode, while `STARPART_READ` (resp. `STARPART_WRITE`) accesses an item in *read-only* (resp. *write-only*) mode.
* The combination `STARPART_CREATE|STARPART_REPLACE` enables to create a new item if it does not already exists, or to replace if it exists

### StarPart Strategy

A strategy is a string of characters defined with the following grammar:

```text
STRAT := METHOD;STRAT
METHOD := PACKAGE_NAME/METHOD_NAME | PACKAGE_NAME/METHOD_NAME[KEYVAL_LIST]
KEYVAL_LIST := KEYVAL,KEYVAL_LIST
KEYVAL := KEY | KEY=VAL | KEY>VAL | KEY<VAL
KEY := ARG_ID/ITEM_ID | ALIAS/ITEM_ID
VAL := <string representation of a StarPart type>
```

To handle elegantly the connection of a task argument (named *arg_id*) to a data (named *data_id*), you can use the following syntax within a string strategy:

* for input arguments: *arg_id<data_id*
* for output arguments: *arg_id>data_id*

The *arg_id* can be conveniently replaced by an alias:

* for input arguments: #in0 (or #in), #in1, #in2, ...
* for output arguments: #out0 (or #out), #out1, #out2, ...
* for local arguments: #local0 (or #local), #local1, #local2, ...

To understand the strategy syntax more easily, the data ID uses the symbol '@' as prefix, while the alias uses the symbol '#' as prefix.

As an example, consider a strategy made up of two tasks "A" and "B" (provided by the package "TEST") that you want to execute consecutively:

```text
TEST/A;TEST/B
```

Lets assume that the task "A" produces a single output argument "argA", while the task "B" consumes a single input argument "argB". In practice, we do not directly connect tasks together, but we connect a task argument (given its ID, e.g. "argA") to an actual data (given its ID, e.g. "@0"). If an output argument of a task is connected to the same data as the input argument of a next task is connected to, we can say these two tasks are connected. In practice, we can connect the task "A" to the task "B" in several different ways, more or less explicit, using the following syntax:

```text
TEST/A[argA>@0];TEST/B[argB<@0]   # connect tasks using argument IDs and data ID "@0" explicitly
TEST/A[#out0>@0];TEST/B[#in0<@0]  # same connection using aliases instead of argument IDs
TEST/A[#out>@0];TEST/B[#in<@0]    # idem
TEST/A[0>@0];TEST/B[0<@0]         # a little bit shorter
TEST/A[>@0];TEST/B[<@0]           # idem
```

The last way is to let StarPart connect consecutive tasks automatically when it is possible. In this case, the first output of "A" is connected to the first input of "B" (if such input and output are available).

```text
TEST/A;TEST/B                     # equivalent to "TEST/A[#out>@0];TEST/B[#in<@0]"
```

To be continued..

### Basic Example

In order to run a basic StarPart example, you can use the
`starpart-run` command provided in the `test` sub-directory.

To get some help on this command:

```bash
./starpart-run -h
```

To list all available methods (depending on installed packages):

```bash
./starpart-run -l
```

To print a detailed description of a specific method:

```bash
./starpart-run -p "BASIC/GRID"
```

For instance, you can run a string strategy as follows:

```bash
./starpart-run -r "BASIC/GRID;METIS/KPART[nparts=4];BASIC/DIAG"
```

In addition, you can find many other examples that are used as test in [CMakeLists.txt](https://gitlab.inria.fr/metapart/starpart/blob/master/test/CMakeLists.txt).

### Register your own method

This example shows how to register a new method named "HELLO" into a
StarPart_Context and to call it through a strategy string.

[starpart-hello.c](https://gitlab.inria.fr/metapart/starpart/blob/master/test/starpart-hello.c)

The execution of this program will produce the following output:

```text
Hello World!
Goodbye World!
Goodbye World!
Goodbye World!
```

### Integrate StarPart in your own Code

Most parallel numerical simulations include a preliminary step of
splitting a mesh into pieces. StarPart offers a wide variety of graph
partitioning methods, which makes it easy to compare these different
methods and to better adjust the parameters of these methods.

For instance, consider the following code snippet that uses *Metis*. For more details, look at [metis-kpart.c](https://gitlab.inria.fr/metapart/starpart/blob/master/test/metis-kpart.c).

```c
#include "metis.h"

/* external Metis graph structure & input parameters */
real_t ub;
idx_t nvtxs, ncon, nparts, edgecut;
idx_t * xadj, * adjncy, * vwgts, * ewgts, * part, * options;

METIS_PartGraphKway(&nvtxs, &ncon, xadj, adjncy, vwgts, NULL, ewgts, &nparts, NULL, &ub, options, &edgecut, part);
```

Here is the way we would re-write this using the *StarPart* library. For more details, look at [starpart-metis-kpart.c](https://gitlab.inria.fr/metapart/starpart/blob/master/test/starpart-metis-kpart.c).

```c
#include "libgraph.h"
#include "starpart.h"
#include "starpart-metis.h"

/* external Metis graph structure & input parameters */
int ubfactor, nparts, edgecut;
idx_t nvtxs, ncon, * xadj, * adjncy, * vwgts, * ewgts, * part, * options;

/* wrap graph data */
LibGraph_Graph g;
LibGraph_wrapGraph(&g, nvtxs, narcs, xadj, adjncy, vwgts, ewgts);

/* process graph with METIS package of StarPart */
StarPart_Context* ctx = StarPart_newContext();
StarPart_registerMetis(ctx);
StarPart_Data* data = StarPart_registerNewData(ctx, "@0", 0);
StarPart_Task* task = StarPart_newTaskByName(ctx, "METIS/KPART", NULL);
StarPart_insertTaskArgByValue(task, "#in", "nparts", &nparts, false);
StarPart_insertTaskArgByValue(task, "#in", "ubfactor", &ubfactor, false);
StarPart_insertTaskArgByValue(task, "#in", "graph", &g, false);
StarPart_submitTask(task);
StarPart_connectTask(task, "#in", "@0");
StarPart_run(ctx);
StarPart_freeAllTasks(ctx);

/* get output partition and compute edgecut */
int* part = StarPart_getIntArray(data, "part");
int edgecut = LibGraph_computeGraphEdgeCut(&g, nparts, part);

StarPart_freeContext(ctx);
LibGraph_freeMesh(&m);
LibGraph_freeGraph(&g);
```

## Python

StarPart is a C library but we offer the ability to use it directly from Python. You can find StarPart module in the subdirectory 
[python](https://gitlab.inria.fr/metapart/starpart/blob/master/python). To give you an idea on on how to use it, see
[starpart-simple.py](https://gitlab.inria.fr/metapart/starpart/blob/master/python/starpart-simple.py), 
[starpart-complex.py](https://gitlab.inria.fr/metapart/starpart/blob/master/python/starpart-complex.py) and
[starpart-run.py](https://gitlab.inria.fr/metapart/starpart/blob/master/python/starpart-run.py).

## Misc

### Update the libgraph submodule

*This section is mainly for developers.*

The [LibGraph](https://gitlab.inria.fr/metapart/libgraph) library provides basic
graph & hypergraph routines. It is a piece of the [MetaPart](https://gitlab.inria.fr/metapart)
project. It is integrated within StarPart as a GIT submodule in the `src/libgraph`
subdirectory.

First, imagine we have done some important commits into the `libgraph`
repository. And we typically want to integrate these new developments
into our `starpart` repository.

In pratice, the `src/libgraph` subdirectory points to a particular
commit ID of the `libgraph` repository. By default, your submodule is
in a special branch (detached head), that will not be updated
automatically (with a pull command at starpart level).

Entering in the `src/libgraph` subdirectory, you can update your
submodule by hand... and then you can update the version of the
submodule that must be used in the main `starpart` project.

```bash
git submodule status               # print the current submodule commit ID
cd src/libgraph                    # enter submodule directory
git checkout master                # because your are in 'detached head"...
git pull                           # synchronize your submodule
cd ../..                           # go back in starpart main module
git add src/libgraph               # update the submodule commit ID
git commit -m "update submodule"   # commit it
git push                           # push it
```

Alternatively, you can update all submodules directly:

```bash
git submodule update --remote --merge   # or --rebase instead of --merge
git commit -am "update all submodules"
git push  
```

Now, if you want to update a GIT submodule, you need to perform two steps:

```bash
git pull
git submodule update  
```

Go further with [GIT Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

### Todo List

*This section is mainly for developers.*

<https://gitlab.inria.fr/metapart/starpart/blob/master/TODO.md>

### License

<https://gitlab.inria.fr/metapart/starpart/blob/master/LICENSE>

### Main Publications

* *Comparison of initial partitioning methods for multilevel direct k-way graph partitioning with fixed vertices.* Maria Predari, Aurélien Esnard, Jean Roman. Parallel Computing, Elsevier, 2017.
* *A k-way Greedy Graph Partitioning with Initial Fixed Vertices for Parallel Applications.* Maria Predari, Aurélien Esnard. PDP 2016.
* *Coupling-Aware Graph Partitioning Algorithms: Preliminary Study.* Maria Predari, Aurélien Esnard. HIPC 2014.
* *Graph Repartitioning with both Dynamic Load and Dynamic Processor Allocation.* Clément Vuchener, Aurélien Esnard. ParCo 2013.
* *Dynamic Load-Balancing with Variable Number of Processors based on Graph Repartitioning.* Clément Vuchener, Aurélien Esnard. HIPC 2012.

### Authors

The following people contribute or contributed to the development of StarPart:

* Aurélien Esnard (main developer)
* Maria Predari (former PhD student, 2014-2017)
* Cyril Bordage (Engineer, 2018)

---
