#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "libgraph.h"
#include "starpart.h"

void StarPart_registerMetis(StarPart_Context* ctx);  // #include "starpart-metis.h"

#define DIM 100

int main(int argc, char** argv)
{
  int ubfactor = 5;
  int nparts = 4;
  if (argc == 2) nparts = atoi(argv[1]);
  assert(nparts > 0);

  // generate a graph in Metis format using LibGraph
  LibGraph_Mesh m;
  LibGraph_generateGrid2D(DIM, DIM, 0, 1.0, 1.0, &m);
  LibGraph_Graph g;
  LibGraph_mesh2Graph(&m, &g);

  /* wrap external graph structure */
  // LibGraph_Graph g;
  // LibGraph_wrapGraph(&g, nvtxs, narcs, xadj, adjncy, vwgts, ewgts);

  /* process graph with METIS package of StarPart */
  StarPart_Context* ctx = StarPart_newContext();
  StarPart_registerMetis(ctx);
  StarPart_Data* data = StarPart_registerNewData(ctx, "@0", 0);
  StarPart_Task* task = StarPart_newTaskByName(ctx, "METIS/KPART", NULL);
  StarPart_insertTaskArgByValue(task, "#in", "nparts", &nparts, false);
  StarPart_insertTaskArgByValue(task, "#in", "ubfactor", &ubfactor, false);
  StarPart_insertTaskArgByValue(task, "#in", "graph", &g, false);
  StarPart_submitTask(task);                // or StarPart_submitAllTasks(ctx);
  StarPart_connectTask(task, "#in", "@0");  // or StarPart_connectAllTasks(ctx, "@0", NULL);
  StarPart_run(ctx);
  StarPart_freeAllTasks(ctx);

  /* get output partition and compute edgecut */
  int* part = StarPart_getIntArray(data, "part");
  int edgecut = LibGraph_computeGraphEdgeCut(&g, nparts, part);

  StarPart_freeContext(ctx);
  LibGraph_freeMesh(&m);
  LibGraph_freeGraph(&g);

  printf("Metis partition of a grid %dx%d in %d parts (ub=%d%%): edgecut=%d\n", DIM, DIM, nparts, ubfactor, edgecut);

  return 0;
}