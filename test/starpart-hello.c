#include <stdio.h>
#include <stdlib.h>
#include "starpart.h"

#define StarPart_TEST_HELLO_desc "hello world!"

/* *********************************************************** */

void StarPart_TEST_HELLO_init(StarPart_Method* method)
{
  StarPart_Data* local = StarPart_getMethodArg(method, "#local");
  StarPart_addNewItem(local, "msg", STARPART_STR, NEW_STR("Hello World!"), true, STARPART_READ);
  StarPart_addNewItem(local, "count", STARPART_INT, NEW_INT(1), true, STARPART_READ);
}

/* *********************************************************** */

void StarPart_TEST_HELLO_call(StarPart_Task* task)
{
  StarPart_Data* local = StarPart_getTaskData(task, "#local");
  char* msg = StarPart_getStr(local, "msg");
  int count = StarPart_getInt(local, "count");
  for (int i = 0; i < count; i++) printf("%s\n", msg);
}

/* *********************************************************** */

int main(int argc, char** argv)
{
  StarPart_Context* ctx = StarPart_newContext();
  StarPart_setDebug(ctx, true);
  StarPart_registerNewMethod(ctx, "TEST/HELLO", StarPart_TEST_HELLO_call, StarPart_TEST_HELLO_init,
                             StarPart_TEST_HELLO_desc);

  StarPart_Task* hello1 = StarPart_newTaskByName(ctx, "TEST/HELLO", "msg=\"Goodbye World!\", count=3");
  StarPart_Task* hello0 = StarPart_newTaskByName(ctx, "TEST/HELLO", NULL);

  StarPart_submitTask(hello0);
  StarPart_submitTask(hello1);

  StarPart_Task* task;
  while ((task = StarPart_step(ctx))) { printf("*** execute task %s ***\n", StarPart_getTaskID(task)); }

  StarPart_freeAllTasks(ctx);
  StarPart_freeContext(ctx);

  return 0;
}

/* *********************************************************** */
