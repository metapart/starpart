#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "starpart.h"

/* *********************************************************** */
/*                          INC                              */
/* *********************************************************** */

#define StarPart_TEST_INC_desc "integer increment"

/* *********************************************************** */

void StarPart_TEST_INC_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "x", STARPART_INT, NEW_INT(1), true, STARPART_READWRITE);
}

/* *********************************************************** */

void StarPart_TEST_INC_call(StarPart_Task* task)
{
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  // int* px = StarPart_getIntAddr(data, "x");
  StarPart_Item * item = StarPart_getItem(data, "x");
  int * px = (int*)StarPart_getItemAddr(item);
  (*px)++;

  // StarPart_setInt(data, "x", x); // WARNING: it will allocate a new integer!!!
}

/* *********************************************************** */
/*                          NESTED                             */
/* *********************************************************** */

#define StarPart_TEST_NESTED_desc "test of nested context"

/* *********************************************************** */

void StarPart_TEST_NESTED_init(StarPart_Method* method)
{
  StarPart_Data* data = StarPart_addMethodArg(method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "x", STARPART_INT, NEW_INT(0), true, STARPART_READWRITE);
}

/* *********************************************************** */

void StarPart_TEST_NESTED_call(StarPart_Task* task)
{
  StarPart_Data* data = StarPart_getTaskData(task, "data");
  StarPart_Context* ctx = StarPart_getTaskContext(task);
  StarPart_Context* _ctx = StarPart_newNestedContext(ctx);
  StarPart_registerData(_ctx, data);
  char* id = StarPart_getDataID(data);
  StarPart_newTaskByName(_ctx, "TEST/INC", NULL);
  StarPart_newTaskByName(_ctx, "TEST/INC", NULL);
  StarPart_submitAllTasks(_ctx);
  StarPart_connectAllTasks(_ctx, id, NULL);
  StarPart_run(_ctx);
  StarPart_freeAllTasks(_ctx);
  StarPart_freeNestedContext(_ctx);
}

/* *********************************************************** */

int main(int argc, char** argv)
{
  StarPart_Context* ctx = StarPart_newContext();
  StarPart_registerNewMethod(ctx, "TEST/INC", StarPart_TEST_INC_call, StarPart_TEST_INC_init, StarPart_TEST_INC_desc);
  StarPart_registerNewMethod(ctx, "TEST/NESTED", StarPart_TEST_NESTED_call, StarPart_TEST_NESTED_init,
                             StarPart_TEST_NESTED_desc);
  StarPart_Data* data = StarPart_registerNewData(ctx, "@1", 0);
  int x = 0;
  StarPart_addNewItem(data, "x", STARPART_INT, &x, false, 0);
  StarPart_Task* nested = StarPart_newTaskByName(ctx, "TEST/NESTED", NULL);
  StarPart_connectTask(nested, "#in", "@1");
  // StarPart_printData(data);
  printf("x = %d\n", x);
  assert(x == 0);
  
  StarPart_submitTask(nested);
  StarPart_run(ctx);

  StarPart_freeAllTasks(ctx);
  // StarPart_printData(data);
  StarPart_freeContext(ctx);
  printf("x = %d\n", x);
  assert(x == 2);

  return 0;
}

/* *********************************************************** */
