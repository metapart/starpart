#include <assert.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef USE_MPI
#include <mpi.h>
#endif

#include "all.h"
#include "starpart.h"

int usage(int argc, char* argv[])
{
  printf("usage: %s [options]\n", argv[0]);
  printf("  -h: print this help message\n");
  printf("  -l: list all available methods\n");
  printf("  -p <package/method>: print method description\n");
  printf("  -r <strat>: run a string strategy\n");
  printf("  -m: MPI master mode\n");
  printf("  -s: MPI slave mode\n");
  printf("  -g: set debug mode\n");
  exit(EXIT_FAILURE);
}

int main(int argc, char* argv[])
{
  int opt;
  bool debug = false;
  bool parallel = false;
#ifdef USE_MPI
  bool master = false;
#endif
  bool list = false;
  char* printdesc = NULL;
  char* strat = NULL;

  while ((opt = getopt(argc, argv, "ghlsmp:r:")) != -1) {
    switch (opt) {
      case 'm':
        parallel = true;
#ifdef USE_MPI
        master = true;
#endif
        break;
      case 's':
        parallel = true;
#ifdef USE_MPI
        master = false;
#endif
        break;
      case 'g': debug = true; break;
      case 'l': list = true; break;
      case 'p': printdesc = optarg; break;
      case 'r': strat = optarg; break;
      case 'h': usage(argc, argv); break;
      default: usage(argc, argv); break;
    }
  }

  StarPart_Context* ctx = NULL;
#ifdef USE_MPI
  MPI_Comm intercomm;
  if (parallel) {
    // MPI_Init(NULL, NULL);
    int required = MPI_THREAD_MULTIPLE;
    int provided = 0;
    MPI_Init_thread(NULL, NULL, required, &provided);
    assert(provided == required);

    if (!master) {
      MPI_Comm_get_parent(&intercomm);
      assert(intercomm != MPI_COMM_NULL);
    }
    ctx = StarPart_newParallelContext(MPI_COMM_WORLD);
  }
#endif

  if (!parallel) ctx = StarPart_newContext();
  assert(ctx);
  StarPart_setDebug(ctx, debug);
  StarPart_registerAllMethods(ctx);

  if (list)
    StarPart_listAllMethods(ctx);
  else if (printdesc) {
    StarPart_Method* m = StarPart_getMethod(ctx, printdesc);
    if (m) StarPart_printMethodDesc(m);
    // else StarPart_error("package/method \"%s\" not found!\n", printdesc);
  }
  else if (strat) {
    StarPart_parseStrat(ctx, strat);
    StarPart_submitAllTasks(ctx);
    StarPart_connectAllTasks(ctx, NULL, NULL);
    StarPart_run(ctx);
  }
  else {
    usage(argc, argv);
  }

  StarPart_freeAllTasks(ctx);
  StarPart_freeContext(ctx);

#ifdef USE_MPI
  if (parallel) {
    if (!master) MPI_Barrier(intercomm);
    MPI_Finalize();
  }
#endif

  return EXIT_SUCCESS;
}
