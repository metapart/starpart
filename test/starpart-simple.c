/* sample 2 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "all.h"
#include "starpart.h"

/* *********************************************************** */

int main(int argc, char** argv)
{
  const bool debug = true;
  const bool autoconnect = true;

  StarPart_Context* ctx = StarPart_newContext();
  StarPart_setDebug(ctx, debug);
  StarPart_registerAllMethods(ctx);

  StarPart_Task* grid = StarPart_newTaskByName(ctx, "BASIC/GRID", NULL);
  StarPart_Task* random = StarPart_newTaskByName(ctx, "BASIC/RANDOM", NULL);
  StarPart_Task* diag = StarPart_newTaskByName(ctx, "BASIC/DIAG", NULL);
  StarPart_Task* vtk = StarPart_newTaskByName(ctx, "BASIC/VTK", NULL);
  StarPart_insertTaskArgs(random, "nparts=4, seed=0");
  StarPart_insertTaskArgs(vtk, "filename=simple.vtk");

  StarPart_submitAllTasks(ctx);

  if (!autoconnect) {
    StarPart_connectTask(grid, "#out", "@0");   // OUT
    StarPart_connectTask(random, "#in", "@0");  // INOUT
    StarPart_connectTask(diag, "#in", "@0");    // IN
    StarPart_connectTask(vtk, "#in", "@0");     // IN
  }
  else {
    StarPart_connectAllTasks(ctx, NULL, NULL);
  }
  // exec all tasks in queue
  StarPart_run(ctx);

  StarPart_freeAllTasks(ctx);
  StarPart_freeContext(ctx);

  return 0;
}
/* *********************************************************** */
