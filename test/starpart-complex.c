/* sample 3 */

#include <stdbool.h>
#include <stdio.h>
#include "all.h"
#include "starpart.h"

/* *********************************************************** */

int main(int argc, char** argv)
{
  const bool debug = true;
  const bool autoconnect = false;

  StarPart_Context* ctx = StarPart_newContext();
  StarPart_setDebug(ctx, debug);
  StarPart_registerAllMethods(ctx);

  StarPart_Task* grid = StarPart_newTaskByName(ctx, "BASIC/GRID", NULL);
  StarPart_Task* random1 = StarPart_newTaskByName(ctx, "BASIC/RANDOM", "nparts=4, seed=-2");
  StarPart_Task* diag1 = StarPart_newTaskByName(ctx, "BASIC/DIAG", NULL);
  StarPart_Task* dup = StarPart_newTaskByName(ctx, "BASIC/DUP", NULL);
  StarPart_Task* random2 = StarPart_newTaskByName(ctx, "BASIC/RANDOM", "nparts=4, seed=-2");
  StarPart_Task* diag2 = StarPart_newTaskByName(ctx, "BASIC/DIAG", NULL);
  StarPart_Task* cmp = StarPart_newTaskByName(ctx, "BASIC/CMP", NULL);
  StarPart_Task* diag3 = StarPart_newTaskByName(ctx, "BASIC/DIAG", NULL);

  StarPart_submitAllTasks(ctx);

  if (!autoconnect) {
    StarPart_connectTask(grid, "#out", "@1");    // OUT
    StarPart_connectTask(dup, "#in", "@1");      // IN
    StarPart_connectTask(dup, "#out", "@2");     // OUT
    StarPart_connectTask(random1, "#in", "@1");  // INOUT
    StarPart_connectTask(random2, "#in", "@2");  // INOUT
    StarPart_connectTask(diag1, "#in", "@1");    // IN
    StarPart_connectTask(diag2, "#in", "@2");    // IN
    StarPart_connectTask(cmp, "#in0", "@1");     // IN0
    StarPart_connectTask(cmp, "#in1", "@2");     // IN1
    StarPart_connectTask(cmp, "#out", "@3");     // OUT
    StarPart_connectTask(diag3, "#in", "@3");    // IN
  }
  else {
    StarPart_connectTask(grid, "#out", "@1");  // OUT
    StarPart_connectTask(dup, "#out", "@2");   // OUT
    StarPart_connectTask(cmp, "#in0", "@1");   // IN0
    StarPart_connectTask(cmp, "#in1", "@2");   // IN1
    // StarPart_connectTask(cmp, "#out", "@3"); // OUT
    StarPart_connectAllTasks(ctx, NULL, NULL);
  }

  // exec all tasks in queue
  StarPart_run(ctx);

  StarPart_freeAllTasks(ctx);
  StarPart_freeContext(ctx);

  return 0;
}

/* *********************************************************** */
