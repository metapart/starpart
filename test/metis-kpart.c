#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "libgraph.h"
#include "metis.h"

#define DIM 100

int main(int argc, char** argv)
{
  assert(sizeof(idx_t) == sizeof(int32_t));
  assert(sizeof(real_t) == sizeof(float));

  int nparts = 4;
  if (argc == 2) nparts = atoi(argv[1]);
  int ubfactor = 5;
  assert(nparts > 0);

  // generate a graph in Metis format using LibGraph
  LibGraph_Mesh m;
  LibGraph_generateGrid2D(DIM, DIM, 0, 1.0, 1.0, &m);
  LibGraph_Graph g;
  LibGraph_mesh2Graph(&m, &g);


  int ncon = 1;  // single constraint
  real_t ub = 1.0 + ubfactor / 100.0;
  int edgecut;
  int* part = malloc(g.nvtxs * sizeof(int));
  assert(part);

  idx_t options[METIS_NOPTIONS];
  METIS_SetDefaultOptions(options);

  METIS_PartGraphKway((idx_t*)&g.nvtxs, (idx_t*)&ncon, (idx_t*)g.xadj, (idx_t*)g.adjncy, (idx_t*)g.vwgts, NULL,
                      (idx_t*)g.ewgts, (idx_t*)&(nparts), NULL, &ub, options, &edgecut, (idx_t*)part);

  printf("Metis partition of a grid %dx%d in %d parts (ub=%d%%): edgecut=%d\n", DIM, DIM, nparts, ubfactor, edgecut);

  LibGraph_freeMesh(&m);
  LibGraph_freeGraph(&g);

  return 0;
}