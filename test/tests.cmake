### BASIC ###
if(USE_BASIC)
  add_test(${testname_prefix}-nop ${test_runner} -g -r "BASIC/NOP")
  add_test(${testname_prefix}-grid ${test_runner} -g -r "BASIC/GRID;BASIC/CHECKITEMS[items=(graph,mesh),strict=1]")
  add_test(${testname_prefix}-grid-coords ${test_runner} -g -r "BASIC/GRID[gcoords=1];BASIC/CHECKITEMS[items=(graph,mesh,coords),strict=1]")
  add_test(${testname_prefix}-grid-grid ${test_runner} -g -r "BASIC/GRID;BASIC/GRID")
  add_test(${testname_prefix}-grid-random-diag ${test_runner} -g -r "BASIC/GRID;BASIC/RANDOM[nparts=10];BASIC/DIAG;BASIC/CHECK")
  add_test(${testname_prefix}-grid-random-vtk ${test_runner} -g -r "BASIC/GRID;BASIC/RANDOM[nparts=4,store];BASIC/VTK[filename=random.vtk]")
  add_test(${testname_prefix}-grid-bfs-vtk ${test_runner} -g -r "BASIC/GRID;BASIC/BFS[store];BASIC/VTK[filename=bfs.vtk]")
  add_test(${testname_prefix}-two-data ${test_runner} -g -r "BASIC/GRID[#out>@0];BASIC/RANDOM[nparts=2];BASIC/GRID[#out>@1];BASIC/RANDOM[nparts=4];BASIC/DIAG[#in<@0];BASIC/DIAG[#in<@1]")
  add_test(${testname_prefix}-split ${test_runner} -g -r "BASIC/GRID[#out>@0];BASIC/SPLIT[#in<@0,#out0>@1,#out1>@2];BASIC/VTK[#in<@1,filename=left.vtk];BASIC/VTK[#in<@2,filename=right.vtk]")
  add_test(${testname_prefix}-dup ${test_runner} -g -r "BASIC/GRID[#out>@0,gcoords=1];BASIC/DUP[#out>@1];BASIC/EQUAL[#in0<@0,#in1<@1,strict=1]")
  add_test(${testname_prefix}-nested-nop ${test_runner} -g -r "BASIC/DATA;BASIC/NESTED[strat={BASIC/NOP}]")
  add_test(${testname_prefix}-nested-random ${test_runner} -g -r "BASIC/GRID;BASIC/NESTED[strat={BASIC/RANDOM}];BASIC/DIAG")
  add_test(${testname_prefix}-nested-nested-nop ${test_runner} -g -r "BASIC/DATA;BASIC/NESTED[strat={BASIC/NESTED[strat={BASIC/NOP}]}]")
  add_test(${testname_prefix}-loop-nop ${test_runner} -g -r "BASIC/DATA;BASIC/LOOP[first=1,last=10,strat={BASIC/NOP}]")
  add_test(${testname_prefix}-loop-random ${test_runner} -g -r "BASIC/GRID;BASIC/LOOP[first=1,last=10,strat={BASIC/RANDOM[nparts=4];BASIC/DIAG}]")
  add_test(${testname_prefix}-grid-part-random ${test_runner} -g -r "BASIC/GRID;BASIC/PART[strat={BASIC/RANDOM}];BASIC/DIAG;BASIC/CHECK")
  add_test(${testname_prefix}-bench ${test_runner} -g -r "BASIC/GRID;BASIC/SAVE[filename=grid.graph,format=metis];BASIC/BENCH[files=(grid.graph),nparts=(2,4,8,16),strats=(BASIC/RANDOM,BASIC/BFS),seeds=(1,2,3,4,5),ubfactors=(5)]")
  add_test(${testname_prefix}-grid-coarsen ${test_runner} -g -r "BASIC/GRID;BASIC/COARSEN;BASIC/VTK[filename=coarsen.vtk]")
  add_test(${testname_prefix}-grid-coarsen-coarsen ${test_runner} -g -r "BASIC/GRID;BASIC/COARSEN;BASIC/COARSEN;BASIC/VTK[filename=coarsen2.vtk]")
  add_test(${testname_prefix}-grid-bfs-coarsen-remap ${test_runner} -g -r "BASIC/GRID[#out>@0];BASIC/BFS;BASIC/COARSEN[#out>@1];BASIC/REMAP[#in0<@0,#in1<@1,store];BASIC/VTK[#in<@0,filename=remap0.vtk];BASIC/VTK[#in<@1,filename=remap1.vtk];BASIC/CHECK[#in<@1]")
  add_test(${testname_prefix}-grid-coarsen-bfs-remap ${test_runner} -g -r "BASIC/GRID[#out>@0];BASIC/COARSEN[#out>@1];BASIC/BFS;BASIC/REMAP[#in0<@0,#in1<@1,inverse=1,store];BASIC/VTK[#in<@0,filename=invmap0.vtk];BASIC/VTK[#in<@1,filename=invmap1.vtk];BASIC/CHECK[#in<@0]")
  add_test(${testname_prefix}-grid-ml-random ${test_runner} -g -r "BASIC/GRID;BASIC/ML[nparts=10,pstrat={BASIC/RANDOM},cstrat={BASIC/COARSEN},rstrat={BASIC/KFM}];BASIC/DIAG;BASIC/CHECK")
  add_test(${testname_prefix}-grid-random-kfm ${test_runner} -g -r "BASIC/GRID;BASIC/RANDOM[nparts=10,store];BASIC/KFM;BASIC/DIAG;BASIC/CHECK;BASIC/VTK")
  add_test(${testname_prefix}-grid-fixed-hun-random ${test_runner} -g -r "BASIC/GRID;BASIC/FIXED[scheme=bubble,nparts=5];BASIC/HUN[pstrat={BASIC/RANDOM}];BASIC/DIAG;BASIC/CHECK")
  add_test(${testname_prefix}-grid-fixed-coarsen-bfs ${test_runner} -g -r "BASIC/GRID;BASIC/FIXED[nparts=2];BASIC/COARSEN;BASIC/BFS[store];BASIC/DIAG;BASIC/CHECK;BASIC/VTK")
endif()

# misc
# add_test(${testname_prefix}-forall-part ${test_runner} -g -r "BASIC/GRID[#out>@0];BASIC/GRID[#out>@1];BASIC/FORALL[strat={BASIC/RANDOM[#in<@0,nparts=2];BASIC/DIAG[#in<@0]}]")
# add_test(${testname_prefix}-forall-vtk ${test_runner} -g -r "GRID[@data=a];GRID[@data=b];FORALL[strat={RANDOM[nparts=4,#store]}];VTK[@data=a,filename=a.vtk];VTK[@data=b,filename=b.vtk]")
# add_test(${testname_prefix}-intarray echo "0 1 2 3 4 5 6 7 8 9" >> pouet.txt && ${test_runner} -g -r "INTARRAY[filename=pouet.txt];PRINT[var=array,dump=1]")
# add_test(${testname_prefix}-load-mm ${test_runner} -g -r "LOAD[filename=qa8fm.mtx,coords=qa8fm.xyz,format=mm];RANDOM[nparts=4];BASIC/DIAG;BASIC/CHECK")


if(USE_BASIC AND USE_MPI)
  add_test(${testname_prefix}-parallel-context-parallel-strat mpirun -np 2 ${test_runner} -g -m -r "BASIC/GRID;BASIC/BFS;BASIC/DIAG")
  add_test(${testname_prefix}-parallel-context-sequential-strat mpirun -np 2 ${test_runner} -g -m -r "!BASIC/GRID;!BASIC/BFS;!BASIC/DIAG")
  add_test(${testname_prefix}-mpispawn mpirun --oversubscribe -np 1 ${test_runner} -g -m -r "BASIC/MPISPAWN[np=2,strat={BASIC/GRID;BASIC/BFS[nparts=4];BASIC/DIAG}]")
  add_test(${testname_prefix}-grid-scatter-gather mpirun --oversubscribe -np 4 ${test_runner} -g -m -r "!BASIC/GRID[#out>@0];BASIC/SCATTER;BASIC/GATHER[#out>@1];!BASIC/EQUAL[#in0<@0,#in1<@1]")

  add_test(${testname_prefix}-grid-redist1 mpirun --oversubscribe -np 4 ${test_runner} -g -m -r "!BASIC/GRID[#out>@0];BASIC/SCATTER[#out>@1];BASIC/RANDOMP;BASIC/REDIST;BASIC/GATHER[#out>@2];BASIC/GATHERP[#in0<@1,#out>@3];!BASIC/PERMUT[#in0<@0,#in1<@3];!BASIC/EQUAL[#in0<@2,#in1<@0]")
  add_test(${testname_prefix}-grid-redist2 mpirun --oversubscribe -np 4 ${test_runner} -g -m -r "!BASIC/GRID[#out>@0]; BASIC/SCATTER; BASIC/RANDOMP; BASIC/REDIST; BASIC/REDIST[inverse=1]; BASIC/GATHER[#out>@1]; !BASIC/EQUAL[#in0<@0, #in1<@1]")
  add_test(${testname_prefix}-grid-redist3 mpirun --oversubscribe -np 4 ${test_runner} -g -m -r "!BASIC/GRID[#out>@0]; BASIC/SCATTER; BASIC/RANDOMP; BASIC/REDIST; BASIC/GATHER[#out>@1]; !BASIC/PERMUT[#in0<@1, #in1<@1, inverse=1]; !BASIC/EQUAL[#in0<@0, #in1<@1]")
  add_test(${testname_prefix}-grid-redist4 mpirun --oversubscribe -np 4 ${test_runner} -g -m -r "!BASIC/GRID[#out>@0]; BASIC/GRAPHTODISTGRAPH; BASIC/RANDOMP; BASIC/REDIST; BASIC/GATHER[#out>@1]; !BASIC/PERMUT[#in0<@1, #in1<@1, inverse=1]; !BASIC/EQUAL[#in0<@0, #in1<@1]")

  add_test(${testname_prefix}-loadp mpirun --oversubscribe -np 4 ${test_runner} -g -m -r "!BASIC/GRID[#out>@0]; !BASIC/SAVE[filename=grid.graph,format=metis]; BASIC/BARRIER; BASIC/LOADP[filename=grid.graph,format=metis]; BASIC/GATHER[#out>@1]; !BASIC/EQUAL[#in0<@0, #in1<@1]")
  add_test(${testname_prefix}-loadpw mpirun --oversubscribe -np 4 ${test_runner} -g -m -r "!BASIC/GRID[#out>@0]; !BASIC/WEIGHT[type=both,scheme=linear]; !BASIC/SAVE[filename=gridw.graph,format=metis]; BASIC/BARRIER; BASIC/LOADP[filename=gridw.graph,format=metis]; BASIC/GATHER[#out>@1]; !BASIC/EQUAL[#in0<@0, #in1<@1]")
endif()

### KGGGP ###if(!local) { StarPart_warning("Cannot execute tags for task %s, because no local data!\n", task->id); return; }

if(USE_BASIC AND USE_KGGGP)
  add_test(${testname_prefix}-grid-kgggp-g ${test_runner} -g -r "BASIC/GRID;KGGGP/PART[greedy=global,nparts=10];BASIC/DIAG;BASIC/CHECK")
  add_test(${testname_prefix}-grid-kgggp-l ${test_runner} -g -r "BASIC/GRID;KGGGP/PART[greedy=local,nparts=10];BASIC/DIAG;BASIC/CHECK")
  add_test(${testname_prefix}-grid-fixed-kgggp-l ${test_runner} -g -r "BASIC/GRID;BASIC/FIXED[scheme=bubble,nparts=4];KGGGP/PART[greedy=local,store];BASIC/DIAG;BASIC/CHECK;BASIC/VTK[filename=kgggp.vtk]")
  add_test(${testname_prefix}-grid-ml-kgggp-l ${test_runner} -g -r "BASIC/GRID;BASIC/ML[nparts=10,pstrat={KGGGP/PART},ninitpass=10];BASIC/DIAG;BASIC/CHECK")
  add_test(${testname_prefix}-grid-fixed-ml-kgggp-l ${test_runner} -g -r "BASIC/GRID;BASIC/FIXED[scheme=bubble,nparts=4];BASIC/ML[pstrat={KGGGP/PART[greedy=local]},ninitpass=10];BASIC/DIAG;BASIC/CHECK")
endif()

### METIS ###

if(USE_BASIC AND USE_METIS)
  add_test(${testname_prefix}-grid-rmetis ${test_runner} -g -r "BASIC/GRID;METIS/RPART[nparts=10];BASIC/DIAG;BASIC/CHECK")
  add_test(${testname_prefix}-grid-kmetis ${test_runner} -g -r "BASIC/GRID;METIS/KPART[nparts=10];BASIC/DIAG;BASIC/CHECK")
  add_test(${testname_prefix}-grid-ometis ${test_runner} -g -r "BASIC/GRID;METIS/ORDER")
  add_test(${testname_prefix}-grid-part-kmetis-vtk ${test_runner} -g -r "BASIC/GRID;BASIC/PART[nparts=4,strat={METIS/KPART},store];BASIC/VTK")
  add_test(${testname_prefix}-grid-kmetis-coarsen-remap ${test_runner} -g -r "BASIC/GRID[#out>@0];METIS/KPART[nparts=2];BASIC/COARSEN[#out>@1];BASIC/REMAP[#in0<@0,#in1<@1,store];BASIC/VTK")
  # add_test(${testname_prefix}-grid-coarsen-remap-vtk ${test_runner} -g -r "BASIC/GRID[#out>@0];METIS/KPART[nparts=2,#store];BASIC/COARSEN[#out>@1];BASIC/REMAP[#in<@0,#out>@1,#store];BASIC/VTK[#in<@0,filename=a.vtk];BASIC/VTK[#in<@1,filename=b.vtk]")
  # add_test(${testname_prefix}-grid-coarsen-remap-inverse ${test_runner} -g -r "BASIC/GRID[@data=a];COARSEN[@out=b];METIS/KPART[@data=b,data/nparts=2];REMAP[@in=b,@out=a,in/inverse=1]")
  # add_test(${testname_prefix}-grid-part-fixed-hun-kmetis ${test_runner} -g -r "BASIC/GRID;BASIC/FIXED[scheme=bubble,nparts=10];BASIC/HUN[pstrat={METIS/KPART}];BASIC/DIAG;BASIC/CHECK")
endif()

### MT-METIS ###

if(USE_BASIC AND USE_MTMETIS)
  add_test(${testname_prefix}-grid-mtmetis ${test_runner} -g -r "BASIC/GRID;MTMETIS/PART[nparts=10,nthreads=4];BASIC/DIAG;BASIC/CHECK")
endif()

  ### SCOTCH ###

if(USE_BASIC AND (USE_SCOTCH OR USE_PTSCOTCH))
  add_test(${testname_prefix}-grid-scotch-default ${test_runner} -g -r "BASIC/GRID;SCOTCH/DEFAULT[nparts=10];BASIC/DIAG;BASIC/CHECK")
  add_test(${testname_prefix}-grid-scotch-rb ${test_runner} -g -r "BASIC/GRID;SCOTCH/RB[nparts=10];BASIC/DIAG;BASIC/CHECK")
  add_test(${testname_prefix}-grid-scotch-kpart ${test_runner} -g -r "BASIC/GRID;SCOTCH/KPART[nparts=10];BASIC/DIAG;BASIC/CHECK")
  add_test(${testname_prefix}-grid-scotch-order ${test_runner} -g -r "BASIC/GRID;SCOTCH/ORDER")
  add_test(${testname_prefix}-grid-scotch-order-cpr ${test_runner} -g -r "BASIC/GRID;SCOTCH/ORDER[cprratio=70]")
  add_test(${testname_prefix}-grid-scotch-order-singlesep ${test_runner} -g -r "BASIC/GRID;SCOTCH/ORDER[maxlvl=1,leaf=simple,sepbal=5]")
  add_test(${testname_prefix}-grid-ml-scotch-rb ${test_runner} -g -r "BASIC/GRID;BASIC/ML[nparts=10,pstrat={SCOTCH/RB},rstrat={SCOTCH/KFM}];BASIC/DIAG;BASIC/CHECK")
  if(USE_SCOTCH_ML)
    add_test(${testname_prefix}-grid-scotch-ml-scotch-rb ${test_runner} -g -r "BASIC/GRID;SCOTCH/ML[nparts=10,pstrat={SCOTCH/RB}];BASIC/DIAG;BASIC/CHECK")
  endif(USE_SCOTCH_ML)
endif()

### PARMETIS ###

if(USE_BASIC AND USE_MPI AND USE_PARMETIS)
  add_test(${testname_prefix}-grid-parmetis-2 mpirun --oversubscribe -np 2 ${test_runner} -g -m -r "!BASIC/GRID[>@0];BASIC/SCATTER;PARMETIS/PART[nparts=4];BASIC/GATHERP[1<@0];!BASIC/DIAG")
  add_test(${testname_prefix}-grid-parmetis-4 mpirun --oversubscribe -np 4 ${test_runner} -g -m -r "!BASIC/GRID[>@0];BASIC/SCATTER;PARMETIS/PART[nparts=4];BASIC/GATHERP[1<@0];!BASIC/DIAG")
  add_test(${testname_prefix}-grid-parmetis-redist mpirun --oversubscribe -np 4 ${test_runner} -g -m -r "!BASIC/GRID[>@0];BASIC/SCATTER;PARMETIS/PART[nparts=4];BASIC/REDIST")

  if(NOT USE_METIS)
  add_test(${testname_prefix}-grid-kmetis ${test_runner} -g -r "BASIC/GRID;METIS/KPART[nparts=10];BASIC/DIAG;BASIC/CHECK")
  endif()
endif()

### PTSCOTCH ###

if(USE_BASIC AND USE_MPI AND USE_PTSCOTCH)
  add_test(${testname_prefix}-grid-ptscotch-2 mpirun --oversubscribe -np 2 ${test_runner} -g -m -r "!BASIC/GRID[>@0];BASIC/SCATTER;PTSCOTCH/PART[nparts=4];BASIC/GATHERP[1<@0];!BASIC/DIAG")
  add_test(${testname_prefix}-grid-ptscotch-4 mpirun --oversubscribe -np 4 ${test_runner} -g -m -r "!BASIC/GRID[>@0];BASIC/SCATTER;PTSCOTCH/PART[nparts=4];BASIC/GATHERP[1<@0];!BASIC/DIAG")
endif()


### ZOLTAN ###

if(USE_BASIC AND USE_ZOLTAN)
  if(USE_MPI)
    # add_test(${testname_prefix}-grid-zoltan-mpi mpirun -np 2 --oversubscribe ${test_runner} -m -r "BASIC/GRID[dimx=4,dimy=4];BASIC/G2HG;ZOLTAN/PART[nparts=2];!BASIC/DIAG;!BASIC/CHECK")
    add_test(${testname_prefix}-grid-zoltan-mpi mpirun -np 2 --oversubscribe ${test_runner} -m -r "BASIC/GRID[dimx=4,dimy=4];BASIC/G2HG;ZOLTAN/PART[nparts=2]")
  else()
    add_test(${testname_prefix}-grid-zoltan ${test_runner} -g -r "BASIC/GRID;BASIC/G2HG;ZOLTAN/PART[nparts=10];BASIC/DIAG;BASIC/CHECK")
  endif()
endif()

### PATOH ###

if(USE_BASIC AND USE_PATOH)
  add_test(${testname_prefix}-grid-patoh ${test_runner} -g -r "BASIC/GRID;BASIC/G2HG;PATOH/PART[nparts=10];BASIC/DIAG;BASIC/CHECK")
endif()

### KAHIP ###

if(USE_BASIC AND USE_KAHIP)
  add_test(${testname_prefix}-grid-kahip ${test_runner} -g -r "BASIC/GRID;KAHIP/PART[nparts=10];BASIC/DIAG;BASIC/CHECK")
endif()

### MONDRIAAN ###

if(USE_BASIC AND USE_MONDRIAAN)
  add_test(${testname_prefix}-grid-mondriaan ${test_runner} -g -r "BASIC/GRID;BASIC/G2HG;MONDRIAAN/PART[nparts=10];BASIC/DIAG;BASIC/CHECK")
endif()

### PULP ###

if(USE_BASIC AND USE_PULP)
  add_test(${testname_prefix}-grid-pulp ${test_runner} -g -r "BASIC/GRID;PULP/PART[nparts=10];BASIC/DIAG;BASIC/CHECK")
endif()

### Mixing SCOTCH & KGGGP ###

if(USE_BASIC AND USE_KGGGP AND USE_SCOTCH_ML)
  add_test(${testname_prefix}-grid-scotch-ml-kgggp-l ${test_runner} -g -r "BASIC/GRID;SCOTCH/ML[nparts=10,pstrat=KGGGP/PART];BASIC/DIAG;BASIC/CHECK")
  add_test(${testname_prefix}-grid-fixed-scotch-ml-hun-scotch-rb ${test_runner} -g -r "BASIC/GRID;BASIC/FIXED[nparts=10,scheme=bubble];SCOTCH/ML[pstrat={BASIC/HUN[pstrat={SCOTCH/RB}]}];BASIC/DIAG;BASIC/CHECK")
  add_test(${testname_prefix}-grid-fixed-scotch-ml-kgggp ${test_runner} -g -r "BASIC/GRID;BASIC/FIXED[nparts=10,scheme=bubble];SCOTCH/ML[pstrat=KGGGP/PART];BASIC/DIAG;BASIC/CHECK")
endif()

### TODO ###

# if(LOAD_DATA)
# add_test(${testname_prefix}-load-classic ${test_runner} -g -r "LOAD[filename=cylinder.avbp.part1.txt,format=mesh]")
# add_test(${testname_prefix}-hload-single ${test_runner} -g -r "HLOAD[filename1=cylinder.avbp.part1.txt,format=mesh,nb_components=1]")
# add_test(${testname_prefix}-hload ${test_runner} -g -r "HLOAD[filename1=cylinder.avbp.part1.txt,filename2=cylinder.avbp.part2.txt,nb_components=2,format=mesh]")
# add_test(${testname_prefix}-hload-part ${test_runner} -g -r "HLOAD[filename1=cylinder.avbp.part1.txt,filename2=cylinder.avbp.part2.txt,format=mesh,nb_components=2];RANDOM[nparts=4];BASIC/DIAG;BASIC/CHECK")
# add_test(${testname_prefix}-hload-part-vtk ${test_runner} -g -r "HLOAD[filename1=cylinder.avbp.part1.txt,filename2=cylinder.avbp.part2.txt,format=mesh,nb_components=2];KMETIS[nparts=4,#store];BASIC/DIAG;BASIC/CHECK;HVTK[avbp.vtk]")
# endif(LOAD_DATA)
