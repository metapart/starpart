/**
 *
 * @file starpart.h
 *
 * @copyright 2017 Univ. Bordeaux, CNRS (LaBRI UMR 5800), Inria. All rights
 * reserved.
 *
 * @author Aurelien Esnard
 *
 * @brief The public StarPart programming interface.
 *
 */

#ifndef STARPART_H
#define STARPART_H

#include <glib.h>
#include <stdarg.h>
#include <stdbool.h>
#include "defs.h"
#include "libgraph.h"

/**
 *
 * @defgroup api StarPart API
 * @{
 * @brief The public StarPart programming interface.
 */

/* *********************************************************** */
/*                   MPI DECLARATION                           */
/* *********************************************************** */

#ifdef USE_MPI
#include <mpi.h>
#else
typedef int MPI_Comm;
#define MPI_COMM_NULL 0
#endif

/* *********************************************************** */
/*                     STARPART STRUCT                         */
/* *********************************************************** */

#ifndef STARPART_STRUCT_H

/**
 * @brief context structure
 * @details The \ref StarPart_Context type represents the context into which any
 * StarPart data & methods are registred and processed.
 */

typedef struct _StarPart_Context StarPart_Context;

/**
 * @brief data table
 * @details A \ref StarPart_DataTable is a hash table of \ref StarPart_Data.
 */

typedef GHashTable StarPart_DataTable;

/**
 * @brief data structure
 * @details A \ref StarPart_Data is a collection of \ref StarPart_Item.
 */

typedef struct _StarPart_Data StarPart_Data;

/**
 * @brief item structure
 * @details A \ref StarPart_Item is the basic data type used to represent
 * integer, arrays, string, graphs or meshes.
 */

typedef struct _StarPart_Item StarPart_Item;

/**
 * @brief method structure
 */

typedef struct _StarPart_Method StarPart_Method;

/**
 * @brief task structure
 */

typedef struct _StarPart_Task StarPart_Task;

/**
 * @brief hook structure
 */

typedef struct _StarPart_Hook StarPart_Hook;

#endif

/* *********************************************************** */
/*                     STARPART CALLBACK                       */
/* *********************************************************** */

/**
 * @brief method init function type
 */

typedef void (*StarPart_MethodInitFunc)(StarPart_Method* method);

/**
 * @brief method call function type
 */

typedef void (*StarPart_MethodCallFunc)(StarPart_Task* task);

/**
 * @brief hook pre- & post-processing function type
 */

typedef void (*StarPart_HookFunc)(StarPart_Task* task);

/**
 * @brief generic exec function type used to process strategy
 */

typedef void (*StarPart_ExecFunc)(GNode* node, void* arg);

/* *********************************************************** */
/*                      STARPART ENUM                          */
/* *********************************************************** */

/**
 * @brief special flags enum for \ref StarPart_Data used as argument
 */

typedef enum _StaPart_DataFlags
{
  STARPART_IN = 1,                             /**< arg in */
  STARPART_OUT = 2,                            /**< arg out */
  STARPART_INOUT = STARPART_IN | STARPART_OUT, /**< arg inout */
  STARPART_LOCAL = 4,                          /**< arg local */
  STARPART_MASTER = 8                          /**< arg only on the master process (rank 0) of a parallel task */

} StarPart_DataFlags;

/**
 * @brief special flags enum for \ref StarPart_Item
 */

typedef enum _StaPart_ItemFlags
{
  STARPART_ALL = 0,           /**< enable all access flags (default) */
  STARPART_READ = 1,          /**< enable to read item */
  STARPART_WRITE = 2,         /**< enable to write item */
  STARPART_READWRITE = 3,     /**< enable both read & write flags */
  STARPART_REPLACE = 4,       /**< if item exists, enable to replace it (involve read & write flags) */
  STARPART_CREATE = 5,        /**< if item does not exist, enable to create it (involve read & write flags) */
  STARPART_CREATEREPLACE = 6, /**< enable both create & replace flags */

} StarPart_ItemFlags;

/**
 * @brief basic type enum
 */

typedef enum _StaPart_Type
{
  STARPART_VOID = 0,    /**< void type */
  STARPART_STRAT,       /**< strat string type */
  STARPART_INT,         /**< integer type */
  STARPART_FLOAT,       /**< float type */
  STARPART_DOUBLE,      /**< double type */
  STARPART_STR,         /**< string type */
  STARPART_INTARRAY,    /**< integer array type */
  STARPART_FLOATARRAY,  /**< float array type */
  STARPART_DOUBLEARRAY, /**< double array type */
  STARPART_STRARRAY,    /**< string array type */
  STARPART_GRAPH,       /**< graph type */
  STARPART_DISTGRAPH,   /**< distributed graph type */
  STARPART_HYPERGRAPH,  /**< hypergraph type */
  STARPART_MESH,        /**< mesh type */
  STARPART_HYBRIDMESH   /**< hybrid mesh type */
} StarPart_Type;

/* *********************************************************** */
/*                       GLOBAL                                */
/* *********************************************************** */

/**
 * @name Global Functions
 * @{
 */

/**
 * @brief get the current \ref StarPart_Context
 * @details The current context is the one on top of the context stack.
 * @return a new \ref StarPart_Context
 */

StarPart_Context* StarPart_getCurrentContext(void);

/**
 * @brief get the context stack
 * @return a \ref GQueue
 */

GQueue* StarPart_getContextStack(void);

/**@}*/

/* *********************************************************** */
/*                       CONTEXT                               */
/* *********************************************************** */

/**
 * @name Context Functions
 * @{
 */

/**
 * @brief create a new \ref StarPart_Context
 * @return a new \ref StarPart_Context
 */

StarPart_Context* StarPart_newContext(void);

/**
 * @brief free a \ref StarPart_Context
 * @param ctx the context to be freed
 */

void StarPart_freeContext(StarPart_Context* ctx);

/**
 * @brief create a new nested context
 * @details The new created context is pushed on top of a stack and becomes the
 * new current context.
 * @param ctx the parent content
 * @return a new \ref StarPart_Context
 */

StarPart_Context* StarPart_newNestedContext(StarPart_Context* ctx);

/**
 * @brief free a nested context
 * @details This context is pop from the context stack.
 * @param ctx the context to be freed
 */

void StarPart_freeNestedContext(StarPart_Context* ctx);

/**
 * @brief get all data
 * @param ctx the context
 * @return a \ref StarPart_DataTable of char* key and \ref StarPart_Data value
 */

StarPart_DataTable* StarPart_getDataTable(StarPart_Context* ctx);

/**
 * @brief create a new \ref StarPart_Data and register it within a given context
 * @param ctx the context in which to register the data
 * @param data_id the data ID
 * @param flags data flags in \ref StarPart_DataFlags
 * @return a new \ref StarPart_Data
 */

StarPart_Data* StarPart_registerNewData(StarPart_Context* ctx, char* data_id, int flags);

/**
 * @brief register a \ref StarPart_Data within a given context
 * @param ctx the context in which to register the data
 * @param data a data to register
 * @return the registred data \ref StarPart_Data
 */

StarPart_Data* StarPart_registerData(StarPart_Context* ctx, StarPart_Data* data);

/**
 * @brief unregister a \ref StarPart_Data from a context
 * @details The unregistered data is removed from the context, but it is not
 * freed from memory.
 * @param ctx the context
 * @param data_id the data ID
 * @return the unregistered \ref StarPart_Data
 */

StarPart_Data* StarPart_unregisterData(StarPart_Context* ctx, char* data_id);

/**
 * @brief get a registered \ref StarPart_Data from a context
 * @param ctx the context
 * @param data_id the data ID
 * @return the \ref StarPart_Data found (or NULL if not found)
 */

StarPart_Data* StarPart_getData(StarPart_Context* ctx, char* data_id);

/**
 * @brief get all methods
 * @param ctx the context
 * @return a \ref GHashTable of char* key and \ref StarPart_Method value
 */

GHashTable* StarPart_getMethodTable(StarPart_Context* ctx);

/**
 * @brief create a new \ref StarPart_Method and register it in a context
 * @param ctx the context in which to register the method
 * @param id an ID of the form "package/method"
 * @param call function to call back each time the method is processed
 * @param init function to call back to initialize the method
 * @param desc method description
 * @return a new \ref StarPart_Method
 */

StarPart_Method* StarPart_registerNewMethod(StarPart_Context* ctx, char* id, StarPart_MethodCallFunc call,
                                            StarPart_MethodInitFunc init, char* desc);

/**
 * @brief unregister a \ref StarPart_Method from a context
 * @details The unregistered method is removed from the context, but it is not
 * freed from memory.
 * @param ctx the context from which to unregister a method
 * @param id the method ID
 * @return the unregistered \ref StarPart_Method
 */

StarPart_Method* StarPart_unregisterMethod(StarPart_Context* ctx, char* id);

/**
 * @brief lookup for a registered method
 * @param ctx the context from which to lookup a method
 * @param method_id the method ID to look up
 * @return the \ref StarPart_Method found (or NULL if not found)
 */

StarPart_Method* StarPart_getMethod(StarPart_Context* ctx, const char* method_id);

/**
 * @brief lookup for a registered method
 * @param ctx the context from which to lookup a method
 * @param method_package the method package where to look up
 * @param method_name the method name to look up in package
 * @return the \ref StarPart_Method found (or NULL if not found)
 */

StarPart_Method* StarPart_getMethodFromPackage(StarPart_Context* ctx, const char* method_package,
                                               const char* method_name);

/**
 * @brief get all hooks
 * @param ctx the context
 * @return a \ref GHashTable of char* key and \ref StarPart_Hook value
 */

GHashTable* StarPart_getHookTable(StarPart_Context* ctx);

StarPart_Hook* StarPart_registerNewHook(StarPart_Context* ctx, char* id, StarPart_HookFunc pre, StarPart_HookFunc post,
                                        char* desc);
StarPart_Hook* StarPart_unregisterHook(StarPart_Context* ctx, char* id);
StarPart_Hook* StarPart_getHook(StarPart_Context* ctx, const char* id);

// TASK

void StarPart_submitAllTasks(StarPart_Context* ctx);
void StarPart_cancelAllTasks(StarPart_Context* ctx);
void StarPart_freeAllTasks(StarPart_Context* ctx);

void StarPart_connectAllTasks(StarPart_Context* ctx, char* in, char* out);

StarPart_Task* StarPart_getHeadTask(StarPart_Context* ctx);
StarPart_Task* StarPart_getTailTask(StarPart_Context* ctx);

void StarPart_parseStrat(StarPart_Context* ctx, char* strat);
void StarPart_run(StarPart_Context* ctx);
StarPart_Task* StarPart_step(StarPart_Context* ctx);

// PARALLEL CONTEXT

StarPart_Context* StarPart_newParallelContext(MPI_Comm comm);
MPI_Comm StarPart_getParallelComm(StarPart_Context* ctx);
int StarPart_getParallelCommRank(StarPart_Context* ctx);
int StarPart_getParallelCommSize(StarPart_Context* ctx);
bool StarPart_isParallelContext(StarPart_Context* ctx);

// MISC

/**
 * @brief set debug mode
 */

void StarPart_setDebug(StarPart_Context* ctx, bool mode);

/**
 * @brief is debug mode enabled?
 */

bool StarPart_isDebug(StarPart_Context* ctx);

/**
 * @brief list all registred methods and print it on standard output
 * @param ctx the context
 */

void StarPart_listAllMethods(StarPart_Context* ctx);

/**
 * @brief list all packages
 */
void StarPart_listAllPackages(StarPart_Context* ctx);

void StarPart_listAllData(StarPart_Context* ctx);

void StarPart_printAllData(StarPart_Context* ctx);

/**@}*/

/* *********************************************************** */
/*                     DATA TABLE                              */
/* *********************************************************** */

/**
 * @name Data Table Functions
 * @{
 */

StarPart_DataTable* StarPart_newDataTable(void);
void StarPart_freeDataTable(StarPart_DataTable* table);
bool StarPart_isDataFromTable(StarPart_DataTable* table, char* data_id);
StarPart_Data* StarPart_getDataFromTable(StarPart_DataTable* table, char* data_id);
StarPart_Data* StarPart_addDataInTable(StarPart_DataTable* table, StarPart_Data* data);
StarPart_Data* StarPart_addNewDataInTable(StarPart_DataTable* table, char* data_id, int flags);
StarPart_Data* StarPart_removeDataFromTable(StarPart_DataTable* table, char* data_id);

/** @} */

/* *********************************************************** */
/*                        DATA                                 */
/* *********************************************************** */

/**
 * @name Data Functions
 * @{
 */

/**
 * @brief create a new empty \ref StarPart_Data without any items
 * @param data_id data ID
 * @param flags data flags in \ref StarPart_DataFlags
 * @return a new \ref StarPart_Data
 */

StarPart_Data* StarPart_newData(char* data_id, int flags);

/**
 * @brief duplicate data
 * @param data a \ref StarPart_Data to duplicate
 * @param deep
 *  - false, shallow-copy of data items
 *  - true, deep-copy of data items
 * @return a new \ref StarPart_Data
 */

StarPart_Data* StarPart_dupData(StarPart_Data* data, bool deep);

void StarPart_dupItems(StarPart_Data* src, StarPart_Data* dst, bool deep);

/**
 * @brief free a \ref StarPart_Data
 * @param data the data to be freed
 */

void StarPart_freeData(StarPart_Data* data);

/**
 * @brief get data ID
 * @param data a \ref StarPart_Data
 * @return the data ID
 */

char* StarPart_getDataID(StarPart_Data* data);

/**
 * @brief get data flags
 *
 * @param data a \ref StarPart_Data
 * @return the data flags (see \ref StarPart_DataFlags)
 */

int StarPart_getDataFlags(StarPart_Data* data);

/**
 * @brief get data items
 * @param data a \ref StarPart_Data
 * @return a \ref GHashTable of char * key and \ref StarPart_Item value
 */

GHashTable* StarPart_getDataItems(StarPart_Data* data);

/** add new data item  */
void StarPart_addItem(StarPart_Data* data, StarPart_Item* item);

/* remove data item and free it */
void StarPart_removeItem(StarPart_Data* data, char* item_id);

/** is data item */
bool StarPart_isItem(StarPart_Data* data, char* item_id);

/** has item value (not NULL) */
bool StarPart_hasItemValue(StarPart_Data* data, char* item_id);

/** get data item */
StarPart_Item* StarPart_getItem(StarPart_Data* data, char* item_id);

/**
 * @brief create a new \ref StarPart_Item and add it to a \ref StarPart_Data
 *
 * @param data the \ref StarPart_Data in which to add the new item
 * @param item_id the item ID
 * @param type the item data type
 * @param val the address to the default value (or NULL else)
 * @param owner
 *  - true, if StarPart takes the memory ownership of the item
 *  - false, if the user takes the memory ownership of the item
 * @details If owner is false, \ref StarPart_freeItem must be explicitly called
 * by user to free the item.
 * @param flags the item special flags (see \ref StarPart_ItemFlags)
 * @return the new created \ref StarPart_Item
 */

StarPart_Item* StarPart_addNewItem(StarPart_Data* data, char* item_id, StarPart_Type type, void* val, bool owner,
                                   int flags);

void StarPart_printItems(StarPart_Data* data, char* prefix);
void StarPart_printData(StarPart_Data* data);

/** @} */

/* *********************************************************** */
/*                         ITEM                                */
/* *********************************************************** */

/**
 * @name Item Functions
 * @{
 */

/**
 * @brief create a new data item
 * @details If owner is true, StarPart will take the memory ownership and will release memory automatically.
 */
StarPart_Item* StarPart_newItem(char* item_id, StarPart_Type type, void* val, bool owner, int flags);

StarPart_Item* StarPart_newItemFromStr(char* item_id, StarPart_Type type, char* strval, int flags);

/**
 * @brief free a \ref StarPart_Item
 * @param item the item to be freed
 */

void StarPart_freeItem(StarPart_Item* item);

/**
 * @brief get item ID
 * @param item a \ref StarPart_Item
 * @return the item ID
 */

char* StarPart_getItemID(StarPart_Item* item);

/**
 * @brief get item type
 * @param item a \ref StarPart_Item
 * @return the item type
 */

StarPart_Type StarPart_getItemType(StarPart_Item* item);

/**
 * @brief get item flags
 * @param item a \ref StarPart_Item
 * @return the item flags (see \ref StarPart_ItemFlags)
 */

int StarPart_getItemFlags(StarPart_Item* item);

/**
 * @brief get raw item address
 * @param item a \ref StarPart_Item
 * @return the pointer on raw item data in memory
 */

void* StarPart_getItemAddr(StarPart_Item* item);

/**
 * @brief set item flags
 * @param item a \ref StarPart_Item
 * @param flags the flags to bet set (see \ref StarPart_ItemFlags)
 */

void StarPart_setItemFlags(StarPart_Item* item, int flags);

void StarPart_setItemType(StarPart_Item* item, StarPart_Type type);

void StarPart_setItemID(StarPart_Item* item, char* item_id);

/**
 * @brief duplicate a data item
 * @details The item versions are not duplicated at all.
 * @param item a \ref StarPart_Item
 * @param deep
 *  - true, raw data memory is deeply copied
 *  - false, raw data memory is only wrapped
 * @return the duplicated item
 */

StarPart_Item* StarPart_dupItem(StarPart_Item* item, bool deep);

StarPart_Item* StarPart_getItemVersion(StarPart_Item* item, int version);
unsigned int StarPart_storeItemVersion(StarPart_Item* item);
unsigned int StarPart_getItemNbVersions(StarPart_Item* item);

void StarPart_printItem(StarPart_Item* item, bool dump, char* prefix);

/** @} */

/* *********************************************************** */
/*                  DATA ITEM SHORTCUTS                        */
/* *********************************************************** */

/**
 * @name Data Item Shortcuts
 * @{
 */

/**
 * @brief get the value of a string item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the item value (as a string)
 */

char* StarPart_getStr(StarPart_Data* data, char* item_id);

/**
 * @brief get the value of a strategy item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the item value (as a string)
 */

char* StarPart_getStrat(StarPart_Data* data, char* item_id);

/**
 * @brief get the value of an integer item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the item value
 */

int StarPart_getInt(StarPart_Data* data, char* item_id);

/**
 * @brief get the value of an float item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the item value
 */

float StarPart_getFloat(StarPart_Data* data, char* item_id);

/**
 * @brief get the value of an double item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the item value
 */

double StarPart_getDouble(StarPart_Data* data, char* item_id);

/**
 * @brief get the value of an array item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the item value
 */

LibGraph_Array* StarPart_getArray(StarPart_Data* data, char* item_id);

/**
 * @brief get the length of an array item
 * @details the array length is defined as: "nb_tuples x nb_components"
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the array length
 */

int StarPart_getArrayLength(StarPart_Data* data, char* item_id);

/**
 * @brief get the base address of an integer-array item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the array address
 */

int* StarPart_getIntArray(StarPart_Data* data, char* item_id);

/**
 * @brief get the base address of an double-array item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the array address
 */

float* StarPart_getFloatArray(StarPart_Data* data, char* item_id);

/**
 * @brief get the base address of an double-array item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the array address
 */

double* StarPart_getDoubleArray(StarPart_Data* data, char* item_id);

/**
 * @brief get the base address of a string-array item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the array address
 */

char** StarPart_getStrArray(StarPart_Data* data, char* item_id);

/**
 * @brief get the value of a graph item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the item value
 */

LibGraph_Graph* StarPart_getGraph(StarPart_Data* data, char* item_id);

/**
 * @brief get the value of a distributed graph item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the item value
 */

LibGraph_DistGraph* StarPart_getDistGraph(StarPart_Data* data, char* item_id);

/**
 * @brief get the value of an hypergraph item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the item value
 */

LibGraph_Hypergraph* StarPart_getHypergraph(StarPart_Data* data, char* item_id);

/**
 * @brief get the value of a mesh item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the item value
 */

LibGraph_Mesh* StarPart_getMesh(StarPart_Data* data, char* item_id);

/**
 * @brief get the value of an hybrid mesh item
 * @param data the \ref StarPart_Data where to get the item
 * @param item_id the item ID
 * @return the item value
 */

LibGraph_HybridMesh* StarPart_getHybridMesh(StarPart_Data* data, char* item_id);

/**
 * @brief set the value of a string item
 * @details The string is duplicated in the \ref StarPart_Item structure.
 * @details The memory allocated for the previous item value is released.
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id the ID of the item to set
 * @param str a string value
 */

void StarPart_setStr(StarPart_Data* data, char* item_id, char* str);

/**
 * @brief set the value of an integer item
 * @details The memory allocated for the previous item value is released.
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id the ID of the item to set
 * @param val an integer value
 */

void StarPart_setInt(StarPart_Data* data, char* item_id, int val);

/**
 * @brief set the value of a float item
 * @details The memory allocated for the previous item value is released.
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id the ID of the item to set
 * @param val an integer value
 */

void StarPart_setFloat(StarPart_Data* data, char* item_id, float val);

/**
 * @brief set the value of an double item
 * @details The memory allocated for the previous item value is released.
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id the ID of the item to set
 * @param val a double value
 */

void StarPart_setDouble(StarPart_Data* data, char* item_id, double val);

/**
 * @brief set the value of an integer-array item
 * @details The memory allocated for the previous item value is released.
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id the ID of the item to set
 * @param length the number of integers in array
 * @param array the base address of an integer array
 * @param owner
 *  - true, if StarPart takes the memory ownership of the item
 *  - false, if the user takes the memory ownership of the item
 */

void StarPart_setIntArray(StarPart_Data* data, char* item_id, int length, int* array, bool owner);

/**
 * @brief set the value of an float-array item
 * @details The memory allocated for the previous item value is released.
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id the ID of the item to set
 * @param length the number of float in array
 * @param array the base address of an float array
 * @param owner
 *  - true, if StarPart takes the memory ownership of the item
 *  - false, if the user takes the memory ownership of the item
 */

void StarPart_setFloatArray(StarPart_Data* data, char* item_id, int length, float* array, bool owner);

/**
 * @brief set the value of an double-array item
 * @details The memory allocated for the previous item value is released.
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id the ID of the item to set
 * @param length the number of double in array
 * @param array the base address of an double array
 * @param owner
 *  - true, if StarPart takes the memory ownership of the item
 *  - false, if the user takes the memory ownership of the item
 */

void StarPart_setDoubleArray(StarPart_Data* data, char* item_id, int length, double* array, bool owner);

/**
 * @brief set the value of a string-array item
 * @details The memory allocated for the previous item value is released.
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id the ID of the item to set
 * @param length the number of strings in array
 * @param array the base address of a string array
 * @param owner
 *  - true, if StarPart takes the memory ownership of the item
 *  - false, if the user takes the memory ownership of the item
 */

void StarPart_setStrArray(StarPart_Data* data, char* item_id, int length, char** array, bool owner);

/**
 * @brief set the value of a graph item
 * @details The memory allocated for the previous item value is released.
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id the ID of the item to set
 * @param g a graph
 * @param owner
 *  - true, if StarPart takes the memory ownership of the item
 *  - false, if the user takes the memory ownership of the item
 */

void StarPart_setGraph(StarPart_Data* data, char* item_id, LibGraph_Graph* g, bool owner);

/**
 * @brief set the value of a distributed graph item
 * @details The memory allocated for the previous item value is released.
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id the ID of the item to set
 * @param dg a distributed graph
 * @param owner
 *  - true, if StarPart takes the memory ownership of the item
 *  - false, if the user takes the memory ownership of the item
 */

void StarPart_setDistGraph(StarPart_Data* data, char* item_id, LibGraph_DistGraph* dg, bool owner);

/**
 * @brief set the value of an hypergraph item
 * @details The memory allocated for the previous item value is released.
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id the ID of the item to set
 * @param hg an hypergraph
 * @param owner
 *  - true, if StarPart takes the memory ownership of the item
 *  - false, if the user takes the memory ownership of the item
 */

void StarPart_setHypergraph(StarPart_Data* data, char* item_id, LibGraph_Hypergraph* hg, bool owner);

/**
 * @brief set the value of a mesh item
 * @details The memory allocated for the previous item value is released.
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id the ID of the item to set
 * @param m a mesh
 * @param owner
 *  - true, if StarPart takes the memory ownership of the item
 *  - false, if the user takes the memory ownership of the item
 */

void StarPart_setMesh(StarPart_Data* data, char* item_id, LibGraph_Mesh* m, bool owner);

/**
 * @brief set the value of an hybrid mesh item
 * @details The memory allocated for the previous item value is released.
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id the ID of the item to set
 * @param hm a mesh
 * @param owner
 *  - true, if StarPart takes the memory ownership of the item
 *  - false, if the user takes the memory ownership of the item
 */

void StarPart_setHybridMesh(StarPart_Data* data, char* item_id, LibGraph_HybridMesh* hm, bool owner);

/**
 * @brief set a \ref LibGraph_Array as a StarPart array item
 *
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id the ID of the item to set
 * @param array a \ref LibGraph_Array
 * @param owner
 *  - true, if StarPart takes the memory ownership of the item
 *  - false, if the user takes the memory ownership of the item
 */

void StarPart_setArray(StarPart_Data* data, char* item_id, LibGraph_Array* array, bool owner);

/**
 * @brief allocate a new \ref LibGraph_Array and set it as a StarPart array item
 *
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id an item iD
 * @param length the length of the array to be allocated
 * @return a raw pointer to the new allocated array
 */

void* StarPart_allocArray(StarPart_Data* data, char* item_id, int length);

/**
 * @brief initialize all array elements of a StarPart array item with a given value
 * @details Not used for string array.
 * @param data the \ref StarPart_Data where to set the item
 * @param item_id an item iD
 * @param val an integer value
 */

void StarPart_initArray(StarPart_Data* data, char* item_id, int val);

/**
 * @brief check array structure fields and more especially its length
 * @details The memory allocated for the previous item value is released.
 * @param data the \ref StarPart_Data of the item to check
 * @param item_id the ID of the item to check
 * @param length the array length to be checked
 */

void StarPart_checkArray(StarPart_Data* data, char* item_id, int length);

void StarPart_checkStr(StarPart_Data* data, char* item_id, char* checklist);

void StarPart_checkStrArray(StarPart_Data* data, char* item_id, char* checklist);

/** @} */

/* *********************************************************** */
/*                        PRIMITIVES                           */
/* *********************************************************** */

/**
 * @name Primitives
 * @{
 */

/**
 * @brief allocate an integer variable
 * @param x the initial value
 * @return the pointer to the allocated integer
 */

int* StarPart_newInt(int x);

/**
 * @brief allocate a double variable
 * @param x the initial value
 * @return the pointer to the allocated double
 */

double* StarPart_newDouble(double x);

/**
 * @brief allocate a string variable
 * @param x the initial string value
 * @return the pointer to the allocated string
 */

char* StarPart_newStr(char* x);

/**
 * @brief allocate a string variable for a strategy
 * @param x the initial string value
 * @return the pointer to the allocated string
 */

char* StarPart_newStrat(char* x);

/**
 * @brief allocate a \ref StarPart_IntArray that wraps an integer array
 * @details It does not allocate an integer array, but only wraps an already
 * allocated array into a \ref StarPart_IntArray type.
 * @param length the array length
 * @param array the base array address
 * @return the pointer to the allocated \ref StarPart_IntArray
 */

LibGraph_Array* StarPart_newIntArray(int length, int* array);
LibGraph_Array* StarPart_newIntArrayV(int length, ...);

/**
 * @brief allocate a \ref StarPart_StrArray that wraps an string array
 * @details It does not allocate a string array, but only wraps an already
 * allocated array into a \ref StarPart_StrArray type.
 * @param length the array length
 * @param array the base array address
 * @return the pointer to the allocated \ref StarPart_StrArray
 */

LibGraph_Array* StarPart_newStrArray(int length, char** array);
LibGraph_Array* StarPart_newStrArrayV(int length, ...);

LibGraph_Graph* StarPart_newGraph(void);
LibGraph_DistGraph* StarPart_newDistGraph(void);
LibGraph_Mesh* StarPart_newMesh(void);
LibGraph_Hypergraph* StarPart_newHypergraph(void);
LibGraph_HybridMesh* StarPart_newHybridMesh(int nb_components);

/**
 * @brief a shortcut macro to call \ref StarPart_newInt
 */

#define NEW_INT(x) (StarPart_newInt(x))

/**
 * @brief a shortcut macro to call \ref StarPart_newDouble
 */

#define NEW_DOUBLE(x) (StarPart_newDouble(x))

/**
 * @brief a shortcut macro to call \ref StarPart_newStr
 */

#define NEW_STR(x) (StarPart_newStr(x))

/**
 * @brief a shortcut macro to call \ref StarPart_newStrat
 */

#define NEW_STRAT(x) (StarPart_newStrat(x))

/**
 * @brief a shortcut macro to call \ref StarPart_newIntArray
 */

#define NEW_INTARRAY(length, array) (StarPart_newIntArray(length, array))

/**
 * @brief a shortcut macro to call \ref StarPart_newStrArray
 */

#define NEW_STRARRAY(length, array) (StarPart_newStrArray(length, array))

/**
 * @brief a shortcut macro to call \ref StarPart_newIntArrayV
 */

#define NEW_INTARRAYV(length, ...) (StarPart_newIntArrayV(length, __VA_ARGS__))

/**
 * @brief a shortcut macro to call \ref StarPart_newStrArrayV
 */

#define NEW_STRARRAYV(length, ...) (StarPart_newStrArrayV(length, __VA_ARGS__))

/** @} */

/* *********************************************************** */
/*                       METHOD                                */
/* *********************************************************** */

/**
 * @name Method Functions
 * @{
 */

/**
 * @brief create a new \ref StarPart_Method
 *
 * @param ctx a \ref StarPart_Context
 * @param id an ID of the form "package/method"
 * @param call function to call back each time the method is processed
 * @param init function to call back to initialize the method
 * @param desc method description
 * @return a new \ref StarPart_Method
 */

StarPart_Method* StarPart_newMethod(StarPart_Context* ctx, char* id, StarPart_MethodCallFunc call,
                                    StarPart_MethodInitFunc init, char* desc);

/**
 * @brief get context of a given method
 *
 * @param method a \ref StarPart_Method
 * @return StarPart_Context*
 */

StarPart_Context* StarPart_getMethodContext(StarPart_Method* method);

/**
 * @brief free a \ref StarPart_Method
 * @param method the method to be freed
 */

void StarPart_freeMethod(StarPart_Method* method);

/**
 * @brief add an argument to a method
 * @param method a \ref StarPart_Method
 * @param arg_id argument ID
 * @param flags set special data flags (see \ref StarPart_DataFlags)
 * @return a new \ref StarPart_Data, that represents the method argument
 */

StarPart_Data* StarPart_addMethodArg(StarPart_Method* method, char* arg_id, int flags);

/**
 * @brief get method ID (of form "package/method")
 */

char* StarPart_getMethodID(StarPart_Method* method);

/**
 * @brief get method description
 */

char* StarPart_getMethodDesc(StarPart_Method* method);

/**
 * @brief get method package
 */

char* StarPart_getMethodPackage(StarPart_Method* method);

/**
 * @brief get method name
 */

char* StarPart_getMethodName(StarPart_Method* method);

/**
 * @brief get method call function
 */

StarPart_MethodCallFunc StarPart_getCallFunc(StarPart_Method* method);

/**
 * @brief get method init function
 */

StarPart_MethodInitFunc StarPart_getInitFunc(StarPart_Method* method);

/**
 * @brief get argument table of a given method
 * @param method a method
 * @return a \ref GHashTable of char* key and \ref StarPart_Data value
 */

StarPart_DataTable* StarPart_getMethodArgTable(StarPart_Method* method);

/**
 * @brief get argument of a given method
 *
 * @param method a method
 * @param arg_id ID of the method argument to get (or an alias as "#local", "#in", "#out")
 * @return the \ref StarPart_Data that represents the asked method argument (or NULL if not found)
 */

StarPart_Data* StarPart_getMethodArg(StarPart_Method* method, char* arg_id);

char* StarPart_getAlias(StarPart_Method* method, char* alias);

int StarPart_getMethodNbInputs(StarPart_Method* method);
int StarPart_getMethodNbOutputs(StarPart_Method* method);

/**
 * @brief get local argument of a given method
 *
 * @param method a method
 * @return the \ref StarPart_Data that represents the asked method argument (or NULL if not found)
 */

StarPart_Data* StarPart_getMethodLocal(StarPart_Method* method);

/**
 * @brief get an input argument of a given method
 *
 * @param method a method
 * @param rank the rank of the asked input argument
 * @return the \ref StarPart_Data that represents the asked method argument (or NULL if not found)
 */

StarPart_Data* StarPart_getMethodInput(StarPart_Method* method, int rank);

/**
 * @brief get an output argument of a given method
 *
 * @param method a method
 * @param rank the rank of the asked output argument
 * @return the \ref StarPart_Data that represents the asked method argument (or NULL if not found)
 */

StarPart_Data* StarPart_getMethodOutput(StarPart_Method* method, int rank);

void StarPart_printMethodDesc(StarPart_Method* method);

/** @} */

/* *********************************************************** */
/*                             TASK                            */
/* *********************************************************** */

/**
 * @name Task
 * @{
 */

/**
 * @brief create a new \ref StarPart_Task within a given context
 *
 * @details See manual for details on the string representation format of arguments & connections.
 * @param ctx the context of the new created task
 * @param id a method ID
 * @param args a string representation of the arguments & connections passed to the new created task
 * @return a new \ref StarPart_Task
 */

StarPart_Task* StarPart_newTaskByName(StarPart_Context* ctx, char* id, char* args);

/**
 * @brief get task context
 *
 * @param task
 * @return StarPart_Context*
 */

StarPart_Context* StarPart_getTaskContext(StarPart_Task* task);

/**
 * @brief insert several user arguments & connections in a task given a string representation
 *
 * @details See manual for details on the string representation format of arguments & connections.
 * @param task a task
 * @param args a string representation of the arguments & connections passed to the task
 * @return the number of arguments passed
 */

int StarPart_insertTaskArgs(StarPart_Task* task, char* args);

/**
 * @brief insert a user argument in a task given its value
 *
 * @param task
 * @param arg_id
 * @param item_id
 * @param val
 * @param owner
 * @return StarPart_Item*
 */
StarPart_Item* StarPart_insertTaskArgByValue(StarPart_Task* task, char* arg_id, char* item_id, void* val, bool owner);

char* StarPart_getTaskID(StarPart_Task* task);
// StarPart_DataTable* StarPart_getTaskUserArgs(StarPart_Task* task);
StarPart_Method* StarPart_getTaskMethod(StarPart_Task* task);

/**
 * @brief
 *
 * @param task
 * @param parallel
 */

void StarPart_setParallelTask(StarPart_Task* task, bool parallel);

/**
 * @brief test if task is parallel or sequential
 *
 * @details A task can only be parallel in a parallel context, while a task can be sequential both in sequential and
 * parallel context.
 *
 * @param task a task
 * @return true if task is parallel
 * @return false if task is sequential
 */

bool StarPart_isParallelTask(StarPart_Task* task);

void StarPart_connectTask(StarPart_Task* task, char* arg_id, char* data_id);

/**
 * @brief disconnect all input & output arguments of a given task
 *
 * @param task a task
 */

void StarPart_disconnectTask(StarPart_Task* task);

/**
 * @brief get connection of a task argument
 *
 * @param task a task
 * @param arg_id an argument ID
 * @return a data ID connected to the task argument
 */

char* StarPart_getTaskConn(StarPart_Task* task, char* arg_id);

/**
 * @brief get the actual data passed to a task as an argument
 *
 * @param task a task
 * @param arg_id an argument ID of the task
 * @return the actual data or NULL if the argument is not connected
 */

StarPart_Data* StarPart_getTaskData(StarPart_Task* task, char* arg_id);

int StarPart_getTaskNbInputs(StarPart_Task* task);
int StarPart_getTaskNbOutputs(StarPart_Task* task);
StarPart_Data* StarPart_getTaskInput(StarPart_Task* task, int rank);
StarPart_Data* StarPart_getTaskOutput(StarPart_Task* task, int rank);
StarPart_Data* StarPart_getTaskLocal(StarPart_Task* task);

/**
 * @brief submit a task for execution in the queue of task context
 *
 * @param ctx
 * @param task
 */

void StarPart_submitTask(StarPart_Task* task);

/**
 * @brief cancel a task from the queue of task context
 *
 * @param ctx
 * @param task
 */

void StarPart_cancelTask(StarPart_Task* task);

void StarPart_printTask(StarPart_Task* task, int flags);

/** @} */

/* *********************************************************** */
/*                     ERROR MANAGEMENT                        */
/* *********************************************************** */

/**
 * @name Error management and debug
 * @{
 */

/**
 * @brief print a message (formatted as printf) on the standard output stream
 */

void StarPart_print(const char* format, ...);

/**
 * @brief print a message (formatted as printf) on the standard output stream
 */

void StarPart_printRed(const char* format, ...);
void StarPart_printBlue(const char* format, ...);

/**
 * @brief print a warning message (formatted as printf) on the standard error
 * stream
 */

void StarPart_warning(const char* format, ...);

/**
 * @brief print an error message (formatted as printf) on the standard error
 * stream and abort the program
 */

void StarPart_error(const char* format, ...);

/** @} */

/* *********************************************************** */
/*                      EXAMPLES                               */
/* *********************************************************** */

/** @example starpart-run.c */
/** @example starpart-hello.c */
/** @example starpart-simple.c */
/** @example starpart-complex.c */
/** @example starpart-nested.c */

/** @} */

#endif
