#!/bin/bash

# git archive --prefix=tmp/ --format=tgz --remote=git@gitlab.inria.fr:metapart/mirror.git master | tar -xvzf -

MIRROR="https://gitlab.inria.fr/metapart/mirror/raw/master"

wget -c "$MIRROR/metis-5.1.0.tgz"
wget -c "$MIRROR/kahip-2.0.tgz"
wget -c "$MIRROR/mondriaan-4.2.tgz"
wget -c "$MIRROR/mt-metis-0.6.0.tgz"
wget -c "$MIRROR/parmetis-4.0.3.tgz"
wget -c "$MIRROR/patoh-3.2.tgz"
wget -c "$MIRROR/pulp-0.2.tgz"
wget -c "$MIRROR/scotch-6.0.tgz"
wget -c "$MIRROR/zoltan-3.83.tgz"

echo "Done!"

